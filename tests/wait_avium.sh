#!/bin/bash
printf 'Waiting AVIUM'
max_attempts=20

while [[ "$(curl -v -o /dev/null -w ''%{http_code}'' localhost:8000/api/method/avium_tournament.api.ping)" != "200" ]]; do 
    printf '.'
    sleep 5; 
done