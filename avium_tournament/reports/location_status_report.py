# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import none_empty
from collections import OrderedDict
from frappe import _
from avium_tournament.reports.table_reportlab import drawpdf

class LocationStatusReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        text_size = 9
        distance_between_lines = 14

        CONF_TABLE = {
            'report_name': _("Location Status Report"),
            'header_titles_gap': 14,
            'levels': 1,
            'tables_per_page': 1,
            'text_size': text_size,
            'title_size': 10,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * text_size) / 2 + 0.4,
            'columns_description': (
                {
                    'key': 'registration',
                    'column_title': _('Reg.'),
                    'title_alignment': 'Center',
                    'column_width': 0.08,
                    'alignment': 'Right',
                    'offset': 10,
                }, {
                    'key': 'exhibitor',
                    'column_title': _('Exhibitor'),
                    'title_alignment': 'Center',
                    'column_width': 0.25,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'group',
                    'column_title': _('Group'),
                    'title_alignment': 'Center',
                    'column_width': 0.08,
                    'alignment': 'Center',
                }, {
                    'key': 'cage',
                    'column_title': _('Cage'),
                    'title_alignment': 'Center',
                    'column_width': 0.09,
                    'alignment': 'Right',
                    'offset': 10,
                }, {
                    'key': 'ring',
                    'column_title': _('Ring'),
                    'title_alignment': 'Center',
                    'column_width': 0.08,
                    'alignment': 'Right',
                    'offset': 10,
                }, {
                    'key': 'comments',
                    'column_title': _('Comments'),
                    'title_alignment': 'Center',
                    'column_width': 0.42,
                    'alignment': 'Left',
                    'in_lines': True,
                },
            ),
        }

        super(LocationStatusReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        status_labels = {
            'not_presented': "Not Presented",
            'tournament': "Tournament",
            'infirmary': "Infirmary",
            'flew': "Flew",
            'deceased': "Deceased",
        }

        filtering_status = [
            status_labels[status]
            for status in self.arguments
            if self.arguments[status] == 1
        ]

        if len(filtering_status) < 1:
            filtering_status.append("Undefined")

        return filtering_status

    def fetch_data(self):
        raw_data = frappe.db.sql(""" 
            SELECT  ireg.name as registration,
                    reg.name as registration_number,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    ireg.location_status_a as location_status_a,
                    NULL as location_status_b,
                    NULL as location_status_c,
                    NULL as location_status_d,
                    ireg.location_status_a_comment as location_status_comments_a,
                    NULL as location_status_comments_b,
                    NULL as location_status_comments_c,
                    NULL as location_status_comments_d,
                    tgro.individual_code as group_code,
                    scote.cage_number as cage_number,
                    ireg.ring_a as ring_a,
                    NULL as ring_b,
                    NULL as ring_c,
                    NULL as ring_d,
                    1 as team_size
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            WHERE ireg.location_status_a IN %(status)s
            UNION ALL
            SELECT  treg.name as registration,
                    reg.name as registration_number,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    treg.location_status_a as location_status_a,
                    treg.location_status_b as location_status_b,
                    treg.location_status_c as location_status_c,
                    treg.location_status_d as location_status_d,
                    treg.location_status_a_comment as location_status_comments_a,
                    treg.location_status_b_comment as location_status_comments_b,
                    treg.location_status_c_comment as location_status_comments_c,
                    treg.location_status_d_comment as location_status_comments_d,
                    tgro.team_code as group_code,
                    scote.cage_number as cage_number,
                    treg.ring_a as ring_a,
                    treg.ring_b as ring_b,
                    treg.ring_c as ring_c,
                    treg.ring_d as ring_d,
                    tgro.team_size as team_size
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
            WHERE 
                treg.location_status_a IN %(status)s OR
                treg.location_status_b IN %(status)s OR
                treg.location_status_c IN %(status)s OR
                treg.location_status_d IN %(status)s
            ORDER BY group_code ASC, cage_number ASC
            """,
            {
                'status': self.get_filter()
            },
            as_dict = True
        )

        data = OrderedDict()

        for item in raw_data:

            registration_number = item['registration_number'].lstrip("REG0")
            exhibitor_name = ", ".join((item["exhibitor_surname"], item["exhibitor_name"]))

            for iteration in range(item['team_size']):
                if item['team_size'] == 1:
                    location_status = item['location_status_a']
                    comments = item['location_status_comments_a']
                    registration = item['registration']
                    cage_number = none_empty(item['cage_number']).lstrip("0")
                    ring = none_empty(item['ring_a'])
                else:
                    if iteration == 0:
                        location_status = item['location_status_a']
                        comments = item['location_status_comments_a']
                        registration = item['registration'] + "A"
                        cage_number = none_empty(item['cage_number']).lstrip("0") + "A"
                        ring = none_empty(item['ring_a'])
                    elif iteration == 1:
                        location_status = item['location_status_b']
                        comments = item['location_status_comments_b']
                        registration = item['registration'] + "B"
                        cage_number = none_empty(item['cage_number']).lstrip("0") + "B"
                        ring = none_empty(item['ring_b'])
                    elif iteration == 2:
                        location_status = item['location_status_c']
                        comments = item['location_status_comments_c']
                        registration = item['registration'] + "C"
                        cage_number = none_empty(item['cage_number']).lstrip("0") + "C"
                        ring = none_empty(item['ring_c'])
                    else:
                        location_status = item['location_status_d']
                        comments = item['location_status_comments_d']
                        registration = item['registration'] + "D"
                        cage_number = none_empty(item['cage_number']).lstrip("0") + "D"
                        ring = none_empty(item['ring_d'])

                if location_status not in self.get_filter():
                    continue
                else:
                    location_status = _(location_status)

                if location_status not in data:
                    data[location_status] = dict()
                    data[location_status]["_"] = OrderedDict()

                data[location_status]["_"][registration] = dict()
                data[location_status]["_"][registration]['registration'] = registration_number
                data[location_status]["_"][registration]['exhibitor'] = exhibitor_name
                data[location_status]["_"][registration]['group'] = item['group_code']
                data[location_status]["_"][registration]['cage'] = cage_number
                data[location_status]["_"][registration]['ring'] = ring
                data[location_status]["_"][registration]['comments'] = none_empty(comments)

        self.data = data

    def gen_pdf(self):
        return drawpdf(self)