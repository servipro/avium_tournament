import frappe

def get_report_type():
    try:
        report_type = frappe.get_value("Report Configuration", "Report Configuration", "report_type")
    except:
        report_type = "Simplified"

    return report_type

def get_marks_stickers_type():
    try:
        stickers_type = frappe.get_value("Marks Stickers Configuration", "Marks Stickers Configuration", "stickers_type")
    except:
        stickers_type = "Simplified"

    return stickers_type