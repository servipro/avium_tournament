# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import blank_undefined, none_empty
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

def get_sorted_prizes(arguments):
    # Prizes data
    if arguments["prize"] == "1":
        prize_description = "description_1"
    elif arguments["prize"] == "2":
        prize_description = "description_2"
    else:
        prize_description = "description_3"

    prizes_raw_data = frappe.db.sql("""
        SELECT  touprides.description_1 as description_1,
                touprides.description_2 as description_2,
                touprides.description_3 as description_3,
                touprides.assignment_order as assignment_order
        FROM `tabTournament Prizes Description` as touprides
        ORDER BY assignment_order ASC
        """,
        as_dict=True
    )

    prizes_list = list()
    for prize in prizes_raw_data:
        if prize[prize_description] not in prizes_list:
            prizes_list.append(none_empty(prize[prize_description]))

    return prizes_list

class CarrierPrizesReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Report Configuration", "Report Configuration")
        raw_report_configuration_data.only_first_page = 0

        # Variables initialized
        column_width = 0.07
        column_offset = 7

        sorted_prizes = get_sorted_prizes(arguments)

        # Columns description definition
        columns_description = [
            {
                'key': 'prize'+str(index),
                'column_title': _(prize_description.title()),
                'column_width': column_width,
                'alignment': 'Right',
                'offset': column_offset,
                'total': True,
            }
            for index, prize_description
            in enumerate(sorted_prizes)
        ]

        columns_description.insert(
            0,
            {
                'key': 'exhibitor',
                'column_title': _("Exhibitors"),
                'title_alignment': 'Left',
                'column_width': 1 - (1 + len(sorted_prizes)) * column_width,
                'alignment': 'Left',
                'in_lines': True,
            }
        )

        columns_description.append(
            {
                'key': 'total',
                'column_title': _('Total'),
                'column_width': column_width,
                'alignment': 'Right',
                'offset': column_offset,
                'total': True,
            }
        )

        CONF_TABLE = {
            'report_name': _("Carrier Prizes Report"),
            'report_configuration': raw_report_configuration_data,
            'levels': 1,
            'columns_description': columns_description,
            'first_level_new_page': True,
        }

        super(CarrierPrizesReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return blank_undefined(self.arguments.get("carrier", None))

    def fetch_data(self):

        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    car.carrier_name as carrier,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                scote.prize != 'NULL' AND
                CASE WHEN %(filter)s != 'Undefined' THEN car.name = %(filter)s ELSE TRUE END
            UNION ALL
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    car.carrier_name as carrier,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                scote.prize != 'NULL' AND
                CASE WHEN %(filter)s != 'Undefined' THEN car.name = %(filter)s ELSE TRUE END
            ORDER BY carrier ASC, exhibitor_surname ASC, exhibitor_name ASC
            """,
            {'filter': self.get_filter()},
            as_dict = True
        )

        data = OrderedDict()

        sorted_prizes = get_sorted_prizes(self.arguments)

        for item in raw_data:
            # Prize description
            if self.arguments["prize"] == "1":
                prize = item['prize_1']
            elif self.arguments["prize"] == "2":
                prize = item['prize_2']
            else:
                prize = item['prize_3']

            # Data registration
            key = blank_undefined(item['carrier'])
            if key not in data:
                data[key] = dict()
                data[key]['_'] = OrderedDict()

            if item['registration'] not in data[key]['_']:
                data[key]['_'][item['registration']] = dict()
                data[key]['_'][item['registration']]['exhibitor'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                for index, sorted_prize in enumerate(sorted_prizes):
                    data[key]['_'][item['registration']]['prize' + str(index)] = 0
                data[key]['_'][item['registration']]['total'] = 0

            data[key]['_'][item['registration']]['prize' + str(sorted_prizes.index(prize))] += 1
            data[key]['_'][item['registration']]['total'] += 1

        self.data = data

    def gen_pdf(self):
    
        return drawpdf(self)