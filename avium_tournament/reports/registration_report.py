# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports import get_report_type
from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import blank_undefined, none_empty
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

class RegistrationReport(BaseReport):

    def __init__(self, arguments, canvas=None):
        report_type = get_report_type()
        if report_type == "Extended":
            tables_per_page = 1
            offset = 10
        else:
            extended_report_filters = ("Family", "Federation", "Territory", "Comunity")
            if arguments.get('breakdown', 0) == 0 and arguments.get('filter', "Family") in extended_report_filters:
                tables_per_page = 1
                offset = 10
            else:
                tables_per_page = 2
                offset = 4

        # CONF_TABLE parameters initialized according to arguments
        report_name = _("Registrations Report")
        if arguments.get('breakdown', 0) == 1:
            levels = 1
            first_column_key = 'group_description'
            first_column_title = _("Group Description")
        else:
            if arguments['filter'] == "Family":
                levels = 0
                first_column_key = 'family'
                first_column_title = _("Family")
            elif arguments['filter'] == "Group":
                levels = 0
                first_column_key = 'group_description'
                first_column_title = _("Group Description")
            elif arguments['filter'] == "Exhibitor":
                levels = 0
                first_column_key = 'exhibitor'
                first_column_title = _("Exhibitor Name")
                if arguments["sorted"] == "Registration":
                    report_name = _("Registrations Report (Registration Order)")
                elif arguments["sorted"] == "Payment":
                    report_name = _("Registrations Report (Payment Order)")
                elif arguments["sorted"] == "Comunity":
                    report_name = _("Registrations Report (Comunity Order)")
                else:
                    report_name = _("Registrations Report (Alphabetical Order)")
            elif arguments['filter'] == "Association":
                levels = 0
                first_column_key = 'association'
                first_column_title = _("Association")
            elif arguments['filter'] == "Federation":
                levels = 0
                first_column_key = 'federation'
                first_column_title = _("Federation")
            elif arguments['filter'] == "Territory":
                levels = 0
                first_column_key = 'territory'
                first_column_title = _("Territory")
            else:
                levels = 0
                first_column_key = 'comunity'
                first_column_title = _("Comunity")

        if arguments.get('pagebreak', 0) == 1:
            first_level_new_page = True
        else:
            first_level_new_page = False

        # CONF_TABLE dictionary definition
        CONF_TABLE = {
            'report_name': report_name,
            'levels': levels,
            'tables_per_page': tables_per_page,
            'columns_description': (
                {
                    'key': first_column_key,
                    'column_title': first_column_title,
                    'title_alignment': 'Left',
                    'column_width': 0.70,
                    'alignment': 'Left',
                    'in_lines': True,
                }, {
                    'key': 'individual',
                    'column_title': _('Individual'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': offset,
                    'total': not arguments.get('no_total', False),
                }, {
                    'key': 'team',
                    'column_title': _('Team'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': offset,
                    'total': not arguments.get('no_total', False),
                }, {
                    'key': 'specimens',
                    'column_title': _('Specimens'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': offset,
                    'total': not arguments.get('no_total', False),
                },
            ),
            'first_level_new_page': first_level_new_page,
        }

        super(RegistrationReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_order(self):
        return (self.arguments.get('filter', "Family"), self.arguments.get('sorted', "Registration"))

    def get_comunity(self):
        return self.arguments.get('comunity', "All")

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  fam.code as family_code,
                    fam.family_name as family_description,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    reg.name as registration,
                    reg.payment_date as payment_date,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.telephone as telephone,
                    reg.mobile_phone as mobile_phone,
                    reg.association as association,
                    reg.federation as federation,
                    reg.territory as territory,
                    parter.territory_name as comunity,
                    1 as specimens
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabFamily` as fam on fam.name = tgro.family
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            WHERE
                CASE WHEN %(comunity)s = 'All' THEN TRUE ELSE ter.parent_territory = %(comunity)s END
            UNION ALL
            SELECT  fam.code as family_code,
                    fam.family_name as family_description,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    reg.name as registration,
                    reg.payment_date as payment_date,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.telephone as telephone,
                    reg.mobile_phone as mobile_phone,
                    reg.association as association,
                    reg.federation as federation,
                    reg.territory as territory,
                    parter.territory_name as comunity,
                    tgro.team_size as specimens
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabFamily` as fam on fam.name = tgro.family
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            WHERE
                CASE WHEN %(comunity)s = 'All' THEN TRUE ELSE ter.parent_territory = %(comunity)s END
            ORDER BY
                CASE WHEN %(first_level_order)s = 'Family' THEN family_code END ASC,
                CASE WHEN %(first_level_order)s = 'Exhibitor' AND %(second_level_order)s = 'Registration' THEN registration END ASC,
                CASE WHEN %(first_level_order)s = 'Exhibitor' AND %(second_level_order)s = 'Payment' THEN payment_date END DESC,
                CASE WHEN %(first_level_order)s = 'Exhibitor' AND %(second_level_order)s = 'Comunity' THEN comunity END ASC,
                CASE WHEN %(first_level_order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(first_level_order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(first_level_order)s = 'Association' THEN association END ASC,
                CASE WHEN %(first_level_order)s = 'Federation' THEN federation END ASC,
                CASE WHEN %(first_level_order)s = 'Territory' THEN territory END ASC,
                CASE WHEN %(first_level_order)s = 'Comunity' THEN comunity END ASC,
                group_code ASC
            """,
            {'first_level_order': self.get_order()[0], 'second_level_order': self.get_order()[1], 'comunity': self.get_comunity()},
            as_dict = True
        )

        if self.arguments.get('breakdown', 0) == 1:
            data = OrderedDict()

            for item in raw_data:

                if self.arguments['filter'] == "Family":
                    key = item['family_code'] + " - " + item['family_description']
                elif self.arguments['filter'] == "Group":
                    key = item['group_code'] + " - " + item['group_description']
                elif self.arguments['filter'] == "Exhibitor":
                    key = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                    if get_report_type() == "Extended" and self.arguments.get("sorted", "name") == "Comunity":
                        key += " - " + blank_undefined(item['comunity'])
                    if self.arguments.get('phone',0) == 1:
                        key += " - " + none_empty(item['telephone']) + " - " + none_empty(item['mobile_phone'])
                elif self.arguments['filter'] == "Association":
                    key = blank_undefined(item['association'])
                elif self.arguments['filter'] == "Federation":
                    key = blank_undefined(item['federation'])
                elif self.arguments['filter'] == "Territory":
                    key = blank_undefined(item['territory'])
                else:
                    key = blank_undefined(item['comunity'])

                if key not in data:
                    data[key] = dict()
                    data[key]["_"] = OrderedDict()
                if item['group_code'] not in data[key]["_"]:
                    data[key]["_"][item['group_code']] = dict()
                    data[key]["_"][item['group_code']]['group_description'] = item['group_code'] + " - " + item['group_description']
                    data[key]["_"][item['group_code']]['specimens'] = 0
                if item['specimens'] == 1:
                    data[key]["_"][item['group_code']]['individual'] = data[key]["_"][item['group_code']].get('individual', 0) + 1
                else:
                    data[key]["_"][item['group_code']]['team'] = data[key]["_"][item['group_code']].get('team', 0) + 1
                data[key]["_"][item['group_code']]['specimens'] += item['specimens']

            self.data = data
        else:
            data = OrderedDict()

            if self.arguments['filter'] == "Family":
                filtering_item = "family_code"
            elif self.arguments['filter'] == "Group":
                filtering_item = "group_code"
            elif self.arguments['filter'] == "Exhibitor":
                filtering_item = "registration"
            elif self.arguments['filter'] == "Association":
                filtering_item = "association"
            elif self.arguments['filter'] == "Federation":
                filtering_item = "federation"
            elif self.arguments['filter'] == "Territory":
                filtering_item = "territory"
            else:
                filtering_item = "comunity"

            for item in raw_data:
                if item[filtering_item] not in data:
                    data[item[filtering_item]] = dict()
                    data[item[filtering_item]]['family'] = item['family_code'] + " - " + item['family_description']
                    data[item[filtering_item]]['group_description'] = item['group_code'] + " - " + item['group_description']
                    exhibitor = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                    if get_report_type() == "Extended" and self.arguments.get("sorted", "name") == "Comunity":
                        exhibitor += " - " + blank_undefined(item['comunity'])
                    if self.arguments.get('phone',0) == 1:
                        exhibitor += " - " + none_empty(item['telephone']) + " - " + none_empty(item['mobile_phone'])
                    data[item[filtering_item]]['exhibitor'] = exhibitor
                    data[item[filtering_item]]['association'] = blank_undefined(item['association'])
                    data[item[filtering_item]]['federation'] = blank_undefined(item['federation'])
                    data[item[filtering_item]]['territory'] = blank_undefined(item['territory'])
                    data[item[filtering_item]]['comunity'] = blank_undefined(item['comunity'])
                    data[item[filtering_item]]['specimens'] = 0
                    if self.arguments['filter'] != "Group" and self.arguments.get('breakdown', 0) == 0:
                        data[item[filtering_item]]['individual'] = 0
                        data[item[filtering_item]]['team'] = 0
                if item['specimens'] == 1:
                    data[item[filtering_item]]['individual'] = data[item[filtering_item]].get('individual', 0) + 1
                else:
                    data[item[filtering_item]]['team'] = data[item[filtering_item]].get('team', 0) + 1
                data[item[filtering_item]]['specimens'] += item['specimens']

            self.data = {
                "_": {
                    "_": data
                }
            }

    def gen_pdf(self):
    
        return drawpdf(self)