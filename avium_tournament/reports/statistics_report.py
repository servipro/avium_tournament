# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports import get_report_type
from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import blank_undefined, get_sorted_prizes
from avium_tournament.reports.table_reportlab import drawpdf
from collections import OrderedDict
from frappe import _

class StatisticsReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        sorted_prizes = get_sorted_prizes(arguments)

        def print_modification_function(print_value):
            return "{0:.1f}".format(print_value) + " %"

        # Variables initialized
        columns_description = list()
        if arguments["grouped"] == "Exhibitor":
            if get_report_type() == "Extended":
                tables_per_page = 1
                column_offset = 7
                column_width = 0.07
                description_column_width = 1 - column_width*(len(sorted_prizes) + 5.5)
            else:
                tables_per_page = 2
                column_offset = 4
                column_width = 0.09
                description_column_width = 1 - column_width*(len(sorted_prizes) + 2.5)
            total_exhibitors = False
        else:
            tables_per_page = 1
            column_offset = 7
            column_width = 0.07
            if arguments["print_statistics"] == "Yes":
                number_of_columns = 6
            else:
                number_of_columns = 2
            description_column_width = 1 - column_width*(len(sorted_prizes) + number_of_columns)
            if arguments["grouped"] == "Family":
                total_exhibitors = False
            else:
                total_exhibitors = True

        # Columns definition
        description_column = {
            'key': 'description',
            'column_title': _(arguments["grouped"]).title(),
            'title_alignment': 'Left',
            'column_width': description_column_width,
            'alignment': 'Left',
            'truncate': True,
        }
        breeder_code_column = {
            'key': 'bc',
            'column_title': _('BC'),
            'title_alignment': 'Left',
            'column_width': 1.5 * column_width,
            'alignment': 'Left',
        }
        exhibitors_column = {
            'key': 'exhibitors',
            'column_title': _('Exhibitors'),
            'column_width': column_width,
            'alignment': 'Right',
            'offset': column_offset,
            'total': total_exhibitors,
        }
        individual_column = {
            'key': 'individual',
            'column_title': _('Individual'),
            'column_width': column_width,
            'alignment': 'Right',
            'offset': column_offset,
            'total': True,
        }
        team_column = {
            'key': 'team',
            'column_title': _('Team'),
            'column_width': column_width,
            'alignment': 'Right',
            'offset': column_offset,
            'total': True,
        }
        specimens_column = {
            'key': 'specimen',
            'column_title': _('Specimens'),
            'column_width': column_width,
            'alignment': 'Right',
            'offset': column_offset,
            'total': True,
        }
        prizes_columns = [
            {
                'key': 'prize' + str(index),
                'column_title': _(prize_description.title()),
                'column_width': column_width,
                'alignment': 'Right',
                'offset': column_offset,
                'total': True,
            }
            for index, prize_description
            in enumerate(sorted_prizes)
        ]
        total_prizes_column = {
            'key': 'total',
            'column_title': _('Total'),
            'column_width': column_width,
            'alignment': 'Right',
            'offset': column_offset,
            'total': True,
        }
        percentage_prizes_column = {
            'key': 'percentage',
            'column_title': _('%'),
            'print_modification': print_modification_function,
            'column_width': column_width,
            'alignment': 'Right',
            'total': True,
        }


        if arguments["grouped"] == "Exhibitor":
            columns_description.append(breeder_code_column)
            columns_description.append(description_column)
            if get_report_type() == "Extended":
                columns_description.append(individual_column)
                columns_description.append(team_column)
                columns_description.append(specimens_column)
            for column in prizes_columns:
                columns_description.append(column)
            columns_description.append(total_prizes_column)
        else:
            columns_description.append(description_column)
            if arguments["print_statistics"] == "Yes":
                columns_description.append(exhibitors_column)
                columns_description.append(individual_column)
                columns_description.append(team_column)
                columns_description.append(specimens_column)
            for column in prizes_columns:
                columns_description.append(column)
            columns_description.append(total_prizes_column)
            columns_description.append(percentage_prizes_column)


        if arguments["print_statistics"] == "Yes":
            if arguments["grouped"] == "Family":
                report_name = _("Family Statistics and Medals")
            elif arguments["grouped"] == "Exhibitor":
                report_name = _("Exhibitor Statistics and Medals")
            elif arguments["grouped"] == "Association":
                report_name = _("Association Statistics and Medals")
            elif arguments["grouped"] == "Federation":
                report_name = _("Federation Statistics and Medals")
            elif arguments["grouped"] == "Territory":
                report_name = _("Territory Statistics and Medals")
            elif arguments["grouped"] == "Comunity":
                report_name = _("Comunity Statistics and Medals")
            else:
                report_name = _("Carrier Statistics and Medals")
        else:
            if arguments["grouped"] == "Family":
                report_name = _("Family Statistics")
            elif arguments["grouped"] == "Exhibitor":
                report_name = _("Exhibitor Statistics")
            elif arguments["grouped"] == "Association":
                report_name = _("Association Statistics")
            elif arguments["grouped"] == "Federation":
                report_name = _("Federation Statistics")
            elif arguments["grouped"] == "Territory":
                report_name = _("Territory Statistics")
            elif arguments["grouped"] == "Comunity":
                report_name = _("Comunity Statistics")
            else:
                report_name = _("Carrier Statistics")

        CONF_TABLE = {
            'report_name': report_name,
            'tables_per_page': tables_per_page,
            'columns_description': columns_description,
        }

        super(StatisticsReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return self.arguments.get('prized', "All")

    def get_order(self):
        return self.arguments.get('grouped', "Exhibitor")

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    reg.association as association,
                    reg.federation as federation,
                    reg.territory as territory,
                    car.carrier_name as carrier,
                    parter.territory_name as comunity,
                    fami.code as family_code,
                    fami.family_name as family_name,
                    1 as team_size,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
            WHERE
                CASE WHEN %(filter)s != 'All' THEN scote.prize != 'NULL' ELSE TRUE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    reg.association as association,
                    reg.federation as federation,
                    reg.territory as territory,
                    car.carrier_name as carrier,
                    parter.territory_name as comunity,
                    fami.code as family_code,
                    fami.family_name as family_name,
                    tgro.team_size as team_size,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
            WHERE
                CASE WHEN %(filter)s != 'All' THEN scote.prize != 'NULL' ELSE TRUE END AND
                exbr.idx = 1
            ORDER BY
                CASE WHEN %(order)s = 'Family' THEN family_code END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(order)s = 'Association' THEN association END ASC,
                CASE WHEN %(order)s = 'Federation' THEN federation END ASC,
                CASE WHEN %(order)s = 'Territory' THEN territory END ASC,
                CASE WHEN %(order)s = 'Comunity' THEN comunity END ASC,
                CASE WHEN %(order)s = 'Carrier' THEN carrier END ASC
            """,
            {
                'filter': self.get_filter(),
                'order': self.get_order(),
            },
            as_dict = True
        )

        data = OrderedDict()
        sorted_prizes = get_sorted_prizes(self.arguments)

        total_prizes = 0
        for item in raw_data:
            if blank_undefined(item['prize_1']) != "Undefined":
                total_prizes += 1

        for item in raw_data:
            # Key definition
            if self.arguments["grouped"] == "Family":
                key = item['family_code'] + " - " + item['family_name']
            elif self.arguments["grouped"] == "Exhibitor":
                key = item['exhibitor_surname'] + ", " + item['exhibitor_name']
            elif self.arguments["grouped"] == "Association":
                key = blank_undefined(item['association'])
            elif self.arguments["grouped"] == "Federation":
                key = blank_undefined(item['federation'])
            elif self.arguments["grouped"] == "Territory":
                key = blank_undefined(item['territory'])
            elif self.arguments["grouped"] == "Comunity":
                key = blank_undefined(item['comunity'])
            else:
                key = blank_undefined(item['carrier'])

            # Prize description
            if self.arguments["prize"] == "1":
                prize = item['prize_1']
            elif self.arguments["prize"] == "2":
                prize = item['prize_2']
            else:
                prize = item['prize_3']

            # Data registration
            if key not in data:
                data[key] = dict()
                data[key]['description'] = key
                data[key]['bc'] = item['bc']
                data[key]['registrations'] = list()
                for index, sorted_prize in enumerate(sorted_prizes):
                    data[key]['prize' + str(index)] = 0
                data[key]['total'] = 0
                data[key]['percentage'] = 0.0
                data[key]['individual'] = 0
                data[key]['team'] = 0
                data[key]['specimen'] = 0

            if item['registration'] not in data[key]['registrations']:
                data[key]['registrations'].append(item['registration'])
                data[key]['exhibitors'] = len(data[key]['registrations'])

            data[key]['specimen'] += item['team_size']
            if item['team_size'] > 1:
                data[key]['team'] += 1
            else:
                data[key]['individual'] += 1

            if blank_undefined(prize) != "Undefined":
                data[key]['prize' + str(sorted_prizes.index(prize))] += 1
                data[key]['total'] += 1
                data[key]['percentage'] = (data[key]['total'] * 100.0) / (total_prizes * 1.0)

            if self.arguments["sorted"] != "Alphabetical order":
                old_data = data
                data_keys = data.keys()
                for index, sorted_prize in enumerate(sorted_prizes):
                    prize_key = 'prize' + str(len(sorted_prizes)-index-1)
                    sorted_data_keys = sorted(data_keys, key=lambda key: data[key].get(prize_key, 0), reverse=True)
                    data_keys = sorted_data_keys
                sorted_data_keys = sorted(data_keys, key=lambda key: data[key]['total'], reverse=True)
                data = OrderedDict()
                for key in sorted_data_keys:
                    data[key] = old_data[key]

        self.data = {
            "_": {
                "_": data
            }
        }

    def gen_pdf(self):
    
        return drawpdf(self)