# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.utils import get_tournament
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import importFonts, split_text_in_lines, text_truncate, none_empty, draw_header, draw_footer, blank_custom, blank_undefined
from frappe import _
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth

class RegistrationSummaryReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Report Configuration", "Report Configuration")
        raw_report_configuration_data.only_first_page = 0

        text_size = 9
        title_size = 10
        distance_between_lines = 15

        CONF_TABLE = {
            'report_name': _("Registration Summary"),
            'report_configuration': raw_report_configuration_data,
            'text_size': text_size,
            'title_size': title_size,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * title_size)/2 + 0.5,
            'gap_between_columns': 3,
            'extra_horizontal_text_offset': 5,
            'exhibitor_background_color': HexColor(0xe4e4e4),
            'title_background_color': colors.fidblue,
            'title_font_color': colors.white,
        }

        super(RegistrationSummaryReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return blank_undefined(self.arguments.get("registration", None))


    def get_order(self):
        return (
            blank_undefined(self.arguments.get('sorted', "Name")),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.dni as dni,
                    reg.association as association,
                    reg.territory as territory,
                    parter.territory_name as comunity,
                    reg.telephone as telephone,
                    reg.mobile_phone as mobile_phone,
                    reg.email as email,
                    reg.allow as allow,
                    reg.membership_discount as membership_discount,
                    reg.specimen_food as specimen_food,
                    reg.individual_registrations as individual_registrations,
                    reg.price_per_individual_registration as price_per_individual_registration,
                    reg.individual_registration_price as individual_registration_price,
                    reg.team_registrations as team_registrations,
                    reg.price_per_team_registration as price_per_team_registration,
                    reg.team_registration_price as team_registration_price,
                    reg.batch_registrations as batch_registrations,
                    reg.price_per_batch_registration as price_per_batch_registration,
                    reg.batch_registration_price as batch_registration_price,
                    reg.summary_books as summary_books,
                    reg.price_per_book as price_per_book,
                    reg.books_price as books_price,
                    reg.summary_meals as summary_meals,
                    reg.price_per_meal as price_per_meal,
                    reg.meals_price as meals_price,
                    reg.league_inscribed_specimens as league_inscribed_specimens,
                    reg.league_price as league_price,
                    reg.league_inscription_price as league_inscription_price,
                    reg.total_price as total_price,
                    reg.total_specimen as total_specimen,
                    reg.payment_date as payment_date,
                    reg.paid_amount as paid_amount,
                    reg.remaining_amount as remaining_amount,
                    (SELECT breeder_code from `tabExhibitor Breeder` where `tabExhibitor Breeder`.parent = reg.name ORDER BY idx LIMIT 1) as bc,
                    tgro.individual_code as group_code,
                    ireg.registration_description as group_description,
                    scote.cage_number as cage,
                    ireg.federation_code_a as federation_code_a,
                    ireg.breeder_code_a as breeder_code_a,
                    ireg.ring_a as ring_a,
                    ireg.tax_a as tax_a,
                    NULL as federation_code_b,
                    NULL as breeder_code_b,
                    NULL as ring_b,
                    NULL as tax_b,
                    NULL as federation_code_c,
                    NULL as breeder_code_c,
                    NULL as ring_c,
                    NULL as tax_c,
                    NULL as federation_code_d,
                    NULL as breeder_code_d,
                    NULL as ring_d,
                    NULL as tax_d,
                    car.carrier_name as carrier,
                    ireg.idx as idx,
                    1 as registration_size,
                    0 as registration_type
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            WHERE
                CASE WHEN %(filter)s != 'Undefined' THEN reg.name = %(filter)s ELSE TRUE END
            UNION ALL
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.dni as dni,
                    reg.association as association,
                    reg.territory as territory,
                    parter.territory_name as comunity,
                    reg.telephone as telephone,
                    reg.mobile_phone as mobile_phone,
                    reg.email as email,
                    reg.allow as allow,
                    reg.membership_discount as membership_discount,
                    reg.specimen_food as specimen_food,
                    reg.individual_registrations as individual_registrations,
                    reg.price_per_individual_registration as price_per_individual_registration,
                    reg.individual_registration_price as individual_registration_price,
                    reg.team_registrations as team_registrations,
                    reg.price_per_team_registration as price_per_team_registration,
                    reg.team_registration_price as team_registration_price,
                    reg.batch_registrations as batch_registrations,
                    reg.price_per_batch_registration as price_per_batch_registration,
                    reg.batch_registration_price as batch_registration_price,
                    reg.summary_books as summary_books,
                    reg.price_per_book as price_per_book,
                    reg.books_price as books_price,
                    reg.summary_meals as summary_meals,
                    reg.price_per_meal as price_per_meal,
                    reg.meals_price as meals_price,
                    reg.league_inscribed_specimens as league_inscribed_specimens,
                    reg.league_price as league_price,
                    reg.league_inscription_price as league_inscription_price,
                    reg.total_price as total_price,
                    reg.total_specimen as total_specimen,
                    reg.payment_date as payment_date,
                    reg.paid_amount as paid_amount,
                    reg.remaining_amount as remaining_amount,
                    (SELECT breeder_code from `tabExhibitor Breeder` where `tabExhibitor Breeder`.parent = reg.name ORDER BY idx LIMIT 1) as bc,
                    tgro.team_code as group_code,
                    treg.registration_description as group_description,
                    scote.cage_number as cage,
                    treg.federation_code_a as federation_code_a,
                    treg.breeder_code_a as breeder_code_a,
                    treg.ring_a as ring_a,
                    treg.tax_a as tax_a,
                    treg.federation_code_b as federation_code_b,
                    treg.breeder_code_b as breeder_code_b,
                    treg.ring_b as ring_b,
                    treg.tax_b as tax_b,
                    treg.federation_code_c as federation_code_c,
                    treg.breeder_code_c as breeder_code_c,
                    treg.ring_c as ring_c,
                    treg.tax_c as tax_c,
                    treg.federation_code_d as federation_code_d,
                    treg.breeder_code_d as breeder_code_d,
                    treg.ring_d as ring_d,
                    treg.tax_d as tax_d,
                    car.carrier_name as carrier,
                    treg.idx as idx,
                    treg.team_size as registration_size,
                    1 as registration_type
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            WHERE
                CASE WHEN %(filter)s != 'Undefined' THEN reg.name = %(filter)s ELSE TRUE END
            ORDER BY
                CASE WHEN %(order)s = 'Registration' THEN registration END ASC,
                CASE WHEN %(order)s = 'Payment' THEN payment_date END ASC,
                CASE WHEN %(order)s = 'Carrier' THEN carrier END ASC,
                CASE WHEN %(order)s = 'Territory' THEN territory END ASC,
                CASE WHEN %(order)s = 'Comunity' THEN comunity END ASC,
                exhibitor_surname ASC, exhibitor_name ASC, group_code ASC, cage ASC, idx ASC
            """,
            {
                'filter': self.get_filter(),
                'order': self.get_order(),
            },
            as_dict = True
        )

        self.data = dict()
        
        self.data['registrations'] = OrderedDict()

        for item in raw_data:

            if item['registration'] not in self.data['registrations']:
                self.data['registrations'][item['registration']] = dict()
                self.data['registrations'][item['registration']]['registration'] = item['registration'].lstrip("REG0")
                self.data['registrations'][item['registration']]['exhibitor_name'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                self.data['registrations'][item['registration']]['dni'] = item['dni']
                self.data['registrations'][item['registration']]['bc'] = item['bc']
                self.data['registrations'][item['registration']]['association'] = item['association']
                self.data['registrations'][item['registration']]['territory'] = item['territory']
                self.data['registrations'][item['registration']]['comunity'] = item['comunity']
                self.data['registrations'][item['registration']]['telephone'] = item['telephone']
                self.data['registrations'][item['registration']]['mobile_phone'] = item['mobile_phone']
                self.data['registrations'][item['registration']]['email'] = none_empty(item['email']).lower()
                self.data['registrations'][item['registration']]['membership_discount'] = item['membership_discount']
                self.data['registrations'][item['registration']]['specimen_food'] = item['specimen_food']
                self.data['registrations'][item['registration']]['individual_registrations_number'] = blank_custom(item['individual_registrations'], 0)
                self.data['registrations'][item['registration']]['price_per_individual_registration'] = blank_custom(item['price_per_individual_registration'], 0)
                self.data['registrations'][item['registration']]['individual_registration_price'] = blank_custom(item['individual_registration_price'], 0)
                self.data['registrations'][item['registration']]['team_registrations_number'] = blank_custom(item['team_registrations'], 0)
                self.data['registrations'][item['registration']]['price_per_team_registration'] = blank_custom(item['price_per_team_registration'], 0)
                self.data['registrations'][item['registration']]['team_registration_price'] = blank_custom(item['team_registration_price'], 0)
                self.data['registrations'][item['registration']]['batch_registrations_number'] = blank_custom(item['batch_registrations'], 0)
                self.data['registrations'][item['registration']]['price_per_batch_registration'] = blank_custom(item['price_per_batch_registration'], 0)
                self.data['registrations'][item['registration']]['batch_registration_price'] = blank_custom(item['batch_registration_price'], 0)
                self.data['registrations'][item['registration']]['summary_books'] = blank_custom(item['summary_books'], 0)
                self.data['registrations'][item['registration']]['price_per_book'] = blank_custom(item['price_per_book'], 0)
                self.data['registrations'][item['registration']]['books_price'] = blank_custom(item['books_price'], 0)
                self.data['registrations'][item['registration']]['summary_meals'] = blank_custom(item['summary_meals'], 0)
                self.data['registrations'][item['registration']]['price_per_meal'] = blank_custom(item['price_per_meal'], 0)
                self.data['registrations'][item['registration']]['meals_price'] = blank_custom(item['meals_price'], 0)
                self.data['registrations'][item['registration']]['league_inscribed_specimens'] = blank_custom(item['league_inscribed_specimens'], 0)
                self.data['registrations'][item['registration']]['league_price'] = blank_custom(item['league_price'], 0)
                self.data['registrations'][item['registration']]['league_inscription_price'] = blank_custom(item['league_inscription_price'], 0)
                self.data['registrations'][item['registration']]['total_price'] = blank_custom(item['total_price'], 0)
                self.data['registrations'][item['registration']]['total_specimen'] = blank_custom(item['total_specimen'], 0)
                self.data['registrations'][item['registration']]['payment_date'] = item['payment_date']
                self.data['registrations'][item['registration']]['paid_amount'] = blank_custom(item['paid_amount'], 0)
                self.data['registrations'][item['registration']]['remaining_amount'] = blank_custom(item['remaining_amount'], 0)
                self.data['registrations'][item['registration']]['carrier'] = item['carrier']
                
            if item['registration_type'] == 0:
                if self.data['registrations'][item['registration']].get('individual_registrations', None) is None:
                    self.data['registrations'][item['registration']]['individual_registrations'] = OrderedDict()
                if item['group_code'] not in self.data['registrations'][item['registration']]['individual_registrations']:
                    self.data['registrations'][item['registration']]['individual_registrations'][item['group_code']] = dict()
                    self.data['registrations'][item['registration']]['individual_registrations'][item['group_code']]['group_code'] = item['group_code']
                    self.data['registrations'][item['registration']]['individual_registrations'][item['group_code']]['group_description'] = item['group_description']
                    self.data['registrations'][item['registration']]['individual_registrations'][item['group_code']]['registrations'] = list()

                self.data['registrations'][item['registration']]['individual_registrations'][item['group_code']]['registrations'].append(
                    {
                        'cage': none_empty(item['cage']).lstrip("0"),
                        'federation_code_a': item['federation_code_a'],
                        'breeder_code_a': item['breeder_code_a'],
                        'ring_a': item['ring_a'],
                        'tax_a': item['tax_a'],
                    }
                )
            else:
                if self.data['registrations'][item['registration']].get('team_registrations', None) is None:
                    self.data['registrations'][item['registration']]['team_registrations'] = OrderedDict()
                if item['group_code'] not in self.data['registrations'][item['registration']]['team_registrations']:
                    self.data['registrations'][item['registration']]['team_registrations'][item['group_code']] = dict()
                    self.data['registrations'][item['registration']]['team_registrations'][item['group_code']]['group_code'] = item['group_code']
                    self.data['registrations'][item['registration']]['team_registrations'][item['group_code']]['registration_size'] = item['registration_size']
                    self.data['registrations'][item['registration']]['team_registrations'][item['group_code']]['group_description'] = item['group_description']
                    self.data['registrations'][item['registration']]['team_registrations'][item['group_code']]['registrations'] = list()
                self.data['registrations'][item['registration']]['team_registrations'][item['group_code']]['registrations'].append(
                    {
                        'cage': none_empty(item['cage']).lstrip("0"),
                        'federation_code_a': item['federation_code_a'],
                        'breeder_code_a': item['breeder_code_a'],
                        'ring_a': item['ring_a'],
                        'tax_a': item['tax_a'],
                        'federation_code_b': item['federation_code_b'],
                        'breeder_code_b': item['breeder_code_b'],
                        'ring_b': item['ring_b'],
                        'tax_b': item['tax_b'],
                        'federation_code_c': item['federation_code_c'],
                        'breeder_code_c': item['breeder_code_c'],
                        'ring_c': item['ring_c'],
                        'tax_c': item['tax_c'],
                        'federation_code_d': item['federation_code_d'],
                        'breeder_code_d': item['breeder_code_d'],
                        'ring_d': item['ring_d'],
                        'tax_d': item['tax_d'],
                    }
                )

    def gen_pdf(self):

        tournament_data = get_tournament()

        # **Set file name**
        self.canvas.setTitle(_("Registration Summaries"))

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']

        width, height = A4

        # **Specific Parameters**
        printable_width = width - configuration.left_margin * cm - configuration.right_margin * cm

        print_horizontal_start = configuration.left_margin * cm + self.configuration['extra_horizontal_text_offset']
        
        exhibitor_dni_position = (printable_width - 2 * self.configuration['extra_horizontal_text_offset']) / 2
        registration_number_position = (printable_width - 2 * self.configuration['extra_horizontal_text_offset']) * 3 / 4

        group_code_width = 30
        cage_width = 40
        ring_width = 70
        if self.arguments.get("cage", 0) == 1:
            group_description_width = printable_width - 2 * self.configuration['extra_horizontal_text_offset'] - group_code_width - cage_width - 4 * ring_width - 6 * self.configuration['gap_between_columns']
        else:
            group_description_width = printable_width - 2 * self.configuration['extra_horizontal_text_offset'] - group_code_width - 4 * ring_width - 5 * self.configuration['gap_between_columns']

        total_columns_gap = 10
        total_columns_width = (printable_width - 2 * self.configuration['extra_horizontal_text_offset'] - 2 * total_columns_gap) / 3

        # **Titles truncate and width calculation**
        association_title = _("Association")+": "
        association_title_width = stringWidth(association_title, self.configuration['font'], self.configuration['title_size'])

        email_title = _("Email")+": "
        email_title_width = stringWidth(email_title, self.configuration['font'], self.configuration['title_size'])

        telephone_title = _("Telephone")+": "
        telephone_title_width = stringWidth(telephone_title, self.configuration['font'], self.configuration['title_size'])

        mobilephone_title = _("Mobile Phone")+": "
        mobilephone_title_width = stringWidth(mobilephone_title, self.configuration['font'], self.configuration['title_size'])


        # **Text truncate and position calculation**
        group_code_position = configuration.left_margin * cm + self.configuration['extra_horizontal_text_offset'] + self.configuration['gap_between_columns']/2.0
        group_description_position = group_code_position + group_code_width + self.configuration['gap_between_columns']
        cage_position = group_description_position + group_description_width + self.configuration['gap_between_columns']
        if self.arguments.get("cage", 0) == 1:
            ring_a_position = cage_position + cage_width + self.configuration['gap_between_columns']
        else:
            ring_a_position = group_description_position + group_description_width + self.configuration['gap_between_columns']
        ring_b_position = ring_a_position + ring_width + self.configuration['gap_between_columns']
        ring_c_position = ring_b_position + ring_width + self.configuration['gap_between_columns']
        ring_d_position = ring_c_position + ring_width + self.configuration['gap_between_columns']

        group_code_text = text_truncate(
            _('Group'),
            group_code_width,
            self.configuration['font'],
            self.configuration['text_size']
        )
        group_description_text = text_truncate(
            _('Registration Description'),
            group_description_width,
            self.configuration['font'],
            self.configuration['text_size']
        )
        cage_text = text_truncate(
            _('Cage'),
            cage_width,
            self.configuration['font'],
            self.configuration['text_size']
        )
        ring_text = text_truncate(
            _('Ring'),
            ring_width,
            self.configuration['font'],
            self.configuration['text_size']
        )
        ring_a_text = text_truncate(
            _('Ring'),
            ring_width - stringWidth(" A", self.configuration['font'], self.configuration['text_size']),
            self.configuration['font'],
            self.configuration['text_size']
        )
        ring_b_text = text_truncate(
            _('Ring'),
            ring_width - stringWidth(" B", self.configuration['font'], self.configuration['text_size']),
            self.configuration['font'],
            self.configuration['text_size']
        )
        ring_c_text = text_truncate(
            _('Ring'),
            ring_width - stringWidth(" C", self.configuration['font'], self.configuration['text_size']),
            self.configuration['font'],
            self.configuration['text_size']
        )
        ring_d_text = text_truncate(
            _('Ring'),
            ring_width - stringWidth(" D", self.configuration['font'], self.configuration['text_size']),
            self.configuration['font'],
            self.configuration['text_size']
        )
        box_text = text_truncate(
            _('Box'),
            ring_width * 0.47,
            self.configuration['font'],
            self.configuration['text_size']
        )
        tax_text = text_truncate(
            _('Tax'),
            ring_width * 0.47,
            self.configuration['font'],
            self.configuration['text_size']
        )

        tax_data_text = box_data_text = '_____'

        # **Variables Inicialization**
        background = False

        # **Custom function**
        def exhibitor_title_print(registration_data, current_height):
            association_in_lines = split_text_in_lines(
                none_empty(registration_data['association']),
                exhibitor_dni_position-self.configuration['gap_between_columns'] - association_title_width,
                self.configuration['font'],
                self.configuration['title_size']
            )
    
            #Print lines and fill background
            self.canvas.saveState()
            self.canvas.setStrokeColor(self.configuration['exhibitor_background_color'])
            self.canvas.setFillColor(self.configuration['exhibitor_background_color'])
            self.canvas.rect(
                configuration.left_margin * cm,
                current_height-(1+len(association_in_lines))*self.configuration['distance_between_lines']-self.configuration['lines_offset'],
                printable_width,
                (2+len(association_in_lines))*self.configuration['distance_between_lines'],
                stroke=1,
                fill=1
            )
            self.canvas.restoreState()
            self.canvas.line(
                configuration.left_margin * cm,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                width - configuration.right_margin * cm,
                current_height + self.configuration['distance_between_lines']-self.configuration['lines_offset']
            )
            self.canvas.line(
                configuration.left_margin * cm,
                current_height-(1+len(association_in_lines))*self.configuration['distance_between_lines']-self.configuration['lines_offset'],
                width - configuration.right_margin * cm,
                current_height-(1+len(association_in_lines))*self.configuration['distance_between_lines']-self.configuration['lines_offset']
            )
    
            # Print text
            email = text_truncate(
                none_empty(registration_data['email']),
                exhibitor_dni_position - self.configuration['gap_between_columns'] - email_title_width,
                self.configuration['font'],
                self.configuration['title_size']
            )
            telephone = text_truncate(
                none_empty(registration_data['telephone']),
                registration_number_position - exhibitor_dni_position - self.configuration['gap_between_columns'] - telephone_title_width,
                self.configuration['font'],
                self.configuration['title_size']
            )
            mobile_phone = text_truncate(
                none_empty(registration_data['mobile_phone']),
                printable_width - 2*self.configuration['extra_horizontal_text_offset'] - registration_number_position - mobilephone_title_width,
                self.configuration['font'],
                self.configuration['title_size']
            )

            self.canvas.setFont(self.configuration['bold_font'], self.configuration['title_size'])
            self.canvas.drawString(print_horizontal_start, current_height, registration_data['exhibitor_name'].upper())
            self.canvas.drawString(print_horizontal_start + exhibitor_dni_position, current_height, none_empty(registration_data['dni']).upper())
            self.canvas.drawString(print_horizontal_start + registration_number_position, current_height, _("Registration Nº") + ": " + registration_data['registration'])
            self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
            current_height -= self.configuration['distance_between_lines']
            self.canvas.drawString(print_horizontal_start, current_height, email_title + email)
            self.canvas.drawString(print_horizontal_start + exhibitor_dni_position, current_height, telephone_title + telephone)
            self.canvas.drawString(print_horizontal_start + registration_number_position, current_height, mobilephone_title + mobile_phone)
            current_height -= self.configuration['distance_between_lines']
            self.canvas.drawString(print_horizontal_start + exhibitor_dni_position, current_height, none_empty(registration_data['territory']))
            self.canvas.drawString(print_horizontal_start + registration_number_position, current_height, none_empty(registration_data['comunity']))
            for index, association_line in enumerate(association_in_lines):
                if index <= 0:
                    self.canvas.drawString(print_horizontal_start, current_height, association_title + association_line)
                else:
                    current_height -= self.configuration['distance_between_lines']
                    self.canvas.drawString(print_horizontal_start + association_title_width, current_height, association_line)
    
            if self.arguments.get("carrier", False):
                current_height -= self.configuration['distance_between_lines']
                self.canvas.saveState()
                self.canvas.setStrokeColor(self.configuration['exhibitor_background_color'])
                self.canvas.setFillColor(self.configuration['exhibitor_background_color'])
                self.canvas.rect(
                    configuration.left_margin * cm,
                    current_height - self.configuration['lines_offset'],
                    printable_width,
                    self.configuration['distance_between_lines'],
                    stroke=1,
                    fill=1
                )
                self.canvas.restoreState()
                self.canvas.line(
                    configuration.left_margin * cm,
                    current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                    width - configuration.right_margin * cm,
                    current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset']
                )
                self.canvas.setFont(self.configuration['bold_font'], self.configuration['title_size'])
                self.canvas.drawString(print_horizontal_start, current_height, _("Carrier") + ": " + none_empty(registration_data['carrier']).upper())
    
            return current_height
    
        def section_title_print(text, current_height):
            self.canvas.saveState()
            self.canvas.setStrokeColor(self.configuration['title_background_color'])
            self.canvas.setFillColor(self.configuration['title_background_color'])
            self.canvas.rect(
                configuration.left_margin * cm,
                current_height - self.configuration['lines_offset'],
                printable_width,
                self.configuration['distance_between_lines'],
                stroke=1,
                fill=1
            )
            self.canvas.restoreState()

            self.canvas.line(
                configuration.left_margin * cm,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                width - configuration.right_margin * cm,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset']
            )
            self.canvas.line(
                configuration.left_margin * cm,
                current_height - self.configuration['lines_offset'],
                width - configuration.right_margin * cm,
                current_height - self.configuration['lines_offset']
            )

            self.canvas.saveState()
            self.canvas.setFont(self.configuration['bold_font'], self.configuration['title_size'])
            self.canvas.setFillColor(self.configuration['title_font_color'])
            self.canvas.setStrokeColor(self.configuration['title_font_color'])
            self.canvas.drawString(print_horizontal_start, current_height, text)
            self.canvas.restoreState()
    
        def registration_titles_print(current_height, individual):
            self.canvas.saveState()
            self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
            self.canvas.drawCentredString(group_code_position+group_code_width/2, current_height, group_code_text)
            self.canvas.drawString(group_description_position, current_height, group_description_text)
            if self.arguments.get("cage", False):
                self.canvas.drawCentredString(cage_position+cage_width/2, current_height, cage_text)
            if individual:
                self.canvas.drawString(ring_a_position, current_height, ring_text)
                if tournament_data.enable_tax:
                    self.canvas.drawString(ring_b_position, current_height, tax_text)
                    box_horizontal_position = ring_c_position
                else:
                    box_horizontal_position = ring_b_position
                self.canvas.drawString(box_horizontal_position, current_height, box_text)
            else:
                self.canvas.drawString(ring_a_position, current_height, ring_a_text+" A")
                self.canvas.drawString(ring_b_position, current_height, ring_b_text+" B")
                self.canvas.drawString(ring_c_position, current_height, ring_c_text+" C")
                self.canvas.drawString(ring_d_position, current_height, ring_d_text+" D")
                current_height -= self.configuration['distance_between_lines']
                if tournament_data.enable_tax:
                    self.canvas.drawString(ring_a_position, current_height, tax_text)
                    self.canvas.drawString(ring_b_position, current_height, tax_text)
                    self.canvas.drawString(ring_c_position, current_height, tax_text)
                    self.canvas.drawString(ring_d_position, current_height, tax_text)
                    box_extra_horizontal_position = 0.53*ring_width
                else:
                    box_extra_horizontal_position = 0
                self.canvas.drawString(ring_a_position + box_extra_horizontal_position, current_height, box_text)
                self.canvas.drawString(ring_b_position + box_extra_horizontal_position, current_height, box_text)
                self.canvas.drawString(ring_c_position + box_extra_horizontal_position, current_height, box_text)
                self.canvas.drawString(ring_d_position + box_extra_horizontal_position, current_height, box_text)
            self.canvas.line(
                configuration.left_margin * cm,
                current_height - self.configuration['lines_offset'],
                width - configuration.right_margin * cm,
                current_height - self.configuration['lines_offset']
            )
            self.canvas.restoreState()
            return current_height
    
        def total_texts_truncate(text, value):
            return text_truncate(
                text,
                total_columns_width - stringWidth(value, self.configuration['font'], self.configuration['title_size']),
                self.configuration['font'],
                self.configuration['title_size']
            ) + ": " + value

        def money_format(price):
            return "{0:.2f}".format(price)+" "+_("€")
    
        def new_line(registration, current_height, comparison_value):
            if (current_height - comparison_value) < (configuration.lower_margin * cm + configuration.footer_height * cm):
                draw_footer(self)
                self.canvas.showPage()
                # **Imprimir capçalera**
                current_height = height - draw_header(self) - self.configuration['header_titles_gap']
                # **Print exhibitor information**
                current_height = exhibitor_title_print(registration, current_height)
                return (True, current_height)
            else:
                current_height -= self.configuration['distance_between_lines']
                return (False, current_height)

        # **Import fonts**
        importFonts()

        for registration in self.data['registrations']:
            # **Print header**
            current_height = height - draw_header(self) - self.configuration['header_titles_gap']

            # **Print exhibitor information**
            current_height = exhibitor_title_print(self.data['registrations'][registration], current_height)

            # **Print individual registrations

            # *Print text*
            individual_registrations = self.data['registrations'][registration].get('individual_registrations', None)
            if individual_registrations is not None:
                first_individual_group = list(individual_registrations.keys())[0]
                first_registration_description_in_lines = split_text_in_lines(
                    individual_registrations[first_individual_group]['group_description'],
                    group_description_width,
                    self.configuration['font'],
                    self.configuration['text_size']
                )
                new_page, current_height = new_line(
                    self.data['registrations'][registration],
                    current_height,
                    (2 + len(first_registration_description_in_lines)) * self.configuration['distance_between_lines'],
                )
                section_title_print(_("Individual Registration"), current_height)
                current_height -= self.configuration['distance_between_lines']
                current_height = registration_titles_print(current_height, True)
            
                for individual_group in individual_registrations:
                    #Process data
                    registration_description_in_lines = split_text_in_lines(
                        individual_registrations[individual_group]['group_description'],
                        group_description_width,
                        self.configuration['font'],
                        self.configuration['text_size']
                    )

                    #Print data
                    self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
                    new_page, current_height = new_line(
                        self.data['registrations'][registration],
                        current_height,
                        len(registration_description_in_lines) * self.configuration['distance_between_lines']
                    )

                    if new_page:
                        current_height -= self.configuration['distance_between_lines']
                        section_title_print(_("Individual Registration"), current_height)
                        current_height -= self.configuration['distance_between_lines']
                        current_height = registration_titles_print(current_height, True) - self.configuration['distance_between_lines']
                        self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
                    self.canvas.line(
                        configuration.left_margin * cm,
                        current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                        width - configuration.right_margin * cm,
                        current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset']
                    )
                    self.canvas.drawCentredString(group_code_position + group_code_width/2, current_height, individual_registrations[individual_group]['group_code'])
                    current_height += self.configuration['distance_between_lines']

                    iteration_number = max(
                        len(registration_description_in_lines),
                        len(individual_registrations[individual_group]['registrations']),
                    )

                    aux_index = 0
                    for index in range(iteration_number):
                        # **Canvi de pàgina**
                        new_page, current_height = new_line(
                            self.data['registrations'][registration],
                            current_height,
                            max(len(registration_description_in_lines) - index, 1) * self.configuration['distance_between_lines']
                        )
                        if new_page:
                            current_height -= self.configuration['distance_between_lines']
                            section_title_print(_("Individual Registration"), current_height)
                            current_height -= self.configuration['distance_between_lines']
                            current_height = registration_titles_print(current_height, True) - self.configuration['distance_between_lines']
                            self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
                            self.canvas.drawCentredString(group_code_position+group_code_width/2, current_height, individual_registrations[individual_group]['group_code'])
                            if iteration_number - index < len(registration_description_in_lines):
                                iteration_number = len(registration_description_in_lines) + index
                            aux_index = index

                        # **Print information
                        if (index-aux_index) < len(registration_description_in_lines):
                            self.canvas.drawString(group_description_position,current_height, registration_description_in_lines[index-aux_index])

                        if index < len(individual_registrations[individual_group]['registrations']):

                            cage_registration = individual_registrations[individual_group]['registrations'][index]

                            if self.arguments.get('cage', 0) == 1 and cage_registration['cage'] is not None:
                                cage = cage_registration['cage']
                            else:
                                cage = ""

                            ring_code_a = (
                                blank_custom(cage_registration['federation_code_a'], "__"),
                                blank_custom(cage_registration['breeder_code_a'], "__"),
                                blank_custom(cage_registration['ring_a'], "__"),
                            )
                            ring_code_a = "/".join(ring_code_a)

                            if blank_undefined(cage_registration['tax_a']) != "Undefined" and cage_registration['tax_a'] > 0:
                                tax_a = cage_registration['tax_a']
                            else:
                                tax_a = tax_data_text

                            self.canvas.drawCentredString(cage_position+cage_width/2, current_height, cage)
                            self.canvas.drawString(ring_a_position, current_height, ring_code_a)
                            if tournament_data.enable_tax:
                                self.canvas.drawString(ring_b_position, current_height, str(tax_a))
                                box_horizontal_position = ring_c_position
                            else:
                                box_horizontal_position = ring_b_position
                            self.canvas.drawString(box_horizontal_position, current_height, box_data_text)

            # **Print team registrations
            team_registrations = self.data['registrations'][registration].get('team_registrations', None)
            if team_registrations is not None:
                first_team_group = list(team_registrations.keys())[0]
                first_registration_description_in_lines = split_text_in_lines(
                    team_registrations[first_team_group]['group_description'],
                    group_description_width,
                    self.configuration['font'],
                    self.configuration['text_size']
                )
                
                new_page, current_height = new_line(
                    self.data['registrations'][registration],
                    current_height,
                    (2 + len(first_registration_description_in_lines)) * self.configuration['distance_between_lines'],
                )
                section_title_print(_("Team Registration"), current_height)
                current_height -= self.configuration['distance_between_lines']
                current_height = registration_titles_print(current_height, False)

                for team_group in team_registrations:
                    #Process data
                    registration_description_in_lines = split_text_in_lines(
                        team_registrations[team_group]['group_description'],
                        group_description_width,
                        self.configuration['font'],
                        self.configuration['text_size']
                    )

                    #Print data
                    self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
                    new_page, current_height = new_line(
                        self.data['registrations'][registration],
                        current_height,
                        len(registration_description_in_lines) * self.configuration['distance_between_lines']
                    )

                    if new_page:
                        current_height -= self.configuration['distance_between_lines']
                        section_title_print(_("Team Registration"), current_height)
                        current_height -= self.configuration['distance_between_lines']
                        current_height = registration_titles_print(current_height, False) - self.configuration['distance_between_lines']
                        self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
                    self.canvas.line(
                        configuration.left_margin * cm,
                        current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                        width - configuration.right_margin * cm,
                        current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset']
                    )
                    self.canvas.drawCentredString(group_code_position + group_code_width/2, current_height, team_registrations[team_group]['group_code'])
                    current_height += self.configuration['distance_between_lines']

                    iteration_number = max(
                        len(registration_description_in_lines),
                        len(team_registrations[team_group]['registrations']) * 2,
                    )

                    aux_index = 0
                    registration_index = 0

                    for index in range(0, iteration_number, 2):
                        
                        # **Canvi de pàgina**
                        new_page, current_height = new_line(
                            self.data['registrations'][registration],
                            current_height,
                            max(len(registration_description_in_lines) - index, 1) * self.configuration['distance_between_lines']
                        )
                        if new_page:
                            current_height -= self.configuration['distance_between_lines']
                            section_title_print(_("Team Registration"), current_height)
                            current_height -= self.configuration['distance_between_lines']
                            current_height = registration_titles_print(current_height, False) - self.configuration['distance_between_lines']
                            self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
                            self.canvas.drawCentredString(group_code_position+group_code_width/2, current_height, team_registrations[team_group]['group_code'])
                            if iteration_number - index < len(registration_description_in_lines):
                                iteration_number = len(registration_description_in_lines) + index
                            aux_index = index

                        # **Print information
                        if (index - aux_index) < len(registration_description_in_lines):
                            self.canvas.drawString(
                                group_description_position,
                                current_height,
                                registration_description_in_lines[index - aux_index]
                            )

                        if (index + 1 - aux_index) < len(registration_description_in_lines):
                            self.canvas.drawString(
                                group_description_position,
                                current_height - self.configuration['distance_between_lines'],
                                registration_description_in_lines[index + 1 - aux_index]
                            )

                        if registration_index < len(team_registrations[team_group]['registrations']):
                            print(team_group)
                            print(registration_index)
                            cage_registration = team_registrations[team_group]['registrations'][registration_index]

                            if self.arguments.get('cage', 0) == 1 and cage_registration['cage'] is not None:
                                cage = cage_registration['cage']
                            else:
                                cage = ""

                            ring_code_a = (
                                blank_custom(cage_registration['federation_code_a'], "__"),
                                blank_custom(cage_registration['breeder_code_a'], "__"),
                                blank_custom(cage_registration['ring_a'], "__"),
                            )
                            ring_code_a = "/".join(ring_code_a)

                            if blank_undefined(cage_registration['tax_a']) != "Undefined" and cage_registration['tax_a'] > 0:
                                tax_a = cage_registration['tax_a']
                            else:
                                tax_a = tax_data_text

                            ring_code_b = (
                                blank_custom(cage_registration['federation_code_b'], "__"),
                                blank_custom(cage_registration['breeder_code_b'], "__"),
                                blank_custom(cage_registration['ring_b'], "__"),
                            )
                            ring_code_b = "/".join(ring_code_b)

                            if blank_undefined(cage_registration['tax_b']) != "Undefined" and cage_registration['tax_b'] > 0:
                                tax_b = cage_registration['tax_b']
                            else:
                                tax_b = tax_data_text

                            ring_code_c = (
                                blank_custom(cage_registration['federation_code_c'], "__"),
                                blank_custom(cage_registration['breeder_code_c'], "__"),
                                blank_custom(cage_registration['ring_c'], "__"),
                            )
                            ring_code_c = "/".join(ring_code_c)

                            if blank_undefined(cage_registration['tax_c']) != "Undefined" and cage_registration['tax_c'] > 0:
                                tax_c = cage_registration['tax_c']
                            else:
                                tax_c = tax_data_text

                            ring_code_d = (
                                blank_custom(cage_registration['federation_code_d'], "__"),
                                blank_custom(cage_registration['breeder_code_d'], "__"),
                                blank_custom(cage_registration['ring_d'], "__"),
                            )
                            ring_code_d = "/".join(ring_code_d)

                            if blank_undefined(cage_registration['tax_d']) != "Undefined" and cage_registration['tax_d'] > 0:
                                tax_d = cage_registration['tax_d']
                            else:
                                tax_d = tax_data_text

                            self.canvas.drawCentredString(cage_position+cage_width/2, current_height, cage)
                            self.canvas.drawString(ring_a_position, current_height, ring_code_a)
                            if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 1:
                                self.canvas.drawString(ring_b_position, current_height, ring_code_b)
                            if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 2:
                                self.canvas.drawString(ring_c_position, current_height, ring_code_c)
                            if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 3:
                                self.canvas.drawString(ring_d_position, current_height, ring_code_d)
                            current_height -= self.configuration['distance_between_lines']
                            if tournament_data.enable_tax:
                                self.canvas.drawString(ring_a_position, current_height, str(tax_a))
                                if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 1:
                                    self.canvas.drawString(ring_b_position, current_height, str(tax_b))
                                if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 2:
                                    self.canvas.drawString(ring_c_position, current_height, str(tax_c))
                                if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 3:
                                    self.canvas.drawString(ring_d_position, current_height, str(tax_d))
                                box_extra_horizontal_position = 0.53*ring_width
                            else:
                                box_extra_horizontal_position = 0
                            self.canvas.drawString(ring_a_position + box_extra_horizontal_position, current_height, box_data_text)
                            if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 1:
                                self.canvas.drawString(ring_b_position + box_extra_horizontal_position, current_height, box_data_text)
                            if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 2:
                                self.canvas.drawString(ring_c_position + box_extra_horizontal_position, current_height, box_data_text)
                            if self.data['registrations'][registration]['team_registrations'][team_group]['registration_size'] > 3:
                                self.canvas.drawString(ring_d_position + box_extra_horizontal_position, current_height, box_data_text)
                        registration_index+=1
                    
            # **Print registration summary
            if self.arguments.get('price', 0) == 0:
                # **Canvi de pàgina**
                new_page, current_height = new_line(
                    self.data['registrations'][registration],
                    current_height,
                    2 * self.configuration['distance_between_lines']
                )
                section_title_print(_("Registration Summary"),current_height)
                self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                current_height -= self.configuration['distance_between_lines']
                self.canvas.drawString(
                    print_horizontal_start + 0 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Membership registration"), _(self.data['registrations'][registration]['membership_discount']))
                )
                self.canvas.drawString(
                    print_horizontal_start + 1 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Specimen Food"), _(self.data['registrations'][registration]['specimen_food']))
                )
                self.canvas.line(
                    configuration.left_margin * cm,
                    current_height - self.configuration['lines_offset'],
                    width - configuration.right_margin * cm,
                    current_height - self.configuration['lines_offset']
                )

                new_page, current_height = new_line(
                    self.data['registrations'][registration],
                    current_height,
                    self.configuration['distance_between_lines']
                )
                if new_page:
                    current_height -= self.configuration['distance_between_lines']
                    section_title_print(_("Registration Summary"), current_height)
                    self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                    current_height -= self.configuration['distance_between_lines']
                self.canvas.drawString(
                    print_horizontal_start + 0 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Individual Registrations"),str(self.data['registrations'][registration]['individual_registrations_number']))
                )
                self.canvas.drawString(
                    print_horizontal_start + 1 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Price Per Individual"), money_format(self.data['registrations'][registration]['price_per_individual_registration']))
                )
                self.canvas.drawString(
                    print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Total Individual"), money_format(self.data['registrations'][registration]['individual_registration_price']))
                )

                new_page, current_height = new_line(
                    self.data['registrations'][registration],
                    current_height,
                    self.configuration['distance_between_lines']
                )
                if new_page:
                    current_height -= self.configuration['distance_between_lines']
                    section_title_print(_("Registration Summary"), current_height)
                    self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                    current_height -= self.configuration['distance_between_lines']
                self.canvas.drawString(
                    print_horizontal_start + 0 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Team Registrations"), str(self.data['registrations'][registration]['team_registrations_number']))
                )
                self.canvas.drawString(
                    print_horizontal_start + 1 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Price Per Team"), money_format(self.data['registrations'][registration]['price_per_team_registration']))
                )
                self.canvas.drawString(
                    print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Total Team"), money_format(self.data['registrations'][registration]['team_registration_price']))
                )

                if len(tournament_data.batch_price) > 0:
                    new_page, current_height = new_line(
                        self.data['registrations'][registration],
                        current_height,
                        self.configuration['distance_between_lines']
                    )
                    if new_page:
                        current_height -= self.configuration['distance_between_lines']
                        section_title_print(_("Registration Summary"), current_height)
                        self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                        current_height -= self.configuration['distance_between_lines']
                    self.canvas.drawString(
                        print_horizontal_start + 0 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Batch Registrations"), str(self.data['registrations'][registration]['batch_registrations_number']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 1 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Price Per Batch"), money_format(self.data['registrations'][registration]['price_per_batch_registration']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Total Batch"), money_format(self.data['registrations'][registration]['batch_registration_price']))
                    )

                if tournament_data.enable_book:
                    new_page, current_height = new_line(
                        self.data['registrations'][registration],
                        current_height,
                        self.configuration['distance_between_lines']
                    )
                    if new_page:
                        current_height -= self.configuration['distance_between_lines']
                        section_title_print(_("Registration Summary"), current_height)
                        self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                        current_height -= self.configuration['distance_between_lines']
                    self.canvas.drawString(
                        print_horizontal_start + 0 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Books"),str(self.data['registrations'][registration]['summary_books']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 1 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Price Per Book"), money_format(self.data['registrations'][registration]['price_per_book']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Total Books"), money_format(self.data['registrations'][registration]['books_price']))
                    )

                if tournament_data.enable_meal:
                    new_page, current_height = new_line(
                        self.data['registrations'][registration],
                        current_height,
                        self.configuration['distance_between_lines']
                    )
                    if new_page:
                        current_height -= self.configuration['distance_between_lines']
                        section_title_print(_("Registration Summary"), current_height)
                        self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                        current_height -= self.configuration['distance_between_lines']
                    self.canvas.drawString(
                        print_horizontal_start + 0 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Meals"),str(self.data['registrations'][registration]['summary_meals']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 1 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Price Per Meal"), money_format(self.data['registrations'][registration]['price_per_meal']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Total Meals"), money_format(self.data['registrations'][registration]['meals_price']))
                    )

                if tournament_data.enable_league_inscriptions:
                    new_page, current_height = new_line(
                        self.data['registrations'][registration],
                        current_height,
                        2 * self.configuration['distance_between_lines']
                    )
                    if new_page:
                        current_height -= self.configuration['distance_between_lines']
                        section_title_print(_("Registration Summary"),current_height)
                        self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                        current_height -= self.configuration['distance_between_lines']
                    self.canvas.drawString(
                        print_horizontal_start + 0 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("League Inscribed Specimens"), str(self.data['registrations'][registration]['league_inscribed_specimens']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 1 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("League Price"), money_format(self.data['registrations'][registration]['league_price']))
                    )
                    self.canvas.drawString(
                        print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Total League Inscriptions"), money_format(self.data['registrations'][registration]['league_inscription_price']))
                    )

                new_page, current_height = new_line(
                    self.data['registrations'][registration],
                    current_height,
                    7 * self.configuration['distance_between_lines']
                )
                if new_page:
                    current_height -= self.configuration['distance_between_lines']
                    section_title_print(_("Registration Summary"),current_height)
                    self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                    current_height -= self.configuration['distance_between_lines']
                self.canvas.rect(
                    configuration.left_margin * cm,
                    current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                    3 * total_columns_width + 2 * total_columns_gap + 2 * self.configuration['extra_horizontal_text_offset'],
                    -7 * self.configuration['distance_between_lines']
                )
                self.canvas.rect(
                    configuration.left_margin * cm,
                    current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                    2 * total_columns_width + total_columns_gap + self.configuration['extra_horizontal_text_offset'],
                    -4 * self.configuration['distance_between_lines']
                )
                self.canvas.rect(
                    configuration.left_margin * cm,
                    current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                    (2 * total_columns_width + total_columns_gap + self.configuration['extra_horizontal_text_offset'])/2,
                    -3 * self.configuration['distance_between_lines']
                )
                self.canvas.rect(
                    configuration.left_margin * cm + (2 * total_columns_width + total_columns_gap + self.configuration['extra_horizontal_text_offset']) / 2,
                    current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                    (2 * total_columns_width + total_columns_gap + self.configuration['extra_horizontal_text_offset'])/2,
                    -3 * self.configuration['distance_between_lines']
                )
                self.canvas.rect(
                    configuration.left_margin * cm + 2 * total_columns_width + total_columns_gap + self.configuration['extra_horizontal_text_offset'],
                    current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                    total_columns_width + total_columns_gap + self.configuration['extra_horizontal_text_offset'],
                    -7 * self.configuration['distance_between_lines']
                )

                self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                self.canvas.drawString(
                    print_horizontal_start,
                    current_height,
                    _("Delivered")
                )
                self.canvas.drawString(
                    print_horizontal_start + (2 * total_columns_width + total_columns_gap + self.configuration['extra_horizontal_text_offset']) / 2,
                    current_height,
                    _("Extracted")
                )

                current_height -= self.configuration['distance_between_lines']
                self.canvas.drawString(
                    print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Total Specimen"), str(self.data['registrations'][registration]['total_specimen']))
                )

                current_height -= self.configuration['distance_between_lines']
                if self.data['registrations'][registration]['payment_date'] is not None:
                    self.canvas.drawString(
                        print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Payment date"), self.data['registrations'][registration]['payment_date'].strftime("%Y-%m-%d, %H:%M"))
                    )
                else:
                    self.canvas.drawString(
                        print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                        current_height,
                        total_texts_truncate(_("Payment date"), "")
                    )

                current_height -= self.configuration['distance_between_lines']
                self.canvas.drawString(
                    print_horizontal_start,
                    current_height,
                    text_truncate(
                        _("Signed") + ": " + none_empty(self.data['registrations'][registration]['carrier']).upper(),
                        2 * total_columns_width + total_columns_gap - self.configuration['extra_horizontal_text_offset'] - 2,
                        self.configuration['font'],
                        self.configuration['title_size']
                    )
                )
                self.canvas.drawString(
                    print_horizontal_start + 2 * (total_columns_width+total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Total Price"), money_format(self.data['registrations'][registration]['total_price']))
                )

                current_height -= self.configuration['distance_between_lines']
                self.canvas.drawString(
                    print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                    current_height,
                    total_texts_truncate(_("Paid Amount"), money_format(self.data['registrations'][registration]['paid_amount']))
                )

                current_height -= self.configuration['distance_between_lines']
                self.canvas.setFont(self.configuration['bold_font'], self.configuration['title_size'])
                self.canvas.drawString(
                    print_horizontal_start + 2 * (total_columns_width + total_columns_gap),
                    current_height,
                    text_truncate(
                        _("Remaining Amount"),
                        total_columns_width - stringWidth(": " + money_format(self.data['registrations'][registration]['remaining_amount']), self.configuration['bold_font'], self.configuration['title_size']),
                        self.configuration['bold_font'],
                        self.configuration['title_size']
                    ) + ": " + money_format(self.data['registrations'][registration]['remaining_amount'])
                )

                current_height -= self.configuration['distance_between_lines']
                self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
                self.canvas.drawString(print_horizontal_start, current_height, tournament_data.association)
    
            # **Imprimir línia peu de pàgina**
            draw_footer(self)
            self.canvas.showPage()
            
        return self.canvas
