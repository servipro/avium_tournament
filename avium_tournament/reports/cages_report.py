# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from frappe import _
from avium_tournament.reports.table_reportlab import drawpdf

class CagesReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        if arguments.get('breakdown', 0) == 1:
            levels = 1
            first_level_total = True
        else:
            levels = 0
            first_level_total = False

        CONF_TABLE = {
            'report_name': _("Cages Report"),
            'levels': levels,
            'first_level_total': first_level_total,
            'columns_description': (
                {
                    'key': 'description',
                    'column_title': _('Description'),
                    'title_alignment': 'Left',
                    'column_width': 0.90,
                    'alignment': 'Left',
                    'in_lines': True,
                }, {
                    'key': 'quantity',
                    'column_title': _('Quantity'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 10,
                    'total': True,
                },
            ),
        }

        super(CagesReport, self).__init__(CONF_TABLE, arguments, canvas)

    def fetch_data(self):
        raw_data = frappe.db.sql(""" 
            SELECT  cage.cage_name as cage_name, 
                    1 as team_size,
                    judtem.template_name as template_name
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabCage` as cage on cage.name = tgro.cage
            LEFT JOIN `tabJudging Template` as judtem on judtem.name = tgro.judging_template
            UNION ALL
            SELECT  cage.cage_name as cage_name, 
                    tgro.team_size as team_size,
                    judtem.template_name as template_name
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabCage` as cage on cage.name = tgro.cage
            LEFT JOIN `tabJudging Template` as judtem on judtem.name = tgro.judging_template
            ORDER BY cage_name ASC, template_name ASC
            """, as_dict=True)

        data = OrderedDict()

        for item in raw_data:

            if self.arguments.get('breakdown', 0) == 1:
                first_level_id = _("Cage") + ": " + item['cage_name'].title()
                content_id = item['template_name']
                description = item['template_name'].title()
            else:
                first_level_id = "_"
                content_id = item['cage_name']
                description = item['cage_name'].title()

            if first_level_id not in data:
                data[first_level_id] = dict()
                data[first_level_id]["_"] = OrderedDict()

            if content_id not in data[first_level_id]["_"]:
                data[first_level_id]["_"][content_id] = dict()
                data[first_level_id]["_"][content_id]['description'] = description
                data[first_level_id]["_"][content_id]['quantity'] = 0
            data[first_level_id]["_"][content_id]['quantity'] += item['team_size']

        self.data = data

    def gen_pdf(self):
    
        return drawpdf(self)