# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from frappe import _
from avium_tournament.reports.table_reportlab import drawpdf

class RingInspectionReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        CONF_TABLE = {
            'report_name': _("Ring Inspection"),
            'levels': 1,
            'first_level_total': False,
            'columns_description': (
                {
                    'key': 'classification',
                    'column_title': _('Clas.'),
                    'title_alignment': 'Left',
                    'column_width': 0.05,
                    'alignment': 'Left',
                    'in_lines': True,
                },{
                    'key': 'prize',
                    'column_title': _('Prize'),
                    'title_alignment': 'Left',
                    'column_width': 0.1,
                    'alignment': 'Left',
                    'in_lines': True,
                },{
                    'key': 'cage',
                    'column_title': _('Cage'),
                    'title_alignment': 'Left',
                    'column_width': 0.06,
                    'alignment': 'Right',
                    'in_lines': True,
                },{
                    'key': 'ring_a',
                    'column_title': _('Ring A'),
                    'title_alignment': 'Left',
                    'column_width': 0.18,
                    'alignment': 'Left',
                    'in_lines': True,
                },{
                    'key': 'ring_b',
                    'column_title': _('Ring B'),
                    'title_alignment': 'Left',
                    'column_width': 0.18,
                    'alignment': 'Left',
                    'in_lines': True,
                },{
                    'key': 'ring_c',
                    'column_title': _('Ring C'),
                    'title_alignment': 'Left',
                    'column_width': 0.18,
                    'alignment': 'Left',
                    'in_lines': True,
                },{
                    'key': 'ring_d',
                    'column_title': _('Ring D'),
                    'title_alignment': 'Left',
                    'column_width': 0.18,
                    'alignment': 'Left',
                    'in_lines': True,
                },{
                    'key': 'square',
                    'column_title': _(''),
                    'title_alignment': 'Left',
                    'column_width': 0.06,
                    'alignment': 'Left',
                    'in_lines': True,
                }
                
            )
        }

        super(RingInspectionReport, self).__init__(CONF_TABLE, arguments, canvas)

    def fetch_data(self):
        data = OrderedDict()

        for item in self.arguments:

            first_level_id = item[0]
            content_id = item[3]
            description = item[0].title()

            if first_level_id not in data:
                data[first_level_id] = dict()
                data[first_level_id]["_"] = OrderedDict()

            if content_id not in data[first_level_id]["_"]:
                data[first_level_id]["_"][content_id] = dict()
                data[first_level_id]["_"][content_id]['classification'] = str(item[1])
                prize = item[2] or ""
                data[first_level_id]["_"][content_id]['prize'] = prize
                data[first_level_id]["_"][content_id]['cage'] = str(item[3].lstrip("0"))
                data[first_level_id]["_"][content_id]['ring_a'] = str(item[4])
                data[first_level_id]["_"][content_id]['ring_b'] = str(item[5])
                data[first_level_id]["_"][content_id]['ring_c'] = str(item[6])
                data[first_level_id]["_"][content_id]['ring_d'] = str(item[7])
                data[first_level_id]["_"][content_id]['square'] = "[  ]"

        self.data = data

    def gen_pdf(self):
        return drawpdf(self)