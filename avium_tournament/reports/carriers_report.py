# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict

from avium_tournament.utils import get_tournament_value
from avium_tournament.reports.commonFunctions import importFonts, draw_header, draw_footer, split_text_in_lines, text_truncate, none_empty, blank_undefined, status_extraction
from frappe import _
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth

class CarriersReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Report Configuration", "Report Configuration")
        raw_report_configuration_data.only_first_page = 0

        CONF_TABLE = {
            'report_name': _("Carriers Report"),
            'report_configuration': raw_report_configuration_data,
            'judge_background_color': colors.fidblue,
            'judge_font_color': colors.white,
            'title_background_color': HexColor(0x989898),
        }

        super(CarriersReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return (
            blank_undefined(self.arguments.get('judge', None)),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql(""" 
            SELECT  tgro.individual_code as group_code,
                    tgro.description as group_description,
                    jud.judge_name as judge_name,
                    scote.cage_number as cage_number,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    scote.total_a as punctuation_a,
                    scote.total_b as punctuation_b,
                    scote.total_c as punctuation_c,
                    scote.total_d as punctuation_d,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_1 as prize_1,
                    1 as team_size,
                    scote.classification as classification
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabJudge` as jud on jud.name = scote.judge_1
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                CASE WHEN %(judge)s != 'Undefined' THEN scote.judge_1 = %(judge)s ELSE TRUE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  tgro.team_code as group_code,
                    tgro.description as group_description,
                    jud.judge_name as judge_name,
                    scote.cage_number as cage_number,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    scote.total_a as punctuation_a,
                    scote.total_b as punctuation_b,
                    scote.total_c as punctuation_c,
                    scote.total_d as punctuation_d,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_1 as prize_1,
                    tgro.team_size as team_size,
                    scote.classification as classification
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabJudge` as jud on jud.name = scote.judge_1
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                CASE WHEN %(judge)s != 'Undefined' THEN scote.judge_1 = %(judge)s ELSE TRUE END AND
                exbr.idx = 1
            ORDER BY judge_name ASC, group_code ASC, cage_number ASC
            """,
            {
                'judge': self.get_filter()[0],
            },
            as_dict = True
        )

        tournament_data = get_tournament_value(['add_tie_break_marks',])

        data = OrderedDict()

        for item in raw_data:

            judge_name = blank_undefined(item['judge_name'])

            if judge_name not in data:
                data[judge_name] = OrderedDict()

            if item['group_code'] not in data[judge_name]:
                data[judge_name][item['group_code']] = OrderedDict()
                data[judge_name][item['group_code']]['group_code'] = item['group_code']
                data[judge_name][item['group_code']]['group_description'] = item['group_description']
                data[judge_name][item['group_code']]['team_size'] = item['team_size']
                data[judge_name][item['group_code']]['cages'] = list()

            cage_information = dict()
            cage_information['cage_number'] = none_empty(item['cage_number']).lstrip("0")
            cage_information['punctuation_a'] = item['punctuation_a']
            if item['team_size'] > 1:
                cage_information['punctuation_b'] = item['punctuation_b']
                cage_information['harmony'] = item['harmony']
            if item['team_size'] > 2:
                cage_information['punctuation_c'] = item['punctuation_c']
            if item['team_size'] > 3:
                cage_information['punctuation_d'] = item['punctuation_d']

            if tournament_data['add_tie_break_marks'] == "Yes":
                cage_information['total'] = item['total_mark'] + item['tie_break_mark']
            else:
                cage_information['total'] = item['total_mark']

            prize = blank_undefined(item['prize_1'])
            status = status_extraction(
                (item['status_a'], item['status_b'], item['status_c'], item['status_d'],),
                (item['location_status_a'], item['location_status_b'], item['location_status_c'], item['location_status_d'],),
                abbreviation=True
            )
            if status != "Judjable":
                cage_information['prize'] = _(status)
            elif prize != "Undefined":
                cage_information['prize'] = prize

            data[judge_name][item['group_code']]['cages'].append(cage_information)

        self.data = data

    def gen_pdf(self):

        # **Set file name**
        self.canvas.setTitle(self.configuration['report_name'])

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']

        width, height = A4

        printable_width = width - configuration.left_margin * cm - configuration.right_margin * cm - 2 * self.configuration['extra_horizontal_text_offset']
        horizontal_start_position = configuration.left_margin * cm + self.configuration['extra_horizontal_text_offset']
        underline_width = printable_width / (8*2)

        cage_title_text = _("Cage Nº")
        A_title_text = "A"
        B_title_text = "B"
        C_title_text = "C"
        D_title_text = "D"
        armony_title_text = _("Armony")
        total_title_text = _("Total")
        prize_title_text = _("Prize")

        # **Titles lines printing function**
        def judge_name_print(self, current_height, judge_name):
            self.canvas.saveState()
            self.canvas.setStrokeColor(self.configuration['judge_background_color'])
            self.canvas.setFillColor(self.configuration['judge_background_color'])
            self.canvas.rect(
                horizontal_start_position,
                current_height - self.configuration['lines_offset'],
                printable_width,
                self.configuration['distance_between_lines'],
                stroke=1,
                fill=1
            )
            self.canvas.restoreState()

            self.canvas.line(
                horizontal_start_position,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                horizontal_start_position + printable_width,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset']
            )
            self.canvas.line(
                horizontal_start_position,
                current_height - self.configuration['lines_offset'],
                horizontal_start_position + printable_width,
                current_height - self.configuration['lines_offset']
            )

            self.canvas.saveState()
            self.canvas.setFont(self.configuration['bold_font'], self.configuration['title_size'])
            self.canvas.setFillColor(self.configuration['judge_font_color'])
            self.canvas.setStrokeColor(self.configuration['judge_font_color'])
            self.canvas.drawString(
                horizontal_start_position + self.configuration['gap_between_columns']/2,
                current_height,
                _("Judge") + ": " + judge_name.upper()
            )
            self.canvas.restoreState()

        def titles_print(self, current_height, group_code, group_description, team_size):

            # **Variables initialization**
            group_title_width = stringWidth(
                _("Group") + ": ",
                self.configuration['bold_font'],
                self.configuration['title_size']
            )

            group_description_in_lines = split_text_in_lines(
                group_code.upper() + " - " + group_description,
                printable_width - stringWidth(_("Group") + ": ", self.configuration['bold_font'], self.configuration['title_size']),
                self.configuration['bold_font'],
                self.configuration['title_size']
            )

            if team_size < 2:
                columns = team_size + 2
            else:
                columns = team_size + 4

            columns_width = printable_width / columns

            # **Custom functions**
            def title_text_truncate(title_text):
                return text_truncate(
                    title_text,
                    columns_width - self.configuration['gap_between_columns'],
                    self.configuration['font'],
                    self.configuration['title_size']
                )

            # Print title
            self.canvas.saveState()

            self.canvas.saveState()
            self.canvas.setStrokeColor(self.configuration['title_background_color'])
            self.canvas.setFillColor(self.configuration['title_background_color'])
            self.canvas.rect(
                horizontal_start_position,
                current_height - self.configuration['lines_offset'],
                printable_width,
                self.configuration['distance_between_lines'],
                stroke=1,
                fill=1
            )
            self.canvas.restoreState()

            self.canvas.line(
                horizontal_start_position,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                horizontal_start_position + printable_width,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset']
            )

            self.canvas.setFont(self.configuration['bold_font'], self.configuration['title_size'])

            self.canvas.drawString(
                horizontal_start_position + self.configuration['gap_between_columns']/2,
                current_height,
                _("Group") + ": "
            )

            for index, description_line in enumerate(group_description_in_lines):
                self.canvas.drawString(
                    horizontal_start_position + self.configuration['gap_between_columns']/2 + group_title_width,
                    current_height,
                    description_line)
                current_height -= self.configuration['distance_between_lines']

            self.canvas.line(
                horizontal_start_position,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset'],
                horizontal_start_position + printable_width,
                current_height + self.configuration['distance_between_lines'] - self.configuration['lines_offset']
            )

            self.canvas.setFont(self.configuration['font'], self.configuration['title_size'])
            horizontal_position = horizontal_start_position + columns_width/2
            self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(cage_title_text))
            horizontal_position += columns_width
            self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(A_title_text))
            if team_size > 1:
                horizontal_position += columns_width
                self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(B_title_text))
            if team_size > 2:
                horizontal_position += columns_width
                self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(C_title_text))
            if team_size > 3:
                horizontal_position += columns_width
                self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(D_title_text))
            if team_size > 1:
                horizontal_position += columns_width
                self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(armony_title_text))
                horizontal_position += columns_width
                self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(total_title_text))
            horizontal_position += columns_width
            self.canvas.drawCentredString(horizontal_position, current_height, title_text_truncate(prize_title_text))

            self.canvas.line(
                horizontal_start_position,
                current_height - self.configuration['lines_offset'],
                horizontal_start_position + printable_width,
                current_height - self.configuration['lines_offset']
            )

            self.canvas.restoreState()

            return current_height

        def content_print(self, current_height, cage_information, team_size, background):

            def text_line_print(text, horizontal_position, current_height):
                if self.arguments.get('results', False):
                    self.canvas.saveState()
                    self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
                    self.canvas.drawCentredString(
                        horizontal_position,
                        current_height,
                        text
                    )
                    self.canvas.restoreState()
                else:
                    self.canvas.line(
                        horizontal_position - underline_width / 2,
                        current_height,
                        horizontal_position + underline_width / 2,
                        current_height
                    )

            # **Variables initialization**
            if team_size < 2:
                columns = team_size + 2
            else:
                columns = team_size + 4

            columns_width = printable_width / columns

            # Print title
            self.canvas.saveState()

            if background:
                self.canvas.saveState()
                self.canvas.setStrokeColor(self.configuration['content_intermittent_background'])
                self.canvas.setFillColor(self.configuration['content_intermittent_background'])
                self.canvas.rect(
                    horizontal_start_position,
                    current_height - self.configuration['lines_offset'],
                    printable_width,
                    self.configuration['distance_between_lines'],
                    stroke=1,
                    fill=1
                )
                self.canvas.restoreState()

            horizontal_position = horizontal_start_position + columns_width/2
            self.canvas.saveState()
            self.canvas.setFont(self.configuration['font'], self.configuration['text_size'])
            self.canvas.drawCentredString(
                horizontal_position,
                current_height,
                str(cage_information['cage_number'])
            )
            self.canvas.restoreState()

            horizontal_position += columns_width
            text_line_print(str(cage_information['punctuation_a']), horizontal_position, current_height)
            if team_size > 1:
                horizontal_position += columns_width
                text_line_print(str(cage_information['punctuation_b']), horizontal_position, current_height)
            if team_size > 2:
                horizontal_position += columns_width
                text_line_print(str(cage_information['punctuation_c']), horizontal_position, current_height)
            if team_size > 3:
                horizontal_position += columns_width
                text_line_print(str(cage_information['punctuation_d']), horizontal_position, current_height)
            if team_size > 1:
                horizontal_position += columns_width
                text_line_print(str(cage_information['harmony']), horizontal_position, current_height)
                horizontal_position += columns_width
                text_line_print(str(cage_information['total']), horizontal_position, current_height)
            horizontal_position += columns_width
            text_line_print(str(cage_information.get('prize', "")), horizontal_position, current_height)

            self.canvas.restoreState()

        def new_line(self, current_height, comparison_value, background, judge_name = None, group_key = None):
            if (current_height - comparison_value) < (configuration.lower_margin * cm + configuration.footer_height * cm):
                draw_footer(self, 1)
                self.canvas.showPage()

                # **Print header**
                current_height = height - draw_header(self) - self.configuration["header_titles_gap"]

                # **Print judge name**
                if judge_name is not None:
                    judge_name_print(self, current_height, judge_name)
                    current_height -= self.configuration["distance_between_lines"]

                # **Print titles**
                if group_key is not None:
                    current_height = titles_print(
                        self,
                        current_height,
                        self.data[judge_name][group_key]['group_code'],
                        self.data[judge_name][group_key]['group_description'],
                        self.data[judge_name][group_key]['team_size']
                    )
                    current_height -= self.configuration["distance_between_lines"]

                return current_height, False
            else:
                current_height -= self.configuration["distance_between_lines"]
                return current_height, background

        # **Importem les fonts**
        importFonts()

        # **Imprimir capçalera**
        current_height = height - draw_header(self) - self.configuration['header_titles_gap']

        for judge_index, judge_name in enumerate(self.data.keys()):
            if judge_index > 0:
                current_height, background = new_line(
                    self,
                    current_height,
                    self.configuration['distance_between_lines'],
                    False
                )

            judge_name_print(self, current_height, judge_name)

            for group in self.data[judge_name].keys():

                group_description_in_lines = split_text_in_lines(
                    self.data[judge_name][group]['group_code'].upper() + " - " + self.data[judge_name][group]['group_description'],
                    printable_width - stringWidth(_("Group") + ": ", self.configuration['bold_font'], self.configuration['title_size']),
                    self.configuration['bold_font'],
                    self.configuration['title_size']
                )

                current_height, background = new_line(
                    self,
                    current_height,
                    (len(group_description_in_lines) + 2) * self.configuration['distance_between_lines'],
                    False,
                    judge_name = judge_name
                )

                current_height = titles_print(
                    self,
                    current_height,
                    self.data[judge_name][group]['group_code'],
                    self.data[judge_name][group]['group_description'],
                    self.data[judge_name][group]['team_size']
                )

                background = False
                for cage_number in self.data[judge_name][group]['cages']:
                    current_height, background = new_line(
                        self,
                        current_height,
                        self.configuration['distance_between_lines'],
                        background,
                        judge_name = judge_name,
                        group_key = group
                    )
                    content_print(self, current_height, cage_number, self.data[judge_name][group]['team_size'], background)
                    background = not background

            current_height = 0

        # **Imprimir línia peu de pàgina**
        draw_footer(self, 1)