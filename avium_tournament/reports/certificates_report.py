# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe
import re
import tempfile

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.utils import get_tournament_value
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import importFonts, split_text_in_lines, text_truncate, load_image, blank_undefined, none_empty, draw_footer
from frappe import _
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.pdfgen import canvas

class CertificatesReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Certificate Configuration", "Certificate Configuration")

        table_text_size = 9
        distance_between_lines = 17


        CONF_TABLE = {
            'report_configuration': raw_report_configuration_data,
            'big_title_size': 18,
            'small_title_size': 8,
            'table_title_size': 10,
            'table_text_size': table_text_size,
            'signature_name_description_text_size': 9,
            'date_city_text_size': 11,
            'gap_between_images': 5,
            'gap_between_lines': 7,
            'extra_horizontal_text_offset': 0,
            'table_margin': 5,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * table_text_size)/2 + 0.5,
            'name_color': colors.red,
        }

        super(CertificatesReport, self).__init__(CONF_TABLE, arguments, canvas)

    def init_canvas(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        self.canvas = canvas.Canvas(self.tempfile)
        height, width = A4
        self.canvas.setPageSize((width, height))

    def get_filter(self):
        return blank_undefined(self.arguments.get('registration', None))

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    tgro.individual_code as group_code,
                    ireg.registration_description as group_description,
                    ireg.ring_a as ring_a,
                    NULL as ring_b,
                    NULL as ring_c,
                    NULL as ring_d,
                    scote.total_mark as total_mark,
                    scote.tie_break_mark as tie_break_mark,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3,
                    touprides.assignment_order as prize_order
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                scote.prize != 'NULL' AND
                CASE WHEN %(filter)s != 'Undefined' THEN reg.name = %(filter)s ELSE TRUE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    tgro.team_code as group_code,
                    treg.registration_description as group_description,
                    treg.ring_a as ring_a,
                    treg.ring_b as ring_b,
                    treg.ring_c as ring_c,
                    treg.ring_d as ring_d,
                    scote.total_mark as total_mark,
                    scote.tie_break_mark as tie_break_mark,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3,
                    touprides.assignment_order as prize_order
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                scote.prize != 'NULL' AND
                CASE WHEN %(filter)s != 'Undefined' THEN reg.name = %(filter)s ELSE TRUE END AND
                exbr.idx = 1
            ORDER BY exhibitor_surname ASC, exhibitor_name ASC, group_code ASC, prize_order ASC
            """,
            {'filter': self.get_filter()},
            as_dict = True
        )

        self.data = dict()

        tournament_data = get_tournament_value(["tournament_name","association","add_tie_break_marks"])
        self.data['tournament'] = tournament_data['tournament_name']
        self.data['association'] = tournament_data['association']
        self.data['exhibitors'] = OrderedDict()

        for item in raw_data:

            if item['registration'] not in self.data['exhibitors']:
                self.data['exhibitors'][item['registration']] = dict()
                self.data['exhibitors'][item['registration']]['exhibitor_name'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                self.data['exhibitors'][item['registration']]['bc'] = item['bc']
                self.data['exhibitors'][item['registration']]['prizes'] = list()

            if tournament_data['add_tie_break_marks'] == "Yes":
                total_mark = item['total_mark'] + item.get('tie_break_mark', 0)
            else:
                total_mark = item['total_mark']

            if self.arguments['prize'] == "1":
                prize = item['prize_1']
            elif self.arguments['prize'] == "2":
                prize = item['prize_2']
            else:
                prize = item['prize_3']


            self.data['exhibitors'][item['registration']]['prizes'].append(
                {
                    'group_description': item['group_description'],
                    'group_code': item['group_code'],
                    'ring_a': none_empty(item['ring_a']),
                    'ring_b': none_empty(item['ring_b']),
                    'ring_c': none_empty(item['ring_c']),
                    'ring_d': none_empty(item['ring_d']),
                    'tie_break_mark': item.get('tie_break_mark', 0),
                    'total_mark': total_mark,
                    'prize': prize,
                }
            )

    def gen_pdf(self):

        tournament_data = get_tournament_value(["add_tie_break_marks"])

        # **Set file name**
        self.canvas.setTitle(_("Certificates"))

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']
        
        height, width = A4
        
        #**Specific Parameters**
        printable_width = width - configuration.left_margin*cm - configuration.right_margin*cm
        printable_height = height - configuration.upper_margin*cm - configuration.lower_margin*cm
        
        width_page_center = configuration.left_margin*cm + printable_width/2
    
        date_city_height = configuration.lower_margin * cm + configuration.footer_height * cm + self.configuration['gap_between_lines']
        signature_name_height = date_city_height + self.configuration['date_city_text_size'] + self.configuration['gap_between_lines']
        signature_description_height = signature_name_height + self.configuration['signature_name_description_text_size'] + self.configuration['gap_between_lines']

        table_width = printable_width - configuration.table_left_margin * cm - configuration.table_right_margin * cm - 2*self.configuration['extra_horizontal_text_offset']
        group_number_width = 50
        ring_width = 30
        if tournament_data['add_tie_break_marks'] == "Yes":
            tie_break_mark_width = 30
        else:
            tie_break_mark_width = 0
        total_width = 50
        prize_width = 70
        registration_description_width = max(
            0,
            table_width - group_number_width - 4*ring_width - tie_break_mark_width - total_width - prize_width
        )
        end_height = configuration.lower_margin*cm + configuration.footer_height*cm + configuration.signature_height*cm + 2 * self.configuration['gap_between_lines']
    
        # **Text truncate and position calculation**
        registration_description_position = configuration.left_margin * cm + self.configuration['extra_horizontal_text_offset'] + configuration.table_left_margin * cm
        group_number_position = registration_description_position + registration_description_width
        ring_A_position = group_number_position + group_number_width
        ring_B_position = ring_A_position + ring_width
        ring_C_position = ring_B_position + ring_width
        ring_D_position = ring_C_position + ring_width
        tie_break_mark_position = ring_D_position + ring_width
        total_position = tie_break_mark_position + tie_break_mark_width
        prize_position = total_position + total_width
    
        registration_description_text = text_truncate(_('Registration Description'), registration_description_width-2*self.configuration['table_margin'], self.configuration['bold_font'], self.configuration['table_title_size'])
        group_number_text = text_truncate(_('Group'), group_number_width-2*self.configuration['table_margin'], self.configuration['bold_font'], self.configuration['table_title_size'])
        ring_text = text_truncate(_('Rings'), 4*ring_width-2*self.configuration['table_margin'], self.configuration['bold_font'], self.configuration['table_title_size'])
        tie_break_mark_text = text_truncate(_('TBM'), tie_break_mark_width-2*self.configuration['table_margin'], self.configuration['bold_font'], self.configuration['table_title_size'])
        total_text = text_truncate(_('Total'), total_width-2*self.configuration['table_margin'], self.configuration['bold_font'], self.configuration['table_title_size'])
        prize_text = text_truncate(_('Record Book'), prize_width-2*self.configuration['table_margin'], self.configuration['bold_font'], self.configuration['table_title_size'])

        # Define max logo size:
        max_logo_height = min(configuration.header_height*cm, configuration.logo_size)
        max_logo_width = configuration.logo_size

        tournament_data = get_tournament_value(['association', 'federation', 'judge_association'])

        # Load left logo
        if configuration.left_logo == "Federation Logo":
            logo_1_directory, logo_1 = load_image(
                frappe.get_value('Federation', tournament_data['federation'], 'logo')
            )
        elif configuration.left_logo == "Association Logo":
            logo_1_directory, logo_1 = load_image(
                frappe.get_value('Association', tournament_data['association'], 'logo')
            )
        elif configuration.left_logo == "Judge Association Logo":
            logo_1_directory, logo_1 = load_image(
                frappe.get_value('Judge Association', tournament_data['judge_association'], 'logo')
            )
        elif configuration.left_logo == "Logo 1":
            logo_1_directory, logo_1 = load_image(
                configuration.logo_1
            )
        elif configuration.left_logo == "Logo 2":
            logo_1_directory, logo_1 = load_image(
                configuration.logo_2
            )
        else:
            logo_1_directory, logo_1 = None, None

        # Load right logo
        if configuration.right_logo == "Federation Logo":
            logo_2_directory, logo_2 = load_image(
                frappe.get_value('Federation', tournament_data['federation'], 'logo')
            )
        elif configuration.right_logo == "Association Logo":
            logo_2_directory, logo_2 = load_image(
                frappe.get_value('Association', tournament_data['association'], 'logo')
            )
        elif configuration.right_logo == "Judge Association Logo":
            logo_2_directory, logo_2 = load_image(
                frappe.get_value('Judge Association', tournament_data['judge_association'], 'logo')
            )
        elif configuration.right_logo == "Logo 1":
            logo_2_directory, logo_2 = load_image(
                configuration.logo_1
            )
        elif configuration.right_logo == "Logo 2":
            logo_2_directory, logo_2 = load_image(
                configuration.logo_2
            )
        else:
            logo_2_directory, logo_2 = None, None

        # Load signatures

        signatures_to_print = 0
        signature_1_directory, signature_1 = load_image(configuration.signature_1)
        if not (signature_1_directory is None and blank_undefined(configuration.signature_1_description) == blank_undefined(configuration.signature_1_name) == "Undefined"):
            signatures_to_print += 1
            signature_2_directory, signature_2 = load_image(configuration.signature_2)
            if not (signature_2_directory is None and blank_undefined(configuration.signature_2_description) == blank_undefined(configuration.signature_2_name) == "Undefined"):
                signatures_to_print += 1
                signature_3_directory, signature_3 = load_image(configuration.signature_3)
                if not (signature_3_directory is None and blank_undefined(configuration.signature_3_description) == blank_undefined(configuration.signature_3_name) == "Undefined"):
                    signatures_to_print += 1
                    signature_4_directory, signature_4 = load_image(configuration.signature_4)
                    if not (signature_4_directory is None and blank_undefined(configuration.signature_1_description) == blank_undefined(configuration.signature_1_name) == "Undefined"):
                        signatures_to_print += 1

        # **Custom functions**
        def print_base_document(table_content):
    
            #Print background
            background_directory, background_image = load_image(configuration.background_image)
    
            if background_directory is not None:
                background_width, background_height = background_image.size
    
                ratio = min(printable_width*1.0/background_width*1.0, printable_height*1.0/background_height*1.0)
                background_width = background_width*ratio
                background_height = background_height*ratio
                background_horizontal_position = configuration.left_margin*cm
                background_vertical_position = configuration.lower_margin*cm + (height-configuration.lower_margin*cm-configuration.upper_margin*cm)/2 - background_height/2
                self.canvas.drawImage(background_directory, background_horizontal_position, background_vertical_position, height=background_height, width=background_width)
    
            # Print header
            if configuration.print_header == "Yes":
    
                # Print left logo
                if logo_1_directory is not None:
                    logo_1_width, logo_1_height = logo_1.size
    
                    ratio = min(max_logo_width*1.0/logo_1_width*1.0, max_logo_height*1.0/logo_1_height*1.0)
                    logo_1_width = logo_1_width*ratio
                    logo_1_height = logo_1_height*ratio
                    logo_1_horizontal_position = configuration.left_margin*cm
                    logo_1_vertical_position = height - configuration.upper_margin*cm - configuration.header_height*cm
                    self.canvas.drawImage(logo_1_directory, logo_1_horizontal_position, logo_1_vertical_position, height=logo_1_height, width=logo_1_width)
                else:
                    logo_1_width = -self.configuration['gap_between_images']
    
                # Print right logo
                if logo_2_directory is not None:
                    logo_2_width, logo_2_height = logo_2.size
    
                    ratio = min(max_logo_width*1.0/logo_2_width*1.0, max_logo_height*1.0/logo_2_height*1.0)
                    logo_2_width = logo_2_width*ratio
                    logo_2_height = logo_2_height*ratio
                    logo_2_horizontal_position = width - configuration.right_margin * cm - logo_2_width
                    logo_2_vertical_position = height - configuration.upper_margin*cm - configuration.header_height*cm
                    self.canvas.drawImage(logo_2_directory, logo_2_horizontal_position, logo_2_vertical_position, height=logo_2_height, width=logo_2_width)
                else:
                    logo_2_width = -self.configuration['gap_between_images']
                    
    
                # Print text
                association_width = printable_width - logo_1_width - logo_2_width -2*self.configuration['gap_between_images']
                association_in_lines = split_text_in_lines(self.data['association'], association_width, self.configuration['bold_font'], self.configuration['big_title_size'])
    
                self.canvas.setFont(self.configuration['bold_font'],self.configuration['big_title_size'])
                current_height = height - configuration.upper_margin*cm - configuration.header_height*cm + self.configuration['gap_between_lines']
                self.canvas.drawCentredString(width_page_center, current_height, _("CERTIFIES"))
                current_height += self.configuration['big_title_size'] + self.configuration['gap_between_lines']
                for index in range(len(association_in_lines)):
                    self.canvas.drawCentredString(width_page_center, current_height, association_in_lines[len(association_in_lines) - 1 - index])
                    current_height += self.configuration['big_title_size'] + self.configuration['gap_between_lines']

            # Print content text
            current_height = height - configuration.upper_margin*cm - configuration.header_height*cm

            current_height -= self.configuration['big_title_size'] + self.configuration['gap_between_lines']

            formal_text_1 = _("That Mr./Mrs.")
            name_text = self.data['exhibitors'][exhibitor]["exhibitor_name"] + "    " + _("BC") + ": " + self.data['exhibitors'][exhibitor]["bc"]

            formal_width = stringWidth(formal_text_1 + " ", self.configuration['font'],
                                       self.configuration['small_title_size'])
            name_width = stringWidth(name_text, self.configuration['bold_font'], self.configuration['big_title_size'])

            formal_position = width_page_center - (formal_width + name_width) / 2
            name_position = formal_position + formal_width

            self.canvas.setFont(self.configuration['font'], self.configuration['small_title_size'])
            self.canvas.drawString(formal_position, current_height, formal_text_1)
            self.canvas.saveState()
            self.canvas.setFont(self.configuration['bold_font'], self.configuration['big_title_size'])
            self.canvas.setStrokeColor(self.configuration['name_color'])
            self.canvas.setFillColor(self.configuration['name_color'])
            self.canvas.drawString(name_position, current_height, name_text)
            self.canvas.restoreState()

            current_height -= self.configuration['big_title_size'] + self.configuration['gap_between_lines']

            formal_text_2 = _("has won in")
            formal_text_3 = _("the following prizes")
            tournament_name = self.data['tournament']

            formal_2_width = stringWidth(formal_text_2 + " ", self.configuration['font'],
                                         self.configuration['small_title_size'])
            formal_3_width = stringWidth(formal_text_3 + " :", self.configuration['font'],
                                         self.configuration['small_title_size'])
            tournament_width = stringWidth(tournament_name, self.configuration['font'],
                                           self.configuration['big_title_size'])

            formal_2_position = width_page_center - (formal_2_width + formal_3_width + tournament_width) / 2
            tournament_position = formal_2_position + formal_2_width
            formal_3_position = tournament_position + tournament_width

            self.canvas.setFont(self.configuration['font'], self.configuration['small_title_size'])
            self.canvas.drawString(formal_2_position, current_height, formal_text_2)
            self.canvas.setFont(self.configuration['font'], self.configuration['big_title_size'])
            self.canvas.drawString(tournament_position, current_height, tournament_name)
            self.canvas.setFont(self.configuration['font'], self.configuration['small_title_size'])
            self.canvas.drawString(formal_3_position, current_height, " " + formal_text_3 + ":")

            # Print signatures
            if configuration.print_signatures == "Yes":
                
                #Print signatures
                signature_1_height = signature_2_height = signature_3_height = signature_4_height = 2*(self.configuration['signature_name_description_text_size'] + self.configuration['gap_between_lines'])
                if signatures_to_print>0:
                    signature_width = (printable_width - (signatures_to_print-1)*self.configuration['gap_between_images'])/signatures_to_print
                    signature_1_position = configuration.left_margin*cm + signature_width/2
                    self.canvas.setFont(self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    if configuration.signature_1_name is not None:
                        signature_1_name_text = text_truncate(configuration.signature_1_name, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_1_name_text = ""
                    if configuration.signature_1_description is not None:
                        signature_1_description_text = text_truncate(configuration.signature_1_description, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_1_description_text = ""
                    self.canvas.drawCentredString(signature_1_position, signature_name_height, signature_1_name_text)
                    self.canvas.drawCentredString(signature_1_position, signature_description_height, signature_1_description_text)
    
                    if signature_1_directory is not None:
                        signature_1_width, signature_1_height = signature_1.size
                        max_signature_1_width = signature_width
                        max_signature_1_height = configuration.signature_height*cm - signature_description_height + date_city_height - self.configuration['signature_name_description_text_size'] - 2*self.configuration['gap_between_lines']
    
                        ratio = min(max_signature_1_width*1.0/signature_1_width*1.0, max_signature_1_height*1.0/signature_1_height*1.0)
                        signature_1_width = signature_1_width*ratio
                        signature_1_height = signature_1_height*ratio
                        signature_1_horizontal_position = signature_1_position - signature_1_width/2
                        signature_1_vertical_position = signature_description_height + self.configuration['signature_name_description_text_size'] + self.configuration['gap_between_lines']
                        self.canvas.drawImage(signature_1_directory, signature_1_horizontal_position, signature_1_vertical_position, height=signature_1_height, width=signature_1_width)
    
                if signatures_to_print>1:
                    signature_2_position = signature_1_position + signature_width + self.configuration['gap_between_images']
                    self.canvas.setFont(self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    if configuration.signature_2_name is not None:
                        signature_2_name_text = text_truncate(configuration.signature_2_name, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_2_name_text = ""
                    if configuration.signature_2_description is not None:
                        signature_2_description_text = text_truncate(configuration.signature_2_description, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_2_description_text = ""
                    self.canvas.drawCentredString(signature_2_position, signature_name_height, signature_2_name_text)
                    self.canvas.drawCentredString(signature_2_position, signature_description_height, signature_2_description_text)
    
                    if signature_2_directory is not None:
                        signature_2_width, signature_2_height = signature_2.size
                        max_signature_2_width = signature_width
                        max_signature_2_height = configuration.signature_height*cm - signature_description_height + date_city_height - self.configuration['signature_name_description_text_size'] - 2*self.configuration['gap_between_lines']
    
                        ratio = min(max_signature_2_width*1.0/signature_2_width*1.0, max_signature_2_height*1.0/signature_2_height*1.0)
                        signature_2_width = signature_2_width*ratio
                        signature_2_height = signature_2_height*ratio
                        signature_2_horizontal_position = signature_2_position - signature_2_width/2
                        signature_2_vertical_position = signature_description_height + self.configuration['signature_name_description_text_size'] + self.configuration['gap_between_lines']
                        self.canvas.drawImage(signature_2_directory, signature_2_horizontal_position, signature_2_vertical_position, height=signature_2_height, width=signature_2_width)
                        
                if signatures_to_print>2:
                    signature_3_position =  signature_2_position + signature_width + self.configuration['gap_between_images']
                    self.canvas.setFont(self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    if configuration.signature_3_name is not None:
                        signature_3_name_text = text_truncate(configuration.signature_3_name, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_3_name_text = ""
                    if configuration.signature_3_description is not None:
                        signature_3_description_text = text_truncate(configuration.signature_3_description, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_3_description_text = ""
                    self.canvas.drawCentredString(signature_3_position, signature_name_height, signature_3_name_text)
                    self.canvas.drawCentredString(signature_3_position, signature_description_height, signature_3_description_text)
    
                    if signature_3_directory is not None:
                        signature_3_width, signature_3_height = signature_3.size
                        max_signature_3_width = signature_width
                        max_signature_3_height = configuration.signature_height*cm - signature_description_height + date_city_height - self.configuration['signature_name_description_text_size'] - 2*self.configuration['gap_between_lines']
    
                        ratio = min(max_signature_3_width*1.0/signature_3_width*1.0, max_signature_3_height*1.0/signature_3_height*1.0)
                        signature_3_width = signature_3_width*ratio
                        signature_3_height = signature_3_height*ratio
                        signature_3_horizontal_position = signature_3_position - signature_3_width/2
                        signature_3_vertical_position = signature_description_height + self.configuration['signature_name_description_text_size'] + self.configuration['gap_between_lines']
                        self.canvas.drawImage(signature_3_directory, signature_3_horizontal_position, signature_3_vertical_position, height=signature_3_height, width=signature_3_width)
                        
                if signatures_to_print>3:
                    signature_4_position =  signature_3_position + signature_width + self.configuration['gap_between_images']
                    self.canvas.setFont(self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    if configuration.signature_4_name is not None:
                        signature_4_name_text = text_truncate(configuration.signature_4_name, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_4_name_text = ""
                    if configuration.signature_4_description is not None:
                        signature_4_description_text = text_truncate(configuration.signature_4_description, signature_width, self.configuration['font'], self.configuration['signature_name_description_text_size'])
                    else:
                        signature_4_description_text = ""
                    self.canvas.drawCentredString(signature_4_position, signature_name_height, signature_4_name_text)
                    self.canvas.drawCentredString(signature_4_position, signature_description_height, signature_4_description_text)
    
                    if signature_4_directory is not None:
                        signature_4_width, signature_4_height = signature_4.size
                        max_signature_4_width = signature_width
                        max_signature_4_height = configuration.signature_height*cm - signature_description_height + date_city_height - self.configuration['signature_name_description_text_size'] - 2*self.configuration['gap_between_lines']
    
                        ratio = min(max_signature_4_width*1.0/signature_4_width*1.0, max_signature_4_height*1.0/signature_4_height*1.0)
                        signature_4_width = signature_4_width*ratio
                        signature_4_height = signature_4_height*ratio
                        signature_4_horizontal_position = signature_4_position - signature_4_width/2
                        signature_4_vertical_position = signature_description_height + self.configuration['signature_name_description_text_size'] + self.configuration['gap_between_lines']
                        self.canvas.drawImage(signature_4_directory, signature_4_horizontal_position, signature_4_vertical_position, height=signature_4_height, width=signature_4_width)
                signature_height = max(signature_1_height, signature_2_height, signature_3_height, signature_4_height)

            # Print date and city
            if configuration.date_city is not None:
                self.canvas.setFont(self.configuration['font'], self.configuration['date_city_text_size'])
                self.canvas.drawCentredString(width_page_center, date_city_height, configuration.date_city)
    
            # Print footer
            #draw_footer(self.canvas,data,print_line_page=False)

            # Calculate table start point
            table_start_height = current_height - 2 * self.configuration['gap_between_lines']

            group_description_column_width = registration_description_width - 2 * self.configuration['table_margin']
            table_height = (sum(
                [len(split_text_in_lines(
                    scoring_template['group_description'],
                    group_description_column_width,
                    self.configuration['font'],
                    self.configuration['table_text_size']
                ))
                 for scoring_template
                 in table_content]
            ) + 1) * self.configuration['distance_between_lines']
            
            if table_height > table_start_height - end_height:
                return table_start_height - self.configuration['distance_between_lines']
            else:
                table_gap_mid_height = (table_start_height - end_height) / 2
                current_height = table_gap_mid_height + end_height + table_height/2 - self.configuration['distance_between_lines']
                return current_height
    
        def print_table_row(current_height, row_data={}, title=False):

            tournament_data = get_tournament_value(["add_tie_break_marks"])

            if title:
                self.canvas.saveState()

                # Print borders
                for horizontal_position, column_width in (
                    (registration_description_position, registration_description_width),
                    (group_number_position, group_number_width),
                    (ring_A_position, 4*ring_width),
                    (tie_break_mark_position, tie_break_mark_width),
                    (total_position, total_width),
                    (prize_position, prize_width),
                ):
                    self.canvas.rect(
                        horizontal_position,
                        current_height,
                        column_width,
                        self.configuration['distance_between_lines']
                    )

                # Print text
                self.canvas.setFont(self.configuration['bold_font'],self.configuration['table_title_size'])
                self.canvas.drawCentredString(registration_description_position + registration_description_width/2, current_height+self.configuration['lines_offset'], registration_description_text)
                self.canvas.drawCentredString(group_number_position + group_number_width/2, current_height+self.configuration['lines_offset'], group_number_text)
                self.canvas.drawCentredString(ring_C_position, current_height+self.configuration['lines_offset'], ring_text)
                if tournament_data['add_tie_break_marks'] == "Yes":
                    self.canvas.drawCentredString(tie_break_mark_position + tie_break_mark_width/2, current_height+self.configuration['lines_offset'], tie_break_mark_text)
                self.canvas.drawCentredString(total_position + total_width/2, current_height+self.configuration['lines_offset'], total_text)
                self.canvas.drawCentredString(prize_position + prize_width/2, current_height+self.configuration['lines_offset'], prize_text)
                self.canvas.restoreState()
            else:
                self.canvas.saveState()
                registration_description_in_lines = split_text_in_lines(row_data['group_description'], registration_description_width - 2 * self.configuration['table_margin'], self.configuration['font'], self.configuration['table_text_size'])

                # Print borders
                for horizontal_position, column_width in (
                    (registration_description_position, registration_description_width),
                    (group_number_position, group_number_width),
                    (ring_A_position, ring_width),
                    (ring_B_position, ring_width),
                    (ring_C_position, ring_width),
                    (ring_D_position, ring_width),
                    (tie_break_mark_position, tie_break_mark_width),
                    (total_position, total_width),
                    (prize_position, prize_width),
                ):
                    self.canvas.rect(
                        horizontal_position,
                        current_height + self.configuration['distance_between_lines'],
                        column_width,
                        - self.configuration['distance_between_lines'] * (1+(len(registration_description_in_lines)-1)*0.7)
                    )

                # Print text
                self.canvas.setFont(self.configuration['font'],self.configuration['table_text_size'])
                self.canvas.drawCentredString(group_number_position + group_number_width / 2, current_height + self.configuration['lines_offset'], row_data['group_code'])
                self.canvas.drawCentredString(ring_A_position + ring_width / 2, current_height + self.configuration['lines_offset'], re.sub('(.+\/)', '', row_data['ring_a']))
                self.canvas.drawCentredString(ring_B_position + ring_width / 2, current_height + self.configuration['lines_offset'], re.sub('(.+\/)', '', row_data['ring_b']))
                self.canvas.drawCentredString(ring_C_position + ring_width / 2, current_height + self.configuration['lines_offset'], re.sub('(.+\/)', '', row_data['ring_c']))
                self.canvas.drawCentredString(ring_D_position + ring_width / 2, current_height + self.configuration['lines_offset'], re.sub('(.+\/)', '', row_data['ring_d']))
                if tournament_data['add_tie_break_marks'] == "Yes":
                    self.canvas.drawCentredString(tie_break_mark_position + tie_break_mark_width / 2, current_height + self.configuration['lines_offset'], str(row_data['tie_break_mark']))
                self.canvas.drawCentredString(total_position + total_width / 2, current_height + self.configuration['lines_offset'], str(row_data['total_mark']))
                self.canvas.drawCentredString(
                    prize_position + prize_width/2,
                    current_height+self.configuration['lines_offset'],
                    text_truncate(row_data['prize'], prize_width - 2 * self.configuration['table_margin'], self.configuration['font'], self.configuration['table_text_size'])
                )
                for line in registration_description_in_lines:
                    self.canvas.drawString(registration_description_position+self.configuration['table_margin'], current_height + self.configuration['lines_offset'], line)
                    current_height -= self.configuration['distance_between_lines']*0.7
                current_height += self.configuration['distance_between_lines']*0.7
                self.canvas.restoreState()
            return current_height

        for exhibitor in self.data['exhibitors']:
    
            current_height = print_base_document(self.data['exhibitors'][exhibitor]['prizes'])
            current_height = print_table_row(current_height, title=True)
    
            for index, scoring_template in enumerate(self.data['exhibitors'][exhibitor]['prizes']):
    
                description_in_lines = split_text_in_lines(
                    scoring_template['group_description'],
                    registration_description_width - 2 * self.configuration['table_margin'],
                    self.configuration['font'],
                    self.configuration['table_text_size']
                )

                if current_height - (1+(len(description_in_lines)-1)*0.7) * self.configuration['distance_between_lines'] < end_height:
                    draw_footer(self, print_line_page=False)
                    self.canvas.showPage()
    
                    current_height = print_base_document(self.data['exhibitors'][exhibitor]['prizes'][index:])

                    current_height = print_table_row(current_height, title=True)
                    current_height -= self.configuration['distance_between_lines']
    
                else:
                    current_height -= self.configuration['distance_between_lines']
    
                current_height = print_table_row(current_height, row_data=scoring_template)

            draw_footer(self, print_line_page=False)
            self.canvas.showPage()