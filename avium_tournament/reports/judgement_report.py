# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import blank_undefined, none_empty, status_extraction
from avium_tournament.reports.table_reportlab import drawpdf
from avium_tournament.utils import get_tournament_value
from collections import OrderedDict
from frappe import _


class JudgementReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        CONF_TABLE = {
            'report_name': _('Judgement Report'),
            'levels': 2,
            'tables_per_page': 3,
            'first_level_new_page': True,
            'description_text': (
                {
                    'text_size': 9,
                    'font': 'Helvetica-Bold',
                    'only_in_first_page': False,
                    'text': _("Judge signature") + ":                                                           " + _("Technical director signature") + ":                                                           ",
                    'alignment': "Center"
                },{
                    'text': "",
                },{
                    'text': "",
                },{
                    'only_in_first_page': True,
                    'text': "__________________________________________",
                },{
                    'text_size': 7.5,
                    'font': 'Helvetica-Bold',
                    'only_in_first_page': True,
                    'text': _("NJ") + ": " + _("Not Judjable") + "; " + _("De") + ": " +_("Declassified") + "; " + _("DeP") + ": " +_("Declassified Puntuated") + "; " + _("Di") + ": " + _("Disqualified") + "; " + _("DiP") + ": " + _("Disqualified Puntuated") + "; " + _("NS") + ": " +_("Did Not Sing") + "; " + _("ISe") + ": " +_("Insuficient Sing") + "; " + _("NP") + ": " + _("Not Presented"),
                    'alignment': "Center"
                },{
                    'text_size': 7.5,
                    'font': 'Helvetica-Bold',
                    'only_in_first_page': True,
                    'text': _("Scoring template status") + " - " + _("Blue") + ": " + _("Pending") + "; " +_("Orange") + ": " + _("Tied") + "; " +_("Red") + ": " + _("Error"),
                    'alignment': "Center"
                },
            ),
            'columns_description': (
                {
                    'key': 'cage',
                    'column_title': _('Cage'),
                    'column_width': 0.21,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'harmony',
                    'column_title': _('Harmony'),
                    'column_width': 0.17,
                    'alignment': 'Right',
                }, {
                    'key': 'tie_break_mark',
                    'column_title': _('TBM'),
                    'column_width': 0.17,
                    'alignment': 'Right',
                }, {
                    'key': 'total',
                    'column_title': _('Total'),
                    'column_width': 0.17,
                    'alignment': 'Right',
                }, {
                    'key': 'prize',
                    'column_title': _('Prize'),
                    'column_width': 0.28,
                    'truncate': True,
                },
            ),
        }

        super(JudgementReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return (
            self.arguments.get('judge', 0),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  ireg.name as registration,
                    exbr.breeder_code as bc,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    ireg.registration_description as description,
                    jud1.judge_name as judge_name_1,
                    jud2.judge_name as judge_name_2,
                    scote.cage_number as cage,
                    scote.status as template_status,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_3 as prize,
                    1 as team_size,
                    scote.classification as classification
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                CASE WHEN %(judge)s != 'Undefined' THEN scote.judge_1 = %(judge)s ELSE TRUE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  treg.name as registration,
                    exbr.breeder_code as bc,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    treg.registration_description as description,
                    jud1.judge_name as judge_name_1,
                    jud2.judge_name as judge_name_2,
                    scote.cage_number as cage,
                    scote.status as template_status,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_3 as prize,
                    tgro.team_size as team_size,
                    scote.classification as classification
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                CASE WHEN %(judge)s != 'Undefined' THEN scote.judge_1 = %(judge)s ELSE TRUE END AND
                exbr.idx = 1
            ORDER BY
                judge_name_1 ASC,
                group_code ASC,
                classification ASC,
                cage ASC
            """,
            {
                'judge': self.get_filter()[0],
            },
            as_dict = True
        )

        tournament_data = get_tournament_value(['add_tie_break_marks',])

        group_judge_cache = dict()
        for item in raw_data:
            if item['group_code'] not in group_judge_cache:
                group_judge_cache[item['group_code']] = list()
            judge_name_1 = blank_undefined(item['judge_name_1'])
            if judge_name_1 != "Undefined" and judge_name_1 not in group_judge_cache[item['group_code']]:
                group_judge_cache[item['group_code']].append(judge_name_1)
            judge_name_2 = blank_undefined(item['judge_name_2'])
            if judge_name_2 != "Undefined" and judge_name_2 not in group_judge_cache[item['group_code']]:
                group_judge_cache[item['group_code']].append(judge_name_2)

        data = OrderedDict()

        for item in raw_data:

            first_level_key = _('Judge') + ": " + " / ".join(group_judge_cache.get(item['group_code'], ""))
            second_level_key = item['group_code'] + ' - ' + item['group_description']

            status = status_extraction(
                (item['status_a'], item['status_b'], item['status_c'], item['status_d'],),
                (item['location_status_a'], item['location_status_b'], item['location_status_c'], item['location_status_d'],),
                abbreviation=True
            )

            if first_level_key not in data:
                data[first_level_key] = OrderedDict()
            if second_level_key not in data[first_level_key]:
                data[first_level_key][second_level_key] = OrderedDict()
            data[first_level_key][second_level_key][item['registration']] = dict()
            data[first_level_key][second_level_key][item['registration']]['cage'] = none_empty(item['cage']).lstrip("0")
            if item['team_size'] > 1:
                data[first_level_key][second_level_key][item['registration']]['harmony'] = item['harmony']
            data[first_level_key][second_level_key][item['registration']]['tie_break_mark'] = item['tie_break_mark']
            if blank_undefined(item['prize']) != "Undefined":
                data[first_level_key][second_level_key][item['registration']]['prize'] = blank_undefined(item['prize'])
            elif status != "Judjable":
                data[first_level_key][second_level_key][item['registration']]['prize'] = _(status)
            if tournament_data['add_tie_break_marks'] == "Yes":
                data[first_level_key][second_level_key][item['registration']]['total'] = item['total_mark'] + item['tie_break_mark']
            else:
                data[first_level_key][second_level_key][item['registration']]['total'] = item['total_mark']

            if item['template_status'] == "Pending":
                data[first_level_key][second_level_key][item['registration']]['background'] = "#2E9AFE"
            elif item['template_status'] == "Tied":
                data[first_level_key][second_level_key][item['registration']]['background'] = "#FACC2E"
            elif item['template_status'] == "Error":
                data[first_level_key][second_level_key][item['registration']]['background'] = "#FE2E2E"

        self.data = data

    def gen_pdf(self):
    
        return drawpdf(self)