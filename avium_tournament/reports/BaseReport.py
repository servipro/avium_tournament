# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe
import tempfile

from avium_tournament.reports import get_report_type
from avium_tournament.reports.commonFunctions import none_empty
from reportlab.lib.colors import HexColor
from reportlab.pdfgen import canvas

class BaseReport(object):

    def __init__(self, configuration, arguments, canvas=None):
        self.arguments = arguments
        self.load_configuration(configuration)

        if canvas is None:
            self.init_canvas()
        else:
            self.tempfile = None
            self.canvas = canvas

    def __enter__(self):
        self.fetch_data()
        self.gen_pdf()
        return self

    def load_configuration(self, configuration):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Report Configuration", "Report Configuration")

        # Get header data
        raw_header_data = frappe.db.sql(""" 
            SELECT tour.tournament_name as tournament_name,
                asso.association_name as association_name, 
                tour.city as city,
                tour.comunity as region,
                asso.logo as association_logo,
                fede.logo as federation_logo,
                jufe.logo as judge_association_logo
            FROM `tabTournament` as tour
            LEFT JOIN `tabAssociation` as asso on asso.name = tour.association
            LEFT JOIN `tabFederation` as fede on fede.name = tour.federation
            LEFT JOIN `tabJudge Association` as jufe on jufe.name = tour.judge_association
            """, as_dict=True)[0]

        # Common configuration values
        report_type = get_report_type()
        if report_type == "Extended":
            distance_between_lines = 17
            title_size = 11
            text_size = 11
        else:
            distance_between_lines = 13
            title_size = 9
            text_size = 9

        if raw_header_data['region'] is None or raw_header_data['region'] == "":
            city = none_empty(raw_header_data['city'])
        else:
            city = none_empty(raw_header_data['city']) + " (" + raw_header_data['region'] + ")"

        # Common configuration dict
        COMMON_CONF = {
            'report_configuration': raw_report_configuration_data,
            'tournament_name': raw_header_data['tournament_name'],
            'association_name': raw_header_data['association_name'],
            'tournament_city': city,
            'association_logo': raw_header_data['association_logo'],
            'federation_logo': raw_header_data['federation_logo'],
            'judge_association_logo': raw_header_data['judge_association_logo'],
            'text_size': text_size,
            'title_size': title_size,
            'font': 'Helvetica',
            'bold_font': 'Helvetica-Bold',
            'header_titles_gap': 20,
            'extra_horizontal_text_offset': 5,
            'gap_between_columns': 5,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * text_size) / 2 + 0.5,
            'title_text_color': HexColor("0x" + raw_report_configuration_data.title_text[1:]),
            'title_background': HexColor("0x" + raw_report_configuration_data.title_background[1:]),
            'first_level_text_color': HexColor("0x" + raw_report_configuration_data.first_level_text[1:]),
            'first_level_background': HexColor("0x" + raw_report_configuration_data.first_level_background[1:]),
            'first_level_horizontal_border': 0.5,
            'first_level_in_lines': True,
            'second_level_text_color': HexColor("0x" + raw_report_configuration_data.second_level_text[1:]),
            'second_level_background': HexColor("0x" + raw_report_configuration_data.second_level_background[1:]),
            'second_level_horizontal_border': 0.2,
            'second_level_in_lines': True,
            'content_intermittent_background': HexColor("0x" + raw_report_configuration_data.content_background[1:]),
        }

        # Merge common and specific configuration values

        self.configuration = {k: configuration.get(k, COMMON_CONF.get(k))
                              for k
                              in list(configuration.keys()) + list(COMMON_CONF.keys())
                              }

    def init_canvas(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        self.canvas = canvas.Canvas(self.tempfile)

    def get_order(self, *args, **kwargs):
        raise NotImplementedError

    def get_filter(self, *args, **kwargs):
        raise NotImplementedError

    def fetch_data(self):
        raise NotImplementedError

    def gen_pdf(self):
        raise NotImplementedError

    def get_pdf(self):
        return self.canvas.getpdfdata()

    def __exit__(self, exc_type, exc_value, traceback):
        if self.tempfile is not None:
            self.tempfile.close()