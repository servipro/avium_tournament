# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.utils import get_tournament_value
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import blank_undefined
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

class ExhibitorClassificationBestPunctuationsReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        CONF_TABLE = {
            'report_name': _("Exhibitor Classification Best Punctuations Report"),
            'levels': 0,
            'columns_description': (
                {
                    'key': 'position',
                    'column_title': _('Position'),
                    'title_alignment': 'Left',
                    'column_width': 0.065,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'exhibitor_name',
                    'column_title': _('Exhibitor'),
                    'title_alignment': 'Left',
                    'column_width': 0.355,
                    'alignment': 'Left',
                    'in_lines': True,
                }, {
                    'key': 'punctuations',
                    'column_title': _('Punctuations'),
                    'title_alignment': 'Left',
                    'column_width': 0.48,
                    'alignment': 'Left',
                    'in_lines': True,
                }, {
                    'key': 'total',
                    'column_title': _('Total'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 2,
                },
            ),
        }

        super(ExhibitorClassificationBestPunctuationsReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):

        tournament_association = get_tournament_value(["association",])
        return (
            blank_undefined(self.arguments.get('start_group', None)),
            blank_undefined(self.arguments.get('end_group', None)),
            self.arguments['associated'],
            tournament_association['association'],
            self.arguments['individual'],
            self.arguments['male'],
        )

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    ireg.sex_a as sex_a,
                    NULL as sex_b,
                    NULL as sex_c,
                    NULL as sex_d,
                    IF(scote.total_a IS NOT NULL, scote.total_a, 0) as total_a,
                    NULL as total_b,
                    NULL as total_c,
                    NULL as total_d,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    1 as team_size
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            WHERE
                CASE WHEN %(start_group)s != 'Undefined' THEN tgro.individual_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(end_group)s != 'Undefined' THEN tgro.individual_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(exhibitor)s = 'Associated' THEN reg.association = %(association)s ELSE TRUE END AND
                CASE WHEN %(exhibitor)s = 'Non Associated' THEN reg.association != %(association)s ELSE TRUE END AND
                CASE WHEN %(individual)s IN ( 'All', 'Individual Specimens' ) THEN TRUE ELSE FALSE END
            UNION ALL
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    treg.sex_a as sex_a,
                    treg.sex_b as sex_b,
                    treg.sex_c as sex_c,
                    treg.sex_d as sex_d,
                    IF(scote.total_a IS NOT NULL, scote.total_a, 0) as total_a,
                    IF(scote.total_b IS NOT NULL, scote.total_b, 0) as total_b,
                    IF(scote.total_c IS NOT NULL, scote.total_c, 0) as total_c,
                    IF(scote.total_d IS NOT NULL, scote.total_d, 0) as total_d,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    tgro.team_size as team_size
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            WHERE
                CASE WHEN %(start_group)s != 'Undefined' THEN tgro.team_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(end_group)s != 'Undefined' THEN tgro.team_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(exhibitor)s = 'Associated' THEN reg.association = %(association)s ELSE TRUE END AND
                CASE WHEN %(exhibitor)s = 'Non Associated' THEN reg.association != %(association)s ELSE TRUE END AND
                CASE WHEN %(individual)s IN ( 'All', 'Team Specimens', 'Team Group' ) THEN TRUE ELSE FALSE END
            ORDER BY exhibitor_surname ASC, exhibitor_name ASC
            """,
            {
                'start_group': self.get_filter()[0],
                'end_group': self.get_filter()[1],
                'exhibitor': self.get_filter()[2],
                'association': self.get_filter()[3],
                'individual': self.get_filter()[4],
            },
            as_dict = True
        )

        grouped_data = dict()
        for item in raw_data:
            if item['registration'] not in grouped_data:
                grouped_data[item['registration']] = dict()
                grouped_data[item['registration']]['exhibitor_name'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                grouped_data[item['registration']]['punctuations'] = list()

            if self.get_filter()[4] == "Team Group":
                if get_tournament_value(["add_tie_break_marks",])['add_tie_break_marks'] == "Yes":
                    grouped_data[item['registration']]['punctuations'].append(item['total_mark'] + item['tie_break_mark'])
                else:
                    grouped_data[item['registration']]['punctuations'].append(item['total_mark'])
            else:
                if item['sex_a'] == self.get_filter()[5] or self.get_filter()[5] == "All":
                    if get_tournament_value(["add_tie_break_marks",])['add_tie_break_marks'] == "Yes":
                        grouped_data[item['registration']]['punctuations'].append(item['total_a'] + item['tie_break_mark'])
                    else:
                        grouped_data[item['registration']]['punctuations'].append(item['total_a'])
                if item['team_size'] > 1 and (item['sex_b'] == self.get_filter()[5] or self.get_filter()[5] == "All"):
                    if get_tournament_value(["add_tie_break_marks",])['add_tie_break_marks'] == "Yes":
                        grouped_data[item['registration']]['punctuations'].append(item['total_b'] + item['tie_break_mark'])
                    else:
                        grouped_data[item['registration']]['punctuations'].append(item['total_b'])
                if item['team_size'] > 2 and (item['sex_c'] == self.get_filter()[5] or self.get_filter()[5] == "All"):
                    if get_tournament_value(["add_tie_break_marks",])['add_tie_break_marks'] == "Yes":
                        grouped_data[item['registration']]['punctuations'].append(item['total_c'] + item['tie_break_mark'])
                    else:
                        grouped_data[item['registration']]['punctuations'].append(item['total_c'])
                if item['team_size'] > 3 and (item['sex_d'] == self.get_filter()[5] or self.get_filter()[5] == "All"):
                    if get_tournament_value(["add_tie_break_marks",])['add_tie_break_marks'] == "Yes":
                        grouped_data[item['registration']]['punctuations'].append(item['total_d'] + item['tie_break_mark'])
                    else:
                        grouped_data[item['registration']]['punctuations'].append(item['total_d'])

            grouped_data[item['registration']]['punctuations'].sort(reverse=True)

        data = OrderedDict()

        sorted_grouped_data_keys = sorted(
            grouped_data.keys(),
            key=lambda key: sum(grouped_data[key]['punctuations']),
            reverse=True
        )

        sorted_grouped_data_keys = sorted(
            sorted_grouped_data_keys,
            key=lambda key: sum(grouped_data[key]['punctuations'][0:min(len(grouped_data[key]['punctuations']), self.arguments['punctuations'])]),
            reverse=True
        )

        for position, key in enumerate(sorted_grouped_data_keys):
            name = grouped_data[key]['exhibitor_name']
            data[name] = dict()
            data[name]['position'] = position + 1
            data[name]['exhibitor_name'] = grouped_data[key]['exhibitor_name']
            punctuations = grouped_data[key]['punctuations'][0:min(len(grouped_data[key]['punctuations']), self.arguments['punctuations'])]
            data[name]['punctuations'] = " / ".join(map(str, punctuations))
            data[name]['total'] = sum(punctuations)

        self.data = {
            "_": {
                "_": data
            }
        }

    def gen_pdf(self):
    
        return drawpdf(self)