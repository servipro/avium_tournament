# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import importFonts, split_text_in_lines, blank_custom, blank_undefined, load_image, text_truncate, draw_header, draw_footer, status_extraction, none_empty
from avium_tournament.utils import get_tournament_value
from collections import OrderedDict
from frappe import _
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase.pdfmetrics import stringWidth

class SimplifiedScoringTemplatesReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Report Configuration", "Report Configuration")

        if raw_report_configuration_data.judge_logo == "Right":
            raw_report_configuration_data.right_logo = "Judge Association Logo"
        elif raw_report_configuration_data.judge_logo == "Left":
            raw_report_configuration_data.left_logo = "Judge Association Logo"

        distance_between_lines = 11.5
        text_size = 8

        CONF_TABLE = {
            'report_configuration': raw_report_configuration_data,
            'report_name': _("Scoring Templates"),
            'header_titles_gap': 0,
            'extra_vertical_text_offset': 5,
            'line_width': 0.03,
            'text_size': text_size,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * text_size) / 2 + 0.5,
            'status_text_color': colors.red,
            'prize_text_color': colors.red,
            'black_transparent': colors.Color(0, 0, 0, alpha=0.3),
            'punctuation_column_width': 25,
        }

        super(SimplifiedScoringTemplatesReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        start_exhibitor_value = self.arguments.get('start_exhibitor', None)
        if start_exhibitor_value is not None:
            start_registration_doctype = frappe.get_all(
                "Registration",
                filters = {"name": start_exhibitor_value},
                fields = {'exhibitor_name', 'exhibitor_surname'}
            )
            exhibitor_name = start_registration_doctype[0]['exhibitor_name']
            exhibitor_surname = start_registration_doctype[0]['exhibitor_surname']
            start_exhibitor_value = exhibitor_surname + ", " + exhibitor_name

        end_exhibitor_value = self.arguments.get('end_exhibitor', None)
        if end_exhibitor_value is not None:
            end_registration_doctype = frappe.get_all(
                "Registration",
                filters = {"name": end_exhibitor_value},
                fields = {'exhibitor_name', 'exhibitor_surname'}
            )
            exhibitor_name = end_registration_doctype[0]['exhibitor_name']
            exhibitor_surname = end_registration_doctype[0]['exhibitor_surname']
            end_exhibitor_value = exhibitor_surname + ", " + exhibitor_name

        return (
            blank_undefined(self.arguments.get('start_group', None)),
            blank_undefined(self.arguments.get('end_group', None)),
            blank_undefined(start_exhibitor_value),
            blank_undefined(end_exhibitor_value),
            blank_undefined(self.arguments.get('start_judge', None)),
            blank_undefined(self.arguments.get('end_judge', None)),
            blank_undefined(self.arguments.get('start_registration', None)),
            blank_undefined(self.arguments.get('end_registration', None)),
            blank_undefined(self.arguments.get('scoring_template', None)),
            self.arguments.get('email', 0),
            self.arguments.get('only_judged', 0),
            self.arguments.get('without_judge', 0),
            self.arguments.get('without_cage', 0)
        )

    def get_order(self):
        return self.arguments.get('sorted', "Group")

    def fetch_data(self):
        scoring_templates_raw_data = frappe.db.sql("""
            SELECT  ireg.name as cage_registration,
                    reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.email as email,
                    parter.territory_name as comunity,
                    parter.flag as flag,
                    exbr.breeder_code as bc,
                    scote.judge_1 as judge_1,
                    scote.judge_2 as judge_2,
                    fami.code as family_code,
                    fami.family_name as family_name,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    ireg.registration_description as registration_description,
                    1 as team_size,
                    scote.cage_number as cage_number,
                    ireg.ring_a as ring_a,
                    NULL as ring_b,
                    NULL as ring_c,
                    NULL as ring_d,
                    conc.idx as concept_index,
                    conc.concept_name as concept_name,
                    conc.negative as negative,
                    conc.value_a as value_a,
                    conc.value_b as value_b,
                    conc.value_c as value_c,
                    conc.value_d as value_d,
                    conc.max_value as max_value,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    scote.sum_mark as sum_mark,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    touprides.description_4 as prize,
                    jud1.judge_name as judge_1_name,
                    jud2.judge_name as judge_2_name,
                    judtem.specimen_image as judging_template_specimen_image,
                    judtemspe.specimen_image as judging_template_specialisation_specimen_image,
                    scote.observations as observations,
                    scote.judgment_date as judgment_date,
                    scote.red_factor as red_factor,
                    scote.size as size,
                    scote.status as judgment_status
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabScoring Template Concept` as conc on conc.parent = scote.name
            LEFT JOIN `tabJudging Template Specialisation` as judtemspe on judtemspe.specialisation_name = scote.group_description
            LEFT JOIN `tabJudging Template` as judtem on judtem.name = judtemspe.parent
            WHERE
                CASE WHEN %(order)s = 'Group' AND %(start_group)s != 'Undefined' THEN tgro.individual_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Group' AND %(end_group)s != 'Undefined' THEN tgro.individual_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(start_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) >= %(start_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(end_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) <= %(end_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(start_judge)s != 'Undefined' THEN scote.judge_1 >= %(start_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(end_judge)s != 'Undefined' THEN scote.judge_1 <= %(end_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(start_registration)s != 'Undefined' THEN reg.name >= %(start_registration)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(end_registration)s != 'Undefined' THEN reg.name <= %(end_registration)s ELSE TRUE END AND
                CASE WHEN %(scoring_template)s != 'Undefined' THEN scote.name = %(scoring_template)s ELSE TRUE END AND
                CASE WHEN %(only_judged)s = '1' THEN scote.status = "Judged" ELSE TRUE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  treg.name as cage_registration,
                    reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.email as email,
                    parter.territory_name as comunity,
                    parter.flag as flag,
                    exbr.breeder_code as bc,
                    scote.judge_1 as judge_1,
                    scote.judge_2 as judge_2,
                    fami.code as family_code,
                    fami.family_name as family_name,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    treg.registration_description as registration_description,
                    tgro.team_size as team_size,
                    scote.cage_number as cage_number,
                    treg.ring_a as ring_a,
                    treg.ring_b as ring_b,
                    treg.ring_c as ring_c,
                    treg.ring_d as ring_d,
                    conc.idx as concept_index,
                    conc.concept_name as concept_name,
                    conc.negative as negative,
                    conc.value_a as value_a,
                    conc.value_b as value_b,
                    conc.value_c as value_c,
                    conc.value_d as value_d,
                    conc.max_value as max_value,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    scote.sum_mark as sum_mark,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    touprides.description_4 as prize,
                    jud1.judge_name as judge_1_name,
                    jud2.judge_name as judge_2_name,
                    judtem.specimen_image as judging_template_specimen_image,
                    judtemspe.specimen_image as judging_template_specialisation_specimen_image,
                    scote.observations as observations,
                    scote.judgment_date as judgment_date,
                    scote.red_factor as red_factor,
                    scote.size as size,
                    scote.status as judgment_status
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabScoring Template Concept` as conc on conc.parent = scote.name
            LEFT JOIN `tabJudging Template Specialisation` as judtemspe on judtemspe.specialisation_name = scote.group_description
            LEFT JOIN `tabJudging Template` as judtem on judtem.name = judtemspe.parent
            WHERE
                CASE WHEN %(order)s = 'Group' AND %(start_group)s != 'Undefined' THEN tgro.team_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Group' AND %(end_group)s != 'Undefined' THEN tgro.team_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(start_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) >= %(start_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(end_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) <= %(end_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(start_judge)s != 'Undefined' THEN scote.judge_1 >= %(start_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(end_judge)s != 'Undefined' THEN scote.judge_1 <= %(end_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(start_registration)s != 'Undefined' THEN reg.name >= %(start_registration)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(end_registration)s != 'Undefined' THEN reg.name <= %(end_registration)s ELSE TRUE END AND
                CASE WHEN %(scoring_template)s != 'Undefined' THEN scote.name = %(scoring_template)s ELSE TRUE END AND
                CASE WHEN %(only_judged)s = '1' THEN scote.status = "Judged" ELSE TRUE END AND
                exbr.idx = 1
            ORDER BY
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(order)s = 'Judge' THEN judge_1_name END ASC,
                CASE WHEN %(order)s = 'Registration' THEN registration END ASC,
                group_code ASC, cage_number ASC, concept_index ASC
            """,
            {
                'start_group': self.get_filter()[0],
                'end_group': self.get_filter()[1],
                'start_exhibitor': self.get_filter()[2],
                'end_exhibitor': self.get_filter()[3],
                'start_judge': self.get_filter()[4],
                'end_judge': self.get_filter()[5],
                'start_registration': self.get_filter()[6],
                'end_registration': self.get_filter()[7],
                'scoring_template': self.get_filter()[8],
                'email': self.get_filter()[9],
                'only_judged': self.get_filter()[10],
                'order': self.get_order(),
            },
            as_dict = True
        )

        tournament_data = get_tournament_value(['add_tie_break_marks', ])

        self.data = OrderedDict()

        for item in scoring_templates_raw_data:

            if blank_undefined(item['email']) == "Undefined" or not self.get_filter()[9]:
            
                if self.get_order() == "Group":
                    ordering_field = item['group_code']
                elif self.get_order() == "Exhibitor":
                    ordering_field = item['registration']
                elif self.get_order() == "Judge":
                    ordering_field = item.get('judge_1_name', "")
                else:
                    ordering_field = item['registration']

                if ordering_field not in self.data:
                    self.data[ordering_field] = OrderedDict()

                if item['group_code'] not in self.data[ordering_field]:
                    self.data[ordering_field][item['group_code']] = OrderedDict()

                if item['cage_registration'] not in self.data[ordering_field][item['group_code']]:
                    self.data[ordering_field][item['group_code']][item['cage_registration']] = dict()
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['registration'] = item['registration'].lstrip("REG0")
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['exhibitor'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['comunity'] = item['comunity']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['flag'] = item['flag']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['bc'] = item['bc']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['judge_1'] = none_empty(item['judge_1_name'])
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['judge_2'] = none_empty(item['judge_2_name'])
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['family'] = item['family_code'] + " - " + item['family_name']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['group_code'] = item['group_code']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['group_description'] = item['group_description']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['registration_description'] = item['registration_description']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['cage_number'] = none_empty(item['cage_number']).lstrip("0")
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['ring_a'] = item['ring_a']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['ring_b'] = item['ring_b']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['ring_c'] = item['ring_c']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['ring_d'] = item['ring_d']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['team_size'] = item['team_size']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['positive_scores'] = list()
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['negative_scores'] = list()
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['total_a'] = item['total_a']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['total_b'] = item['total_b']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['total_c'] = item['total_c']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['total_d'] = item['total_d']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['sum_mark'] = item['sum_mark']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['harmony'] = item['harmony']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['tie_break_mark'] = item['tie_break_mark']
                    if tournament_data['add_tie_break_marks'] == "Yes":
                        self.data[ordering_field][item['group_code']][item['cage_registration']]['total_mark'] = item['total_mark'] + item['tie_break_mark']
                    else:
                        self.data[ordering_field][item['group_code']][item['cage_registration']]['total_mark'] = item['total_mark']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['status'] = (item['status_a'], item['status_b'], item['status_c'], item['status_d'],)
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['location_status'] = (item['location_status_a'], item['location_status_b'], item['location_status_c'], item['location_status_d'],)
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['prize'] = item['prize']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['observations'] = item['observations']

                    if blank_undefined(item['judgment_date']) != "Undefined":
                        self.data[ordering_field][item['group_code']][item['cage_registration']]['judgment_date'] = item["judgment_date"].strftime('%d/%m/%Y')
                    else:
                        self.data[ordering_field][item['group_code']][item['cage_registration']]['judgment_date'] = ""
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['red_factor'] = item.get('red_factor', 0)
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['size'] = item['size']
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['judgment_status'] = item['judgment_status']
                    if blank_undefined(item['judging_template_specialisation_specimen_image']) != "Undefined":
                        self.data[ordering_field][item['group_code']][item['cage_registration']]['specimen_image'] = item['judging_template_specialisation_specimen_image']
                    else:
                        if blank_undefined(item['judging_template_specimen_image']) != "Undefined":
                            self.data[ordering_field][item['group_code']][item['cage_registration']]['specimen_image'] = item['judging_template_specimen_image']
                        else:
                            self.data[ordering_field][item['group_code']][item['cage_registration']]['specimen_image'] = None

                if item['negative'] == 0:
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['positive_scores'].append(
                        {
                            'concept_title': item['concept_name'],
                            'max_mark': item['max_value'],
                            'marks': (item['value_a'], item['value_b'], item['value_c'], item['value_d'],)
                        }
                    )
                else:
                    self.data[ordering_field][item['group_code']][item['cage_registration']]['negative_scores'].append(
                        {
                            'concept_title': item['concept_name'],
                            'max_mark': item['max_value'],
                            'marks': (item['value_a'], item['value_b'], item['value_c'], item['value_d'],)
                        }
                    )

    def gen_pdf(self):

        # **Set file name**
        self.canvas.setTitle(self.configuration['report_name'])

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']

        width, height = A4

        printable_width = width - configuration.left_margin * cm - configuration.right_margin * cm
        printable_height = height - configuration.upper_margin * cm - configuration.lower_margin * cm

        horizontal_gap_between_tables = printable_width * 0.03
        vertical_gap_between_tables = printable_height * 0.01
        table_horizontal_margin = 2
        table_vertical_margin = 2

        max_half_table_height = (printable_height - vertical_gap_between_tables) / 2
        table_width = (printable_width - horizontal_gap_between_tables) / 2

        header_column_height = self.configuration['distance_between_lines'] * 4
        header_column_width = table_width
        max_header_logo_height = header_column_height - 4 * table_vertical_margin
        max_header_logo_width = max_header_logo_height

        if configuration.left_logo == "Federation Logo":
            logo_1_directory, logo_1 = load_image(self.configuration["federation_logo"])
        elif configuration.left_logo == "Association Logo":
            logo_1_directory, logo_1 = load_image(self.configuration["association_logo"])
        elif configuration.left_logo == "Judge Association Logo":
            logo_1_directory, logo_1 = load_image(self.configuration["judge_association_logo"])
        elif configuration.left_logo == "Logo 1":
            logo_1_directory, logo_1 = load_image(configuration.logo_1)
        elif configuration.left_logo == "Logo 2":
            logo_1_directory, logo_1 = load_image(configuration.logo_2)
        else:
            logo_1_directory, logo_1 = None, None

        if configuration.right_logo == "Federation Logo":
            logo_2_directory, logo_2 = load_image(self.configuration["federation_logo"])
        elif configuration.right_logo == "Association Logo":
            logo_2_directory, logo_2 = load_image(self.configuration["association_logo"])
        elif configuration.right_logo == "Judge Association Logo":
            logo_2_directory, logo_2 = load_image(self.configuration["judge_association_logo"])
        elif configuration.right_logo == "Logo 1":
            logo_2_directory, logo_2 = load_image(configuration.logo_1)
        elif configuration.right_logo == "Logo 2":
            logo_2_directory, logo_2 = load_image(configuration.logo_2)
        else:
            logo_2_directory, logo_2 = None, None

        tournament_name_in_lines = split_text_in_lines(
            self.configuration["tournament_name"],
            table_width - 2 * max_header_logo_width - 8 * table_horizontal_margin,
            self.configuration['font'],
            self.configuration['text_size']
        )

        cage_title_text = _("Cage Nº")
        cage_column_height = self.configuration['distance_between_lines'] * 5
        cage_column_width = table_width * 0.4
        cage_title_horizontal_position = cage_column_width / 2.0
        cage_number_horizontal_position = cage_column_width / 2.0
        cage_letter_horizontal_position = cage_column_width / 2.0

        group_title_text = _("Group")
        group_column_height = cage_column_height
        group_column_width = table_width - cage_column_width
        group_title_horizontal_position = cage_column_width + table_horizontal_margin
        group_text_horizontal_position = cage_column_width + group_column_width / 2.0

        family_column_height = self.configuration['distance_between_lines'] * 1
        family_column_width = table_width
        family_text_horizontal_position = table_width / 2.0

        judging_specifications_column_height = self.configuration['distance_between_lines'] * 2
        judging_specifications_column_width = table_width * 0.4
        judging_specifications_text_horizontal_position = table_horizontal_margin

        registration_description_column_height = judging_specifications_column_height
        registration_description_column_width = table_width - judging_specifications_column_width
        registration_description_text_horizontal_position = judging_specifications_column_width + registration_description_column_width / 2.0

        A_title_text = "A"
        B_title_text = "B"
        C_title_text = "C"
        D_title_text = "D"
        cage_letters_text_list = (A_title_text, B_title_text, C_title_text, D_title_text)
        positive_concepts_title_text = _("Positive Concepts")
        negative_concepts_title_text = _("Negative Concepts")
        total_title_text = _("Total")

        separation_between_max_and_punctuation = 0
        concept_column_height = self.configuration['distance_between_lines'] * 1
        concept_column_width = table_width - 5.0 * self.configuration["punctuation_column_width"] - separation_between_max_and_punctuation

        concept_text_horizontal_position = table_horizontal_margin

        maximum_column_horizontal_position = concept_column_width
        maximum_text_horizontal_position = maximum_column_horizontal_position + 1 * self.configuration["punctuation_column_width"] - table_horizontal_margin

        punctuation_A_column_horizontal_position = maximum_column_horizontal_position + self.configuration["punctuation_column_width"] + separation_between_max_and_punctuation
        punctuation_A_title_horizontal_position = punctuation_A_column_horizontal_position + 0.5 * self.configuration["punctuation_column_width"]
        punctuation_A_text_horizontal_position = punctuation_A_column_horizontal_position + self.configuration["punctuation_column_width"] - table_horizontal_margin

        punctuation_B_column_horizontal_position = punctuation_A_column_horizontal_position + self.configuration["punctuation_column_width"]
        punctuation_B_title_horizontal_position = punctuation_B_column_horizontal_position + 0.5 * self.configuration["punctuation_column_width"]
        punctuation_B_text_horizontal_position = punctuation_B_column_horizontal_position + self.configuration["punctuation_column_width"] - table_horizontal_margin

        punctuation_C_column_horizontal_position = punctuation_B_column_horizontal_position + self.configuration["punctuation_column_width"]
        punctuation_C_title_horizontal_position = punctuation_C_column_horizontal_position + 0.5 * self.configuration["punctuation_column_width"]
        punctuation_C_text_horizontal_position = punctuation_C_column_horizontal_position + self.configuration["punctuation_column_width"] - table_horizontal_margin

        punctuation_D_column_horizontal_position = punctuation_C_column_horizontal_position + self.configuration["punctuation_column_width"]
        punctuation_D_title_horizontal_position = punctuation_D_column_horizontal_position + 0.5 * self.configuration["punctuation_column_width"]
        punctuation_D_text_horizontal_position = punctuation_D_column_horizontal_position + self.configuration["punctuation_column_width"] - table_horizontal_margin

        observations_title_text = _("OBSERVATIONS").title()
        observations_title_width = stringWidth(observations_title_text + ": ", self.configuration['font'], self.configuration["text_size"])
        observations_column_height = self.configuration['distance_between_lines'] * 3
        observations_column_width = table_width - 4.0 * self.configuration["punctuation_column_width"]
        observations_text_position = table_horizontal_margin

        judge_title_text = _("Judge")
        judge_title_width = stringWidth(judge_title_text + ": ", self.configuration['font'], self.configuration["text_size"])
        judgement_date_title = _("Judgement Date")
        judgement_date_title_width = stringWidth(judgement_date_title + ": ", self.configuration['font'], self.configuration["text_size"])
        judge_column_height = self.configuration['distance_between_lines'] * 2
        judge_column_width = observations_column_width
        judge_title_position = table_horizontal_margin
        judge_text_position = judge_title_position + judge_title_width
        judgement_date_text_position = judge_title_position + judgement_date_title_width

        total_team_column_height = self.configuration['distance_between_lines']
        total_team_title_column_width = 4.0 * self.configuration["punctuation_column_width"]
        total_team_title_position = observations_column_width + table_horizontal_margin
        total_team_text_position = table_width - table_horizontal_margin
        total_team_title_text = text_truncate(
            _("Total Team"),
            total_team_title_column_width - 2 * table_horizontal_margin,
            self.configuration["font"],
            self.configuration["title_size"]
        )

        harmony_column_height = self.configuration['distance_between_lines']
        harmony_title_column_width = 4.0 * self.configuration["punctuation_column_width"]
        harmony_title_position = observations_column_width + table_horizontal_margin
        harmony_text_position = table_width - table_horizontal_margin
        harmony_title_text = text_truncate(
            _("Harmony"),
            harmony_title_column_width - 2 * table_horizontal_margin,
            self.configuration["font"],
            self.configuration["title_size"]
        )

        tie_break_mark_column_height = self.configuration['distance_between_lines']
        tie_break_mark_title_column_width = 4.0 * self.configuration["punctuation_column_width"]
        tie_break_mark_title_position = observations_column_width + table_horizontal_margin
        tie_break_mark_text_position = table_width - table_horizontal_margin
        tie_break_mark_title_text = text_truncate(
            _("Tie Break Mark"),
            tie_break_mark_title_column_width - 2 * table_horizontal_margin,
            self.configuration["font"],
            self.configuration["title_size"]
        )

        total_points_column_height = 2 * self.configuration['distance_between_lines']
        total_points_title_column_width = 4.0 * self.configuration["punctuation_column_width"]
        total_points_title_position = observations_column_width + table_horizontal_margin
        total_points_text_position = table_width - table_horizontal_margin
        total_points_title_text = text_truncate(
            _("Total"),
            total_points_title_column_width - 2 * table_horizontal_margin,
            self.configuration["font"],
            2 * self.configuration["title_size"]
        )

        exhibitor_name_factor = 1.4
        exhibitor_height = self.configuration['distance_between_lines'] * (3 + exhibitor_name_factor)
        breeder_code_title_text = _("BC")
        registration_title_text = _("Reg.")
        max_flag_height = exhibitor_height - 4 * table_vertical_margin
        max_flag_width = max_flag_height * 1.2
        max_prize_height = max_flag_height
        max_prize_width = max_flag_width * 0.7
        exhibitor_text_position = max_flag_width + 4 * table_horizontal_margin

        # **Custom functions**
        def punctuation_line_draw(current_height, current_column_position, team_size=0, title=False, concept_title=None, positive_title=False, negative_title=False, max_mark=None, marks=None):
            self.canvas.saveState()

            if concept_title is not None:
                concept_title_in_lines = split_text_in_lines(
                    concept_title,
                    concept_column_width - 2 * table_horizontal_margin,
                    self.configuration["font"],
                    self.configuration["text_size"]
                )
                lines = len(concept_title_in_lines)
            else:
                lines = 1

            self.canvas.rect(
                current_column_position,
                current_height - (lines - 1) * concept_column_height,
                table_width,
                lines * concept_column_height
            )

            if positive_title:
                self.canvas.drawString(
                    current_column_position + concept_text_horizontal_position,
                    current_height + self.configuration["lines_offset"],
                    positive_concepts_title_text
                )

            elif negative_title:
                self.canvas.drawString(
                    current_column_position + concept_text_horizontal_position,
                    current_height + self.configuration["lines_offset"],
                    negative_concepts_title_text
                )

            else:
                columns_positions = [
                    punctuation_A_column_horizontal_position,
                    punctuation_B_column_horizontal_position,
                    punctuation_C_column_horizontal_position,
                    punctuation_D_column_horizontal_position,
                ]
                if title:
                    self.canvas.saveState()
                    self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"])
                    column_titles = (
                        (punctuation_A_title_horizontal_position, A_title_text),
                        (punctuation_B_title_horizontal_position, B_title_text),
                        (punctuation_C_title_horizontal_position, C_title_text),
                        (punctuation_D_title_horizontal_position, D_title_text),
                    )

                    for text_position, text in column_titles:
                        self.canvas.drawCentredString(
                            current_column_position + text_position,
                            current_height + self.configuration["lines_offset"],
                            text
                        )
                    self.canvas.restoreState()

                elif concept_title is not None:
                    self.canvas.setFont(self.configuration["font"], self.configuration["text_size"])
                    columns_positions.insert(
                        0,
                        maximum_column_horizontal_position
                    )

                    for index, concept_title_line in enumerate(concept_title_in_lines):
                        if index > 0:
                            current_height -= concept_column_height

                        self.canvas.drawString(
                            current_column_position + concept_text_horizontal_position,
                            current_height + self.configuration["lines_offset"],
                            concept_title_line
                        )

                        if index == len(concept_title_in_lines)-1:
                            self.canvas.drawRightString(
                                current_column_position + maximum_text_horizontal_position,
                                current_height + self.configuration["lines_offset"],
                                str(none_empty(max_mark))
                            )

                    if not self.arguments.get("empty", False):
                        column_titles = (
                            (punctuation_A_text_horizontal_position, marks[0]),
                            (punctuation_B_text_horizontal_position, marks[1]),
                            (punctuation_C_text_horizontal_position, marks[2]),
                            (punctuation_D_text_horizontal_position, marks[3]),
                        )

                        column_titles = column_titles[:team_size]

                        self.canvas.saveState()
                        self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"])
                        for text_position, text in column_titles:
                            self.canvas.drawRightString(
                                current_column_position + text_position,
                                current_height + self.configuration["lines_offset"],
                                str(text)
                            )
                        self.canvas.restoreState()

                for column_position in columns_positions:
                    self.canvas.rect(
                        current_column_position + column_position,
                        current_height,
                        self.configuration["punctuation_column_width"],
                        lines * concept_column_height
                    )

                return current_height

            self.canvas.restoreState()

        # **Start printing**
        current_column = 0
        current_row = 0

        current_height = initial_height = height - configuration.upper_margin * cm

        for first_level in self.data.keys():
            for group in self.data[first_level].keys():
                for scoring_template in self.data[first_level][group].keys():
                    self.canvas.setLineWidth(self.configuration["line_width"])

                    self.canvas.setFont(self.configuration["font"], self.configuration["text_size"])

                    # **New position calculation**
                    horizontal_position = configuration.left_margin * cm + current_column * (table_width + horizontal_gap_between_tables)

                    scoring_template_data = self.data[first_level][group][scoring_template]

                    # **Print header**
                    current_height -= header_column_height
                    self.canvas.rect(
                        horizontal_position,
                        current_height,
                        header_column_width,
                        header_column_height
                    )

                    # **Print left logo**
                    if logo_1_directory is not None:
                        logo1_width, logo1_height = logo_1.size
                        width_ratio = max_header_logo_width * 1.0 / logo1_width
                        height_ratio = max_header_logo_height * 1.0 / logo1_height
                        ratio = min(width_ratio, height_ratio)
                        logo_height = logo1_height * ratio
                        logo_width = logo1_width * ratio
                        self.canvas.drawImage(
                            logo_1_directory,
                            horizontal_position + 2 * table_horizontal_margin,
                            current_height + header_column_height / 2.0 - logo_height / 2.0,
                            height=logo_height,
                            width=logo_width
                        )

                    # **Print right logo**
                    if logo_2_directory is not None:
                        logo2_width, logo2_height = logo_2.size
                        width_ratio = max_header_logo_width * 1.0 / logo2_width
                        height_ratio = max_header_logo_height * 1.0 / logo2_height
                        ratio = min(width_ratio, height_ratio)
                        logo_height = logo2_height * ratio
                        logo_width = logo2_width * ratio
                        self.canvas.drawImage(
                            logo_2_directory,
                            horizontal_position + header_column_width - 2 * table_horizontal_margin - logo_width,
                            current_height + header_column_height / 2.0 - logo_height / 2.0,
                            height=logo_height,
                            width=logo_width
                        )

                    # **Print header text**
                    first_line_height = header_column_height / 2.0 + (len(tournament_name_in_lines) * 0.5 - 1) * self.configuration['distance_between_lines']
                    for index, tournament_name_line in enumerate(tournament_name_in_lines[:min(4, len(tournament_name_in_lines))]):
                        self.canvas.drawCentredString(
                            horizontal_position + header_column_width / 2.0,
                            current_height + first_line_height - index * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                            tournament_name_line
                        )

                    # **Print cage column**
                    current_height -= cage_column_height
                    self.canvas.rect(
                        horizontal_position,
                        current_height,
                        cage_column_width,
                        cage_column_height
                    )

                    self.canvas.drawCentredString(
                        horizontal_position + cage_title_horizontal_position,
                        current_height + cage_column_height - self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                        cage_title_text
                    )

                    self.canvas.saveState()
                    self.canvas.setFont(self.configuration["font"], 3 * self.configuration["text_size"])
                    if self.get_filter()[12]==0:
                        self.canvas.drawCentredString(
                            horizontal_position + cage_number_horizontal_position,
                            current_height + 2 * self.configuration['distance_between_lines'],
                            scoring_template_data['cage_number']
                        )
                    self.canvas.restoreState()

                    if scoring_template_data['team_size'] > 1:
                        self.canvas.drawCentredString(
                            horizontal_position + cage_letter_horizontal_position,
                            current_height + self.configuration["lines_offset"],
                            "".join(cage_letters_text_list[:scoring_template_data['team_size']])
                        )

                    # **Print group column**
                    self.canvas.rect(
                        horizontal_position + cage_column_width,
                        current_height,
                        group_column_width,
                        group_column_height
                    )

                    self.canvas.drawString(
                        horizontal_position + group_title_horizontal_position,
                        current_height + group_column_height - self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                        group_title_text
                    )

                    self.canvas.saveState()
                    self.canvas.setFont(self.configuration["bold_font"], 2 * self.configuration["text_size"])
                    self.canvas.drawCentredString(
                        horizontal_position + group_text_horizontal_position,
                        current_height + group_column_height - 2 * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                        scoring_template_data['group_code']
                    )
                    self.canvas.restoreState()

                    group_description_in_lines = split_text_in_lines(
                        scoring_template_data['group_description'],
                        group_column_width - 2.0 * table_horizontal_margin,
                        self.configuration["font"],
                        self.configuration["text_size"]
                    )

                    for index, group_description_in_line in enumerate(group_description_in_lines[:min(3, len(group_description_in_lines))]):
                        self.canvas.drawCentredString(
                            horizontal_position + group_text_horizontal_position,
                            current_height + (2 - index) * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                            group_description_in_line
                        )

                    # **Print family column**
                    family_in_lines = split_text_in_lines(
                        scoring_template_data['family'],
                        family_column_width - 2.0 * table_horizontal_margin,
                        self.configuration["bold_font"],
                        self.configuration["text_size"]
                    )

                    self.canvas.rect(
                        horizontal_position,
                        current_height - len(family_in_lines) * family_column_height,
                        family_column_width,
                        len(family_in_lines) * family_column_height
                    )

                    self.canvas.saveState()
                    self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"])
                    for index, family_line in enumerate(family_in_lines):
                        current_height -= family_column_height
                        self.canvas.drawCentredString(
                            horizontal_position + family_text_horizontal_position,
                            current_height + self.configuration["lines_offset"],
                            family_line
                        )
                    self.canvas.restoreState()

                    # **Print judging specifications column**
                    current_height -= judging_specifications_column_height

                    self.canvas.rect(
                        horizontal_position,
                        current_height,
                        judging_specifications_column_width,
                        judging_specifications_column_height
                    )

                    judging_specifications_text_list = list()

                    if scoring_template_data["red_factor"] == 1:
                        judging_specifications_text_list.append(_("Red Factor Allowed"))

                    if blank_undefined(scoring_template_data["size"]) != "Undefined":
                        judging_specifications_text_list.append(scoring_template_data["size"])

                    judging_specifications_text = " / ".join(judging_specifications_text_list)

                    judging_specifications_in_lines = split_text_in_lines(
                        judging_specifications_text,
                        judging_specifications_column_width - 2.0 * table_horizontal_margin,
                        self.configuration["font"],
                        self.configuration["text_size"]
                    )

                    for index, judging_specifications_line in enumerate(judging_specifications_in_lines[:min(2, len(judging_specifications_in_lines))]):
                        self.canvas.drawString(
                            horizontal_position + judging_specifications_text_horizontal_position,
                            current_height + (1 - index) * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                            judging_specifications_line
                        )

                    # **Print registration description**
                    self.canvas.rect(
                        horizontal_position + judging_specifications_column_width,
                        current_height,
                        registration_description_column_width,
                        registration_description_column_height
                    )

                    if scoring_template_data['registration_description'] != scoring_template_data['group_description']:
                        registration_description_in_lines = split_text_in_lines(
                            scoring_template_data['registration_description'],
                            registration_description_column_width - 2.0 * table_horizontal_margin,
                            self.configuration["font"],
                            self.configuration["text_size"]
                        )

                        for index, registration_description_line in enumerate(registration_description_in_lines[:min(2, len(registration_description_in_lines))]):
                            self.canvas.drawCentredString(
                                horizontal_position + registration_description_text_horizontal_position,
                                current_height + (1 - index) * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                                registration_description_line
                            )

                    # **Print scoring table**
                    current_height -= concept_column_height
                    punctuation_line_draw(current_height, horizontal_position, title=True)

                    if len(scoring_template_data['negative_scores']) > 0:
                        current_height -= concept_column_height
                        punctuation_line_draw(current_height, horizontal_position, positive_title=True)

                    if not self.arguments.get("empty", False):
                        status = status_extraction(scoring_template_data["status"], scoring_template_data["location_status"])
                        if status != "Judjable":
                            self.canvas.saveState()
                            self.canvas.setFont(self.configuration["font"], 2 * self.configuration["text_size"])
                            self.canvas.setStrokeColor(self.configuration['status_text_color'])
                            self.canvas.setFillColor(self.configuration['status_text_color'])

                            self.canvas.translate(
                                horizontal_position + concept_column_width + self.configuration["lines_offset"],
                                current_height - table_vertical_margin
                            )
                            self.canvas.rotate(-90)

                            total_status = list(scoring_template_data["status"])
                            for index, status in enumerate(scoring_template_data["location_status"]):
                                if status == "Not Presented":
                                    total_status[index] = "Not Presented"

                            for status in total_status:
                                self.canvas.rotate(90)
                                self.canvas.translate(self.configuration['punctuation_column_width'], 0)
                                self.canvas.rotate(-90)
                                if status != "Judjable":
                                    self.canvas.drawString(0, 0, _(status))

                            self.canvas.restoreState()

                    for concept in scoring_template_data['positive_scores']:
                        current_height -= self.configuration['distance_between_lines']
                        current_height = punctuation_line_draw(
                            current_height,
                            horizontal_position,
                            team_size=scoring_template_data['team_size'],
                            concept_title = concept['concept_title'].title(),
                            max_mark= concept['max_mark'],
                            marks= concept['marks']
                        )

                    if len(scoring_template_data['negative_scores']) > 0:
                        current_height -= concept_column_height
                        punctuation_line_draw(current_height, horizontal_position, negative_title=True)
                        for concept in scoring_template_data['negative_scores']:
                            current_height -= self.configuration['distance_between_lines']
                            current_height = punctuation_line_draw(
                                current_height,
                                horizontal_position,
                                team_size=scoring_template_data['team_size'],
                                concept_title = concept['concept_title'].title(),
                                max_mark= concept['max_mark'],
                                marks= concept['marks'],
                            )

                    current_height -= concept_column_height
                    punctuation_line_draw(
                        current_height,
                        horizontal_position,
                        team_size=scoring_template_data['team_size'],
                        concept_title = total_title_text,
                        marks = (
                            scoring_template_data['total_a'],
                            scoring_template_data['total_b'],
                            scoring_template_data['total_c'],
                            scoring_template_data['total_d'],
                        ),
                    )

                    # **Print total team column**
                    current_height -= total_team_column_height
                    self.canvas.rect(
                        horizontal_position + observations_column_width,
                        current_height,
                        total_team_title_column_width,
                        total_team_column_height
                    )

                    self.canvas.drawString(
                        horizontal_position + total_team_title_position,
                        current_height + self.configuration["lines_offset"],
                        total_team_title_text
                    )

                    if scoring_template_data['team_size'] > 1:
                        total_team_text = str(scoring_template_data['sum_mark'])
                    else:
                        total_team_text = "--"

                    if not self.arguments.get("empty", False) or total_team_text == "--":
                        self.canvas.saveState()
                        self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"])
                        self.canvas.drawRightString(
                            horizontal_position + total_team_text_position,
                            current_height + self.configuration["lines_offset"],
                            total_team_text
                        )
                        self.canvas.restoreState()

                    # **Print total harmony**
                    current_height -= harmony_column_height
                    self.canvas.rect(
                        horizontal_position + observations_column_width,
                        current_height,
                        harmony_title_column_width,
                        harmony_column_height
                    )

                    self.canvas.drawString(
                        horizontal_position + harmony_title_position,
                        current_height + self.configuration["lines_offset"],
                        harmony_title_text
                    )

                    if scoring_template_data['team_size'] > 1:
                        harmony_text = str(scoring_template_data['harmony'])
                    else:
                        harmony_text = "--"

                    if not self.arguments.get("empty", False) or harmony_text == "--":
                        self.canvas.saveState()
                        self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"])
                        self.canvas.drawRightString(
                            horizontal_position + harmony_text_position,
                            current_height + self.configuration["lines_offset"],
                            harmony_text
                        )
                        self.canvas.restoreState()

                    # **Print tie break points column**
                    current_height -= tie_break_mark_column_height
                    self.canvas.rect(
                        horizontal_position + judge_column_width,
                        current_height,
                        tie_break_mark_title_column_width,
                        tie_break_mark_column_height
                    )

                    self.canvas.drawString(
                        horizontal_position + tie_break_mark_title_position,
                        current_height + self.configuration["lines_offset"],
                        tie_break_mark_title_text
                    )

                    if not self.arguments.get("empty", False):
                        self.canvas.saveState()
                        self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"])
                        self.canvas.drawRightString(
                            horizontal_position + tie_break_mark_text_position,
                            current_height + self.configuration["lines_offset"],
                            str(scoring_template_data['tie_break_mark'])
                        )
                        self.canvas.restoreState()

                    # **Print observations**
                    self.canvas.rect(
                        horizontal_position,
                        current_height,
                        observations_column_width,
                        observations_column_height
                    )

                    self.canvas.drawString(
                        horizontal_position + observations_text_position,
                        current_height + 2 * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                        observations_title_text + ": "
                    )

                    if not self.arguments.get("empty", False) and blank_undefined(scoring_template_data["observations"]) != "Undefined":
                        observations_in_lines = split_text_in_lines(
                            scoring_template_data["observations"],
                            observations_column_width - observations_title_width - 2 * table_horizontal_margin,
                            self.configuration["font"],
                            self.configuration["text_size"],
                            multiline=True
                        )

                        self.canvas.drawString(
                            horizontal_position + observations_text_position + observations_title_width,
                            current_height + 2 * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                            observations_in_lines[0]
                        )

                        if len(observations_in_lines) > 1:
                            rest_observations_lines = " ".join(observations_in_lines[1:])
                            observations_in_lines = split_text_in_lines(
                                rest_observations_lines,
                                observations_column_width - 2 * table_horizontal_margin,
                                self.configuration["font"],
                                self.configuration["text_size"],
                                multiline=True
                            )

                            for index, observations_line in enumerate(observations_in_lines[:min(2, len(observations_in_lines))]):
                                self.canvas.drawString(
                                    horizontal_position + observations_text_position,
                                    current_height + (1 - index) * self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                                    observations_line
                                )

                    # **Print total points column**
                    current_height -= total_points_column_height
                    self.canvas.rect(
                        horizontal_position + judge_column_width,
                        current_height,
                        total_points_title_column_width,
                        total_points_column_height
                    )

                    self.canvas.saveState()
                    self.canvas.setFont(self.configuration["font"], 2 * self.configuration["title_size"])
                    self.canvas.drawString(
                        horizontal_position + total_points_title_position,
                        current_height + self.configuration["lines_offset"],
                        total_points_title_text
                    )
                    if not self.arguments.get("empty", False):
                        self.canvas.setFont(self.configuration["bold_font"], 2 * self.configuration["title_size"])
                        self.canvas.drawRightString(
                            horizontal_position + total_points_text_position,
                            current_height + self.configuration["lines_offset"],
                            str(scoring_template_data['total_mark'])
                        )
                    self.canvas.restoreState()

                    # **Print judge**
                    self.canvas.rect(
                        horizontal_position,
                        current_height,
                        judge_column_width,
                        judge_column_height
                    )

                    self.canvas.drawString(
                        horizontal_position + judge_title_position,
                        current_height + self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                        judge_title_text + ": "
                    )

                    if not self.arguments.get("empty", False):
                        self.canvas.saveState()
                        self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"])
                        self.canvas.drawString(
                            horizontal_position + judge_text_position,
                            current_height + self.configuration['distance_between_lines'] + self.configuration["lines_offset"],
                            scoring_template_data['judge_1']
                        )
                        self.canvas.restoreState()

                    self.canvas.drawString(
                        horizontal_position + judge_title_position,
                        current_height + self.configuration["lines_offset"],
                        judgement_date_title + ": "
                    )

                    if not self.arguments.get("empty", False):
                        self.canvas.drawString(
                            horizontal_position + judgement_date_text_position,
                            current_height + self.configuration["lines_offset"],
                            scoring_template_data['judgment_date']
                        )

                    # **Print exhibitor**
                    if not self.arguments.get("empty", False) and not self.arguments.get("wo_exhibitor", False):
                        rings_list = (
                            blank_custom(scoring_template_data["ring_a"], "*"),
                            blank_custom(scoring_template_data["ring_b"], "*"),
                            blank_custom(scoring_template_data["ring_c"], "*"),
                            blank_custom(scoring_template_data["ring_d"], "*"),
                        )
                        rings_text = " / ".join(rings_list[:scoring_template_data['team_size']])

                        current_height -= self.configuration['distance_between_lines'] * exhibitor_name_factor

                        self.canvas.saveState()
                        self.canvas.setFont(self.configuration["bold_font"], self.configuration["text_size"] * exhibitor_name_factor)
                        self.canvas.drawString(
                            horizontal_position + exhibitor_text_position,
                            current_height + self.configuration["lines_offset"],
                            scoring_template_data['exhibitor']
                        )

                        current_height -= self.configuration['distance_between_lines'] * exhibitor_name_factor
                        self.canvas.drawString(
                            horizontal_position + exhibitor_text_position,
                            current_height + self.configuration["lines_offset"],
                            breeder_code_title_text + ":" + scoring_template_data['bc'] + "  " + registration_title_text + ":" + scoring_template_data['registration']
                        )

                        self.canvas.restoreState()

                        current_height -= self.configuration['distance_between_lines']
                        self.canvas.drawString(
                            horizontal_position + exhibitor_text_position,
                            current_height + self.configuration["lines_offset"],
                            scoring_template_data['comunity']
                        )
                        current_height -= self.configuration['distance_between_lines']
                        self.canvas.drawString(
                            horizontal_position + exhibitor_text_position,
                            current_height + self.configuration["lines_offset"],
                            _("Rings") + ": " + rings_text
                        )

                        if blank_undefined(scoring_template_data['flag']) != "Undefined":
                            flag_image_directory, flag_image = load_image(scoring_template_data['flag'])
                            if flag_image_directory is not None:
                                flag_image_width, flag_image_height = flag_image.size
                                width_ratio = max_flag_width * 1.0 / flag_image_width
                                height_ratio = max_flag_height * 1.0 / flag_image_height
                                ratio = min(width_ratio, height_ratio)
                                flag_image_width = flag_image_width * ratio
                                flag_image_height = flag_image_height * ratio
                                self.canvas.drawImage(
                                    flag_image_directory,
                                    horizontal_position + 2 * table_horizontal_margin,
                                    current_height + exhibitor_height / 2.0 - flag_image_height / 2.0,
                                    height=flag_image_height,
                                    width=flag_image_width
                                )

                        if blank_undefined(scoring_template_data['prize']) != "Undefined":
                            prize_image_directory, prize_image = load_image(scoring_template_data['prize'])
                            if prize_image_directory is not None:
                                prize_image_width, prize_image_height = prize_image.size
                                width_ratio = max_prize_width * 1.0 / prize_image_width
                                height_ratio = max_prize_height * 1.0 / prize_image_height
                                ratio = min(width_ratio, height_ratio)
                                prize_image_width = prize_image_width * ratio
                                prize_image_height = prize_image_height * ratio
                                self.canvas.drawImage(
                                    prize_image_directory,
                                    horizontal_position + table_width - 2 * table_horizontal_margin - prize_image_width,
                                    current_height + exhibitor_height / 2.0 - prize_image_height / 2.0,
                                    height=prize_image_height,
                                    width=prize_image_width
                                )

                    # **Print judgement status
                    if not self.arguments.get("empty", 0) and scoring_template_data['judgment_status'] == "Pending":
                        self.canvas.saveState()

                        self.canvas.setStrokeColor(self.configuration['black_transparent'])
                        self.canvas.setFillColor(self.configuration['black_transparent'])

                        if current_row > 0:
                            height = initial_height - max_half_table_height - vertical_gap_between_tables - max_half_table_height / 2.0
                        else:
                            height = initial_height - max_half_table_height / 2.0

                        width = horizontal_position + table_width /2.0 + 15

                        self.canvas.translate(width, height)
                        self.canvas.rotate(60)

                        self.canvas.setFont(self.configuration['bold_font'], 30)
                        self.canvas.drawCentredString(0, 0, _("Pending for Judgement"))

                        self.canvas.restoreState()

                    # **Next scoring template**
                    current_column += 1
                    if current_column > 1:
                        current_column = 0
                        if current_height < initial_height - max_half_table_height:
                            current_row = 0
                            self.canvas.showPage()
                            current_height = initial_height
                        else:
                            current_row += 1
                            current_height = initial_height - max_half_table_height - vertical_gap_between_tables
                    else:
                        if current_row > 0:
                            current_height = initial_height - max_half_table_height - vertical_gap_between_tables
                        else:
                            current_height = initial_height

                if current_column != 0 or current_row != 0:
                    current_column = 0
                    current_row = 0
                    self.canvas.showPage()
                current_height = initial_height
