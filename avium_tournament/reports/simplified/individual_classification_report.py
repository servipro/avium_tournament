# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import blank_undefined, none_empty, status_extraction
from avium_tournament.reports.table_reportlab import drawpdf
from avium_tournament.utils import get_tournament_value
from collections import OrderedDict
from frappe import _
from reportlab.lib.colors import HexColor


class SimplifiedIndividualClassificationReport(BaseReport):

    def __init__(self, arguments, canvas=None):
        individual_groups_print = arguments.get('print_individual',0)
        team_groups_print = arguments.get('print_team',0)
        if arguments['filter'] == "Group":
            code_title = _('BC')
            description_title = _('Exhibitor')
            report_name = _("Individual Groups Classification Report")
        elif arguments['filter'] == "Association":
            code_title = _('Group')
            description_title = _('Registration Description')
            report_name = _("Individual Association Classification Report")
        else:
            code_title = _('Group')
            description_title = _('Registration Description')
            report_name = _("Individual Classification Report")

        if arguments['filter'] == "Group":
            levels = 2
        elif arguments['filter'] == "Association":
            levels = 2
        else:
            levels = 1

        CONF_TABLE = {
            'report_name': report_name,
            'levels': levels,
            'gap_between_columns': 3,
            'tables_per_page': 2,
            'description_text': (
                {
                    'text_size': 7.5,
                    'font': 'Helvetica-Bold',
                    'only_in_first_page': True,
                    'text': _("NJ") + ": " + _("Not Judjable") + "; " + _("De") + ": " +_("Declassified") + "; " + _("DeP") + ": " +_("Declassified Puntuated") + "; " + _("Di") + ": " + _("Disqualified") + "; " + _("DiP") + ": " + _("Disqualified Puntuated") + "; " + _("NS") + ": " +_("Did Not Sing") + "; " + _("ISe") + ": " +_("Insuficient Sing") + "; " + _("NP") + ": " + _("Not Presented"),
                    'alignment': "Center"
                },
            ),
            'columns_description': (
                {
                    'key': 'code',
                    'column_title': code_title,
                    'column_width': 0.13,
                    'alignment': 'Left',
                }, {
                    'key': 'description',
                    'column_title': description_title,
                    'title_alignment': 'Left',
                    'column_width': 0.41,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'cage',
                    'column_title': _('Cage'),
                    'column_width': 0.11,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'ring',
                    'column_title': _('Ring'),
                    'column_width': 0.11,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'tie_break_mark',
                    'column_title': _('TBM'),
                    'column_width': 0.07,
                    'alignment': 'Right',
                    'offset': 4,
                }, {
                    'key': 'total',
                    'column_title': _('Total'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'prize',
                    'column_title': _('Prize'),
                    'column_width': 0.07,
                    'truncate': True,
                },
            ),
        }

        super(SimplifiedIndividualClassificationReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return (
            self.arguments.get('print_individual', 0),
            self.arguments.get('print_team', 0),
        )

    def get_order(self):
        return (
            self.arguments.get('filter', "Exhibitor"),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  ireg.name as registration,
                    exbr.breeder_code as bc,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.association as association,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    ireg.registration_description as description,
                    jud1.judge_name as judge_name_1,
                    jud2.judge_name as judge_name_2,
                    scote.cage_number as cage,
                    ireg.ring_a as ring_a,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    scote.status_a as status_a,
                    scote.location_status_a as location_status_a,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3,
                    1 as team_size,
                    scote.classification as classification
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                CASE WHEN %(individual)s = 1 THEN TRUE ELSE FALSE END AND
                exbr.idx = 1
            ORDER BY
                CASE WHEN %(filter)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(filter)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(filter)s = 'Association' THEN association END ASC,
                CASE WHEN %(filter)s = 'Association' THEN reg.exhibitor_surname END ASC,
                CASE WHEN %(filter)s = 'Association' THEN reg.exhibitor_name END ASC,
                tgro.individual_code ASC,
                classification ASC,
                cage ASC
            """,
            {
                'individual': self.get_filter()[0],
                'filter': self.get_order()[0],
            },
            as_dict=True
        )

        tournament_data = get_tournament_value(['add_tie_break_marks',])

        group_judge_cache = dict()
        for item in raw_data:
            if item['group_code'] not in group_judge_cache:
                group_judge_cache[item['group_code']] = list()
            judge_name_1 = blank_undefined(item['judge_name_1'])
            if judge_name_1 != "Undefined" and judge_name_1 not in group_judge_cache[item['group_code']]:
                group_judge_cache[item['group_code']].append(judge_name_1)
            judge_name_2 = blank_undefined(item['judge_name_2'])
            if judge_name_2 != "Undefined" and judge_name_2 not in group_judge_cache[item['group_code']]:
                group_judge_cache[item['group_code']].append(judge_name_2)

        data = OrderedDict()

        for item in raw_data:
            if self.arguments['filter'] == "Group":
                first_level_key = item['group_code'] + ' - ' + item['group_description']
                second_level_key = _('Judge') + ": " + " / ".join(group_judge_cache[item['group_code']])
            elif self.arguments['filter'] == "Association":
                first_level_key = blank_undefined(item['association'])
                second_level_key = item['bc'] + ' - ' + item['exhibitor_surname'] + ', ' + item['exhibitor_name']
            else:
                first_level_key = item['bc'] + ' - ' + item['exhibitor_surname'] + ', ' + item['exhibitor_name']
                second_level_key = "_"

            if self.arguments["prize"] == "1":
                prize = blank_undefined(item['prize_1'])
            elif self.arguments["prize"] == "2":
                prize = blank_undefined(item['prize_2'])
            else:
                prize = blank_undefined(item['prize_3'])

            status = status_extraction(
                (item['status_a'], ),
                (item['location_status_a'], ),
                abbreviation=True
            )

            if first_level_key not in data:
                data[first_level_key] = OrderedDict()
            if second_level_key not in data[first_level_key]:
                data[first_level_key][second_level_key] = OrderedDict()
            data[first_level_key][second_level_key][item['registration']] = dict()
            data[first_level_key][second_level_key][item['registration']]['cage'] = none_empty(item['cage']).lstrip("0")
            data[first_level_key][second_level_key][item['registration']]['ring'] = none_empty(item['ring_a'])
            if prize != "Undefined":
                data[first_level_key][second_level_key][item['registration']]['prize'] = prize
            elif status != "Judjable":
                data[first_level_key][second_level_key][item['registration']]['prize'] = _(status)
            data[first_level_key][second_level_key][item['registration']]['tie_break_mark'] = item['tie_break_mark']
            if tournament_data['add_tie_break_marks'] == "Yes":
                data[first_level_key][second_level_key][item['registration']]['total'] = item['total_mark'] + item['tie_break_mark']
            else:
                data[first_level_key][second_level_key][item['registration']]['total'] = item['total_mark']
            if self.arguments['filter'] in ("Exhibitor", "Association"):
                data[first_level_key][second_level_key][item['registration']]['code'] = item['group_code']
                data[first_level_key][second_level_key][item['registration']]['description'] = item['description']
            else:
                data[first_level_key][second_level_key][item['registration']]['code'] = item['bc']
                data[first_level_key][second_level_key][item['registration']]['description'] = item['exhibitor_surname'] + ', ' + item['exhibitor_name']

        self.data = data

    def gen_pdf(self):
        if len(self.data.keys()) > 0:
            drawpdf(self)
            self.canvas.showPage()
            return self.canvas
        else:
            return self.canvas