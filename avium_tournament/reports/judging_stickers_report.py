# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import none_empty, importFonts, split_text_in_lines, blank_undefined
from frappe import _
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth
from avium_tournament.reports.stickers_reportlab import drawpdf

class JudgingStickersReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Judging Stickers Configuration", "Judging Stickers Configuration")

        CONF_TABLE = {
            'report_name': _("Judging Stickers"),
            'report_configuration': raw_report_configuration_data,
            'extra_vertical_text_offset': 0,
        }

        super(JudgingStickersReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):

        return (
            blank_undefined(self.arguments.get('registration', None)),
            blank_undefined(self.arguments.get('start_group', None)),
            blank_undefined(self.arguments.get('end_group', None)),
            blank_undefined(self.arguments.get('start_cage', None)),
            blank_undefined(self.arguments.get('end_cage', None)),
            blank_undefined(self.arguments.get('individual', "All")),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql(""" 
            SELECT  scote.cage_number as cage_number,
                    tgro.individual_code as group_code,
                    ireg.registration_description as description,
                    reg.name as registration,
                    1 as team_size
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            WHERE
                CASE WHEN %(registration)s != 'Undefined' THEN reg.name = %(registration)s ELSE TRUE END AND
                CASE WHEN %(start_group)s != 'Undefined' THEN tgro.individual_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(end_group)s != 'Undefined' THEN tgro.individual_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(start_cage)s != 'Undefined' THEN scote.cage_number >= %(start_cage)s ELSE TRUE END AND
                CASE WHEN %(end_cage)s != 'Undefined' THEN scote.cage_number <= %(end_cage)s ELSE TRUE END AND
                CASE WHEN %(individual)s != 'Team' THEN TRUE ELSE FALSE END
            UNION ALL
            SELECT  scote.cage_number as cage_number,
                    tgro.team_code as group_code,
                    treg.registration_description as description,
                    reg.name as registration,
                    tgro.team_size as team_size
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
            WHERE
                CASE WHEN %(registration)s != 'Undefined' THEN reg.name = %(registration)s ELSE TRUE END AND
                CASE WHEN %(start_group)s != 'Undefined' THEN tgro.team_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(end_group)s != 'Undefined' THEN tgro.team_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(start_cage)s != 'Undefined' THEN scote.cage_number >= %(start_cage)s ELSE TRUE END AND
                CASE WHEN %(end_cage)s != 'Undefined' THEN scote.cage_number <= %(end_cage)s ELSE TRUE END AND
                CASE WHEN %(individual)s != 'Individual' THEN TRUE ELSE FALSE END
            ORDER BY group_code ASC, cage_number ASC
            """,
            {
                'registration': self.get_filter()[0],
                'start_group': self.get_filter()[1],
                'end_group': self.get_filter()[2],
                'start_cage': self.get_filter()[3],
                'end_cage': self.get_filter()[4],
                'individual': self.get_filter()[5],
            },
            as_dict = True
        )

        self.data = list()

        for item in raw_data:

            for iteration in range(item['team_size']):
                if item['team_size'] == 1:
                    cage_number = none_empty(item['cage_number'])
                else:
                    if iteration == 0:
                        cage_number = none_empty(item['cage_number'])+'A'
                    elif iteration == 1:
                        cage_number = none_empty(item['cage_number'])+'B'
                    elif iteration == 2:
                        cage_number = none_empty(item['cage_number'])+'C'
                    else:
                        cage_number = none_empty(item['cage_number'])+'D'

                cage_data = dict()
                cage_data['description'] = item['description']
                cage_data['group_code'] = item['group_code']
                cage_data['cage_number'] = cage_number.lstrip("0")
                self.data.append(cage_data)

    def gen_pdf(self):

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']

        text_size = configuration.text_size
        title_text_size = text_size - 3
        group_code_text_size = text_size - 2
        group_description_text_size = text_size - 3
        cage_text_size = text_size

        group_title_text = _('GROUP') + ': '
        group_title_width = stringWidth(group_title_text, self.configuration['font'], title_text_size)
        cage_title_text = _('CAGE') + ': '
        cage_title_width = stringWidth(cage_title_text, self.configuration['font'], title_text_size)

        label_printable_height = configuration.rows_height * cm - configuration.label_upper_margin * cm - configuration.label_lower_margin * cm - 2 * self.configuration['extra_vertical_text_offset']
        vertical_gap_between_text = (label_printable_height - 5 * text_size) / 4.0
        lines_offset = (vertical_gap_between_text + text_size - 0.75 * text_size) / 2.0 + 0.5
        base_height = configuration.label_lower_margin * cm + self.configuration['extra_vertical_text_offset']

        label_printable_width = configuration.column_width * cm - configuration.label_left_margin * cm - configuration.label_right_margin * cm - 2 * self.configuration['extra_horizontal_text_offset']

        # **Custom functions**
        def text_height(factor):
            return base_height + lines_offset + factor * (text_size + vertical_gap_between_text)

        group_text_height = text_height(4)

        one_line_specialization_text_height = text_height(2)

        two_lines_specialization_text_height_1 = text_height(2.5)
        two_lines_specialization_text_height_2 = text_height(1.5)

        three_lines_specialization_text_height_1 = text_height(3)
        three_lines_specialization_text_height_2 = text_height(2)
        three_lines_specialization_text_height_3 = text_height(1)

        cage_text_height = text_height(0)

        def text_printing(canvas, label_data, horizontal_position, vertical_position):

            # **Variables initialization**
            horizontal_center = horizontal_position + configuration.label_left_margin * cm + self.configuration['extra_horizontal_text_offset'] + label_printable_width / 2.0

            # **Split description in lines**
            group_description_in_lines = split_text_in_lines(
                label_data.get('description', ''),
                label_printable_width,
                self.configuration['font'],
                group_description_text_size
            )

            # **Text position calculation**
            group_code_width = stringWidth(label_data.get('group_code', ''), self.configuration['bold_font'], group_code_text_size)
            group_text_start = configuration.label_left_margin * cm + self.configuration['extra_horizontal_text_offset'] + (label_printable_width - group_title_width - group_code_width) / 2.0
            cage_number_width = stringWidth(label_data.get('cage_number', ''), self.configuration['bold_font'], cage_text_size)
            cage_text_start = configuration.label_left_margin * cm + self.configuration['extra_horizontal_text_offset'] + (label_printable_width - cage_title_width - cage_number_width) / 2.0

            # **Print text**
            canvas.setFont(self.configuration['bold_font'], cage_text_size)
            canvas.drawString(
                horizontal_position + cage_text_start + cage_title_width,
                vertical_position + cage_text_height,
                label_data.get('cage_number', '')
            )
            canvas.setFont(self.configuration['font'], title_text_size)
            canvas.drawString(
                horizontal_position + cage_text_start,
                vertical_position + cage_text_height,
                cage_title_text
            )

            canvas.setFont(self.configuration['bold_font'], group_code_text_size)
            canvas.drawString(
                horizontal_position + group_text_start + group_title_width,
                vertical_position + group_text_height,
                label_data.get('group_code', '')
            )

            canvas.setFont(self.configuration['font'], title_text_size)
            canvas.drawString(
                horizontal_position + group_text_start,
                vertical_position + group_text_height,
                group_title_text
            )

            canvas.setFont(self.configuration['font'], group_description_text_size)
            if len(group_description_in_lines) < 2:
                canvas.drawCentredString(
                    horizontal_center,
                    vertical_position + one_line_specialization_text_height,
                    group_description_in_lines[0]
                )
            elif len(group_description_in_lines) < 3:
                canvas.drawCentredString(
                    horizontal_center,
                    vertical_position + two_lines_specialization_text_height_1,
                    group_description_in_lines[0]
                )
                canvas.drawCentredString(
                    horizontal_center,
                    vertical_position + two_lines_specialization_text_height_2,
                    group_description_in_lines[1]
                )
            else:
                canvas.drawCentredString(
                    horizontal_center,
                    vertical_position + three_lines_specialization_text_height_1,
                    group_description_in_lines[0]
                )
                canvas.drawCentredString(
                    horizontal_center,
                    vertical_position + three_lines_specialization_text_height_2,
                    group_description_in_lines[1]
                )
                canvas.drawCentredString(
                    horizontal_center,
                    vertical_position + three_lines_specialization_text_height_3,
                    group_description_in_lines[2]
                )

            return canvas

        return drawpdf(self, text_printing)