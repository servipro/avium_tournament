# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import blank_undefined, none_empty
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

class BoxReport(BaseReport):

    def __init__(self, arguments, canvas=None):
        # Variables initialized
        distance_between_lines = 25
        text_size = 11

        CONF_TABLE = {
            'report_name': _("Box Report"),
            'levels': 1,
            'text_size': text_size,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * text_size) / 2 + 0.5,
            'content_horizontal_border': 0.8,
            'content_vertical_border': 0.8,
            'columns_description': (
                {
                    'key': 'registration',
                    'column_title': _('Registration'),
                    'column_width': 0.07,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'bc',
                    'column_title': _('BC'),
                    'column_width': 0.08,
                    'alignment': 'Left',
                }, {
                    'key': 'exhibitor',
                    'column_title': _('Exhibitor'),
                    'title_alignment': 'Left',
                    'column_width': 0.34,
                    'alignment': 'Left',
                    'in_lines': True,
                }, {
                    'key': 'group_code',
                    'column_title': _('Group Code'),
                    'column_width': 0.07,
                    'alignment': 'Left',
                }, {
                    'key': 'group_description',
                    'column_title': _('Group Description'),
                    'title_alignment': 'Left',
                    'column_width': 0.29,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'cage_number',
                    'column_title': _('Cage'),
                    'column_width': 0.07,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'box',
                    'column_title': _('Box'),
                    'column_width': 0.07,
                },
            ),
            'first_level_new_page': True,
        }

        super(BoxReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return blank_undefined(self.arguments.get("carrier", None))

    def get_order(self):
        return self.arguments.get("sorted", "Exhibitor")

    def fetch_data(self):

        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration,
                    ireg.name as cage_registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    car.carrier_name as carrier,
                    scote.cage_number as cage_number
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            WHERE
                CASE WHEN %(filter)s != 'Undefined' THEN reg.carrier = %(filter)s ELSE TRUE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  reg.name as registration,
                    treg.name as cage_registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    car.carrier_name as carrier,
                    scote.cage_number as cage_number
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabCarrier` as car on car.name = reg.carrier
            LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
            WHERE
                CASE WHEN %(filter)s != 'Undefined' THEN reg.carrier = %(filter)s ELSE TRUE END AND
                exbr.idx = 1
            ORDER BY
                carrier ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(order)s = 'Group' THEN group_code END ASC,
                cage_number ASC
            """,
            {'filter': self.get_filter(), 'order': self.get_order()},
            as_dict = True
        )

        data = OrderedDict()

        for item in raw_data:
            # Data registration
            first_key = blank_undefined(item['carrier'])
            second_key = item['cage_registration']

            if first_key not in data:
                data[first_key] = dict()
                data[first_key]['_'] = OrderedDict()

            data[first_key]['_'][second_key] = dict()
            data[first_key]['_'][second_key]['registration'] = item['registration'].lstrip("REG0")
            data[first_key]['_'][second_key]['bc'] = item['bc']
            data[first_key]['_'][second_key]['exhibitor'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
            data[first_key]['_'][second_key]['group_code'] = item['group_code']
            data[first_key]['_'][second_key]['group_description'] = item['group_description']
            data[first_key]['_'][second_key]['cage_number'] = none_empty(item['cage_number']).lstrip("0")

        self.data = data

    def gen_pdf(self):
    
        return drawpdf(self)