# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

class PaymentsReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        def print_modification_function(print_value):
            return str("{0:.2f}".format(print_value)) + " €"

        CONF_TABLE = {
            'report_name': _("Payments Report"),
            'levels': 0,
            'columns_description': (
                {
                    'key': 'registration_number',
                    'column_title': _('Registration number'),
                    'title_alignment': 'Center',
                    'column_width': 0.10,
                    'alignment': 'Center',
                }, {
                    'key': 'exhibitor_name',
                    'column_title': _('Exhibitor Name'),
                    'title_alignment': 'Left',
                    'column_width': 0.57,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'price',
                    'print_modification': print_modification_function,
                    'column_title': _('Price'),
                    'title_alignment': 'Center',
                    'column_width': 0.11,
                    'alignment': 'Right',
                    'offset': 5,
                    'total': True,
                }, {
                    'key': 'paid',
                    'print_modification': print_modification_function,
                    'column_title': _('Paid'),
                    'title_alignment': 'Center',
                    'column_width': 0.11,
                    'alignment': 'Right',
                    'offset': 5,
                    'total': True,
                }, {
                    'key': 'remaining',
                    'print_modification': print_modification_function,
                    'column_title': _('Remaining'),
                    'title_alignment': 'Center',
                    'column_width': 0.11,
                    'alignment': 'Right',
                    'offset': 5,
                    'total': True,
                },
            ),
        }

        super(PaymentsReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return self.arguments.get("only_unpaid", 0)

    def get_order(self):
        return self.arguments.get("sorted", "Exhibitor")

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.exhibitor_name as name,
                    reg.exhibitor_surname as surname,
                    reg.name as registration_number,
                    reg.total_price as price,
                    reg.paid_amount as paid,
                    reg.remaining_amount as remaining
            FROM `tabRegistration` as reg
            WHERE
                total_price > 0 AND
                CASE WHEN %(filter)s = 1 THEN reg.remaining_amount != 0 ELSE TRUE END
            ORDER BY
                CASE WHEN %(order)s = 'Exhibitor' THEN surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN name END ASC,
                CASE WHEN %(order)s = 'Registration' THEN registration_number END ASC
            """,
            {'filter': self.get_filter(), 'order': self.get_order()},
            as_dict = True
        )

        data = OrderedDict()

        for item in raw_data:
            if item['registration_number'] not in data and not item['registration_number'] is None:
                data[item['registration_number']] = dict()
                data[item['registration_number']]['registration_number'] = item['registration_number'].lstrip("REG0")
                data[item['registration_number']]['exhibitor_name'] = item['surname'] + ", " + item['name']
                data[item['registration_number']]['price'] = item['price']
                data[item['registration_number']]['paid'] = item['paid']
                data[item['registration_number']]['remaining'] = item['remaining']

        self.data = {
            "_": {
                "_": data
            }
        }

    def gen_pdf(self):
    
        return drawpdf(self)