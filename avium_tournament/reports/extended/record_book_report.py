# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import blank_undefined, none_empty
from avium_tournament.reports.table_reportlab import drawpdf
from avium_tournament.utils import get_tournament_value
from collections import OrderedDict
from frappe import _
from reportlab.lib.colors import HexColor


class ExtendedRecordBookReport(BaseReport):

    def __init__(self, arguments, canvas=None):
        if arguments['filter'] == "Group":
            code_title = _("BC")
            description_title = _("Exhibitor")
            report_name = _("Groups Record Book Report")
        elif arguments['filter'] == "Association":
            code_title = _('Group')
            description_title = _('Registration Description')
            report_name = _("Association Record Book Report")
        else:
            code_title = _("Group")
            description_title = _("Registration Description")
            report_name = _("Exhibitors Record Book Report")

        if arguments['filter'] == "Group":
            levels = 2
        elif arguments['filter'] == "Association":
            levels = 2
        else:
            levels = 1

        ring_column_width = 0.04
        ring_alignment = 'Right'

        CONF_TABLE = {
            'report_name': report_name,
            'levels': levels,
            'text_over_title': (
                {
                    'text': _("Ring"),
                    'alignment': "Center",
                    'position': 0.56,
                }, {
                    'text': _("Punctuation"),
                    'alignment': "Center",
                    'position': 0.72,
                },
            ),
            'columns_description': (
                {
                    'key': 'code',
                    'column_title': code_title,
                    'column_width': 0.08,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'description',
                    'column_title': description_title,
                    'column_width': 0.33,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'cage',
                    'column_title': _("Cage"),
                    'column_width': 0.07,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'ring_a',
                    'column_title': _("A"),
                    'column_width': ring_column_width,
                    'alignment': ring_alignment,
                }, {
                    'key': 'ring_b',
                    'column_title': _("B"),
                    'column_width': ring_column_width,
                    'alignment': ring_alignment,
                }, {
                    'key': 'ring_c',
                    'column_title': _("C"),
                    'column_width': ring_column_width,
                    'alignment': ring_alignment,
                }, {
                    'key': 'ring_d',
                    'column_title': _("D"),
                    'column_width': ring_column_width,
                    'alignment': ring_alignment,
                }, {
                    'key': 'punctuation_a',
                    'column_title': _("A"),
                    'column_width': 0.04,
                    'alignment': 'Right',
                }, {
                    'key': 'punctuation_b',
                    'column_title': _("B"),
                    'column_width': 0.04,
                    'alignment': 'Right',
                }, {
                    'key': 'punctuation_c',
                    'column_title': _("C"),
                    'column_width': 0.04,
                    'alignment': 'Right',
                }, {
                    'key': 'punctuation_d',
                    'column_title': _("D"),
                    'column_width': 0.04,
                    'alignment': 'Right',
                }, {
                    'key': 'harmony',
                    'column_title': _("Harmony"),
                    'column_width': 0.04,
                    'alignment': 'Right',
                }, {
                    'key': 'tie_break_mark',
                    'column_title': _("TBM"),
                    'column_width': 0.04,
                    'alignment': 'Right',
                }, {
                    'key': 'total',
                    'column_title': _("Total"),
                    'column_width': 0.06,
                    'alignment': 'Right',
                }, {
                    'key': 'prize',
                    'column_title': _("Prize"),
                    'column_width': 0.06,
                    'truncate': True,
                },
            ),
        }

        super(ExtendedRecordBookReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_order(self):
        return (
            self.arguments.get('filter', "Exhibitor"),
            self.arguments.get('breakdown', 0),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  ireg.name as registration,
                    exbr.breeder_code as bc,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.association as association,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    ireg.registration_description as description,
                    jud1.judge_name as judge_name_1,
                    jud2.judge_name as judge_name_2,
                    scote.cage_number as cage,
                    ireg.ring_a as ring_a,
                    NULL as ring_b,
                    NULL as ring_c,
                    NULL as ring_d,
                    scote.total_a as punctuation_a,
                    scote.total_b as punctuation_b,
                    scote.total_c as punctuation_c,
                    scote.total_d as punctuation_d,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3,
                    1 as team_size,
                    scote.classification as classification
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                scote.prize != 'NULL' AND exbr.idx = 1
            UNION ALL
            SELECT  treg.name as registration,
                    exbr.breeder_code as bc,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.association as association,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    treg.registration_description as description,
                    jud1.judge_name as judge_name_1,
                    jud2.judge_name as judge_name_2,
                    scote.cage_number as cage,
                    treg.ring_a as ring_a,
                    treg.ring_b as ring_b,
                    treg.ring_c as ring_c,
                    treg.ring_d as ring_d,
                    scote.total_a as punctuation_a,
                    scote.total_b as punctuation_b,
                    scote.total_c as punctuation_c,
                    scote.total_d as punctuation_d,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3,
                    tgro.team_size as team_size,
                    scote.classification as classification
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE scote.prize != 'NULL' AND exbr.idx = 1
            ORDER BY
                CASE WHEN %(filter)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(filter)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(filter)s = 'Association' THEN association END ASC,
                CASE WHEN %(filter)s = 'Association' THEN exhibitor_surname END ASC,
                CASE WHEN %(filter)s = 'Association' THEN exhibitor_name END ASC,
                group_code ASC,
                classification ASC,
                cage ASC
            """,
            {
                'filter': self.get_order()[0],
            },
            as_dict = True
        )

        tournament_data = get_tournament_value(['add_tie_break_marks',])

        group_judge_cache = dict()
        for item in raw_data:
            if item['group_code'] not in group_judge_cache:
                group_judge_cache[item['group_code']] = list()
            judge_name_1 = blank_undefined(item['judge_name_1'])
            if judge_name_1 != "Undefined" and judge_name_1 not in group_judge_cache[item['group_code']]:
                group_judge_cache[item['group_code']].append(judge_name_1)
            judge_name_2 = blank_undefined(item['judge_name_2'])
            if judge_name_2 != "Undefined" and judge_name_2 not in group_judge_cache[item['group_code']]:
                group_judge_cache[item['group_code']].append(judge_name_2)

        data = OrderedDict()

        for item in raw_data:
            if self.arguments['filter'] == "Group":
                first_level_key = item['group_code'] + ' - ' + item['group_description']
                second_level_key = _('Judge') + ": " + " / ".join(group_judge_cache[item['group_code']])
            elif self.arguments['filter'] == "Association":
                first_level_key = blank_undefined(item['association'])
                second_level_key = item['bc'] + ' - ' + item['exhibitor_surname'] + ', ' + item['exhibitor_name']
            else:
                first_level_key = item['bc'] + ' - ' + item['exhibitor_surname'] + ', ' + item['exhibitor_name']
                second_level_key = "_"

            if self.arguments["prize"] == "1":
                prize = blank_undefined(item['prize_1'])
            elif self.arguments["prize"] == "2":
                prize = blank_undefined(item['prize_2'])
            else:
                prize = blank_undefined(item['prize_3'])

            if first_level_key not in data:
                data[first_level_key] = OrderedDict()
            if second_level_key not in data[first_level_key]:
                data[first_level_key][second_level_key] = OrderedDict()
            data[first_level_key][second_level_key][item['registration']] = dict()
            data[first_level_key][second_level_key][item['registration']]['cage'] = none_empty(item['cage']).lstrip("0")
            data[first_level_key][second_level_key][item['registration']]['ring_a'] = none_empty(item['ring_a'])
            data[first_level_key][second_level_key][item['registration']]['punctuation_a'] = item['punctuation_a']
            if item['team_size'] > 1:
                data[first_level_key][second_level_key][item['registration']]['ring_b'] = none_empty(item['ring_b'])
                data[first_level_key][second_level_key][item['registration']]['punctuation_b'] = item['punctuation_b']
                data[first_level_key][second_level_key][item['registration']]['harmony'] = item['harmony']
            if item['team_size'] > 2:
                data[first_level_key][second_level_key][item['registration']]['ring_c'] = none_empty(item['ring_c'])
                data[first_level_key][second_level_key][item['registration']]['punctuation_c'] = item['punctuation_c']
            if item['team_size'] > 3:
                data[first_level_key][second_level_key][item['registration']]['ring_d'] = none_empty(item['ring_d'])
                data[first_level_key][second_level_key][item['registration']]['punctuation_d'] = item['punctuation_d']
            data[first_level_key][second_level_key][item['registration']]['tie_break_mark'] = item['tie_break_mark']
            data[first_level_key][second_level_key][item['registration']]['prize'] = prize
            if tournament_data['add_tie_break_marks'] == "Yes":
                data[first_level_key][second_level_key][item['registration']]['total'] = item['total_mark'] + item['tie_break_mark']
            else:
                data[first_level_key][second_level_key][item['registration']]['total'] = item['total_mark']
            if self.arguments['filter'] in ("Exhibitor", "Association"):
                data[first_level_key][second_level_key][item['registration']]['code'] = item['group_code']
                data[first_level_key][second_level_key][item['registration']]['description'] = item['description']
            else:
                data[first_level_key][second_level_key][item['registration']]['code'] = item['bc']
                data[first_level_key][second_level_key][item['registration']]['description'] = item['exhibitor_surname'] + ', ' + item['exhibitor_name']

        self.data = data

    def gen_pdf(self):
    
        return drawpdf(self)