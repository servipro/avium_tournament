# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import none_empty, importFonts, split_text_in_lines, blank_undefined, load_image, status_extraction
from avium_tournament.reports.stickers_reportlab import drawpdf
from avium_tournament.utils import get_tournament_value, get_taxes
from frappe import _
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth

class ExtendedMarksStickersReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Marks Stickers Configuration", "Marks Stickers Configuration")

        CONF_TABLE = {
            'report_name': _("Marks Stickers"),
            'report_configuration': raw_report_configuration_data,
            'extra_vertical_text_offset': 5,
            'prize_text_color': colors.red,
            'prize_background_color': colors.yellow,
        }

        super(ExtendedMarksStickersReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):

        return (
            blank_undefined(self.arguments.get('registration', None)),
            blank_undefined(self.arguments.get('start_group', None)),
            blank_undefined(self.arguments.get('end_group', None)),
            blank_undefined(self.arguments.get('start_cage', None)),
            blank_undefined(self.arguments.get('end_cage', None)),
            blank_undefined(self.arguments.get('individual', "All")),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    reg.allow as allow,
                    reg.telephone as telephone,
                    reg.mobile_phone as mobile_phone,
                    reg.email as email,
                    reg.city as city,
                    reg.territory as territory,
                    parter.flag as flag,
                    tgro.individual_code as group_code,
                    ireg.registration_description as description,
                    1 as team_size,
                    scote.cage_number as cage_number,
                    ireg.ring_a as ring_a,
                    NULL as ring_b,
                    NULL as ring_c,
                    NULL as ring_d,
                    ireg.tax_a as tax_a,
                    NULL as tax_b,
                    NULL as tax_c,
                    NULL as tax_d,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3,
                    touprides.description_4 as prize_4
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            WHERE
                CASE WHEN %(registration)s != 'Undefined' THEN reg.name = %(registration)s ELSE TRUE END AND
                CASE WHEN %(start_group)s != 'Undefined' THEN tgro.individual_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(end_group)s != 'Undefined' THEN tgro.individual_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(start_cage)s != 'Undefined' THEN scote.cage_number >= %(start_cage)s ELSE TRUE END AND
                CASE WHEN %(end_cage)s != 'Undefined' THEN scote.cage_number <= %(end_cage)s ELSE TRUE END AND
                CASE WHEN %(individual)s != 'Team' THEN TRUE ELSE FALSE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    reg.allow as allow,
                    reg.telephone as telephone,
                    reg.mobile_phone as mobile_phone,
                    reg.email as email,
                    reg.city as city,
                    reg.territory as territory,
                    parter.flag as flag,
                    tgro.team_code as group_code,
                    treg.registration_description as description,
                    tgro.team_size as team_size,
                    scote.cage_number as cage_number,
                    treg.ring_a as ring_a,
                    treg.ring_b as ring_b,
                    treg.ring_c as ring_c,
                    treg.ring_d as ring_d,
                    treg.tax_a as tax_a,
                    treg.tax_b as tax_b,
                    treg.tax_c as tax_c,
                    treg.tax_d as tax_d,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    touprides.description_1 as prize_1,
                    touprides.description_2 as prize_2,
                    touprides.description_3 as prize_3,
                    touprides.description_4 as prize_4
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTerritory` as ter on ter.territory_name = reg.territory
            LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            WHERE
                CASE WHEN %(registration)s != 'Undefined' THEN reg.name = %(registration)s ELSE TRUE END AND
                CASE WHEN %(start_group)s != 'Undefined' THEN tgro.team_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(end_group)s != 'Undefined' THEN tgro.team_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(start_cage)s != 'Undefined' THEN scote.cage_number >= %(start_cage)s ELSE TRUE END AND
                CASE WHEN %(end_cage)s != 'Undefined' THEN scote.cage_number <= %(end_cage)s ELSE TRUE END AND
                CASE WHEN %(individual)s != 'Individual' THEN TRUE ELSE FALSE END AND
                exbr.idx = 1
            ORDER BY group_code ASC, cage_number ASC
            """,
            {
                'registration': self.get_filter()[0],
                'start_group': self.get_filter()[1],
                'end_group': self.get_filter()[2],
                'start_cage': self.get_filter()[3],
                'end_cage': self.get_filter()[4],
                'individual': self.get_filter()[5],
            },
            as_dict = True
        )

        self.data = list()

        tournament_data = get_tournament_value(['add_tie_break_marks', 'extra_tax', 'add_deduct_tax', 'fix_percentage_tax'])
        extra_tax = tournament_data['extra_tax']
        add_deduct_tax = tournament_data['add_deduct_tax']
        fix_percentage_tax = tournament_data['fix_percentage_tax']

        for item in raw_data:
            contact_information = ""
            if item['allow'] == "Yes":
                contact_information_list = list()
                if self.arguments['information'] == "Phone and Email":
                    if blank_undefined(item['telephone']) != "Undefined":
                        contact_information_list.append(item['telephone'])
                    if blank_undefined(item['mobile_phone']) != "Undefined":
                        contact_information_list.append(item['mobile_phone'])
                    if blank_undefined(item['email']) != "Undefined":
                        contact_information_list.append(item['email'].lower())
                else:
                    if blank_undefined(item['city']) != "Undefined":
                        contact_information_list.append(item['city'])
                    if blank_undefined(item['territory']) != "Undefined":
                        contact_information_list.append(item['territory'])
                contact_information += ", ".join(contact_information_list)

            if self.arguments["prize"] == "1":
                prize = blank_undefined(item['prize_1'])
            elif self.arguments["prize"] == "2":
                prize = blank_undefined(item['prize_2'])
            elif self.arguments["prize"] == "3":
                prize = blank_undefined(item['prize_3'])
            else:
                prize = blank_undefined(item['prize_4'])

            for iteration in range(item['team_size']):
                if item['team_size'] == 1:
                    cage_type = "Individual"

                    cage_number = none_empty(item['cage_number'])
                    ring = none_empty(item['ring_a'])
                    if blank_undefined(item['tax_a']) != "Undefined" and item['tax_a'] != 0.0:
                        dummy,dummy,tax = get_taxes(item['tax_a'], extra_tax, add_deduct_tax, fix_percentage_tax)
                    else:
                        tax = "Undefined"
                    status = item['status_a']
                    location_status = item['location_status_a']

                    if tournament_data['add_tie_break_marks'] == "Yes":
                        mark = str(item['total_mark'] + item['tie_break_mark'])
                    else:
                        mark = str(item['total_mark'])
                else:
                    cage_type = "Team"

                    if tournament_data['add_tie_break_marks'] == "Yes":
                        total_mark = str(item['total_mark'] + item['tie_break_mark'])
                    else:
                        total_mark = str(item['total_mark'])

                    if iteration == 0:
                        cage_number = none_empty(item['cage_number'])+'A'
                        ring = none_empty(item['ring_a'])
                        if blank_undefined(item['tax_a']) != "Undefined" and item['tax_a'] != 0.0:
                            dummy,dummy,tax = get_taxes(item['tax_a'], extra_tax, add_deduct_tax, fix_percentage_tax)
                        else:
                            tax = "Undefined"
                        status = item['status_a']
                        location_status = item['location_status_a']
                        mark = str(item['total_a']) + "/" + str(total_mark)
                    elif iteration == 1:
                        cage_number = none_empty(item['cage_number'])+'B'
                        ring = none_empty(item['ring_b'])
                        if blank_undefined(item['tax_b']) != "Undefined" and item['tax_b'] != 0.0:
                            dummy,dummy,tax = get_taxes(item['tax_b'], extra_tax, add_deduct_tax, fix_percentage_tax)
                        else:
                            tax = "Undefined"
                        status = item['status_b']
                        location_status = item['location_status_b']
                        mark = str(item['total_b']) + "/" + str(total_mark)
                    elif iteration == 2:
                        cage_number = none_empty(item['cage_number'])+'C'
                        ring = none_empty(item['ring_c'])
                        if blank_undefined(item['tax_c']) != "Undefined" and item['tax_c'] != 0.0:
                            dummy,dummy,tax = get_taxes(item['tax_c'], extra_tax, add_deduct_tax, fix_percentage_tax)
                        else:
                            tax = "Undefined"
                        status = item['status_c']
                        location_status = item['location_status_c']
                        mark = str(item['total_c']) + "/" + str(total_mark)
                    else:
                        cage_number = none_empty(item['cage_number'])+'D'
                        ring = none_empty(item['ring_d'])
                        if blank_undefined(item['tax_d']) != "Undefined" and item['tax_d'] != 0.0:
                            dummy,dummy,tax = get_taxes(item['tax_d'], extra_tax, add_deduct_tax, fix_percentage_tax)
                        else:
                            tax = "Undefined"
                        status = item['status_d']
                        location_status = item['location_status_d']
                        mark = str(item['total_d']) + "/" + str(total_mark)

                cage_data = dict()
                cage_data['cage_type'] = cage_type
                cage_data['exhibitor'] = item['exhibitor_surname'] + ", " + item['exhibitor_name'] + ". " + _("BC") + ": " + item['bc']
                cage_data['contact_information'] = contact_information
                cage_data['flag'] = item['flag']
                cage_data['description'] = item['description']
                cage_data['group_code'] = item['group_code']
                cage_data['cage_number'] = cage_number.lstrip("0")
                cage_data['ring'] = ring
                cage_data['tax'] = tax
                if tax == "Undefined":
                    cage_data['status'] = status_extraction((status,), (location_status,), abbreviation=False)
                else:
                    cage_data['status'] = status_extraction((status,), (location_status,), abbreviation=True)
                cage_data['mark'] = mark
                cage_data['tie_break_mark'] = item['tie_break_mark']
                cage_data['prize'] = prize
                self.data.append(cage_data)

    def gen_pdf(self):

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']

        label_printable_height = configuration.rows_height * cm - configuration.label_upper_margin * cm - configuration.label_lower_margin * cm - 2 * self.configuration['extra_vertical_text_offset']
        label_printable_width = configuration.column_width * cm - configuration.label_left_margin * cm - configuration.label_right_margin * cm - 2 * self.configuration['extra_horizontal_text_offset']

        vertical_gap_between_text = (label_printable_height - 7.6 * configuration.text_size) / 6.0
        lines_offset = (vertical_gap_between_text + configuration.text_size - 0.75 * configuration.text_size) / 2.0
        base_height = configuration.label_lower_margin * cm + self.configuration['extra_vertical_text_offset']

        # Load left logo
        max_logos_height = 2 * configuration.text_size + vertical_gap_between_text
        max_left_logo_width = max_logos_height
        max_flag_width = 1.5 * max_logos_height
        
        if configuration.left_logo == "Federation Logo":
            left_logo_directory, left_logo = load_image(self.configuration["federation_logo"])
        elif configuration.left_logo == "Association Logo":
            left_logo_directory, left_logo = load_image(self.configuration["association_logo"])
        elif configuration.left_logo == "Judge Association Logo":
            left_logo_directory, left_logo = load_image(self.configuration["judge_association_logo"])
        elif configuration.left_logo == "Logo 1":
            left_logo_directory, left_logo = load_image(configuration.logo_1)
        elif configuration.left_logo == "Logo 2":
            left_logo_directory, left_logo = load_image(configuration.logo_2)
        else:
            left_logo_directory, left_logo = None, None

        tournament_name_in_lines = split_text_in_lines(
            self.configuration["tournament_name"],
            label_printable_width - 2 * max_flag_width - 10,
            self.configuration['bold_font'],
            configuration.text_size
        )

        # **Custom functions**
        def text_height(factor):
            if factor > 0:
                return base_height + lines_offset + (factor + 0.3) * configuration.text_size + factor * vertical_gap_between_text
            else:
                return base_height + lines_offset

        # **Text height calculation**
        tournament_name_height = text_height(6)

        tournament_city_height = text_height(5)

        group_text_height = text_height(3.85)

        exhibitor_text_height = text_height(2.85)

        contact_information_text_height = text_height(1.85)

        specialization_text_height = text_height(0.85)

        marks_text_height = text_height(0)

        # **Text horizontal calculation**
        group_title_text = _("Group") + ": "
        group_title_width = stringWidth(group_title_text, self.configuration['font'], configuration.text_size)

        cage_title_text = _("Cage") + ": "
        cage_title_width = stringWidth(cage_title_text, self.configuration['font'], configuration.text_size)

        ring_title_text = _("Ring") + ": "
        ring_title_width = stringWidth(ring_title_text, self.configuration['font'], configuration.text_size)

        def text_printing(canvas, label_data, horizontal_position, vertical_position):

            tournament_data = get_tournament_value(['add_tie_break_marks'])

            # **Variables initialization**
            horizontal_start_position = horizontal_position + configuration.label_left_margin * cm + self.configuration['extra_horizontal_text_offset']

            cage_number_width = stringWidth(label_data['cage_number'], self.configuration['bold_font'], 1.3 * configuration.text_size)
            cage_width = cage_title_width + cage_number_width
            cage_title_horizontal_position = label_printable_width / 2.0 - cage_width / 2.0
            cage_number_horizontal_position = cage_title_horizontal_position + cage_title_width

            ring_number_width = stringWidth(label_data['ring'], self.configuration['bold_font'], 1.3 * configuration.text_size)

            # **Split description in lines**
            group_description_in_lines = split_text_in_lines(
                label_data.get('description', '').upper(),
                label_printable_width,
                self.configuration['font'],
                configuration.text_size
            )

            # Print header
            if left_logo_directory is not None:
                left_logo_width, left_logo_height = left_logo.size

                width_ratio = max_left_logo_width * 1.0 / left_logo_width
                height_ratio = max_logos_height * 1.0 / left_logo_height
                ratio = min(width_ratio, height_ratio)
                left_logo_width = left_logo_width * ratio
                left_logo_height = left_logo_height * ratio
                self.canvas.drawImage(
                    left_logo_directory,
                    horizontal_start_position,
                    vertical_position + tournament_city_height,
                    height=left_logo_height,
                    width=left_logo_width
                )

            if blank_undefined(label_data['flag']) != "Undefined":
                flag_image_directory, flag_image = load_image(label_data['flag'])
                if flag_image_directory is not None:
                    flag_image_width, flag_image_height = flag_image.size
                    width_ratio = max_flag_width * 1.0 / flag_image_width
                    height_ratio = max_logos_height * 1.0 / flag_image_height
                    ratio = min(width_ratio, height_ratio)
                    flag_image_width = flag_image_width * ratio
                    flag_image_height = flag_image_height * ratio
                    self.canvas.drawImage(
                        flag_image_directory,
                        horizontal_start_position + label_printable_width - flag_image_width,
                        vertical_position + tournament_city_height,
                        height=flag_image_height,
                        width=flag_image_width
                    )

            canvas.saveState()
            canvas.setFont(self.configuration['bold_font'], configuration.text_size)
            canvas.drawCentredString(
                horizontal_start_position + label_printable_width / 2.0,
                vertical_position + tournament_name_height,
                tournament_name_in_lines[0]
            )
            if len(tournament_name_in_lines) > 1:
                canvas.drawCentredString(
                    horizontal_start_position + label_printable_width / 2.0,
                    vertical_position + tournament_city_height,
                    tournament_name_in_lines[1]
                )
                canvas.restoreState()
            else:
                canvas.restoreState()
                canvas.setFont(self.configuration['font'], configuration.text_size)
                canvas.drawCentredString(
                    horizontal_start_position + label_printable_width / 2.0,
                    vertical_position + tournament_city_height,
                    self.configuration["tournament_city"]
                )

            # **Print text**
            canvas.setFont(self.configuration['font'], configuration.text_size)
            canvas.drawString(
                horizontal_start_position,
                vertical_position + group_text_height,
                group_title_text
            )

            canvas.drawString(
                horizontal_start_position + cage_title_horizontal_position,
                vertical_position + group_text_height,
                cage_title_text
            )

            canvas.drawRightString(
                horizontal_start_position + label_printable_width - ring_number_width,
                vertical_position + group_text_height,
                ring_title_text
            )

            canvas.setFont(self.configuration['bold_font'], 1.3 * configuration.text_size)
            canvas.drawString(
                horizontal_start_position + group_title_width,
                vertical_position + group_text_height,
                label_data['group_code']
            )

            canvas.drawString(
                horizontal_start_position + cage_number_horizontal_position,
                vertical_position + group_text_height,
                label_data['cage_number']
            )

            canvas.drawRightString(
                horizontal_start_position + label_printable_width,
                vertical_position + group_text_height,
                label_data['ring']
            )

            canvas.setFont(self.configuration['font'], configuration.text_size)
            canvas.drawString(
                horizontal_start_position,
                vertical_position + exhibitor_text_height,
                label_data['exhibitor']
            )
            canvas.drawString(
                horizontal_start_position,
                vertical_position + contact_information_text_height,
                label_data['contact_information']
            )
            canvas.drawCentredString(
                horizontal_start_position + label_printable_width / 2.0,
                vertical_position + specialization_text_height,
                group_description_in_lines[0].upper()
            )

            canvas.setFont(self.configuration['bold_font'], 1.3 * configuration.text_size)

            points_text = _("POINTS") + ":" + label_data["mark"]

            if tournament_data['add_tie_break_marks'] == "Yes" and label_data["tie_break_mark"] > 0:
                points_text += " " + _("TBM") + ":" + str(label_data["tie_break_mark"])

            if label_data["status"] != "Judjable":
                points_text += " " + _(label_data["status"])

            canvas.drawString(
                horizontal_start_position,
                vertical_position + marks_text_height,
                points_text
            )

            if label_data["tax"] != "Undefined":
                canvas.drawCentredString(
                    horizontal_start_position + label_printable_width / 2.0,
                    vertical_position + marks_text_height,
                    _("TAX") + ": " + str(label_data["tax"])
                )

            if label_data["prize"] != "Undefined":
                if self.arguments["prize"] != "4":
                    canvas.saveState()
                    canvas.setFillColor(self.configuration['prize_background_color'])
                    canvas.setStrokeColor(self.configuration['prize_background_color'])
                    canvas.rect(
                        horizontal_start_position + label_printable_width * 2.0 / 3.0,
                        vertical_position + marks_text_height,
                        label_printable_width / 3.0,
                        configuration.text_size,
                        stroke = 1,
                        fill = 1
                    )
                    canvas.setFillColor(self.configuration['prize_text_color'])
                    canvas.setStrokeColor(self.configuration['prize_text_color'])
                    canvas.drawCentredString(
                        horizontal_start_position + label_printable_width * 2.5 / 3.0,
                        vertical_position + marks_text_height,
                        label_data["prize"]
                    )
                    canvas.restoreState()
                else:
                    # Print background
                    prize_directory, prize_image = load_image(label_data["prize"])

                    if not prize_directory is None:
                        prize_width, prize_height = prize_image.size

                        prize_width = (vertical_gap_between_text + 2.3 * configuration.text_size) * prize_width / prize_height
                        prize_height = (vertical_gap_between_text + 2.3 * configuration.text_size)
                        prize_horizontal_position = horizontal_start_position + label_printable_width - prize_width
                        prize_vertical_position = vertical_position + configuration.label_lower_margin * cm
                        canvas.drawImage(prize_directory, prize_horizontal_position, prize_vertical_position, height=prize_height, width=prize_width)

            return canvas

        return drawpdf(self, text_printing)