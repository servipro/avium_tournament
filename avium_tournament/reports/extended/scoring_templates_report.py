# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import importFonts, split_text_in_lines, blank_custom, blank_undefined, none_empty, load_image, text_truncate, draw_header, draw_footer, status_extraction
from avium_tournament.utils import get_tournament_value
from collections import OrderedDict
from frappe import _
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase.pdfmetrics import stringWidth

class ExtendedScoringTemplatesReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Report Configuration", "Report Configuration")

        if raw_report_configuration_data.judge_logo == "Right":
            raw_report_configuration_data.right_logo = "Judge Association Logo"
        elif raw_report_configuration_data.judge_logo == "Left":
            raw_report_configuration_data.left_logo = "Judge Association Logo"

        raw_report_configuration_data.only_first_page = 0

        CONF_TABLE = {
            'report_configuration': raw_report_configuration_data,
            'report_name': _("Scoring Templates"),
            'header_titles_gap': 0,
            'extra_vertical_text_offset': 5,
            'prize_text_color': colors.red,
        }

        super(ExtendedScoringTemplatesReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        start_exhibitor_value = self.arguments.get('start_exhibitor', None)
        if start_exhibitor_value is not None:
            start_registration_doctype = frappe.get_all(
                "Registration",
                filters = {"name": start_exhibitor_value},
                fields = {'exhibitor_name', 'exhibitor_surname'}
            )
            exhibitor_name = start_registration_doctype[0]['exhibitor_name']
            exhibitor_surname = start_registration_doctype[0]['exhibitor_surname']
            start_exhibitor_value = exhibitor_surname + ", " + exhibitor_name

        end_exhibitor_value = self.arguments.get('end_exhibitor', None)
        if end_exhibitor_value is not None:
            end_registration_doctype = frappe.get_all(
                "Registration",
                filters = {"name": end_exhibitor_value},
                fields = {'exhibitor_name', 'exhibitor_surname'}
            )
            exhibitor_name = end_registration_doctype[0]['exhibitor_name']
            exhibitor_surname = end_registration_doctype[0]['exhibitor_surname']
            end_exhibitor_value = exhibitor_surname + ", " + exhibitor_name

        return (
            blank_undefined(self.arguments.get('start_group', None)),
            blank_undefined(self.arguments.get('end_group', None)),
            blank_undefined(start_exhibitor_value),
            blank_undefined(end_exhibitor_value),
            blank_undefined(self.arguments.get('start_judge', None)),
            blank_undefined(self.arguments.get('end_judge', None)),
            blank_undefined(self.arguments.get('start_registration', None)),
            blank_undefined(self.arguments.get('end_registration', None)),
            blank_undefined(self.arguments.get('scoring_template', None)),
            self.arguments.get('email', 0),
            self.arguments.get('only_judged', 0),
            self.arguments.get('without_judge', 0),
            self.arguments.get('without_cage', 0)
        )

    def get_order(self):
        return self.arguments.get('sorted', "Group")

    def fetch_data(self):
        judges_raw_data = frappe.db.sql("""
            SELECT  jud.name as id,
                    jud.judge_name as name,
                    jud.uploaded_signature as uploaded_signature,
                    jud.signature as signature,
                    fami.family_name as family_name,
                    judcap.parentfield as parentfield
            FROM `tabJudge` as jud
            LEFT JOIN `tabJudgement Capabilities` as judcap on judcap.parent = jud.name
            LEFT JOIN `tabFamily` as fami on fami.name = judcap.family
            """,
            as_dict = True
        )

        judges_cache = dict()
        for item in judges_raw_data:
            if item['id'] not in judges_cache:
                judges_cache[item['id']] = dict()
                judges_cache[item['id']]['name'] = item['name']
                judges_cache[item['id']]['uploaded_signature'] = item['uploaded_signature']
                judges_cache[item['id']]['signature'] = item['signature']
                judges_cache[item['id']]['national_families'] = list()
                judges_cache[item['id']]['international_families'] = list()
            if item['parentfield'] == "national_families":
                judges_cache[item['id']]['national_families'].append(item['family_name'])
            if item['parentfield'] == "international_families":
                judges_cache[item['id']]['international_families'].append(item['family_name'])

        scoring_templates_raw_data = frappe.db.sql("""
            SELECT  ireg.name as cage_registration,
                    reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.email as email,
                    exbr.breeder_code as bc,
                    scote.judge_1 as judge_1,
                    scote.judge_2 as judge_2,
                    jud1.judge_name as judge_1_name,
                    jud2.judge_name as judge_2_name,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    ireg.registration_description as registration_description,
                    1 as team_size,
                    scote.cage_number as cage_number,
                    ireg.ring_a as ring_a,
                    NULL as ring_b,
                    NULL as ring_c,
                    NULL as ring_d,
                    conc.idx as concept_index,
                    conc.concept_name as concept_name,
                    conc.negative as negative,
                    conc.value_a as value_a,
                    conc.value_b as value_b,
                    conc.value_c as value_c,
                    conc.value_d as value_d,
                    conc.max_value as max_value,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    scote.sum_mark as sum_mark,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    touprides.description_1 as prize,
                    judtem.specimen_image as judging_template_specimen_image,
                    judtemspe.specimen_image as judging_template_specialisation_specimen_image,
                    scote.observations as observations,
                    scote.judgment_date as judgment_date,
                    scote.red_factor as red_factor,
                    scote.size as size,
                    scote.status as judgment_status
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabScoring Template Concept` as conc on conc.parent = scote.name
            LEFT JOIN `tabJudging Template Specialisation` as judtemspe on judtemspe.specialisation_name = scote.group_description
            LEFT JOIN `tabJudging Template` as judtem on judtem.name = judtemspe.parent
            WHERE
                CASE WHEN %(order)s = 'Group' AND %(start_group)s != 'Undefined' THEN tgro.individual_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Group' AND %(end_group)s != 'Undefined' THEN tgro.individual_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(start_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) >= %(start_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(end_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) <= %(end_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(start_judge)s != 'Undefined' THEN scote.judge_1 >= %(start_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(end_judge)s != 'Undefined' THEN scote.judge_1 <= %(end_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(start_registration)s != 'Undefined' THEN reg.name >= %(start_registration)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(end_registration)s != 'Undefined' THEN reg.name <= %(end_registration)s ELSE TRUE END AND
                CASE WHEN %(scoring_template)s != 'Undefined' THEN scote.name = %(scoring_template)s ELSE TRUE END AND
                CASE WHEN %(only_judged)s = '1' THEN scote.status = "Judged" ELSE TRUE END AND
                exbr.idx = 1
            UNION ALL
            SELECT  treg.name as cage_registration,
                    reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.email as email,
                    exbr.breeder_code as bc,
                    scote.judge_1 as judge_1,
                    scote.judge_2 as judge_2,
                    jud1.judge_name as judge_1_name,
                    jud2.judge_name as judge_2_name,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    treg.registration_description as registration_description,
                    tgro.team_size as team_size,
                    scote.cage_number as cage_number,
                    treg.ring_a as ring_a,
                    treg.ring_b as ring_b,
                    treg.ring_c as ring_c,
                    treg.ring_d as ring_d,
                    conc.idx as concept_index,
                    conc.concept_name as concept_name,
                    conc.negative as negative,
                    conc.value_a as value_a,
                    conc.value_b as value_b,
                    conc.value_c as value_c,
                    conc.value_d as value_d,
                    conc.max_value as max_value,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    scote.sum_mark as sum_mark,
                    scote.harmony as harmony,
                    scote.tie_break_mark as tie_break_mark,
                    scote.total_mark as total_mark,
                    scote.status_a as status_a,
                    scote.status_b as status_b,
                    scote.status_c as status_c,
                    scote.status_d as status_d,
                    scote.location_status_a as location_status_a,
                    scote.location_status_b as location_status_b,
                    scote.location_status_c as location_status_c,
                    scote.location_status_d as location_status_d,
                    touprides.description_1 as prize,
                    judtem.specimen_image as judging_template_specimen_image,
                    judtemspe.specimen_image as judging_template_specialisation_specimen_image,
                    scote.observations as observations,
                    scote.judgment_date as judgment_date,
                    scote.red_factor as red_factor,
                    scote.size as size,
                    scote.status as judgment_status
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabJudge` as jud1 on jud1.name = scote.judge_1
            LEFT JOIN `tabJudge` as jud2 on jud2.name = scote.judge_2
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            LEFT JOIN `tabScoring Template Concept` as conc on conc.parent = scote.name
            LEFT JOIN `tabJudging Template Specialisation` as judtemspe on judtemspe.specialisation_name = scote.group_description
            LEFT JOIN `tabJudging Template` as judtem on judtem.name = judtemspe.parent
            WHERE
                CASE WHEN %(order)s = 'Group' AND %(start_group)s != 'Undefined' THEN tgro.team_code >= %(start_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Group' AND %(end_group)s != 'Undefined' THEN tgro.team_code <= %(end_group)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(start_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) >= %(start_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Exhibitor' AND %(end_exhibitor)s != 'Undefined' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) <= %(end_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(start_judge)s != 'Undefined' THEN scote.judge_1 >= %(start_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Judge' AND %(end_judge)s != 'Undefined' THEN scote.judge_1 <= %(end_judge)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(start_registration)s != 'Undefined' THEN reg.name >= %(start_registration)s ELSE TRUE END AND
                CASE WHEN %(order)s = 'Registration' AND %(end_registration)s != 'Undefined' THEN reg.name <= %(end_registration)s ELSE TRUE END AND
                CASE WHEN %(scoring_template)s != 'Undefined' THEN scote.name = %(scoring_template)s ELSE TRUE END AND
                CASE WHEN %(only_judged)s = '1' THEN scote.status = "Judged" ELSE TRUE END AND
                exbr.idx = 1
            ORDER BY
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(order)s = 'Judge' THEN judge_1_name END ASC,
                CASE WHEN %(order)s = 'Registration' THEN registration END ASC,
                group_code ASC, cage_number ASC, concept_index ASC
            """,
            {
                'start_group': self.get_filter()[0],
                'end_group': self.get_filter()[1],
                'start_exhibitor': self.get_filter()[2],
                'end_exhibitor': self.get_filter()[3],
                'start_judge': self.get_filter()[4],
                'end_judge': self.get_filter()[5],
                'start_registration': self.get_filter()[6],
                'end_registration': self.get_filter()[7],
                'scoring_template': self.get_filter()[8],
                'email': self.get_filter()[9],
                'only_judged': self.get_filter()[10],
                'order': self.get_order(),
            },
            as_dict = True
        )

        tournament_data = get_tournament_value(['add_tie_break_marks', ])

        self.data = OrderedDict()

        for item in scoring_templates_raw_data:

            if blank_undefined(item['email']) == "Undefined" or not self.get_filter()[9]:

                if item['cage_registration'] not in self.data:
                    self.data[item['cage_registration']] = dict()
                    self.data[item['cage_registration']]['registration'] = item['registration'].lstrip("REG0")
                    self.data[item['cage_registration']]['exhibitor'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                    self.data[item['cage_registration']]['bc'] = item['bc']
                    if blank_undefined(item['judge_1']) != "Undefined":
                        self.data[item['cage_registration']]['judge_1'] = judges_cache[item['judge_1']]
                    else:
                        self.data[item['cage_registration']]['judge_1'] = None
                    if blank_undefined(item['judge_2']) != "Undefined":
                        self.data[item['cage_registration']]['judge_2'] = judges_cache[item['judge_2']]
                    else:
                        self.data[item['cage_registration']]['judge_2'] = None
                    self.data[item['cage_registration']]['group_code'] = item['group_code']
                    self.data[item['cage_registration']]['group_description'] = item['group_description']
                    self.data[item['cage_registration']]['registration_description'] = item['registration_description']
                    self.data[item['cage_registration']]['cage_number'] = none_empty(item['cage_number']).lstrip("0")
                    self.data[item['cage_registration']]['ring_a'] = item['ring_a']
                    self.data[item['cage_registration']]['ring_b'] = item['ring_b']
                    self.data[item['cage_registration']]['ring_c'] = item['ring_c']
                    self.data[item['cage_registration']]['ring_d'] = item['ring_d']
                    self.data[item['cage_registration']]['team_size'] = item['team_size']
                    self.data[item['cage_registration']]['positive_scores'] = list()
                    self.data[item['cage_registration']]['negative_scores'] = list()
                    self.data[item['cage_registration']]['total_a'] = item['total_a']
                    self.data[item['cage_registration']]['total_b'] = item['total_b']
                    self.data[item['cage_registration']]['total_c'] = item['total_c']
                    self.data[item['cage_registration']]['total_d'] = item['total_d']
                    self.data[item['cage_registration']]['sum_mark'] = item['sum_mark']
                    self.data[item['cage_registration']]['harmony'] = item['harmony']
                    self.data[item['cage_registration']]['tie_break_mark'] = item['tie_break_mark']
                    if tournament_data['add_tie_break_marks'] == "Yes":
                        self.data[item['cage_registration']]['total_mark'] = item['total_mark'] + item['tie_break_mark']
                    else:
                        self.data[item['cage_registration']]['total_mark'] = item['total_mark']
                    self.data[item['cage_registration']]['status'] = (item['status_a'], item['status_b'], item['status_c'], item['status_d'],)
                    self.data[item['cage_registration']]['location_status'] = (item['location_status_a'], item['location_status_b'], item['location_status_c'], item['location_status_d'],)
                    self.data[item['cage_registration']]['prize'] = item['prize']
                    self.data[item['cage_registration']]['observations'] = item['observations']
                    self.data[item['cage_registration']]['judgment_date'] = item['judgment_date']
                    self.data[item['cage_registration']]['red_factor'] = item['red_factor']
                    self.data[item['cage_registration']]['size'] = item['size']
                    self.data[item['cage_registration']]['judgment_status'] = item['judgment_status']
                    if blank_undefined(item['judging_template_specialisation_specimen_image']) != "Undefined":
                        self.data[item['cage_registration']]['specimen_image'] = item['judging_template_specialisation_specimen_image']
                    else:
                        if blank_undefined(item['judging_template_specimen_image']) != "Undefined":
                            self.data[item['cage_registration']]['specimen_image'] = item['judging_template_specimen_image']
                        else:
                            self.data[item['cage_registration']]['specimen_image'] = None

                if item['negative'] == 0:
                    self.data[item['cage_registration']]['positive_scores'].append(
                        {
                            'concept_name': item['concept_name'],
                            'max_value': item['max_value'],
                            'marks': (item['value_a'], item['value_b'], item['value_c'], item['value_d'],)
                        }
                    )
                elif item['negative'] == 1:
                    self.data[item['cage_registration']]['negative_scores'].append(
                        {
                            'concept_name': item['concept_name'],
                            'max_value': item['max_value'],
                            'marks': (item['value_a'], item['value_b'], item['value_c'], item['value_d'],)
                        }
                    )

    def gen_pdf(self):

        # **Set file name**
        self.canvas.setTitle(self.configuration['report_name'])

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']

        width, height = A4

        title_size = 12
        punctuation_text_size = 9
        observations_text_size = 10
        judge_specialization_size = 7
        gap_between_signatures = 5
        table_extra_margin = 5
        distance_between_lines = 15
        distance_between_judge_specialization_lines = 10
        punctuation_lines_offset = (distance_between_lines - 0.70 * punctuation_text_size)/2 + 0.5
        title_lines_offset = (distance_between_lines - 0.70 * title_size)/2 + 0.5

        prize_text_color = colors.red

        first_level_color = colors.lightblue
        second_level_color = colors.lightgrey
        black_transparent = colors.Color(0, 0, 0, alpha=0.5)


        printable_width = width - configuration.left_margin * cm - configuration.right_margin * cm - 2 * self.configuration['extra_horizontal_text_offset']
        horizontal_start_position = configuration.left_margin * cm + self.configuration['extra_horizontal_text_offset']

        punctuation_column_width = 27
        observation_column_width = printable_width * 0.3
        observation_printable_width = observation_column_width - 2 * table_extra_margin
        concept_column_width = printable_width - 5 * punctuation_column_width - observation_column_width

        concept_column_position = horizontal_start_position
        maximum_punctuation_column_position = concept_column_position + concept_column_width
        punctuation_A_column_position = maximum_punctuation_column_position + punctuation_column_width
        punctuation_B_column_position = punctuation_A_column_position + punctuation_column_width
        punctuation_C_column_position = punctuation_B_column_position + punctuation_column_width
        punctuation_D_column_position = punctuation_C_column_position + punctuation_column_width
        observation_column_position = punctuation_D_column_position + punctuation_column_width
    
        signature_width = (printable_width - 2 * gap_between_signatures) / 3

        association_stamp_position = horizontal_start_position
        judge_1_position = association_stamp_position + signature_width + gap_between_signatures
        judge_2_position = judge_1_position + signature_width + gap_between_signatures
    
        rows_height = 15
        distance_between_lines_in_table = 10
    
        observations_maximum_height = 10 * distance_between_lines
        signature_maximum_height = 80

        cage_title_text = _("Cage Nº")
        A_title_text = "A"
        B_title_text = "B"
        C_title_text = "C"
        D_title_text = "D"
        positive_concepts_title_text = _("Positive Concepts").upper()
        negative_concepts_title_text = _("Negative Concepts").upper()
        total_title_text = _("Total").upper()
        total_team_title_text = _("Total Team").upper()
        harmony_title_text = _("Harmony").upper()
        tie_break_mark_title_text = _("Tie Break Mark").upper()
        total_points_title_text = _("Total Points").upper()
        prize_title_text = _("Prize").upper()
        judge_title_text = _("Judge").upper()
        group_title_text = _("Group").upper()
        cage_title_text = _("Cage Nº").upper()

        judge_title_width = stringWidth(judge_title_text + ": ", self.configuration['bold_font'], title_size)

        # **Custom functions**
        def punctuation_line_draw(current_height, title, max_value=None, scores=None, fill_background=False, combine_columns=False,  bold_title=False):
            if bold_title:
                title_in_lines = split_text_in_lines(title, concept_column_width - 2 * table_extra_margin, self.configuration['bold_font'], punctuation_text_size)
            else:
                title_in_lines = split_text_in_lines(title, concept_column_width - 2 * table_extra_margin, self.configuration['font'], punctuation_text_size)

            self.canvas.saveState()
            if combine_columns:
                self.canvas.saveState()
                self.canvas.setFillColor(second_level_color)
                self.canvas.rect(
                    concept_column_position,
                    current_height,
                    concept_column_width + 5 * punctuation_column_width,
                    len(title_in_lines) * rows_height,
                    fill = fill_background
                )
                self.canvas.restoreState()
    
                self.canvas.setFont(self.configuration['font'], punctuation_text_size)

                self.canvas.saveState()
                if self.configuration['bold_font']:
                    self.canvas.setFont(self.configuration['bold_font'], punctuation_text_size)
                self.canvas.drawString(concept_column_position + table_extra_margin, current_height + punctuation_lines_offset, title)
                self.canvas.restoreState()

            else:
                self.canvas.saveState()
                self.canvas.setFillColor(second_level_color)
                if bold_title:
                    self.canvas.rect(
                        concept_column_position,
                        current_height + rows_height,
                        concept_column_width + punctuation_column_width,
                        -rows_height - (len(title_in_lines) - 1) * distance_between_lines_in_table,
                        fill=fill_background
                    )
                else:
                    self.canvas.rect(
                        concept_column_position,
                        current_height + rows_height,
                        concept_column_width,
                        -rows_height - (len(title_in_lines) - 1) * distance_between_lines_in_table,
                        fill=fill_background
                    )
                    self.canvas.rect(
                        maximum_punctuation_column_position,
                        current_height + rows_height,
                        punctuation_column_width,
                        -rows_height - (len(title_in_lines) - 1) * distance_between_lines_in_table,
                        fill=1
                    )
                self.canvas.restoreState()

                if type(scores) in (str,int):
                    self.canvas.rect(
                        punctuation_A_column_position,
                        current_height + rows_height,
                        4 * punctuation_column_width,
                        -rows_height - (len(title_in_lines)-1) * distance_between_lines_in_table,
                        fill=0
                    )
                else:
                    for position in (punctuation_A_column_position, punctuation_B_column_position, punctuation_C_column_position, punctuation_D_column_position):
                        self.canvas.rect(
                            position,
                            current_height + rows_height,
                            punctuation_column_width,
                            - rows_height - (len(title_in_lines)-1) * distance_between_lines_in_table,
                            fill=0
                        )
    
                if bold_title:
                    self.canvas.setFont(self.configuration['bold_font'], punctuation_text_size)
                    for line_index, title_line in enumerate(title_in_lines):
                        self.canvas.drawRightString(
                            concept_column_position + punctuation_column_width + concept_column_width - table_extra_margin,
                            current_height - distance_between_lines_in_table * line_index + punctuation_lines_offset,
                            title_line
                        )
                else:
                    self.canvas.setFont(self.configuration['font'], punctuation_text_size)
                    for line_index, title_line in enumerate(title_in_lines):
                        self.canvas.drawString(
                            concept_column_position + table_extra_margin,
                            current_height - distance_between_lines_in_table * line_index + punctuation_lines_offset,
                            title_line
                        )
    
                self.canvas.setFont(self.configuration['font'], punctuation_text_size)
    
                if type(scores) in (str, int):
                    if self.arguments.get("empty", 0) == 0:
                        self.canvas.drawCentredString(punctuation_A_column_position + 2*punctuation_column_width, current_height + punctuation_lines_offset, str(scores))
                else:
                    if not bold_title and max_value is not None:
                        self.canvas.drawCentredString(maximum_punctuation_column_position + 0.5*punctuation_column_width, current_height + punctuation_lines_offset, str(max_value))
                    if self.arguments.get("empty", 0) == 0:
                        positions = (punctuation_A_column_position, punctuation_B_column_position, punctuation_C_column_position, punctuation_D_column_position)
                        for index, score in enumerate(scores):
                            self.canvas.drawCentredString(
                                positions[index] + 0.5*punctuation_column_width,
                                current_height + punctuation_lines_offset,
                                str(score)
                            )

            self.canvas.restoreState()
            return current_height - (len(title_in_lines)-1) * distance_between_lines_in_table - rows_height

        for scoring_template_key in self.data:
            scoring_template = self.data[scoring_template_key]

            # **Print organitzation stamp and judges names**
            judge_1_height = judge_2_height = configuration.lower_margin * cm + configuration.footer_height * cm

            # if data["report_configuration"].association_stamp is not None:
            #     self.canvas.setFont(self.configuration['bold_font'], title_size)
            #     self.canvas.drawCentredString(association_stamp_position + signature_width * 0.5, judge_1_height, _("Organization stamp"))

            self.canvas.setFont(self.configuration['font'], judge_specialization_size)
            if blank_undefined(scoring_template['judge_1']) != "Undefined" and len(scoring_template['judge_1']['international_families']) > 0:
                judge_1_international_specialization_text = text_truncate(
                    _("International") + ": " + ", ".join(scoring_template['judge_1']['international_families']),
                    signature_width,
                    self.configuration['font'],
                    judge_specialization_size
                )
                self.canvas.drawCentredString(judge_1_position+signature_width * 0.5, judge_1_height, judge_1_international_specialization_text)
                judge_1_height += distance_between_judge_specialization_lines

            if blank_undefined(scoring_template['judge_1']) != "Undefined" and len(scoring_template['judge_1']['national_families']) > 0:
                judge_1_national_specialization_text = text_truncate(
                    _("National") + ": " + ", ".join(scoring_template['judge_1']['national_families']),
                    signature_width,
                    self.configuration['font'],
                    judge_specialization_size
                )
                self.canvas.drawCentredString(judge_1_position + signature_width * 0.5, judge_1_height, judge_1_national_specialization_text)
                judge_1_height += distance_between_judge_specialization_lines

            if blank_undefined(scoring_template['judge_2']) != "Undefined" and len(scoring_template['judge_2']['international_families']) > 0:
                judge_2_international_specialization_text = text_truncate(
                    _("International") + ": " + ", ".join(scoring_template['judge_2']['international_families']),
                    signature_width,
                    self.configuration['font'],
                    judge_specialization_size
                )
                self.canvas.drawCentredString(judge_2_position+signature_width * 0.5, judge_2_height, judge_2_international_specialization_text)
                judge_2_height += distance_between_judge_specialization_lines

            if blank_undefined(scoring_template['judge_2']) != "Undefined" and len(scoring_template['judge_2']['national_families']) > 0:
                judge_2_national_specialization_text = text_truncate(
                    _("National") + ": " + ", ".join(scoring_template['judge_2']['national_families']),
                    signature_width,
                    self.configuration['font'],
                    judge_specialization_size
                )
                self.canvas.drawCentredString(judge_2_position + signature_width * 0.5, judge_2_height, judge_2_national_specialization_text)
                judge_2_height += distance_between_judge_specialization_lines

            if blank_undefined(scoring_template['judge_1']) != "Undefined" and self.get_filter()[11]==0:
                judge_1_name = text_truncate(
                    scoring_template['judge_1']['name'],
                    signature_width - judge_title_width,
                    self.configuration['font'],
                    title_size
                )
                judge_1_width = stringWidth(judge_1_name, self.configuration['font'], title_size)

                title_position_judge_1 =  judge_1_position + signature_width/2 - (judge_title_width + judge_1_width)/2
                name_position_judge_1 = title_position_judge_1 + judge_title_width

                self.canvas.setFont(self.configuration['bold_font'], title_size)
                self.canvas.drawString(title_position_judge_1, judge_1_height, judge_title_text + ": ")

                self.canvas.setFont(self.configuration['font'], title_size)
                self.canvas.drawString(name_position_judge_1, judge_1_height, judge_1_name)

            else:
                self.canvas.setFont(self.configuration['bold_font'], title_size)
                self.canvas.drawCentredString(judge_1_position+signature_width/2, judge_1_height, judge_title_text + ": ")

            if blank_undefined(scoring_template['judge_2']) != "Undefined" and self.get_filter()[11]==0:
                judge_2_name = text_truncate(
                    scoring_template['judge_2']['name'],
                    signature_width - judge_title_width,
                    self.configuration['font'],
                    title_size
                )
                judge_2_width = stringWidth(judge_2_name, self.configuration['font'], title_size)

                title_position_judge_2 = judge_2_position + signature_width/2 - (judge_title_width + judge_2_width)/2
                name_position_judge_2 = title_position_judge_2 + judge_title_width

                self.canvas.setFont(self.configuration['bold_font'], title_size)
                self.canvas.drawString(title_position_judge_2, judge_2_height, judge_title_text + ": ")

                self.canvas.setFont(self.configuration['font'], title_size)
                self.canvas.drawString(name_position_judge_2, judge_2_height, judge_2_name)

            current_height = max(judge_1_height, judge_2_height)

        # **Print judges signatures**
            if self.arguments.get("empty", 0) == 0:
                current_height += distance_between_lines

                max_print_height = signature_maximum_height
                max_print_width = signature_width

                if blank_undefined(configuration.association_stamp) != "Undefined":
                    association_stamp_directory,association_stamp_image = load_image(configuration.association_stamp)
                    if association_stamp_directory is not None:
                        association_stamp_width, association_stamp_height = association_stamp_image.size

                        ratio = min(
                            max_print_width * 1.0 / association_stamp_width * 1.0,
                            max_print_height * 1.0 / association_stamp_height * 1.0
                        )

                        self.canvas.drawImage(
                            association_stamp_directory,
                            association_stamp_position + signature_width * 0.5 - association_stamp_width * ratio * 0.5,
                            current_height,
                            height = association_stamp_height * ratio,
                            width = association_stamp_width * ratio
                        )
                    else:
                        association_stamp_height = 3 * distance_between_lines
                else:
                    association_stamp_height = 3 * distance_between_lines

                if blank_undefined(scoring_template["judge_1"]) != "Undefined":
                    signature_judge_1_directory, signature_judge_1_image = load_image(scoring_template["judge_1"]['uploaded_signature'])
                    if signature_judge_1_directory is not None:
                        signature_judge_1_width, signature_judge_1_height = signature_judge_1_image.size

                        ratio = min(
                            max_print_width * 1.0 / signature_judge_1_width * 1.0,
                            max_print_height * 1.0 / signature_judge_1_height * 1.0
                        )

                        self.canvas.drawImage(
                            signature_judge_1_directory,
                            judge_1_position + signature_width * 0.5 - signature_judge_1_width * ratio * 0.5,
                            current_height,
                            height = signature_judge_1_height * ratio,
                            width = signature_judge_1_width * ratio
                        )
                    else:
                        if blank_undefined(scoring_template["judge_1"]['signature']) != "Undefined":
                            signature_judge_1 = scoring_template["judge_1"]['signature']
                            signature_judge_1_width = max_print_width
                            signature_judge_1_height = max_print_height
                            self.canvas.drawImage(
                                signature_judge_1,
                                judge_1_position + signature_width * 0.5 - signature_judge_1_width * 0.5,
                                current_height,
                                height=signature_judge_1_height,
                                width=signature_judge_1_width,
                                mask='auto'
                            )
                        else:
                            signature_judge_1_height = 3 * distance_between_lines
                else:
                    signature_judge_1_height = 3 * distance_between_lines

                if blank_undefined(scoring_template["judge_2"]) != "Undefined":
                    signature_judge_2_directory, signature_judge_2_image = load_image(scoring_template["judge_2"]['uploaded_signature'])
                    if signature_judge_2_directory is not None:
                        signature_judge_2_width, signature_judge_2_height = signature_judge_2_image.size

                        ratio = min(
                            max_print_width * 1.0 / signature_judge_2_width * 1.0,
                            max_print_height * 1.0 / signature_judge_2_height * 1.0
                        )

                        self.canvas.drawImage(
                            signature_judge_2_directory,
                            signature_judge_2_width * ratio,
                            current_height,
                            height = signature_judge_2_height * ratio,
                            width = judge_2_position + signature_width * 0.5 - signature_judge_2_width * ratio * 0.5
                        )
                    else:
                        if blank_undefined(scoring_template["judge_2"]['signature']) != "Undefined":
                            signature_judge_2 = scoring_template["judge_2"]['signature']
                            signature_judge_2_width = max_print_width
                            signature_judge_2_height = max_print_height
                            self.canvas.drawImage(
                                signature_judge_2,
                                judge_2_position + signature_width * 0.5 - signature_judge_2_width * 0.5,
                                current_height,
                                height = signature_judge_2_height,
                                width = signature_judge_2_width,
                                mask = 'auto'
                            )
                        else:
                            signature_judge_2_height = 3 * distance_between_lines
                else:
                    signature_judge_2_height = 3 * distance_between_lines
            else:
                association_stamp_height = signature_judge_1_height = signature_judge_2_height = 3 * distance_between_lines

            signatures_print_height = current_height + max(association_stamp_height, signature_judge_1_height, signature_judge_2_height) + distance_between_lines

            # **Print header**
            current_height = height - draw_header(self) - self.configuration['header_titles_gap']

            # **Print Group Description**
            group_description_in_lines = split_text_in_lines(
                scoring_template["group_description"],
                printable_width,
                self.configuration['bold_font'],
                title_size * 1.5
            )

            self.canvas.setFont(self.configuration['font'], title_size * 1.5)
            for line in group_description_in_lines:
                current_height -= distance_between_lines * 1.6
                self.canvas.drawCentredString(
                    configuration.left_margin * cm + self.configuration['extra_horizontal_text_offset'] + printable_width/2,
                    current_height,
                    line
                )

            # **Print Registration Description**
            if scoring_template["group_description"] != scoring_template["registration_description"]:
                registration_description_in_lines = split_text_in_lines(
                    scoring_template["registration_description"],
                    printable_width,
                    self.configuration['font'],
                    title_size * 1.2
                )

                self.canvas.setFont(self.configuration['font'], title_size * 1.2)
                for line in registration_description_in_lines:
                    current_height -= distance_between_lines * 1.3
                    self.canvas.drawCentredString(
                        configuration.left_margin * cm + self.configuration['extra_horizontal_text_offset'] + printable_width/2,
                        current_height,
                        line
                    )

            current_height -= title_lines_offset + distance_between_lines * 1.2

            # **Print Group Code and Cage Number line**
            cage_start_position = concept_column_position + concept_column_width/2

            self.canvas.setFont(self.configuration['bold_font'], title_size)
            self.canvas.drawString(
                horizontal_start_position,
                current_height + title_lines_offset,
                group_title_text + ": " + scoring_template['group_code']
            )
            if self.get_filter()[12]==0:
                self.canvas.drawString(
                    cage_start_position,
                    current_height + title_lines_offset,
                    cage_title_text + ": " + scoring_template['cage_number']
                )
            else:
                self.canvas.drawString(
                    cage_start_position,
                    current_height + title_lines_offset,
                    cage_title_text + ": "
                )

            # **PRINT MARKS TABLE**
            # **Print Punctuation table column titles**
            table_initial_height = current_height

            self.canvas.saveState()
            self.canvas.setFillColor(first_level_color)
            self.canvas.rect(punctuation_A_column_position, current_height, punctuation_column_width, distance_between_lines, fill=1)
            self.canvas.rect(punctuation_B_column_position, current_height, punctuation_column_width, distance_between_lines, fill=1)
            self.canvas.rect(punctuation_C_column_position, current_height, punctuation_column_width, distance_between_lines, fill=1)
            self.canvas.rect(punctuation_D_column_position, current_height, punctuation_column_width, distance_between_lines, fill=1)
            self.canvas.restoreState()

            self.canvas.setFont(self.configuration['bold_font'], title_size)
            self.canvas.drawCentredString(punctuation_A_column_position + 0.5 * punctuation_column_width, current_height + title_lines_offset, A_title_text)
            self.canvas.drawCentredString(punctuation_B_column_position + 0.5 * punctuation_column_width, current_height + title_lines_offset, B_title_text)
            self.canvas.drawCentredString(punctuation_C_column_position + 0.5 * punctuation_column_width, current_height + title_lines_offset, C_title_text)
            self.canvas.drawCentredString(punctuation_D_column_position + 0.5 * punctuation_column_width, current_height + title_lines_offset, D_title_text)

            # **Print positive marks title row**
            current_height -= rows_height
            if len(scoring_template["negative_scores"]) > 0:
                current_height = punctuation_line_draw(
                    current_height,
                    positive_concepts_title_text,
                    fill_background=True,
                    combine_columns=True,
                    bold_title=True
                )

            # **Print positive marks**
            for score in scoring_template["positive_scores"]:
                current_height = punctuation_line_draw(
                    current_height,
                    score['concept_name'],
                    max_value=score['max_value'],
                    scores=score['marks'][:scoring_template['team_size']]
                )

            # **Print negative marks title row and negative marks**
            if len(scoring_template["negative_scores"]) > 0:
                current_height = punctuation_line_draw(
                    current_height,
                    negative_concepts_title_text,
                    fill_background=True,
                    combine_columns=True,
                    bold_title=True
                )
                for score in scoring_template["negative_scores"]:
                    current_height = punctuation_line_draw(
                        current_height,
                        score['concept_name'],
                        max_value=score['max_value'],
                        scores=score['marks'][:scoring_template['team_size']]
                    )

            #**Print specimens total row**
            current_height = punctuation_line_draw(
                current_height,
                total_title_text,
                scores=(
                    scoring_template["total_a"],
                    scoring_template["total_b"],
                    scoring_template["total_c"],
                    scoring_template["total_d"],
                )[:scoring_template['team_size']],
                fill_background=True,
                bold_title=True
            )

            if scoring_template["team_size"] > 1:
                # **Print team marks sum row**
                current_height = punctuation_line_draw(
                    current_height,
                    total_team_title_text,
                    scores=scoring_template["sum_mark"],
                    fill_background=True,
                    bold_title=True
                )

                # **Print harmony row**
                current_height = punctuation_line_draw(
                    current_height,
                    harmony_title_text,
                    scores=scoring_template["harmony"],
                    fill_background=True,
                    bold_title=True
                )

            # **Print tie break marks row**
            current_height = punctuation_line_draw(
                current_height,
                tie_break_mark_title_text,
                scores=scoring_template["tie_break_mark"],
                fill_background=True,
                bold_title=True
            )

            # **Print total row**
            current_height = punctuation_line_draw(
                current_height,
                total_points_title_text,
                scores=scoring_template["total_mark"],
                fill_background=True,
                bold_title=True
            )

            # **PRINT OBSERVATION TABLE**
            # **Print observation borders**
            self.canvas.saveState()
            self.canvas.setFillColor(first_level_color)
            self.canvas.rect(
                concept_column_position,
                current_height,
                concept_column_width + 5 * punctuation_column_width,
                distance_between_lines,
                fill=1
            )
            self.canvas.restoreState()

            if scoring_template["specimen_image"] is None:
                observation_table_height = min(observations_maximum_height, current_height - signatures_print_height)

                self.canvas.rect(
                    concept_column_position,
                    current_height,
                    concept_column_width + 5 * punctuation_column_width,
                    -observation_table_height
                )
                observation_end_height = current_height - observation_table_height
            else:
                observation_table_height = current_height - signatures_print_height

                self.canvas.rect(
                    concept_column_position,
                    current_height,
                    concept_column_width + 5 * punctuation_column_width,
                    -observation_table_height
                )
                observation_end_height = current_height - observation_table_height

            # **Print observation title**
            self.canvas.setFont(self.configuration['bold_font'], title_size)
            self.canvas.drawString(
                concept_column_position + table_extra_margin,
                current_height + title_lines_offset,
                _('OBSERVATIONS')
            )

            # **Print observation content**
            if self.arguments.get("empty", 0) == 0 and blank_undefined(scoring_template["observations"]) != "Undefined":
                observations_lines = split_text_in_lines(
                    scoring_template["observations"],
                    concept_column_width + 5 * punctuation_column_width - 2 * table_extra_margin,
                    self.configuration['font'],
                    punctuation_text_size,
                    multiline=True
                )

                self.canvas.setFont(self.configuration['font'], punctuation_text_size)
                while current_height > signatures_print_height and len(observations_lines) > 0:
                    current_height -= distance_between_lines_in_table
                    self.canvas.drawString(
                        concept_column_position + table_extra_margin,
                        current_height,
                        observations_lines.pop(0)
                    )

            # **PRINT REGISTRATION TABLE**
            # **Print registration borders**
            current_height = table_initial_height

            self.canvas.saveState()
            self.canvas.setFillColor(first_level_color)
            self.canvas.rect(
                observation_column_position,
                current_height,
                observation_column_width,
                distance_between_lines,
                fill=1
            )
            self.canvas.restoreState()
            self.canvas.rect(
                observation_column_position,
                current_height,
                observation_column_width,
                distance_between_lines
            )
            self.canvas.rect(
                observation_column_position,
                current_height,
                observation_column_width,
                -1.5 * distance_between_lines
            )

            # **Print registration title**
            self.canvas.setFont(self.configuration['bold_font'], title_size)
            self.canvas.drawString(
                observation_column_position + table_extra_margin,
                current_height + title_lines_offset,
                _("REGISTRATION")
            )

            # **Print registration**
            current_height -= 1.25 * distance_between_lines
            if self.arguments.get("empty", 0) == 0 and self.arguments.get("wo_exhibitor", 0) == 0:
                self.canvas.setFont(self.configuration['bold_font'], title_size)
                self.canvas.drawCentredString(
                    observation_column_position + observation_column_width/2,
                    current_height + title_lines_offset,
                    scoring_template["registration"]
                )
            current_height -= 1.25 * distance_between_lines

            # **PRINT EXHIBITOR TABLE**
            exhibitor_name_in_lines = split_text_in_lines(
                scoring_template["exhibitor"],
                observation_printable_width,
                self.configuration['font'],
                observations_text_size
            )

            # **Print exhibitor borders**
            self.canvas.saveState()
            self.canvas.setFillColor(first_level_color)
            self.canvas.rect(
                observation_column_position,
                current_height,
                observation_column_width,
                distance_between_lines,
                fill=1
            )
            self.canvas.restoreState()
            self.canvas.rect(
                observation_column_position,
                current_height,
                observation_column_width,
                -(3.5+len(exhibitor_name_in_lines)) * distance_between_lines
            )

            # **Print exhibitor title**
            self.canvas.setFont(self.configuration['bold_font'], title_size)
            self.canvas.drawString(observation_column_position + table_extra_margin, current_height + title_lines_offset, _("EXHIBITOR"))

            # **Print exhibitor information**
            if self.arguments.get("empty", 0) == 0 and self.arguments.get("wo_exhibitor", 0) == 0:
                self.canvas.setFont(self.configuration['font'], observations_text_size)
                for line in exhibitor_name_in_lines:
                    current_height -= distance_between_lines
                    self.canvas.drawString(observation_column_position + table_extra_margin, current_height + title_lines_offset, line)

                rings_list = (
                    blank_custom(scoring_template["ring_a"], "*"),
                    blank_custom(scoring_template["ring_b"], "*"),
                    blank_custom(scoring_template["ring_c"], "*"),
                    blank_custom(scoring_template["ring_d"], "*"),
                )

                bc_text = scoring_template["bc"]
                rings_text = "/".join(rings_list[:scoring_template['team_size']])

                bc_title = text_truncate(
                    _("BC"),
                    observation_printable_width - stringWidth(":" + bc_text, self.configuration['font'], observations_text_size),
                    self.configuration['font'],
                    observations_text_size
                ) + ": "
                rings_title = text_truncate(
                    _("Rings"),
                    observation_printable_width - stringWidth(":" + rings_text, self.configuration['font'], observations_text_size),
                    self.configuration['font'],
                    observations_text_size
                ) + ": "

                current_height -= distance_between_lines
                self.canvas.setFont(self.configuration['bold_font'], observations_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin,
                    current_height + title_lines_offset,
                    bc_title
                )
                self.canvas.setFont(self.configuration['font'], observations_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin + stringWidth(bc_title, self.configuration['bold_font'], observations_text_size),
                    current_height + title_lines_offset,
                    bc_text
                )

                current_height -= distance_between_lines
                self.canvas.setFont(self.configuration['bold_font'], observations_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin,
                    current_height + title_lines_offset,
                    rings_title
                )
                self.canvas.setFont(self.configuration['font'], observations_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin + stringWidth(rings_title, self.configuration['bold_font'], observations_text_size),
                    current_height + title_lines_offset,
                    rings_text
                )

            else:
                current_height -= (2+len(exhibitor_name_in_lines)) * distance_between_lines

            # **Print Prize information**
            if blank_undefined(scoring_template["prize"]) != "Undefined" and self.arguments.get("empty", 0) == 0:
                prize_text = scoring_template["prize"]

                prize_title = text_truncate(
                    _("Record Book"),
                    observation_printable_width - stringWidth(":" + prize_text, self.configuration['font'], observations_text_size * 1.5),
                    self.configuration['font'],
                    observations_text_size
                ) + ": "

                current_height -= distance_between_lines * 1.25
                self.canvas.setFont(self.configuration['bold_font'], observations_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin,
                    current_height + title_lines_offset,
                    prize_title
                )

                self.canvas.setFont(self.configuration['font'], observations_text_size*1.5)
                self.canvas.saveState()
                self.canvas.setFillColor(prize_text_color)
                self.canvas.setStrokeColor(prize_text_color)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin + stringWidth(prize_title, self.configuration['bold_font'], observations_text_size),
                    current_height + title_lines_offset,
                    prize_text
                )
                self.canvas.restoreState()
            else:
                current_height -= distance_between_lines * 1.25

            # **PRINT JUDGING INFORMATION**
            # **Print judging information borders**
            current_height -= distance_between_lines

            self.canvas.saveState()
            self.canvas.setFillColor(first_level_color)
            self.canvas.rect(
                observation_column_position,
                current_height,
                observation_column_width,
                distance_between_lines,
                fill=1
            )
            self.canvas.restoreState()

            self.canvas.rect(
                observation_column_position,
                observation_end_height,
                observation_column_width,
                current_height - observation_end_height
            )

            # **Print judging information title**
            self.canvas.setFont(self.configuration['bold_font'], title_size)
            self.canvas.drawString(
                observation_column_position + table_extra_margin,
                current_height + title_lines_offset,
                text_truncate(_('JUDGING INFORMATION'), observation_printable_width, self.configuration['bold_font'], title_size)
            )

            # **Print judging information date**
            if blank_undefined(scoring_template["judgment_date"]) != "Undefined" and self.arguments.get("empty", 0) == 0:
                judgment_date_text = scoring_template["judgment_date"].strftime('%d/%m/%Y')
            else:
                judgment_date_text = ""

            judgment_date_title = text_truncate(
                _("Judgement Date"),
                observation_printable_width - stringWidth(": " + judgment_date_text, self.configuration['font'], punctuation_text_size),
                self.configuration['bold_font'],
                punctuation_text_size
            ) + ": "
            judgment_date_title_width = stringWidth(judgment_date_title, self.configuration['bold_font'], punctuation_text_size)

            current_height -= distance_between_lines
            self.canvas.setFont(self.configuration['bold_font'], punctuation_text_size)
            self.canvas.drawString(
                observation_column_position + table_extra_margin,
                current_height + title_lines_offset,
                judgment_date_title
            )
            self.canvas.setFont(self.configuration['font'], punctuation_text_size)
            self.canvas.drawString(
                observation_column_position + table_extra_margin + judgment_date_title_width,
                current_height + title_lines_offset,
                judgment_date_text
            )

            # **Print judging information Red Factor**
            if scoring_template["red_factor"] == 1:
                red_factor_text = _("Allowed")
                red_factor_title = text_truncate(
                    _("Red Factor"),
                    observation_printable_width - stringWidth(": " + red_factor_text, self.configuration['font'], punctuation_text_size),
                    self.configuration['bold_font'],
                    punctuation_text_size
                ) + ": "
                red_factor_title_width = stringWidth(red_factor_title, self.configuration['bold_font'], punctuation_text_size)

                current_height -= distance_between_lines
                self.canvas.setFont(self.configuration['bold_font'], punctuation_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin,
                    current_height + title_lines_offset,
                    red_factor_title
                )
                self.canvas.setFont(self.configuration['font'], punctuation_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin + red_factor_title_width,
                    current_height + title_lines_offset,
                    red_factor_text
                )

            # **Print judging information Size**
            if blank_undefined(scoring_template["size"]) != "Undefined":
                size_text = scoring_template["size"]
                size_title = text_truncate(
                    _("Size"),
                    observation_printable_width - stringWidth(": " + size_text, self.configuration['font'], punctuation_text_size),
                    self.configuration['bold_font'],
                    punctuation_text_size
                ) + ": "
                size_title_width = stringWidth(size_title, self.configuration['bold_font'], punctuation_text_size)

                current_height -= distance_between_lines
                self.canvas.setFont(self.configuration['bold_font'], punctuation_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin,
                    current_height + title_lines_offset,
                    size_title
                )
                self.canvas.setFont(self.configuration['font'], punctuation_text_size)
                self.canvas.drawString(
                    observation_column_position + table_extra_margin + size_title_width,
                    current_height + title_lines_offset,
                    size_text
                )

            # **Print judging information Specimen Image**
            if blank_undefined(scoring_template["specimen_image"]) != "Undefined":
                specimen_image_directory, specimen_image = load_image(scoring_template["specimen_image"])
                if specimen_image_directory is not None:
                    max_specimen_image_width = observation_printable_width
                    max_specimen_image_height = current_height-signatures_print_height - 2*table_extra_margin
                    specimen_image_width, specimen_image_height = specimen_image.size

                    ratio = min(
                        max_specimen_image_width * 1.0 / specimen_image_width * 1.0,
                        max_specimen_image_height * 1.0 / specimen_image_height * 1.0
                    )

                    self.canvas.drawImage(
                        specimen_image_directory,
                        observation_column_position + observation_column_width * 0.5 - specimen_image_width * ratio * 0.5,
                        signatures_print_height + (current_height - signatures_print_height) * 0.5 - specimen_image_height * ratio * 0.5,
                        height = specimen_image_height * ratio,
                        width = specimen_image_width * ratio
                    )

            # **Imprimir texte del estatus

            if self.arguments.get("empty", 0) == 0:
                if scoring_template["judgment_status"] != "Pending":
                    self.canvas.saveState()

                    self.canvas.setStrokeColor(colors.red)
                    self.canvas.setLineWidth(3)
                    self.canvas.setFillColor(colors.red)
                    self.canvas.setFont(self.configuration['bold_font'], 30)

                    status = status_extraction(scoring_template["status"], scoring_template["location_status"])
                    if status != "Judjable":
                        self.canvas.line(width/2 - 10 + 35*width/height + 100, height/2 + 135, width/2 - 10 + 35*width/height, height/2 + 35)
                        self.canvas.line(width/2 + 10 + 35*width/height + 100, height/2 + 135, width/2 + 10 + 35*width/height, height/2 + 35)
                        self.canvas.line(width/2 - 10 - 7*width/height, height/2 - 5, width/2 - 10 - 7*width/height - 100, height/2 - 5 - 100)
                        self.canvas.line(width/2 + 10 - 7*width/height, height/2 - 5, width/2 + 10 - 7*width/height - 100, height/2 - 5 - 100)
                        self.canvas.drawCentredString(width/2, height/2 + 7, _(status))

                    self.canvas.restoreState()
                else:
                    self.canvas.saveState()

                    self.canvas.setStrokeColor(black_transparent)
                    self.canvas.setFillColor(black_transparent)

                    self.canvas.setFont(self.configuration['bold_font'], 30)
                    self.canvas.drawCentredString(width/2, height/2 + 7, _("Pending for Judgement"))

                    self.canvas.restoreState()

            # **Print footer**
            draw_footer(self, 1)
            self.canvas.showPage()
