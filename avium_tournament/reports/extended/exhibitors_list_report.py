# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import none_empty, blank_undefined
from avium_tournament.reports.table_reportlab import drawpdf
from collections import OrderedDict
from frappe import _
from reportlab.lib.colors import HexColor


class ExtendedExhibitorsListReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Common configuration values
        CONF_TABLE = {
            'report_name': _("Exhibitors List Report"),
            'levels': 0,
            'columns_description': (
                {
                    'key': 'name',
                    'column_title': _('Name'),
                    'title_alignment': 'Left',
                    'column_width': 0.23,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'bc',
                    'column_title': _('BC'),
                    'column_width': 0.07,
                    'alignment': 'Center',
                }, {
                    'key': 'address',
                    'column_title': _('Address'),
                    'column_width': 0.30,
                    'alignment': 'Left',
                    'truncate': True,
                }, {
                    'key': 'telephone',
                    'column_title': _('Telephone'),
                    'column_width': 0.10,
                    'alignment': 'Center',
                }, {
                    'key': 'email',
                    'column_title': _('Email'),
                    'title_alignment': 'Left',
                    'column_width': 0.30,
                    'alignment': 'Left',
                    'truncate': True,
                },
            ),
        }

        super(ExtendedExhibitorsListReport, self).__init__(CONF_TABLE, arguments, canvas)

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration,
                    reg.exhibitor_name as name,
                    reg.exhibitor_surname as surname,
                    exbr.breeder_code as bc,
                    reg.address as address,
                    reg.postal_code as postal_code,
                    reg.city as city,
                    reg.territory as territory,
                    reg.telephone as telephone,
                    reg.mobile_phone as mobile_phone,
                    reg.email as email,
                    reg.allow as allow
            FROM `tabRegistration` as reg
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            WHERE exbr.idx = 1
            ORDER BY surname ASC, name ASC
            """, as_dict=True)

        data = OrderedDict()

        for item in raw_data:
            if item['registration'] not in data:
                data[item['registration']] = dict()
            data[item['registration']]['name'] = item['surname'] + ", " + item['name']
            data[item['registration']]['bc'] = item['bc']

            if item['allow'] == "Yes":
                address_text_list = list()
                if blank_undefined(item['postal_code']) != "Undefined":
                    address_text_list.append(item['postal_code'].title())
                if blank_undefined(item['city']) != "Undefined":
                    address_text_list.append(item['city'].title())
                if blank_undefined(item['territory']) != "Undefined":
                    address_text_list.append(item['territory'].title())
                data[item['registration']]['address'] = (
                    none_empty(item['address']).title(),
                    ", ".join(address_text_list)
                )
                data[item['registration']]['telephone'] = (
                    none_empty(item['telephone']),
                    none_empty(item['mobile_phone'])
                )
                data[item['registration']]['email'] = none_empty(item['email']).lower()

        self.data = {
            "_": {
                "_": data
            }
        }

        if self.arguments.get('total', "Yes") == "Yes":
            self.configuration['totals'] = dict()
            self.configuration['totals']['bc'] = len(data)

    def gen_pdf(self):
    
        return drawpdf(self)