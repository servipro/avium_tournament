# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

class LeaguePunctuationsReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        CONF_TABLE = {
            'report_name': _("League Punctuations Report"),
            'levels': 2,
            'columns_description': (
                {
                    'key': 'breeder_code',
                    'column_title': _('Breeder'),
                    'column_width': 0.40,
                }, {
                    'key': 'ring',
                    'column_title': _('Ring'),
                    'column_width': 0.30,
                    'alignment': 'Right',
                    'offset': 75,
                }, {
                    'key': 'punctuation',
                    'column_title': _('Punctuation'),
                    'column_width': 0.30,
                    'alignment': 'Right',
                    'offset': 75,
                },
            ),
        }

        super(LeaguePunctuationsReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_order(self):
        return self.arguments.get('filter', "Exhibitor")

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    ireg.name as registration,
                    ireg.league_inscription_a as league_inscription_a,
                    'No' as league_inscription_b,
                    'No' as league_inscription_c,
                    'No' as league_inscription_d,
                    ireg.ring_a as ring_a,
                    NULL as ring_b,
                    NULL as ring_c,
                    NULL as ring_d,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    1 as team_size
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            WHERE exbr.idx = 1 AND ireg.league_inscription_a = 'Yes'
            UNION ALL
            SELECT  reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    exbr.breeder_code as bc,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    treg.name as registration,
                    treg.league_inscription_a as league_inscription_a,
                    treg.league_inscription_b as league_inscription_b,
                    treg.league_inscription_c as league_inscription_c,
                    treg.league_inscription_d as league_inscription_d,
                    treg.ring_a as ring_a,
                    treg.ring_b as ring_b,
                    treg.ring_c as ring_c,
                    treg.ring_d as ring_d,
                    scote.total_a as total_a,
                    scote.total_b as total_b,
                    scote.total_c as total_c,
                    scote.total_d as total_d,
                    tgro.team_size as team_size
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            WHERE exbr.idx = 1 AND (treg.league_inscription_a = 'Yes' OR treg.league_inscription_b = 'Yes' OR treg.league_inscription_c = 'Yes' OR treg.league_inscription_d = 'Yes')
            ORDER BY
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN group_code END ASC,
                CASE WHEN %(order)s = 'Group' THEN group_code END ASC,
                CASE WHEN %(order)s = 'Group' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Group' THEN exhibitor_name END ASC
            """,
            {'order': self.get_order()},
            as_dict = True
        )

        data = OrderedDict()

        for item in raw_data:

            if self.arguments['filter'] == "Group":
                first_level_key = item['group_code'] + ' - ' + item['group_description']
                second_level_key = item['exhibitor_surname'] + ', ' + item['exhibitor_name']
            else:
                first_level_key = item['exhibitor_surname'] + ', ' + item['exhibitor_name']
                second_level_key = item['group_code'] + ' - ' + item['group_description']
            content_key_a = item['registration'] + "A"
            content_key_b = item['registration'] + "B"
            content_key_c = item['registration'] + "C"
            content_key_d = item['registration'] + "D"

            if first_level_key not in data:
                data[first_level_key] = OrderedDict()
            if second_level_key not in data[first_level_key]:
                data[first_level_key][second_level_key] = OrderedDict()
            if item['league_inscription_a'] == "Yes":
                data[first_level_key][second_level_key][content_key_a] = dict()
                data[first_level_key][second_level_key][content_key_a]['breeder_code'] = item['bc']
                data[first_level_key][second_level_key][content_key_a]['ring'] = item['ring_a']
                data[first_level_key][second_level_key][content_key_a]['punctuation'] = item['total_a']
            if item['team_size'] > 1 and item['league_inscription_b'] == "Yes":
                data[first_level_key][second_level_key][content_key_b] = dict()
                data[first_level_key][second_level_key][content_key_b]['breeder_code'] = item['bc']
                data[first_level_key][second_level_key][content_key_b]['ring'] = item['ring_b']
                data[first_level_key][second_level_key][content_key_b]['punctuation'] = item['total_b']
            if item['team_size'] > 2 and item['league_inscription_c'] == "Yes":
                data[first_level_key][second_level_key][content_key_c] = dict()
                data[first_level_key][second_level_key][content_key_c]['breeder_code'] = item['bc']
                data[first_level_key][second_level_key][content_key_c]['ring'] = item['ring_c']
                data[first_level_key][second_level_key][content_key_c]['punctuation'] = item['total_c']
            if item['team_size'] > 3 and item['league_inscription_d'] == "Yes":
                data[first_level_key][second_level_key][content_key_d] = dict()
                data[first_level_key][second_level_key][content_key_d]['breeder_code'] = item['bc']
                data[first_level_key][second_level_key][content_key_d]['ring'] = item['ring_d']
                data[first_level_key][second_level_key][content_key_d]['punctuation'] = item['total_d']

        self.data = data

    def gen_pdf(self):
    
        return drawpdf(self)