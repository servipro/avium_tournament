# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from os import path, sep

import frappe
import re

from frappe import _
from PIL import Image
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth, registerFont

# **Font import function**
def importFonts():
    font = 'Helvetica'
    bold_font = 'Helvetica-Bold'
    return font, bold_font
#     fonts_directory = path.dirname(path.abspath(__file__)) + sep + 'Fonts'
#
#     registerFont(TTFont('FreeSerif', path.join(fonts_directory, 'FreeSerif.ttf')))
#     registerFont(TTFont('FreeSerifBold', path.join(fonts_directory, 'FreeSerifBold.ttf')))
#     registerFont(TTFont('FreeSerifItalic', path.join(fonts_directory, 'FreeSerifItalic.ttf')))
#     registerFont(TTFont('FreeSerifBoldItalic', path.join(fonts_directory, 'FreeSerifBoldItalic.ttf')))

# **Header printing function**
def draw_header(report):
    # ** Set file title
    report.canvas.setTitle(report.configuration["report_name"])

    # ** Variables initialization
    configuration = report.configuration["report_configuration"]

    # ** Print only header in the first page if Simplified reports is selected
    if configuration.name == "Report Configuration" and configuration.only_first_page == 1 and report.canvas.getPageNumber() > 1:
        return configuration.upper_margin * cm

    # ** Variables initialization
    width, height = report.canvas._pagesize

    header_height = 75

    tournament_name_relative_height_four_lines = 20
    association_name_relative_height_four_lines = 35
    tournament_city_relative_height_four_lines = 50
    document_name_relative_height_four_lines = 65

    tournament_name_relative_height_three_lines = 27.5
    association_name_relative_height_three_lines = 42.5
    document_name_relative_height_three_lines = 57.5

    # **External rectangle**
    report.canvas.saveState()
    report.canvas.rect(
        configuration.left_margin * cm,
        height - configuration.upper_margin * cm - header_height,
        width - configuration.left_margin * cm - configuration.right_margin * cm,
        header_height,
        fill=0
    )

    # **Logos printing**
    logos_height = min((configuration.logo_size, 65))

    # Load left logo
    if configuration.left_logo == "Federation Logo":
        logo_1_directory,logo_1 = load_image(report.configuration["federation_logo"])
    elif configuration.left_logo == "Association Logo":
        logo_1_directory,logo_1 = load_image(report.configuration["association_logo"])
    elif configuration.left_logo == "Judge Association Logo":
        logo_1_directory,logo_1 = load_image(report.configuration["judge_association_logo"])
    elif configuration.left_logo == "Logo 1":
        logo_1_directory,logo_1 = load_image(configuration.logo_1)
    elif configuration.left_logo == "Logo 2":
        logo_1_directory,logo_1 = load_image(configuration.logo_2)
    else:
        logo_1_directory,logo_1 = None,None

    # Print left logo
    if logo_1_directory is not None:
        logo1_width, logo1_height = logo_1.size
        logo1_width = logos_height * logo1_width / logo1_height
        report.canvas.drawImage(
            logo_1_directory,
            configuration.left_margin * cm + 5,
            height - configuration.upper_margin * cm - header_height/2 - logos_height/2 - 1,
            height=logos_height,
            width=logo1_width
        )

    # Load right logo
    if configuration.right_logo == "Federation Logo":
        logo_2_directory,logo_2 = load_image(report.configuration["federation_logo"])
    elif configuration.right_logo == "Association Logo":
        logo_2_directory,logo_2 = load_image(report.configuration["association_logo"])
    elif configuration.right_logo == "Judge Association Logo":
        logo_2_directory,logo_2 = load_image(report.configuration["judge_association_logo"])
    elif configuration.right_logo == "Logo 1":
        logo_2_directory,logo_2 = load_image(configuration.logo_1)
    elif configuration.right_logo == "Logo 2":
        logo_2_directory,logo_2 = load_image(configuration.logo_2)
    else:
        logo_2_directory,logo_2 = None,None

    # Print right logo
    if logo_2_directory is not None:
        logo2_width, logo2_height = logo_2.size
        logo2_width = logos_height * logo2_width / logo2_height
        report.canvas.drawImage(
            logo_2_directory,
            width - configuration.right_margin * cm - logo2_width - 5,
            height - configuration.upper_margin * cm - header_height/2 - logos_height/2 - 1,
            height=logos_height,
            width=logo2_width
        )

    # **Print header texts**
    if report.configuration["tournament_city"] != "":
        report.canvas.setFont(report.configuration["bold_font"], 12)
        report.canvas.drawCentredString(configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm) / 2,
                                        height - configuration.upper_margin * cm - tournament_name_relative_height_four_lines,
                                        report.configuration["tournament_name"]
                                        )
        report.canvas.setFont(report.configuration["font"], 12)
        report.canvas.drawCentredString(configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm) / 2,
                                        height - configuration.upper_margin * cm - association_name_relative_height_four_lines,
                                        report.configuration["association_name"]
                                        )
        report.canvas.setFont(report.configuration["font"], 11)
        report.canvas.drawCentredString(configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm) / 2,
                                        height - configuration.upper_margin * cm - tournament_city_relative_height_four_lines,
                                        report.configuration["tournament_city"]
                                        )
        report.canvas.setFont(report.configuration["font"], 11)
        report.canvas.drawCentredString(configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm) / 2,
                                        height - configuration.upper_margin * cm - document_name_relative_height_four_lines,
                                        report.configuration["report_name"]
                                        )
    else:
        report.canvas.setFont(report.configuration["bold_font"], 12)
        report.canvas.drawCentredString(configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm) / 2,
                                        height - configuration.upper_margin * cm - tournament_name_relative_height_three_lines,
                                        report.configuration["tournament_name"]
                                        )
        report.canvas.setFont(report.configuration["font"], 12)
        report.canvas.drawCentredString(configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm) / 2,
                                        height - configuration.upper_margin * cm - association_name_relative_height_three_lines,
                                        report.configuration["association_name"]
                                        )
        report.canvas.setFont(report.configuration["font"], 11)
        report.canvas.drawCentredString(configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm) / 2,
                                        height - configuration.upper_margin * cm - document_name_relative_height_three_lines,
                                        report.configuration["report_name"]
                                        )

    report.canvas.restoreState()

    return configuration.upper_margin * cm + header_height

# **Footer printing function**
def draw_footer(report, totalPagines=1, print_line_page=True):

    # ** Variables initialization
    configuration = report.configuration["report_configuration"]

    width, height = report.canvas._pagesize

    # **Print footer line**
    if print_line_page:
        report.canvas.line(
            configuration.left_margin * cm,
            configuration.lower_margin * cm + configuration.footer_height * cm - 4,
            width - configuration.right_margin * cm,
            configuration.lower_margin * cm + configuration.footer_height * cm - 4
        )

    # **Print footer texts**
    footer_text_size = min(configuration.footer_text_size, configuration.footer_height * cm - 1)

    report.canvas.setFont(report.configuration["font"], footer_text_size)

    report.canvas.drawString(
        configuration.left_margin * cm,
        configuration.lower_margin * cm + configuration.footer_height * cm / 2 - footer_text_size * 0.3,
        _('Document generated with AVIUM')
    )

    # ** Print report name if Simplified reports is selected
    if configuration.name == "Report Configuration" and configuration.no_report_name == 0:
        report.canvas.drawCentredString(
            configuration.left_margin * cm + (width - configuration.left_margin * cm - configuration.right_margin * cm ) / 2.0,
            configuration.lower_margin * cm + configuration.footer_height * cm / 2 - footer_text_size * 0.3,
            report.configuration["report_name"].upper()
        )

    # ** Print page number
    if configuration.name == "Report Configuration" and configuration.no_page_number == 0:
        report.canvas.drawRightString(
            width - configuration.right_margin * cm,
            configuration.lower_margin * cm + configuration.footer_height * cm / 2 - footer_text_size * 0.3,
            _('Page') + ' ' + str(report.canvas.getPageNumber())
        )

# **Sorting function by letter and number**
def ordenarLletraNumero(data):

    dataSplit = []
    dataSorted = []
    counter = 0

    if len(data)<1:
        return data

    for i in data:
        dataSplit.append((re.sub('[0-9]{2,}', '', i), re.findall('[0-9]{2,}', i), counter))
        counter += 1

    dataSplitSortedNumero = sorted(dataSplit, key=lambda xifra: int(xifra[1][0]))

    dataSplitSortedLletraNumero = sorted(dataSplitSortedNumero, key=lambda lletra: lletra[0])

    for i in dataSplitSortedLletraNumero:
        dataSorted.append(data[i[2]])

    return dataSorted

# **Sorting function by number and letter**
def ordenarNumeroLletra(data):

    dataSplit = []
    dataSorted = []
    counter = 0

    if len(data)<1:
        return data

    for i in data:
        dataSplit.append((re.sub('[0-9]{2,}', '', i), re.findall('[0-9]{2,}', i), counter))
        counter += 1

    dataSplitSortedLletra = sorted(dataSplit, key=lambda lletra: lletra[0])

    dataSplitSortedNumeroLletra = sorted(dataSplitSortedLletra, key=lambda xifra: int(xifra[1][0]))

    for i in dataSplitSortedNumeroLletra:
        dataSorted.append(data[i[2]])

    return dataSorted

# **Text truncating function**
def text_truncate(text, text_width, font, size):

    if text is None or text=="" or stringWidth(text, font, size) <= text_width:
        return text

    truncated_text = ''

    for letter_index in range(len(text)):

        if stringWidth(text[:letter_index+1], font, size) <= text_width - 2.25:
            truncated_text += text[letter_index]
        else:
            if truncated_text != '' and truncated_text[-1] != ' ':
                truncated_text += '.'
            break

    return truncated_text

# **Split text in lines**
def split_text_in_lines(text, lines_width, font, size, multiline=False):

    lines = []
    line = ''

    if text is None or text == "":
        return text

    if stringWidth(text, font, size) <= lines_width:
        lines.append(text)
        return lines

    words = text.split()
    if multiline:
        words = text.split(" ")
        #Split by new line
        words = list(map(lambda word: word.split('\n'), words))
        #Make the array flat
        words = [item for sublist in words for item in sublist]

    for word in words:
        if multiline and word == '':
            lines.append(line)
            lines.append("")
            line=""
            continue

        if line == '':
            line += word
        else:
            if stringWidth(line + ' ' + word, font, size) <= lines_width:
                line += ' ' + word
            else:
                lines.append(line)
                line = word

    lines.append(line)

    return lines

# **Load image**
def load_image(data):
    try:
        image_directory = frappe.get_site_path("public") + "/" + str(data)
        image = Image.open(image_directory)
    except:
        try:
            image_directory = frappe.get_site_path() + "/" + str(data)
            image = Image.open(image_directory)
        except:
            image_directory = None
            image = None
    return image_directory, image

# **Return custom text if None or ""**
def blank_custom(field, text):
    if field is None or field=="":
        return text
    else:
        return field

# **Return "Undefined" if None or ""**
def blank_undefined(field):
    return_value = blank_custom(field, "Undefined")
    return return_value

# **Return "Undefined" if None**
def none_undefined(field):
    if field is None:
        return "Undefined"
    else:
        return field

# **Return "" if None**
def none_empty(field):
    if field is None:
        return ""
    else:
        return field

# **Return status abbreviations**
def status_extraction(status, location_status=None, abbreviation=False):
    if location_status is not None:
        if "Not Presented" in location_status:
            status = list(status)
            status.append("Not Presented")
    if not abbreviation:
        if "Not Judjable" in status:
            return "Not Judjable"
        elif "Declassified" in status:
            return "Declassified"
        elif "Declassified Puntuated" in status:
            return "Declassified Puntuated"
        elif "Disqualified" in status:
            return "Disqualified"
        elif "Disqualified Puntuated" in status:
            return "Disqualified Puntuated"
        elif "Not Presented" in status:
            return "Not Presented"
        elif "Did Not Sing" in status:
            return "Did Not Sing"
        elif "Insuficient Sing" in status:
            return "Insuficient Sing"
        else:
            return "Judjable"
    else:
        if "Not Judjable" in status:
            return "NJ"
        elif "Declassified" in status:
            return "De"
        elif "Declassified Puntuated" in status:
            return "DeP"
        elif "Disqualified" in status:
            return "Di"
        elif "Disqualified Puntuated" in status:
            return "DiP"
        elif "Not Presented" in status:
            return "NP"
        elif "Did Not Sing" in status:
            return "NS"
        elif "Insuficient Sing" in status:
            return "ISe"
        else:
            return "Judjable"

# **Return prizes description by assignment order**
def get_sorted_prizes(arguments):
    # Prizes data
    if arguments["prize"] == "1":
        prize_description = "description_1"
    elif arguments["prize"] == "2":
        prize_description = "description_2"
    else:
        prize_description = "description_3"

    prizes_raw_data = frappe.db.sql("""
        SELECT  touprides.description_1 as description_1,
                touprides.description_2 as description_2,
                touprides.description_3 as description_3,
                touprides.assignment_order as assignment_order
        FROM `tabTournament Prizes Description` as touprides
        ORDER BY assignment_order ASC
        """,
        as_dict=True
    )

    prizes_list = list()
    for prize in prizes_raw_data:
        if prize[prize_description] not in prizes_list:
            prizes_list.append(none_empty(prize[prize_description]))

    return prizes_list

