# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict

from avium_tournament.reports.commonFunctions import blank_undefined
from frappe import _
from avium_tournament.reports.table_reportlab import drawpdf

class JudgesReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        def print_modification_function(print_value):
            print_value_string = str(print_value)
            decimal_position = print_value_string.find(".")
            return str(print_value)[:decimal_position+2] + " %"

        if arguments.get('breakdown', 0) == 1:
            levels = 1
            first_column_key = 'group_description'
            first_column_title = _("Group Description")
        else:
            levels = 0
            first_column_key = 'judge_name'
            first_column_title = _("Judge Name")

        CONF_TABLE = {
            'report_name': _("Judges Report"),
            'levels': levels,
            'columns_description': (
                {
                    'key': first_column_key,
                    'column_title': first_column_title,
                    'title_alignment': 'Left',
                    'column_width': 0.40,
                    'alignment': 'Left',
                    'in_lines': True,
                }, {
                    'key': 'pending',
                    'column_title': _('Pending'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 10,
                    'total': True,
                }, {
                    'key': 'tied',
                    'column_title': _('Tied'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 10,
                    'total': True,
                }, {
                    'key': 'error',
                    'column_title': _('Error'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 10,
                    'total': True,
                }, {
                    'key': 'judged',
                    'column_title': _('Judged'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 10,
                    'total': True,
                }, {
                    'key': 'percentage',
                    'print_modification': print_modification_function,
                    'column_title': _('Percentage'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 5,
                    'total': True,
                }, {
                    'key': 'specimens',
                    'column_title': _('Specimens'),
                    'column_width': 0.10,
                    'alignment': 'Right',
                    'offset': 10,
                    'total': True,
                },
            ),
        }

        super(JudgesReport, self).__init__(CONF_TABLE, arguments, canvas)

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  jud.judge_name as judge_name,
                    tgro.individual_code as group_code,
                    tgro.description as group_description,
                    scote.status as status,
                    1 as specimens
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabJudge` as jud on jud.name = scote.judge_1
            UNION ALL
            SELECT  jud.judge_name as judge_name,
                    tgro.team_code as group_code,
                    tgro.description as group_description,
                    scote.status as status,
                    tgro.team_size as specimens
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabJudge` as jud on jud.name = scote.judge_1
            ORDER BY group_code ASC, judge_name ASC
            """, as_dict=True)

        total_judged_scoring_templates = 0
        total_assigned_scoring_templates = 0

        if self.arguments.get('breakdown', 0) == 1:
            data = OrderedDict()

            for item in raw_data:
                judge_name = blank_undefined(item['judge_name'])
                if judge_name not in data:
                    data[judge_name] = dict()
                    data[judge_name]["_"] = OrderedDict()
                if item['group_code'] not in data[judge_name]["_"]:
                    data[judge_name]["_"][item['group_code']] = dict()
                    data[judge_name]["_"][item['group_code']]['group_description'] = item['group_code'] + " - " + item['group_description']
                    data[judge_name]["_"][item['group_code']]['pending'] = 0
                    data[judge_name]["_"][item['group_code']]['tied'] = 0
                    data[judge_name]["_"][item['group_code']]['error'] = 0
                    data[judge_name]["_"][item['group_code']]['judged'] = 0
                    data[judge_name]["_"][item['group_code']]['specimens'] = 0
                if item['status'] == "Judged":
                    data[judge_name]["_"][item['group_code']]['judged'] += 1
                    total_judged_scoring_templates += 1
                elif item['status'] == "Tied":
                    data[judge_name]["_"][item['group_code']]['tied'] += 1
                elif item['status'] == "Error":
                    data[judge_name]["_"][item['group_code']]['error'] += 1
                else:
                    data[judge_name]["_"][item['group_code']]['pending'] += 1
                data[judge_name]["_"][item['group_code']]['specimens'] += item['specimens']
                total_assigned_scoring_templates += 1

                judged_scoring_templates = data[judge_name]["_"][item['group_code']]['judged']
                assigned_scoring_templates = data[judge_name]["_"][item['group_code']]['judged'] + data[judge_name]["_"][item['group_code']]['tied'] + data[judge_name]["_"][item['group_code']]['error'] + data[judge_name]["_"][item['group_code']]['pending']
                data[judge_name]["_"][item['group_code']]['percentage'] = (judged_scoring_templates * 100.0) / (assigned_scoring_templates * 1.0)

            self.data = data
        else:
            data = OrderedDict()

            for item in raw_data:
                judge_name = blank_undefined(item['judge_name'])
                if judge_name not in data:
                    data[judge_name] = dict()
                    data[judge_name]['judge_name'] = blank_undefined(judge_name)
                    data[judge_name]['pending'] = 0
                    data[judge_name]['tied'] = 0
                    data[judge_name]['error'] = 0
                    data[judge_name]['judged'] = 0
                    data[judge_name]['specimens'] = 0
                if item['status'] == "Judged":
                    data[judge_name]['judged'] += 1
                    total_judged_scoring_templates += 1
                elif item['status'] == "Tied":
                    data[judge_name]['tied'] += 1
                elif item['status'] == "Error":
                    data[judge_name]['error'] += 1
                else:
                    data[judge_name]['pending'] += 1
                data[judge_name]['specimens'] += item['specimens']
                total_assigned_scoring_templates += 1

                judged_scoring_templates = data[judge_name]['judged']
                assigned_scoring_templates = data[judge_name]['judged'] + data[judge_name]['tied'] + data[judge_name]['error'] + data[judge_name]['pending']
                data[judge_name]['percentage'] = (judged_scoring_templates * 100.0) / (assigned_scoring_templates * 1.0)

            self.data = {
                "_": {
                    "_": data
                }
            }

        # **Calculate total judged percentage**
        if total_assigned_scoring_templates > 0:
            self.configuration['columns_description'][5]['total_value'] = (total_judged_scoring_templates * 100.0) / (total_assigned_scoring_templates * 1.0)
        else:
            self.configuration['columns_description'][5]['total_value'] = 0

    def gen_pdf(self):
    
        return drawpdf(self)