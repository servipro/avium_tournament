# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

class GroupsListReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        text_size = 8
        distance_between_lines = 11

        CONF_TABLE = {
            'report_name': _("Groups List Report"),
            'header_titles_gap': 10,
            'levels': 1,
            'tables_per_page': 2,
            'text_size': text_size,
            'title_size': 8,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * text_size) / 2 + 0.4,
            'columns_description': (
                {
                    'key': 'team',
                    'column_title': _('Team'),
                    'title_alignment': 'Left',
                    'column_width': 0.15,
                    'alignment': 'Left',
                }, {
                    'key': 'individual',
                    'column_title': _('Individual'),
                    'title_alignment': 'Left',
                    'column_width': 0.15,
                    'alignment': 'Left',
                }, {
                    'key': 'group_description',
                    'column_title': _('Group Description'),
                    'title_alignment': 'Left',
                    'column_width': 0.7,
                    'alignment': 'Left',
                    'in_lines': True,
                },
            ),
        }

        super(GroupsListReport, self).__init__(CONF_TABLE, arguments, canvas)

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  fami.code as family_code,
                    fami.family_name as family_name,
                    tgro.team_code as team,
                    tgro.individual_code as individual,
                    tgro.description as group_description
            FROM `tabTournament Group` as tgro
            LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
            LEFT JOIN `tabJudging Template Specialisation` as jutespe on jutespe.specialisation_name = tgro.description
            ORDER BY
                fami.printing_order ASC,
                IF(tgro.individual_code IS NOT NULL AND CHAR_LENGTH(tgro.individual_code)>0, tgro.individual_code, tgro.team_code) ASC
            """, as_dict=True)

        data = OrderedDict()
        
        for item in raw_data:
            family_description = item['family_code'] + " - " + item['family_name']
            if family_description not in data:
                data[family_description] = dict()
                data[family_description]["_"] = OrderedDict()
            if item['group_description'] not in data[family_description]["_"]:
                data[family_description]["_"][item['group_description']] = dict()
            data[family_description]["_"][item['group_description']]['team'] = item['team']
            data[family_description]["_"][item['group_description']]['individual'] = item['individual']
            data[family_description]["_"][item['group_description']]['group_description'] = item['group_description']

        self.data = data

    def gen_pdf(self):
        return drawpdf(self)