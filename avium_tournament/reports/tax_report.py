# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.utils import get_tournament_value, get_taxes
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import none_empty
from frappe import _
from reportlab.lib.colors import HexColor
from avium_tournament.reports.table_reportlab import drawpdf

class TaxReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        CONF_TABLE = {
            'report_name': _("Tax Report"),
            'levels': 1,
            'tables_per_page': 2,
            'columns_description': (
                {
                    'key': 'group',
                    'column_title': _('Group'),
                    'title_alignment': 'Center',
                    'column_width': 0.20,
                }, {
                    'key': 'cage',
                    'column_title': _('Cage'),
                    'title_alignment': 'Center',
                    'column_width': 0.20,
                }, {
                    'key': 'tax_1',
                    'column_title': _('Tax 1'),
                    'title_alignment': 'Center',
                    'column_width': 0.20,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'tax_2',
                    'column_title': _('Tax 2'),
                    'title_alignment': 'Center',
                    'column_width': 0.20,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'tax',
                    'column_title': _('Tax'),
                    'title_alignment': 'Center',
                    'column_width': 0.20,
                    'alignment': 'Right',
                    'offset': 5,
                },
            ),
        }

        super(TaxReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):
        return (
            self.arguments.get('registration', None),
            self.arguments.get('taxed', 0),
        )

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.exhibitor_name as name,
                    reg.exhibitor_surname as surname,
                    tgro.individual_code as group_code,
                    ireg.name as id,
                    ireg.registration_description as description,
                    1 as team_size,
                    scote.cage_number as cage_number,
                    ireg.tax_a as tax_a,
                    NULL as tax_b,
                    NULL as tax_c,
                    NULL as tax_d,
                    ireg.taxed_a as taxed_a,
                    NULL as taxed_b,
                    NULL as taxed_c,
                    NULL as taxed_d
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = ireg.group
            WHERE
                CASE WHEN %(registration)s != 'Undefined' THEN reg.name = %(registration)s ELSE TRUE END AND
                ireg.tax_a > 0
            UNION ALL
            SELECT  reg.exhibitor_name as name,
                    reg.exhibitor_surname as surname,
                    tgro.team_code as group_code,
                    treg.name as id,
                    treg.registration_description as description,
                    tgro.team_size as team_size,
                    scote.cage_number as cage_number,
                    treg.tax_a as tax_a,
                    treg.tax_b as tax_b,
                    treg.tax_c as tax_c,
                    treg.tax_d as tax_d,
                    treg.taxed_a as taxed_a,
                    treg.taxed_b as taxed_b,
                    treg.taxed_c as taxed_c,
                    treg.taxed_d as taxed_d
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabTournament Group` as tgro on tgro.name = treg.group
            WHERE
                CASE WHEN %(registration)s != 'Undefined' THEN reg.name = %(registration)s ELSE TRUE END AND
                treg.tax_a + treg.tax_b + treg.tax_c + treg.tax_d > 0
            ORDER BY
                surname ASC, name ASC, group_code ASC, cage_number ASC
            """,
            {
                'registration': self.get_filter()[0],
            },
            as_dict = True
        )

        data = OrderedDict()
        old_exhibitor = None
        total_tax_1 = 0
        total_tax_2 = 0
        total_tax = 0

        tournament_data = get_tournament_value(['extra_tax', 'add_deduct_tax', 'fix_percentage_tax'])
        extra_tax = tournament_data['extra_tax']
        add_deduct_tax = tournament_data['add_deduct_tax']
        fix_percentage_tax = tournament_data['fix_percentage_tax']

        for item in raw_data:

            if self.get_filter()[1] == "Only Taxed" and not "Yes" in [item['taxed_a'], item['taxed_b'], item['taxed_c'], item['taxed_d']]:
                continue

            exhibitor = ", ".join([item['surname'].title(),item['name'].title()])

            if old_exhibitor!=exhibitor:
                if old_exhibitor is not None and len(data[old_exhibitor]["_"]) > 0:
                    data[old_exhibitor]["_"]["Total"] = dict()
                    data[old_exhibitor]["_"]["Total"]["cage"] = "Total"
                    data[old_exhibitor]["_"]["Total"]["tax_1"] = total_tax_1
                    data[old_exhibitor]["_"]["Total"]["tax_2"] = total_tax_2
                    data[old_exhibitor]["_"]["Total"]["tax"] = total_tax

                data[exhibitor]=dict()
                data[exhibitor]["_"]=OrderedDict()
                
                old_exhibitor = exhibitor
                total_tax_1 = 0
                total_tax_2 = 0
                total_tax = 0

            for iteration in range(item['team_size']):
                if item['team_size'] == 1:
                    identification = item['id']
                    cage_number = none_empty(item['cage_number'])
                    tax = float(item['tax_a'])
                    taxed = item['taxed_a']
                else:
                    if iteration == 0:
                        identification = item['id']+'A'
                        cage_number = none_empty(item['cage_number'])+'A'
                        tax = float(item['tax_a'])
                        taxed = item['taxed_a']
                    elif iteration == 1:
                        identification = item['id']+'B'
                        cage_number = none_empty(item['cage_number'])+'B'
                        tax = float(item['tax_b'])
                        taxed = item['taxed_b']
                    elif iteration == 2:
                        identification = item['id']+'C'
                        cage_number = none_empty(item['cage_number'])+'C'
                        tax = float(item['tax_c'])
                        taxed = item['taxed_c']
                    else:
                        identification = item['id']+'D'
                        cage_number = none_empty(item['cage_number'])+'D'
                        tax = float(item['tax_d'])
                        taxed = item['taxed_d']

                if (self.get_filter()[1] == "All" or taxed == "Yes") and tax > 0:
                    data[exhibitor]["_"][identification] = dict()
                    data[exhibitor]["_"][identification]['group'] = item['group_code']
                    data[exhibitor]["_"][identification]['cage'] = cage_number.lstrip("0")
                    exhibitor_tax, tournament_tax, tax = get_taxes(tax, extra_tax, add_deduct_tax, fix_percentage_tax)
                    data[exhibitor]["_"][identification]['tax_1'] = exhibitor_tax
                    data[exhibitor]["_"][identification]['tax_2'] = tournament_tax 
                    data[exhibitor]["_"][identification]['tax'] = tax
                    total_tax_1 += exhibitor_tax
                    total_tax_2 += tournament_tax
                    total_tax += tax
                    
        if old_exhibitor is not None and len(data[old_exhibitor]["_"]) > 0:
            data[old_exhibitor]["_"]["Total"] = dict()
            data[old_exhibitor]["_"]["Total"]["cage"] = "Total"
            data[old_exhibitor]["_"]["Total"]["tax_1"] = total_tax_1
            data[old_exhibitor]["_"]["Total"]["tax_2"] = total_tax_2
            data[old_exhibitor]["_"]["Total"]["tax"] = total_tax

        self.data = data

    def gen_pdf(self):
    
        return drawpdf(self)