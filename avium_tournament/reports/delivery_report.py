# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from collections import OrderedDict
from avium_tournament.reports.commonFunctions import none_empty, blank_undefined
from frappe import _
from avium_tournament.reports.table_reportlab import drawpdf

class DeliveryReport(BaseReport):

    def __init__(self, arguments, canvas=None):
        distance_between_lines = 25
        text_size = 11

        CONF_TABLE = {
            'report_name': _("Delivery Report"),
            'levels': 0,
            'title_size': 10,
            'text_size': text_size,
            'distance_between_lines': distance_between_lines,
            'lines_offset': (distance_between_lines - 0.75 * text_size) / 2 + 0.5,
            'content_horizontal_border': 0.8,
            'content_vertical_border': 0.8,
            'columns_description': (
                {
                    'key': 'registration',
                    'column_title': _('Registration'),
                    'column_width': 0.07,
                    'alignment': 'Right',
                    'offset': 5,
                }, {
                    'key': 'bc',
                    'column_title': _('BC'),
                    'column_width': 0.08,
                    'alignment': 'Left',
                }, {
                    'key': 'exhibitor',
                    'column_title': _('Exhibitor'),
                    'title_alignment': 'Left',
                    'column_width': 0.34,
                    'alignment': 'Left',
                    'in_lines': True,
                }, {
                    'key': 'books',
                    'column_title': _('Books'),
                    'column_width': 0.07,
                    'total': True,
                }, {
                    'key': 'scoring_templates',
                    'column_title': _('Scoring Templates'),
                    'column_width': 0.07,
                }, {
                    'key': 'medals',
                    'column_title': _('Medals'),
                    'column_width': 0.07,
                }, {
                    'key': 'retrieved_by',
                    'column_title': _('Retrieved By'),
                    'title_alignment': 'Left',
                    'column_width': 0.29,
                },
            ),
        }

        super(DeliveryReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_order(self):
        return self.arguments.get('sorted', "Exhibitor")

    def fetch_data(self):
        raw_data = frappe.db.sql("""
            SELECT  reg.name as registration_number,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.books as books,
                    reg.email as email,
                    exbr.breeder_code as bc,
                    touprides.assignment_order as prize
            FROM `tabIndividual Registration` as ireg
            LEFT JOIN `tabRegistration` as reg on reg.name = ireg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            JOIN `tabScoring Template` as scote on scote.registration = ireg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                exbr.idx = 1
            UNION ALL
            SELECT  reg.name as registration_number,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.books as books,
                    reg.email as email,
                    exbr.breeder_code as bc,
                    touprides.assignment_order as prize
            FROM `tabTeam Registration` as treg
            LEFT JOIN `tabRegistration` as reg on reg.name = treg.parent
            LEFT JOIN `tabExhibitor Breeder` as exbr on exbr.parent = reg.name
            JOIN `tabScoring Template` as scote on scote.registration = treg.name
            LEFT JOIN `tabTournament Prizes` as toupri on toupri.name = scote.prize
            LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
            WHERE
                exbr.idx = 1
            ORDER BY
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(order)s = 'Registration' THEN registration_number END ASC
            """,
            {'order': self.get_order()},
            as_dict = True
        )

        prizes_description = frappe.db.sql("""
            SELECT touprides.assignment_order as assignment_order
            FROM `tabTournament Prizes Description` as touprides
            ORDER BY assignment_order
            """,
            as_dict = False
        )

        prizes_cache = {
            prize[0]: prizes_description.index(prize)
            for prize
            in prizes_description
        }

        data = OrderedDict()

        total_prizes = [0]*len(prizes_description)
        total_medals = "-".join(map(str, total_prizes))

        for item in raw_data:
            if item['registration_number'] not in data:
                data[item['registration_number']] = dict()
                data[item['registration_number']]['registration'] = item['registration_number'].lstrip("REG0")
                data[item['registration_number']]['bc'] = item['bc']
                data[item['registration_number']]['exhibitor'] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
                data[item['registration_number']]['books'] = item['books']
                if blank_undefined(item['email']) != "Undefined":
                    data[item['registration_number']]['scoring_templates'] = "email"
                data[item['registration_number']]['prizes'] = [0]*len(prizes_description)
                data[item['registration_number']]['medals'] = "-".join(
                    map(str, data[item['registration_number']]['prizes'])
                )
            if blank_undefined(item['prize']) != "Undefined":
                data[item['registration_number']]['prizes'][prizes_cache[item['prize']]] += 1
                data[item['registration_number']]['medals'] = "-".join(
                    map(str, data[item['registration_number']]['prizes'])
                )
                total_prizes[prizes_cache[item['prize']]] += 1
                total_medals = "-".join(map(str, total_prizes))

        self.data = {
            "_": {
                "_": data
            }
        }

        self.configuration['totals'] = dict()
        self.configuration['totals']['medals'] = total_medals

    def gen_pdf(self):
    
        return drawpdf(self)