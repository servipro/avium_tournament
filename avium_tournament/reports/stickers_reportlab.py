# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm

def drawpdf(report, text_printing):
    # **Set file name**
    report.canvas.setTitle(report.configuration['report_name'])

    # **Variables initialization**
    width, height = A4

    configuration = report.configuration['report_configuration']

    current_column = max(min(report.arguments.get('column', 1), configuration.columns_number) - 1, 0)
    current_row = max(min(report.arguments.get('row', 1), configuration.rows_number) - 1, 0)

    printable_width = width - configuration.page_left_margin * cm - configuration.page_right_margin * cm
    printable_height = height - configuration.page_upper_margin * cm - configuration.page_lower_margin * cm

    gap_between_columns = (printable_width - configuration.columns_number * configuration.column_width * cm) / (configuration.columns_number - 1)
    gap_between_rows = (printable_height - configuration.rows_number * configuration.rows_height * cm) / (configuration.rows_number - 1)

    # **Prinit labels**
    for cage_data in report.data:
        # **New position calculation**
        horizontal_position = configuration.page_left_margin * cm + current_column * (configuration.column_width * cm + gap_between_columns)
        vertical_position = height - configuration.page_upper_margin * cm - configuration.rows_height * cm - current_row * (configuration.rows_height * cm + gap_between_rows)

        # **Draw border**
        if configuration.print_border == 1:
            report.canvas.rect(
                horizontal_position,
                vertical_position,
                configuration.column_width * cm,
                configuration.rows_height * cm,
                fill=0
            )

        # **Print label text**
        report.canvas = text_printing(report.canvas, cage_data, horizontal_position, vertical_position)

        # **Move to next position**
        if report.arguments.get('direction', "Horizontal") == "Horizontal":
            current_column += 1
            if current_column >= configuration.columns_number:
                current_row += 1
                current_column = 0
                if current_row >= configuration.rows_number:
                    report.canvas.showPage()
                    current_row = 0
        else:
            current_row += 1
            if current_row >= configuration.rows_number:
                current_column += 1
                current_row = 0
                if current_column >= configuration.columns_number:
                    report.canvas.showPage()
                    current_column = 0

    return report.canvas