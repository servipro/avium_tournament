# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

#**Table report configuration parameters**
#
#   'report_name'[string]: name of the report.
#   'levels'[int:0-2]: number of levels in the report.
#   'tables_per_page'[int]: number of tables in a page.
#   'gap_between_page_tables'[float]: distance between tables in a page.
#   'description_text'[dict or list dicts]: parameters to define the text to print over the title. If list, the list components will be the printed lines.
#      'font': font used for the description text.
#      'text_size': text size used for the description text.
#      'only_in_first_page'[bool]: if True, the text is printed only in the first page.
#      'print_modification'[function]: the return value will be printed in the column.
#      'text'[string]: text to print in the column titles.
#      'alignment'[string:Left-Center-Right]: alignment of the text.
#   'text_over_title'[list-dict]: parameters to define the text to print over the title.
#      'print_modification'[function]: the return value will be printed in the column.
#      'text'[string]: text to print in the column titles.
#      'alignment'[string:Left-Center-Right]: alignment of the text.
#      'position'[float]: normalized position of the text.
#   'columns_description'[list-dict]: parameters to define the columns.
#      'key'[string]: key used in the data dictionary to obtain the text to print.
#      'print_modification'[function]: the return value will be printed in the column.
#      'column_title'[string]: text to print in the column titles.
#      'title_alignment'[string:Left-Center-Right]: alignment of the column titles.
#      'column_width'[float]: normalized width of the column. sum of all columns should be 1.00.
#      'offset'[float]: displacement of the text according to the starting point.
#                       If alignment is Left or Center, positive offset moves text to the right.
#                       If alignment is Right, positive offset moves text to the left.
#      'alignment'[string:Left-Center-Right]: alignment of the text.
#      'truncate'[bool]: if True, the text is truncated according to the column width.
#      'in_lines'[bool]: if True, the text is divided in lines according to the column width.
#      'total'[bool]: if True, total will be printed at the end of the page.
#      'total_value'[float]: value to replace if the total is not a sum of the values.
#   'title_text_color'[color]: color, in html format, used to print text of the title.
#   'title_background'[color]: color, in html format, used to fill the background of the title.
#   'first_level_text_color'[color]: color, in html format, used to print text of the first level.
#   'first_level_background'[color]: color, in html format, used to fill the background in the first level.
#   'first_level_horizontal_border'[float]: width of the border between lines in the first level.
#   'first_level_vertical_border'[float]: width of the border between columns in the first level.
#   'first_level_truncate'[bool]: if True, the text is truncated according to the column width.
#   'first_level_in_lines'[bool]: if True, the first level texts is divided in lines according to the column width.
#   'first_level_total'[bool]: if True, total values will be printed on each first level.
#   'second_level_text_color'[color]: color, in html format, used to print text of the second level.
#   'second_level_background'[color]: color, in html format, used to fill the background in the second level.
#   'second_level_horizontal_border'[float]: width of the border between lines in the second level.
#   'second_level_vertical_border'[float]: width of the border between columns in the second level.
#   'second_level_truncate'[bool]: if True, the text is truncated according to the column width.
#   'second_level_in_lines'[bool]: if True, the second level texts is divided in lines according to the column width.
#   'second_level_total'[bool]: if True, total values will be printed on each first level.
#   'content_intermittent_background'[color]: color used to fill the background in the content.
#   'content_horizontal_border'[float]: width of the border between lines in the content.
#   'content_vertical_border'[float]: width of the border between columns in the content.
#   'totals'[dict]: strings to print in the total line. Keys used should be the defined in the 'columns_description'.
#   'first_level_new_page'[bool]: Each first level will start in a new page.

from frappe import _
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from avium_tournament.reports.commonFunctions import importFonts, draw_header, draw_footer, text_truncate, split_text_in_lines, none_empty
from reportlab.lib import colors

def drawpdf(report):

    if len(report.data.keys()) > 0:
        # **Variables initialization**
        importFonts()

        configuration = report.configuration["report_configuration"]

        width, height = A4

        current_page_column = 1
        current_page_column_position = configuration.left_margin*cm + report.configuration["extra_horizontal_text_offset"]
        page_tables = report.configuration.get('tables_per_page', 1)
        gap_between_page_tables = report.configuration.get('gap_between_page_tables', 5)

        page_printable_width = width - configuration.left_margin*cm - configuration.right_margin*cm - 2*report.configuration["extra_horizontal_text_offset"]
        printable_width = (width - configuration.left_margin*cm - configuration.right_margin*cm - 2*report.configuration["extra_horizontal_text_offset"] - (page_tables-1)*gap_between_page_tables) / page_tables

        column_widths = [printable_width * column['column_width'] for column in report.configuration['columns_description']]

        column_positions = list()
        for index, column_width in enumerate(column_widths):
            first_column_position = 0
            column_positions.append(first_column_position + sum(column_widths[0:index]))

        column_title_texts = list()
        for index, column_width in enumerate(column_widths):
            column_title_texts.append(
                text_truncate(report.configuration['columns_description'][index]['column_title'],
                              column_width - report.configuration["gap_between_columns"],
                              report.configuration["bold_font"],
                              report.configuration["title_size"]
                              )
            )

        totals = None
        for column_description in report.configuration['columns_description']:
            if column_description.get('total', False):
                if totals is None:
                    totals = dict()
                totals[column_description['key']] = 0

        first_level_totals = None
        if report.configuration.get('first_level_total', False):
            for column_description in report.configuration['columns_description']:
                if column_description.get('total', False):
                    if first_level_totals is None:
                        first_level_totals = dict()
                    first_level_totals[column_description['key']] = 0

        second_level_totals = None
        if report.configuration.get('second_level_total', False):
            for column_description in report.configuration['columns_description']:
                if column_description.get('total', False):
                    if second_level_totals is None:
                        second_level_totals = dict()
                    second_level_totals[column_description['key']] = 0

        content_background_fill = True

        # **Custom functions**
        def titles_print(current_height,current_page_column_position):
            report.canvas.saveState()

            report.canvas.setFont(report.configuration["bold_font"], report.configuration["title_size"])

            text_over_title = report.configuration.get('text_over_title', None)
            title_text_color = report.configuration.get('title_text_color', colors.black)
            title_background_color = report.configuration.get('title_background',None)

            report.canvas.saveState()
            if title_background_color is not None:
                report.canvas.setFillColor(title_background_color)
                report.canvas.setStrokeColor(title_background_color)
                if text_over_title is not None:
                    report.canvas.rect(
                        current_page_column_position,
                        current_height - report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                        printable_width,
                        report.configuration["distance_between_lines"] * 2.0,
                        stroke=0,
                        fill=1
                    )
                else:
                    report.canvas.rect(
                        current_page_column_position,
                        current_height - report.configuration["lines_offset"],
                        printable_width,
                        report.configuration["distance_between_lines"],
                        stroke=0,
                        fill=1
                    )

            report.canvas.setFillColor(title_text_color)
            report.canvas.setStrokeColor(title_text_color)

            if text_over_title is not None:
                for index, text in enumerate(text_over_title):
                    text_alignment = text.get('alignment', 'Center')
                    text_position = current_page_column_position + printable_width * text['position']

                    if text_alignment == 'Left':
                        report.canvas.drawString(
                            text_position,
                            current_height,
                            text['text']
                        )
                    elif text_alignment == 'Right':
                        report.canvas.drawRightString(
                            text_position,
                            current_height,
                            text['text']
                        )
                    else:
                        report.canvas.drawCentredString(
                            text_position,
                            current_height,
                            text['text']
                        )

                current_height -= report.configuration["distance_between_lines"]

            for index, column_title in enumerate(column_title_texts):
                title_alignment = report.configuration['columns_description'][index].get('title_alignment', 'Center')
                column_position = current_page_column_position + column_positions[index]
                column_width = column_widths[index]

                if title_alignment == 'Left':
                    report.canvas.drawString(
                        column_position + report.configuration["gap_between_columns"]/2.0,
                        current_height,
                        column_title
                    )
                elif title_alignment == 'Right':
                    report.canvas.drawRightString(
                        column_position + column_width - report.configuration["gap_between_columns"]/2.0,
                        current_height,
                        column_title
                    )
                else:
                    report.canvas.drawCentredString(
                        column_position + column_width/2.0,
                        current_height,
                        column_title
                    )

            report.canvas.restoreState()

            report.canvas.line(
                current_page_column_position,
                current_height - report.configuration["lines_offset"],
                current_page_column_position + printable_width,
                current_height - report.configuration["lines_offset"]
            )

            report.canvas.restoreState()
            return current_height

        def description_text_print(current_height):

            if report.configuration.get('description_text', None) is not None:

                report.canvas.saveState()

                for line in report.configuration['description_text']:

                    if not (line.get('only_in_first_page', False) and report.canvas.getPageNumber() > 1):

                        description_text_in_lines = split_text_in_lines(
                            line.get('text', ""),
                            page_printable_width,
                            line.get('font', "Helvetica"),
                            line.get('text_size', 9)
                        )

                        report.canvas.setFont(
                            line.get('font', "Helvetica"),
                            line.get('text_size', 9),
                        )

                        for text_line in description_text_in_lines:
                            if line.get('alignment', "Center") == 'Left':
                                report.canvas.drawString(
                                    configuration.left_margin * cm + report.configuration["extra_horizontal_text_offset"],
                                    current_height,
                                    text_line
                                )
                            elif line.get('alignment', "Center") == 'Right':
                                report.canvas.drawRightString(
                                    width - configuration.right_margin * cm - report.configuration["extra_horizontal_text_offset"],
                                    current_height,
                                    text_line
                                )
                            else:
                                report.canvas.drawCentredString(
                                    configuration.left_margin * cm + report.configuration["extra_horizontal_text_offset"] + page_printable_width / 2.0,
                                    current_height,
                                    text_line
                                )

                            current_height -= report.configuration["distance_between_lines"]

                report.canvas.restoreState()

                current_height -= report.configuration["distance_between_lines"] * 0.2

            return current_height

        def level_decription_print(current_height,level,description,current_page_column_position):
            report.canvas.saveState()
            # **Load level description printing configuration
            if level == 1:
                text_color = report.configuration.get('first_level_text_color', colors.black)
                background_color = report.configuration.get('first_level_background',None)
                horizontal_border_width = report.configuration.get('first_level_horizontal_border',None)
                vertical_border_width = report.configuration.get('first_level_vertical_border',None)
                truncate = report.configuration.get('first_level_truncate',False)
                in_lines = report.configuration.get('first_level_in_lines',False)
            elif level == 2:
                text_color = report.configuration.get('second_level_text_color', colors.black)
                background_color = report.configuration.get('second_level_background',None)
                horizontal_border_width = report.configuration.get('second_level_horizontal_border',None)
                vertical_border_width = report.configuration.get('second_level_vertical_border',None)
                truncate = report.configuration.get('second_level_truncate',False)
                in_lines = report.configuration.get('second_level_in_lines',False)
            else:
                text_color = report.configuration.get('title_text_color', colors.black)
                background_color = None
                vertical_border_width = None
                horizontal_border_width = None
                truncate = False
                in_lines = False

            # **Prepare description string according to configuration
            if truncate:
                description = (
                    text_truncate(
                        description,
                        printable_width - report.configuration["gap_between_columns"],
                        report.configuration["bold_font"],
                        report.configuration["title_size"]
                    ),
                )
            elif in_lines:
                description = split_text_in_lines(
                    description,
                    printable_width - report.configuration["gap_between_columns"],
                    report.configuration["bold_font"],
                    report.configuration["title_size"]
                )
            else:
                description = (description,)
            lines_to_print = len(description)

            # **Print background
            if background_color != None:
                report.canvas.saveState()
                report.canvas.setFillColor(background_color)
                report.canvas.rect(
                    current_page_column_position,
                    current_height - report.configuration["distance_between_lines"] * (len(description)-1) - report.configuration["lines_offset"],
                    printable_width,
                    report.configuration["distance_between_lines"] * len(description),
                    stroke=0,
                    fill=1
                )
                report.canvas.restoreState()

            # **Print horizontal border
            if horizontal_border_width != None:
                report.canvas.setLineWidth(horizontal_border_width)
                report.canvas.line(
                    current_page_column_position,
                    current_height - report.configuration["distance_between_lines"] * (lines_to_print-1) - report.configuration["lines_offset"],
                    current_page_column_position + printable_width,
                    current_height - report.configuration["distance_between_lines"] * (lines_to_print-1) - report.configuration["lines_offset"],
                )
                report.canvas.line(
                    current_page_column_position,
                    current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                    current_page_column_position + printable_width,
                    current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"]
                )

            # **Print vertical border
            if vertical_border_width != None:
                report.canvas.setLineWidth(vertical_border_width)
                report.canvas.line(
                    current_page_column_position,
                    current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                    current_page_column_position,
                    current_height - (lines_to_print-1)*report.configuration["distance_between_lines"] - report.configuration["lines_offset"]
                )
                report.canvas.line(
                    current_page_column_position + printable_width,
                    current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                    current_page_column_position + printable_width,
                    current_height - (lines_to_print-1)*report.configuration["distance_between_lines"] - report.configuration["lines_offset"]
                )

            # **Print level description text
            report.canvas.setFont(report.configuration["bold_font"], report.configuration["title_size"])
            report.canvas.setFillColor(text_color)
            report.canvas.setStrokeColor(text_color)
            for index, description_line in enumerate(description):
                report.canvas.drawString(
                    current_page_column_position + column_positions[0] + report.configuration["gap_between_columns"]/2.0,
                    current_height - index * report.configuration["distance_between_lines"],
                    description_line
                )

            report.canvas.restoreState()
            return current_height - (len(description)-1) * report.configuration["distance_between_lines"]

        def content_print(current_height,content,content_background_fill,current_page_column_position):
            report.canvas.saveState()
            # **Load level description printing configuration
            background_color = content.get('background',None)
            if background_color is None:
                background_color = report.configuration.get('content_intermittent_background',None)
            else:
                content_background_fill = True
            horizontal_border_width = report.configuration.get('content_horizontal_border',None)
            vertical_border_width = report.configuration.get('content_vertical_border',None)

            # **Prepare content to print according to configuration and check the lines to print
            lines_to_print = 1
            for index, column_description in enumerate(report.configuration['columns_description']):
                key = column_description.get('key', None)
                text = none_empty(content.get(key, ''))
                print_modification = column_description.get('print_modification', None)
                truncate = column_description.get('truncate', False)
                in_lines = column_description.get('in_lines', False)
                column_width = column_widths[index]
                offset = column_description.get('offset', 0)
                total = column_description.get('total', False)

                if total and type(text) in (int, float):
                    totals[key] += text
                if report.configuration.get('first_level_total', False) and total and type(text) in (int, float):
                    first_level_totals[key] += text
                if report.configuration.get('second_level_total', False) and total and type(text) in (int, float):
                    second_level_totals[key] += text

                if print_modification is None:
                    if type(text) in (int, float):
                        text = str(text)
                else:
                    text = print_modification(text)

                if (type(text) is str):
                    if truncate:
                        content[key] = (
                            text_truncate(
                                text,
                                column_width - report.configuration["gap_between_columns"] - offset,
                                report.configuration["font"],
                                report.configuration["text_size"]
                            ),
                        )
                    elif in_lines:
                        content[key] = split_text_in_lines(
                            text,
                            column_width - report.configuration["gap_between_columns"] - offset,
                            report.configuration["font"],
                            report.configuration["text_size"]
                        )
                    else:
                        content[key] = (text,)
                else:
                    if truncate:
                        content[key] = map(
                            lambda x: text_truncate(
                                x,
                                column_width - report.configuration["gap_between_columns"] - offset,
                                report.configuration["font"],
                                report.configuration["text_size"]
                            ),
                            text
                        )
                    else:
                        content[key] = text


                lines_to_print = max(lines_to_print,len(list(content[key])))

            # **Print background
            if background_color != None and content_background_fill:
                report.canvas.saveState()
                report.canvas.setFillColor(background_color)
                report.canvas.setStrokeColor(background_color)

                report.canvas.rect(
                    current_page_column_position,
                    current_height - (lines_to_print-1)*report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                    printable_width,
                    lines_to_print*report.configuration["distance_between_lines"],
                    stroke=0,
                    fill=1
                )
                report.canvas.restoreState()

            # **Print horizontal border
            if horizontal_border_width != None:
                report.canvas.setLineWidth(horizontal_border_width)
                report.canvas.line(
                    current_page_column_position,
                    current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                    current_page_column_position + printable_width,
                    current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"]
                )

            # **Print vertical border
            if vertical_border_width != None:
                report.canvas.setLineWidth(vertical_border_width)
                for column_position in column_positions:
                    report.canvas.line(
                        current_page_column_position + column_position,
                        current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                        current_page_column_position + column_position,
                        current_height - (lines_to_print-1)*report.configuration["distance_between_lines"] - report.configuration["lines_offset"]
                    )
                report.canvas.line(
                    current_page_column_position + printable_width,
                    current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                    current_page_column_position + printable_width,
                    current_height - (lines_to_print-1)*report.configuration["distance_between_lines"] - report.configuration["lines_offset"]
                )

            # **Print content text
            report.canvas.setFont(report.configuration["font"], report.configuration["text_size"])
            for index, column_description in enumerate(report.configuration['columns_description']):
                key = column_description.get('key', None)
                text = content[key]
                column_position = column_positions[index]
                column_width = column_widths[index]
                offset = column_description.get('offset', 0)
                alignment = column_description.get('alignment', 'Center')

                for line_index, text_line in enumerate(text):
                    if alignment == 'Left':
                        report.canvas.drawString(
                            current_page_column_position + column_position + offset + report.configuration["gap_between_columns"]/2.0,
                            current_height - line_index*report.configuration["distance_between_lines"],
                            text_line
                        )
                    elif alignment == 'Right':
                        report.canvas.drawRightString(
                            current_page_column_position + column_position + column_width - offset - report.configuration["gap_between_columns"]/2.0,
                            current_height - line_index*report.configuration["distance_between_lines"],
                            text_line
                        )
                    else:
                        report.canvas.drawCentredString(
                            current_page_column_position + column_position + column_width/2.0 + offset,
                            current_height - line_index*report.configuration["distance_between_lines"],
                            text_line
                        )

            report.canvas.restoreState()

            # **Return next content_background_fill value
            return current_height - (lines_to_print-1)*report.configuration["distance_between_lines"]

        def new_line(current_height, comparison_value, initial_height, current_page_column, current_page_column_position, content_background_fill=False):
            if (current_height - comparison_value) < (configuration.lower_margin * cm + configuration.footer_height * cm):
                # **Print horizontal border
                horizontal_border_width = report.configuration.get('content_horizontal_border',None)
                if horizontal_border_width != None:
                    current_height -= report.configuration["distance_between_lines"]
                    report.canvas.setLineWidth(horizontal_border_width)
                    report.canvas.line(
                        current_page_column_position,
                        current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                        current_page_column_position + printable_width,
                        current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"]
                    )

                if current_page_column < page_tables:
                    current_page_column += 1
                    current_page_column_position += printable_width + gap_between_page_tables

                    current_height = titles_print(initial_height, current_page_column_position)
                else:
                    draw_footer(report)
                    report.canvas.showPage()

                    current_page_column = 1
                    current_page_column_position = configuration.left_margin*cm + report.configuration["extra_horizontal_text_offset"]

                    # **Print header**
                    current_height = height - draw_header(report) - report.configuration["header_titles_gap"]

                    # **Print description text**
                    initial_height = current_height = description_text_print(current_height)

                    # **Print titles**
                    current_height = titles_print(current_height,current_page_column_position)

                return current_height - report.configuration["distance_between_lines"], current_page_column, current_page_column_position, initial_height, False
            else:
                current_height -= report.configuration["distance_between_lines"]
                return current_height, current_page_column, current_page_column_position, initial_height, not content_background_fill

        def calculate_lines_to_print(first_level_key=None,second_level_key=None,content_key=None):
            if report.configuration.get('levels', 0) > 0 and second_level_key is None and content_key is None:
                first_level_in_lines = report.configuration.get('first_level_in_lines', False)
                if not first_level_in_lines:
                    first_level_lines = 1
                else:
                    first_level_text = first_level_key
                    first_level_lines = len(
                        split_text_in_lines(
                            first_level_text,
                            printable_width - report.configuration["gap_between_columns"],
                            report.configuration["bold_font"],
                            report.configuration["title_size"]
                        )
                    )
            else:
                first_level_lines = 0

            if second_level_key is None:
                second_level_key = list(report.data[first_level_key].keys())[0]
            else:
                second_level_key = second_level_key
            if report.configuration.get('levels', 0) > 1 and content_key is None:
                second_level_in_lines = report.configuration.get('second_level_in_lines', False)
                if not second_level_in_lines:
                    second_level_lines = 1
                else:
                    if second_level_key is None:
                        second_level_text = report.data[first_level_key].keys()[0]
                    else:
                        second_level_text = second_level_key
                    second_level_lines = len(
                        split_text_in_lines(
                            second_level_text,
                            printable_width - report.configuration["gap_between_columns"],
                            report.configuration["bold_font"],
                            report.configuration["title_size"]
                        )
                    )
            else:
                second_level_lines = 0

            content_lines = 0
            if content_key is None:
                content_key = list(report.data[first_level_key][second_level_key].keys())[0]
            else:
                content_key = content_key
            for index, column_description in enumerate(report.configuration['columns_description']):
                content_in_lines = column_description.get('in_lines', False)
                if not content_in_lines:
                    content_lines = max(content_lines, 1)
                else:
                    item_lines = len(
                        split_text_in_lines(
                            report.data[first_level_key][second_level_key][content_key][column_description['key']],
                            column_widths[index] - report.configuration["gap_between_columns"],
                            report.configuration["font"],
                            report.configuration["text_size"]
                        )
                    )
                    content_lines = max(content_lines, item_lines)

            return first_level_lines + second_level_lines + content_lines

        def print_totals(current_height, initial_height, current_page_column, current_page_column_position, totals):

            current_height, current_page_column, current_page_column_position, initial_height, content_background_fill = new_line(
                current_height,
                report.configuration["distance_between_lines"],
                initial_height,
                current_page_column,
                current_page_column_position,
                content_background_fill=False
            )

            report.canvas.line(
                current_page_column_position,
                current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
                current_page_column_position + printable_width,
                current_height + report.configuration["distance_between_lines"] - report.configuration["lines_offset"],
            )

            report.canvas.saveState()
            report.canvas.setFont(report.configuration["bold_font"], report.configuration["title_size"])
            report.canvas.drawString(
                current_page_column_position + column_positions[0] + report.configuration["gap_between_columns"] / 2.0,
                current_height,
                _("Total")
            )

            for index, column_description in enumerate(report.configuration['columns_description']):
                total = column_description.get(
                    'total_value',
                    totals.get(column_description['key'], None)
                )
                if total is not None:
                    column_position = column_positions[index]
                    column_width = column_widths[index]
                    offset = column_description.get('offset', 0)
                    alignment = column_description.get('alignment', 'Center')
                    print_modification = column_description.get('print_modification', None)

                    if print_modification is None:
                        value_to_print = str(total)
                    else:
                        value_to_print = print_modification(total)

                    if alignment == 'Left':
                        report.canvas.drawString(
                            current_page_column_position + column_position + offset + report.configuration["gap_between_columns"]/2.0,
                            current_height,
                            value_to_print
                        )
                    elif alignment == 'Right':
                        report.canvas.drawRightString(
                            current_page_column_position + column_position + column_width - offset - report.configuration["gap_between_columns"]/2.0,
                            current_height,
                            value_to_print
                        )
                    else:
                        report.canvas.drawCentredString(
                            current_page_column_position + column_position + column_width/2.0 + offset,
                            current_height,
                            value_to_print
                        )
                    totals[column_description['key']] = 0

            report.canvas.restoreState()

        # **Print header**
        current_height = height - draw_header(report) - report.configuration["header_titles_gap"]

        # **Print description text**
        initial_height = current_height = description_text_print(current_height)

        # **Print titles**
        current_height = titles_print(current_height, current_page_column_position)

        # **Print content**
        for first_level_index, first_level_description in enumerate(report.data.keys()):

            if report.configuration.get('levels', 0) > 0:
                # Move height to the end of the page to force a page break
                if report.configuration.get('first_level_new_page', False) and first_level_index != 0:
                    if totals is not None:
                        print_totals(current_height, initial_height, current_page_column, current_page_column_position, totals)
                    current_height = 0
                    current_page_column = report.configuration.get('tables_per_page', 1)

                # Check number of lines to print
                lines_to_print = calculate_lines_to_print(
                    first_level_key=first_level_description
                )

                # Add line and print first level description
                current_height, current_page_column, current_page_column_position, initial_height, content_background_fill = new_line(
                    current_height,
                    lines_to_print * report.configuration["distance_between_lines"],
                    initial_height,
                    current_page_column,
                    current_page_column_position,
                    content_background_fill=False
                )
                current_height = level_decription_print(current_height, 1, first_level_description, current_page_column_position)

            for second_level_description in report.data[first_level_description].keys():

                if report.configuration.get('levels', 0) > 1:
                    # Check number of lines to print
                    lines_to_print = calculate_lines_to_print(
                        first_level_key=first_level_description,
                        second_level_key=second_level_description
                    )

                    # Add line and print second level description
                    current_height, current_page_column, current_page_column_position, initial_height, content_background_fill = new_line(
                        current_height,
                        lines_to_print * report.configuration["distance_between_lines"],
                        initial_height,
                        current_page_column,
                        current_page_column_position,
                        content_background_fill=False
                    )
                    current_height = level_decription_print(current_height, 2, second_level_description, current_page_column_position)

                for content_description in report.data[first_level_description][second_level_description].keys():
                    content = report.data[first_level_description][second_level_description][content_description]

                    # Check number of lines to print
                    lines_to_print = calculate_lines_to_print(
                        first_level_key=first_level_description,
                        second_level_key=second_level_description,
                        content_key=content_description
                    )

                    # Add line and print second level description
                    current_height, current_page_column, current_page_column_position, initial_height, content_background_fill = new_line(
                        current_height,
                        lines_to_print * report.configuration["distance_between_lines"],
                        initial_height,
                        current_page_column,
                        current_page_column_position,
                        content_background_fill=content_background_fill
                    )
                    current_height = content_print(current_height, content, content_background_fill, current_page_column_position)

                if report.configuration.get('second_level_total', False) and second_level_totals is not None:
                    print_totals(current_height, initial_height, current_page_column, current_page_column_position, second_level_totals)
                    for key in second_level_totals.keys():
                        second_level_totals[key] = 0
                    current_height, current_page_column, current_page_column_position, initial_height, content_background_fill = new_line(
                        current_height,
                        lines_to_print * report.configuration["distance_between_lines"],
                        initial_height,
                        current_page_column,
                        current_page_column_position,
                        content_background_fill=False
                    )

            if report.configuration.get('first_level_total', False) and first_level_totals is not None:
                print_totals(current_height, initial_height, current_page_column, current_page_column_position, first_level_totals)
                for key in first_level_totals.keys():
                    first_level_totals[key] = 0
                current_height, current_page_column, current_page_column_position, initial_height, content_background_fill = new_line(
                    current_height,
                    lines_to_print * report.configuration["distance_between_lines"],
                    initial_height,
                    current_page_column,
                    current_page_column_position,
                    content_background_fill=False
                )


        # Print total line
        totals_configurations = report.configuration.get('totals', None)
        if totals_configurations is not None:
            if totals is not None:
                totals = {
                    k: totals_configurations.get(k, totals.get(k))
                    for k
                    in list(totals_configurations.keys()) + list(totals.keys())
                }
            else:
                totals = totals_configurations
        if totals is not None:
            print_totals(current_height, initial_height, current_page_column, current_page_column_position, totals)

        draw_footer(report)

    return report.canvas