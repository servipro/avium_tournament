# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

import frappe

from avium_tournament.reports.BaseReport import BaseReport
from avium_tournament.reports.commonFunctions import none_empty, importFonts, split_text_in_lines, text_truncate, blank_undefined
from frappe import _
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth
from avium_tournament.reports.stickers_reportlab import drawpdf

class ExhibitorStickersReport(BaseReport):

    def __init__(self, arguments, canvas=None):

        # Get report_configuration doctype data
        raw_report_configuration_data = frappe.get_doc("Exhibitor Stickers Configuration", "Exhibitor Stickers Configuration")

        CONF_TABLE = {
            'report_name': _("Exhibitor Stickers"),
            'report_configuration': raw_report_configuration_data,
            'extra_vertical_text_offset': 5,
        }
        super(ExhibitorStickersReport, self).__init__(CONF_TABLE, arguments, canvas)

    def get_filter(self):

        start_exhibitor_value = self.arguments.get('start_exhibitor', None)
        if start_exhibitor_value is not None:
            start_registration_doctype = frappe.get_all(
                "Registration",
                filters = {"name": start_exhibitor_value},
                fields = {'exhibitor_name', 'exhibitor_surname'}
            )
            exhibitor_name = start_registration_doctype[0]['exhibitor_name']
            exhibitor_surname = start_registration_doctype[0]['exhibitor_surname']
            start_exhibitor_value = exhibitor_surname + ", " + exhibitor_name

        end_exhibitor_value = self.arguments.get('end_exhibitor', None)
        if end_exhibitor_value is not None:
            end_registration_doctype = frappe.get_all(
                "Registration",
                filters = {"name": end_exhibitor_value},
                fields = {'exhibitor_name', 'exhibitor_surname'}
            )
            exhibitor_name = end_registration_doctype[0]['exhibitor_name']
            exhibitor_surname = end_registration_doctype[0]['exhibitor_surname']
            end_exhibitor_value = exhibitor_surname + ", " + exhibitor_name

        return (
            blank_undefined(start_exhibitor_value),
            blank_undefined(end_exhibitor_value),
            blank_undefined(self.arguments.get('start_registration', None)),
            blank_undefined(self.arguments.get('end_registration', None)),
        )

    def get_order(self):
        return self.arguments.get('sorted', "Exhibitor")

    def fetch_data(self):
        raw_data = frappe.db.sql(""" 
            SELECT  reg.name as registration,
                    reg.exhibitor_name as exhibitor_name,
                    reg.exhibitor_surname as exhibitor_surname,
                    reg.address as address,
                    reg.postal_code as postal_code,
                    reg.city as city,
                    reg.territory as territory
            FROM `tabRegistration` as reg
            WHERE
                total_price > 0 AND
                CASE WHEN %(start_exhibitor)s != 'Undefined' AND %(order)s = 'Exhibitor' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) >= %(start_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(end_exhibitor)s != 'Undefined' AND %(order)s = 'Exhibitor' THEN CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name) <= %(end_exhibitor)s ELSE TRUE END AND
                CASE WHEN %(start_registration)s != 'Undefined' AND %(order)s = 'Registration' THEN reg.name >= %(start_registration)s ELSE TRUE END AND
                CASE WHEN %(end_registration)s != 'Undefined' AND %(order)s = 'Registration' THEN reg.name <= %(end_registration)s ELSE TRUE END
            ORDER BY
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_surname END ASC,
                CASE WHEN %(order)s = 'Exhibitor' THEN exhibitor_name END ASC,
                CASE WHEN %(order)s = 'Registration' THEN registration END ASC
            """,
            {
                'start_exhibitor': self.get_filter()[0],
                'end_exhibitor': self.get_filter()[1],
                'start_registration': self.get_filter()[2],
                'end_registration': self.get_filter()[3],
                'order': self.get_order(),
            },
            as_dict = True
        )

        self.data = list()

        for item in raw_data:
            registration_data = dict()
            registration_data['registration'] = item['registration'].lstrip("REG0")
            registration_data["name"] = item['exhibitor_surname'] + ", " + item['exhibitor_name']
            registration_data["address"] = none_empty(item['address'])

            city_texts_list = list()
            postal_code = str(none_empty(item['postal_code']))
            if postal_code != "":
                city_texts_list.append(postal_code)
            city = none_empty(item['city'])
            if city != "":
                city_texts_list.append(city)
            registration_data["city"] = ", ".join(city_texts_list)

            registration_data["territory"] = none_empty(item['territory'])
            self.data.append(registration_data)

    def gen_pdf(self):

        # **Variables initialization**
        importFonts()

        configuration = self.configuration['report_configuration']

        registration_text = text_truncate("Registration", 30, self.configuration['font'], configuration.text_size)

        label_printable_height = configuration.rows_height * cm - configuration.label_lower_margin * cm - configuration.label_upper_margin * cm
        label_printable_width = configuration.column_width * cm - configuration.label_left_margin * cm - configuration.label_right_margin * cm

        distance_between_lines = min(
            0.7 * configuration.text_size,
            max((label_printable_height - 4*configuration.text_size)/3.0, 0)
        )

        lines_offset = (distance_between_lines - 0.75 * configuration.text_size)/2.0 + 0.5

        label_middle_height = configuration.label_lower_margin * cm + label_printable_height / 2.0 + lines_offset

        # **Text position calculation**
        name_text_vertical_position = label_middle_height + 1.5 * distance_between_lines + configuration.text_size
        address_text_vertical_position = label_middle_height + 0.5 * distance_between_lines
        city_text_vertical_position = label_middle_height - 0.5 * distance_between_lines - configuration.text_size
        registration_text_vertical_position = territory_text_vertical_position = label_middle_height - 1.5 * distance_between_lines - 2 * configuration.text_size

        name_text_horizontal_position = configuration.label_left_margin * cm
        address_text_horizontal_position = configuration.label_left_margin * cm
        city_text_horizontal_position = configuration.label_left_margin * cm
        territory_text_horizontal_position = configuration.label_left_margin * cm
        registration_text_horizontal_position = configuration.column_width * cm - configuration.label_right_margin * cm

        def text_printing(canvas, label_data, horizontal_position, vertical_position):

            canvas.saveState()

            # **Print text**
            canvas.setFont(self.configuration['font'], configuration.text_size)
            canvas.drawString(
                horizontal_position + name_text_horizontal_position,
                vertical_position + name_text_vertical_position,
                text_truncate(label_data["name"], label_printable_width, self.configuration['font'], configuration.text_size)
            )
            canvas.drawString(
                horizontal_position + address_text_horizontal_position,
                vertical_position + address_text_vertical_position,
                text_truncate(label_data["address"], label_printable_width, self.configuration['font'], configuration.text_size)
            )
            canvas.drawString(
                horizontal_position + city_text_horizontal_position,
                vertical_position + city_text_vertical_position,
                text_truncate(label_data["city"], label_printable_width, self.configuration['font'], configuration.text_size)
            )
            canvas.drawString(
                horizontal_position + territory_text_horizontal_position,
                vertical_position + territory_text_vertical_position,
                text_truncate(label_data["territory"], label_printable_width, self.configuration['font'], configuration.text_size)
            )
            if self.arguments.get('registration',"Yes") == "Yes":
                canvas.setFont(self.configuration['bold_font'], configuration.text_size*1.5)
                canvas.drawRightString(
                    horizontal_position + registration_text_horizontal_position,
                    vertical_position + registration_text_vertical_position,
                    label_data["registration"]
                )

            canvas.restoreState()

            return canvas

        return drawpdf(self, text_printing)