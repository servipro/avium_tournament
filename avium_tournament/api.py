# coding=utf-8
import frappe
from frappe.desk.page.setup_wizard.setup_wizard import setup_complete
import zipfile
import requests
import io

@frappe.whitelist(allow_guest=True)
def ping():
    return "ok"

@frappe.whitelist()
def run_migration(site_name):
    migration = frappe.new_doc('Data Migration Run')
    migration.data_migration_plan = "AVIUM Central"
    migration.data_migration_connector = "AVIUM Central"
    migration.current_mapping_time = "Pull"
    migration.save()
    migration.run()
    return {"staus":"ok"}

@frappe.whitelist()
def setup_full_wizard(email, password):
    args = {
        "language":u"Español",
        "country":"Spain",
        "timezone":"Europe/Madrid",
        "currency":"EUR",
        "full_name":"AVIUM",
        "email":email,
        "password":password
    }
    setup_complete(args)
    web_settings = frappe.get_doc("Website Settings")
    web_settings.disable_signup = True
    web_settings.save()

@frappe.whitelist()
def setup_oauth(client, secret, url):
    slk = frappe.get_doc("Social Login Keys")
    slk.frappe_client_id = client
    slk.frappe_client_secret = secret
    slk.frappe_server_url = url
    slk.save()

@frappe.whitelist()
def setup_tournament(association):
    ts = frappe.get_doc("Tournament Settings")
    ts.db_set("association", association)

@frappe.whitelist()
def setup_publicfiles(zip_file_url):
    r = requests.get(zip_file_url, stream=True)
    zip_ref = zipfile.ZipFile(io.BytesIO(r.content))
    zip_ref.extractall(frappe.utils.get_files_path())
    zip_ref.close()

@frappe.whitelist()
def setup_backup(notify_email, frequency, access_key_id, secret_access_key, bucket, backup_limit, endpoint_url=None):
    s3b = frappe.get_doc("S3 Backup Settings")
    s3b.enabled=True
    s3b.notify_email = notify_email
    s3b.frequency = frequency
    s3b.access_key_id = access_key_id
    s3b.secret_access_key = secret_access_key
    s3b.bucket = bucket
    s3b.backup_limit = backup_limit
    if endpoint_url is not None:
        s3b.endpoint_url = endpoint_url
    s3b.save()

@frappe.whitelist()
def setup_email(domain, email_from, imap_server, imap_port, smtp_server, smtp_port, user, password):
    if frappe.db.exists("Email Domain", domain):
        email_domain = frappe.get_doc("Email Domain", domain)
    else:
        email_domain = frappe.new_doc("Email Domain")

    email_domain.domain_name = domain
    email_domain.email_id = email_from
    email_domain.email_server = imap_server
    email_domain.use_imap = True
    email_domain.use_ssl = True
    email_domain.smtp_server = smtp_server
    email_domain.smtp_port = int(smtp_port)
    email_domain.use_tls = True

    if frappe.db.exists("Email Domain", domain):
        email_domain.db_update()
    else:
        email_domain.db_insert()

    frappe.db.commit()

    email_account = frappe.get_doc("Email Account", "Notifications")
    email_account.email_id = email_from
    email_account.login_id_is_different = True
    email_account.login_id = user
    email_account.password = password
    email_account.domain = domain
    email_account.enable_outgoing = True
    email_account.default_outgoing = True
    email_account.smtp_server = smtp_server
    email_account.smtp_port = smtp_port
    email_account.use_tls = True
    email_account.save()

@frappe.whitelist()
def get_statistics():

    groups = frappe.db.sql("""
        select count(individual=1) as individual, count(team=1) as team from `tabTournament Group`
    """)

    registrations = frappe.db.sql("""
        select count(*) from tabRegistration
    """)

    individual_specimens = frappe.db.sql("""
        select count(*) from `tabIndividual Registration`
    """)

    no_sing_individual_specimens = frappe.db.sql("""
        SELECT count(*)
        FROM `tabIndividual Registration` as reg
        LEFT JOIN `tabTournament Group` as tgro on tgro.name = reg.group
        LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
        WHERE fami.sing_family = 0
    """)

    team_specimens = frappe.db.sql("""
        select COALESCE(sum(team_size),0) from `tabTeam Registration`
    """)

    no_sing_team_specimens = frappe.db.sql("""
        SELECT COALESCE(sum(reg.team_size),0)
        FROM `tabTeam Registration` as reg
        LEFT JOIN `tabTournament Group` as tgro on tgro.name = reg.group
        LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
        WHERE fami.sing_family = 0
    """)

    total_templates = frappe.db.sql("""
       select count(*) from `tabScoring Template`
    """)

    templates_status = frappe.db.sql("""
        select count(*), status from `tabScoring Template`
        GROUP by status
    """)

    pending_templates = judged_templates = tied_templates = error_templates = closed_templates = 0

    for item in templates_status:
        if item[1]=="Judged":
            judged_templates=item[0]
        elif item[1]=="Pending":
            pending_templates=item[0]
        elif item[1]=="Tied":
            tied_templates=item[0]
        elif item[1]=="Error":
            error_templates=item[0]
        elif item[1]=="Closed":
            closed_templates=item[0]

    data = {
        "individual_groups": groups[0][0],
        "team_groups": groups[0][1],
        "registrations": registrations[0][0],
        "individual_specimens": individual_specimens[0][0],
        "no_sing_individual_specimens": no_sing_individual_specimens[0][0],
        "team_specimens": int(team_specimens[0][0]),
        "no_sing_team_specimens": int(no_sing_team_specimens[0][0]),
        "total_templates": total_templates[0][0],
        "pending_templates":pending_templates,
        "judged_templates":judged_templates,
        "tied_templates":tied_templates,
        "error_templates":error_templates,
        "closed_templates":closed_templates
    }

    return data
