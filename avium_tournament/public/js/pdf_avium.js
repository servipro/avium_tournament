frappe.provide("avium_tournament.pdf");

$.extend(avium_tournament.pdf, {
	pdf_doc_function: function (doctype, name, method, args, donwload_name) {
		var w = window.open(
					frappe.urllib.get_full_url("/api/method/avium_tournament.pdf_generation.pdf_doc_function?"
                        +"doctype="+encodeURIComponent(doctype)
                        +"&name="+encodeURIComponent(name)
                        +"&function="+encodeURIComponent(method)
                        +"&arguments="+encodeURIComponent(JSON.stringify(args))
                        +"&donwload_name="+encodeURIComponent(donwload_name))
				);

		if(!w) {
			msgprint(__("Please enable pop-ups")); return;
		}
	}
});