window.onload = function () {

    $("#group").change(function(){
        var group_code = window.grid.api.getFilterInstance('group_code');
        group_code.setModel({
            type:'equals',
            filter:$("#group").val()
        });
        window.grid.api.onFilterChanged();
    })

    $("#status").change(function(){
        var group_code = window.grid.api.getFilterInstance('status');
        group_code.setModel({
            type:'equals',
            filter:$("#status").val()
        });
        window.grid.api.onFilterChanged();
    })

    $("#clean_filters").click(function(){
        $("#status").val("")
        $("#group").val("")
        window.grid.api.setFilterModel({})
    })

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    this.init_tables = function(rowData){

        //Assignment Table
        var columnDefs = [
            {
                headerName: __("Status"),
                field: "status",
                width: 25,
                cellRenderer: StatusCellRenderer,
                valueGetter: function get(params) {
                    return __(params.data.status);
                },
                filter: "agTextColumnFilter",
                suppressFilter: true
            },
            {
                headerName: __("Group"),
                field: "group_code",
                width: 25,
                suppressFilter: true,
                filter: "agTextColumnFilter"
            },
            {
                headerName: __("Cage"),
                field: "cage_number",
                filter: "agTextColumnFilter",
                width: 25
            },
            {
                headerName: __("Total Mark"),
                field: "total_mark",
                filter: "agTextColumnFilter",
                width: 25
            },
            {
                headerName: __("Tie Break Mark"),
                field: "tie_break_mark",
                filter: "agTextColumnFilter",
                width: 25,
                cellRenderer: TieBreakCellRenderer,
                valueGetter: function get(params) {
                    return __(params.data.status);
                },
            },
            {
                headerName: __("Classification"),
                field: "classification",
                filter: "agTextColumnFilter",
                width: 25
            },
            {
                headerName: __("Prize"),
                field: "prize_visualization",
                width: 25
            },
            {
                headerName: "",
                field: "name",
                width: 45,
                cellRenderer: function(params) {
                    return '<a class="btn btn-block btn-success" href="scoring_interface?st='+params.value+'">'+ __("Edit Template") +'</a>'
                },
                suppressFilter: true
            }
        ];

        function StatusCellRenderer() {}
        StatusCellRenderer.prototype.init = function(params) {
            if(params.node.group){
                var tempDiv = document.createElement('div');
                this.eGui = tempDiv.firstChild;
            }
            else{
                var tempDiv = document.createElement('div');
                if (params.data.status=="Judged") {
                    tempDiv.innerHTML = '<b style="color:green">' + __(params.value) + '</b>';
                } else if (params.data.status=="Tied"){
                    tempDiv.innerHTML = '<b style="color:orange">' + __(params.value) + '</b>';
                } else if (params.data.status=="Error"){
                    tempDiv.innerHTML = '<b style="color:red">' + __(params.value) + '</b>';
                } else {
                    tempDiv.innerHTML = '<b style="color:blue">' + __(params.value) + '</b>';
                }
                this.eGui = tempDiv.firstChild;
            }
        };
        StatusCellRenderer.prototype.getGui = function() {
            return this.eGui;
        };

        function TieBreakCellRenderer() {}
        TieBreakCellRenderer.prototype.init = function(params) {
            if(params.node.group){
                var tempDiv = document.createElement('div');
                this.eGui = tempDiv.firstChild;
            }
            else{
                var tempDiv = document.createElement('div');
                if (params.data.status=="Error"){
                    tempDiv.innerHTML = '<b style="color:red">' + params.data.tie_break_mark + '</b>';
                } else {
                    tempDiv.innerHTML = '<style="color:black">' + params.data.tie_break_mark + '</b>';
                }
                this.eGui = tempDiv.firstChild;
            }
        };
        TieBreakCellRenderer.prototype.getGui = function() {
            return this.eGui;
        };

        judgingGridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            onGridReady: function () {
                judgingGridOptions.api.sizeColumnsToFit();
            },
            rowHeight: 50,
            toolPanelSuppressSideButtons: true,
            enableColResize: true,
            enableFilter: true,
            rowSelection: 'single',
            suppressCellSelection: true,
            floatingFilter: true,
            onFilterChanged: function() {
                localStorage.setItem("filters", JSON.stringify(judgingGridOptions.api.getFilterModel()))
            },
            localeText: {
                noRowsToShow: __("No more rows"),
            }
        };

        var eGridDiv = document.querySelector('#judging_grid');

        agGrid.LicenseManager.setLicenseKey("Other_MjQ3MzQ5NTYzNDAwMA==c2eb110b024f78f48d3f4e5cb896950f");
        new agGrid.Grid(eGridDiv, judgingGridOptions);

        if(localStorage.getItem("filters") != null){
            var filters = JSON.parse(localStorage.getItem('filters'));
            judgingGridOptions.api.setFilterModel(filters)
            if(filters.status){
                $("#status").val(capitalizeFirstLetter(filters.status.filter))
            }
            if(filters.group_code){
                $("#group").val(filters.group_code.filter.toUpperCase())
            }
        }

        this.grid = judgingGridOptions;
    }

    frappe.call({
        method: 'avium_tournament.utils.judge_interface.judge_interface_data',
        callback: function(r) {
                $.each(r.message.groups, function (i, item) {
                    $('#group').append($('<option>', {
                        value: item.group_code,
                        text : item.group_code
                    }));
                });
                $.extend(frappe._messages, r.message._messages);
                init_tables(r.message.scoring_templates);
        }
    });
}


function add_child_position(doc,table_name,doctype,position){
//    asd
    if(position>doc[table_name].length){
        var row = frappe.model.add_child(doc, doctype, table_name);
        return row;
    }
    else if(position < doc[table_name][0].idx){
        var row = frappe.model.add_child(doc, doctype, table_name);
        row.idx = position;
        return row;
    }
    else{
        for(var curr_pos= position; curr_pos<doc[table_name].length; curr_pos++){
            doc[table_name][curr_pos].idx+=1;
        }
        var row = frappe.model.add_child(doc, doctype, table_name);
        row.idx = position;
        return row;
    }
}