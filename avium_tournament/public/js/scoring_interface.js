window.onload = function () {
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^]*)').exec(window.location.href);
        if (results==null){
           return null;
        }
        else{
           return results[1] || 0;
        }
    }

    function confirmDialog(message, onConfirm, onCancel, disable_cancel=false, ok_message="Ok"){
        var fClose = function(){
            modal.modal("hide");
        };

        var fCancel = function(){
            onCancel();
            fClose();
        }


        var modal = $("#confirmModal");
        if(disable_cancel){
            $("#confirmCancel").css("display", "none");
        }
        $("#confirmYes").empty().append(__(ok_message));
        modal.modal("show");
        $("#confirmMessage").empty().append(message);
        $("#confirmYes").one('click', onConfirm);
        $("#confirmYes").one('click', fClose);
        $("#confirmNo").one("click", fCancel);
        $("#confirmCancel").one('click', fClose);
    };

    var go_home = function(){
        window.location.href = "/judging";
    };

    var get_harmony = function(array, specimens){
        if(specimens==1){
            return 0
        }
        array = array.slice(0,specimens)

        var max = Math.max.apply(Math,array);
        var min = Math.min.apply(Math,array);

        var difference = max - min

        if(difference >= 6){
            return 0
        }

        return 6 - difference
    };

    var go_to_next = function(){
        frappe.call({
            method: 'avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.get_next_template',
            args:{
                current_template:app.doc.name
            },
            callback: function(r) {
                window.location.href = "/scoring_interface?st="+r.message;
            }
        });
    };

    Vue.component('concept_value', {
        template: '<select class="form-control" v-model=sel_value v-on:change="updateVal()" v-show="isEnabled">'+
        '<option v-for="item in values"> [[ item ]]</option>'+
        '</select>',
        data:function(){
            return {
                "sel_value": this.value
            }
        },
        props: ['value', 'min', 'max', 'default', 'team_size', 'position', 'status'],
        created() {
            this.getValues();
        },
        methods: {
            updateVal() {
                this.$emit('input', Number(this.sel_value))
            },
            getValues:function () {
                this.values = Array(this.max - this.min + 1).fill().map((_, idx) => this.max - idx)
            }
        },
        computed:{
            isEnabled: function(){
                if(this.team_size < this.position) {
                    return false;
                }
                else if($.inArray(this.status, [ "Judjable", "Declassified Puntuated", "Disqualified Puntuated"])<0){
                    return false;
                }
                else{
                    return true;
                }
            }
        },
        delimiters: ["[[","]]"]
    });

    frappe._messages["Saving"] = "Guardando";
    frappe._messages["Correctly Saved"] = "Guardado Correctamente";
    frappe._messages["Judjable"] = "Enjuiciable";
    frappe._messages["Not Judjable"] = "No Enjuiciable";
    frappe._messages["Declassified"] = "Desclasificado";
    frappe._messages["Declassified Puntuated"] = "Desclasificado Puntuado";
    frappe._messages["Disqualified"] = "Descalificado";
    frappe._messages["Not Presented"] = "No Presentado";
    frappe._messages["Did Not Sing"] = "No cantó";
    frappe._messages["Insuficient Sing"] = "Canto Insuficiente";
    frappe._messages["Changes will not be saved if you leave this window. Do you really want to leave the window?"] = "Las modificaciones no se guardaran si deja esta página. ¿Realmente quiere salir?";
    frappe._messages["If a specimen is not in Tournament, please fill observations"] = "No se ha guardado. Si un ejemplar no está en torneo, hay que indicar el motivo en observaciones";
    frappe._messages["You are not a judge assigned to this template"] = "No eres un juez asignado a esta plantilla, contacte con la organización";
    frappe._messages["Saving with default values"] = "Guardando con valores por defecto";

    frappe.call({
        method: 'avium_tournament.www.scoring_interface.get_data',
        args:{
            st: $.urlParam('st')
        },
        callback: function(r) {
            st = r.message.doc;
            tournament = r.message.tournament;

            app = new Vue({
                el: '#app',
                data: {
                    doc: st,
                    tournament:tournament,
                    status_options: [{
                        "value":"Judjable",
                        "string": __("Judjable")
                    },{
                        "value":"Not Judjable",
                        "string": __("Not Judjable")
                    },{
                        "value":"Declassified",
                        "string": __("Declassified")
                    },{
                        "value":"Declassified Puntuated",
                        "string": __("Declassified Puntuated")
                    },{
                        "value":"Disqualified",
                        "string": __("Disqualified")
                    },{
                        "value":"Did Not Sing",
                        "string": __("Did Not Sing")
                    },{
                        "value":"Insuficient Sing",
                        "string": __("Insuficient Sing")
                    }
                    ],
                    modified: false,
                    is_manual_harmony: r.message.is_manual_harmony,
                    is_manual_trophy: r.message.is_manual_trophy,
                    available_trophy: r.message.available_trophy
                },
                methods: {
                    pdf_st(callback){
                        window.open("/api/method/frappe.utils.print_format.download_pdf?doctype=Scoring%20Template&name="+this.doc.name);
                    },
                    recal_totals(){
                        var total_a = total_b = total_c = total_d = sum_mark = harmony = total_mark = 0;

                        $.each(app.doc.concepts, function( index, value ) {
                            if(value.negative==1){
                                total_a-=value.value_a;
                                total_b-=value.value_b;
                                total_c-=value.value_c;
                                total_d-=value.value_d;
                            }
                            else{
                                total_a+=value.value_a;
                                total_b+=value.value_b;
                                total_c+=value.value_c;
                                total_d+=value.value_d;
                            }
                        });

                        sum_mark = total_a + total_b + total_c + total_d;
                        harmony = get_harmony([total_a, total_b, total_c, total_d], app.doc.team_size)
                        if(app.is_manual_harmony){
                            harmony = app.doc.harmony;
                        }
                        else{
                            harmony = get_harmony([total_a, total_b, total_c, total_d], app.doc.team_size)
                        }


                        var tie_break = app.doc.tie_break_mark;

                        total_mark = sum_mark + harmony + tie_break;

                        app.doc.total_a = total_a;
                        app.doc.total_b = total_b;
                        app.doc.total_c = total_c;
                        app.doc.total_d = total_d;
                        app.doc.sum_mark = sum_mark;
                        app.doc.harmony = harmony;
                        app.doc.total_mark = total_mark;
                    },
                    cancel_st(callback){
                        if(app.modified){
                            confirmDialog(
                                __('Changes will not be saved if you leave this window. Do you really want to leave the window?'),
                                function(){
                                    go_home();
                                },
                                function(){
                                    return;
                                },
                            );
                        }
                        else{
                            go_home()
                        }
                    },
                    save_st() {

                        delete this.doc.creation;
                        delete this.doc.docstatus;
                        delete this.doc.modified;
                        delete this.doc.modified_by;
                        
                        any_change=false;
                        $.each(app.doc.concepts, function( index, concept ) {
                            if(app.doc.team_size>=1 && concept.value_a != concept.default_value){
                                any_change=true
                            }
                            if (app.doc.team_size>=2 && concept.value_b != concept.default_value){
                                any_change=true
                            }
                            if (app.doc.team_size>=3 && concept.value_c != concept.default_value){
                                any_change=true
                            }
                            if (app.doc.team_size>=4 && concept.value_d != concept.default_value){
                                any_change=true
                            }
                        });

                        if(!app.is_manual_trophy){
                            this.doc.manual_prize = null;
                        }

                        if((this.doc.status_a!="Judjable" || this.doc.status_b!="Judjable" || this.doc.status_c!="Judjable" || this.doc.status_d!="Judjable") && (this.doc.observations==undefined || this.doc.observations=="")){
                            confirmDialog(
                                __("If a specimen is not in Tournament, please fill observations"),
                                function(){
                                    return;
                                },
                                function(){
                                    return;
                                },
                                true,
                                "Ok"
                            );
                            return;
                        }
                        else{
                            if(any_change){
                                frappe.show_message("<span stle='font-size: 20px'>"+__("Saving")+ "</span")
                            }
                            else {
                                frappe.show_message("<span stle='font-size: 20px'>"+__("Saving with default values") + "</span")
                            }

                            $.ajax({
                                type: "PUT",
                                url: "/api/resource/Scoring Template/"+this.doc.name,
                                data: JSON.stringify(this.doc),
                                contentType: "application/json",
                                success: function(data){
                                    data.data.concepts = this.doc.concepts;
                                    this.doc = data.data;
                                    frappe.show_message(__("Correctly Saved"))

                                    $('.concepts select').css("background-color", "");
                                    $('.status select').css("background-color", "");
                                    $('.tied select').css("background-color", "");

                                    setTimeout(function(){
                                        frappe.hide_message();
                                        go_home();
                                    },1000);
                                    app.modified = false
                                }.bind(this),
                                error: function(data){
                                    var error = data.responseJSON["exc"]
                                    
                                    if (error.includes("If a specimen is not in Tournament, please fill observations")){
                                        frappe.show_message("<span style='color:red;font-size:30px'>" + __("If a specimen is not in Tournament, please fill observations") + "</span>")
                                    }
                                    else if(error.includes("You are not a judge assigned to this template")){
                                        frappe.show_message("<span style='color:red;font-size:30px'>" +__("You are not a judge assigned to this template")+ "</span>")
                                    }
                                    else if(error.includes("All values cannot be 0")){
                                        frappe.show_message("<span style='color:red;font-size:30px'>" +__("All values cannot be 0")+ "</span>")
                                    }
                                    else{
                                        frappe.show_message("<span style='color:red;font-size:30px'>" + error+ "</span>")
                                    }

                                    setTimeout(function(){
                                        frappe.hide_message();
                                    },3000);
                                    console.log(this.doc)
                                }
                            });
                        }
                    }
                },
                delimiters: ["[[","]]"]
            });

            setTimeout(function(){
                app.recal_totals();
            },200);

            $('.concepts select').change(function() {
                this.style.background = "#A8D5FF";
                app.modified = true
                app.recal_totals()
            });

            $('.status select').change(function() {
                this.style.background = "#A8D5FF";
                app.modified = true
            });

            $('.tied').change(function() {
                this.style.background = "#A8D5FF";
                app.modified = true
            });
        }
    });
};