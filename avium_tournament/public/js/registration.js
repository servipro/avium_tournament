frappe.ready(function () {
    app = new Vue({
      el: '#app',
      data: {
        registration: {},
        registration_original: {},
        tournament: {},
        carriers: [],
        individual_groups: [],
        team_groups: [],
        last_individual: {},
        last_team: {},
        loading: true
      },
      methods: {
        validate: function(event){
            var error_list = [];

            if (app.tournament.mandatory_carrier && (typeof(app.registration.carrier)=='undefined' || app.registration.carrier=="")){
                    error_list.push({"field": __("Porteador"), "error": " es obligatorio"})
            }

            app.registration.individual_table.forEach(function(individual){
                if (app.tournament.mandatory_rings && (typeof(individual.ring_a)=='undefined' || individual.ring_a=="")){
                    error_list.push({"field": "Individual grupo " + individual.group_code + " línea " + individual.idx, "error": "anilla A obligatoria"})
                }
            });

            app.registration.team_table.forEach(function(team){
                if (app.tournament.mandatory_rings && (typeof(team.ring_a)=='undefined' || team.ring_a=="")){
                    error_list.push({"field": "Equipo grupo " + team.group_code + " línea " + team.idx, "error": "anilla A obligatoria"})
                }
                if (app.tournament.mandatory_rings && team.team_size>=2 && (typeof(team.ring_b)=='undefined' || team.ring_b=="")){
                    error_list.push({"field": "Equipo grupo " + team.group_code + " línea " + team.idx, "error": "anilla B obligatoria"})
                }
                if (app.tournament.mandatory_rings && team.team_size>=3 && (typeof(team.ring_c)=='undefined' || team.ring_c=="")){
                    error_list.push({"field": "Equipo grupo " + team.group_code + " línea " + team.idx, "error": "anilla C obligatoria"})
                }
                if (app.tournament.mandatory_rings && team.team_size>=4 && (typeof(team.ring_d)=='undefined' || team.ring_d=="")){
                    error_list.push({"field": "Equipo grupo " + team.group_code + " línea " + team.idx, "error": "anilla D obligatoria"})
                }
            });

            if(app.registration.books !== parseInt(app.registration.books, 10)){
                error_list.push({"field": __("Books"), "error": __("is not a number")})
            }

            if(error_list.length>0){
                var error_str = $.map(error_list, function( val, i ) {
                  return "<li><b>"+val.field+"</b> "+val.error+"</li>"
                });

                frappe.msgprint("<ul>" + error_str.join("") + "</ul>", 'Error')

                return false;
            }
            return true;
        },
        get_registration: function (event) {
            var me = this;
            frappe.call({
                method: 'avium_tournament.www.registration.get_registration',
                callback: function(r) {
                    me.registration = r.message.registration
                    me.registration_original = JSON.parse(JSON.stringify(r.message.registration))
                    me.tournament = r.message.tournament
                    me.loading = false
                }
            });
        },
        get_carriers: function (event) {
            var me = this;
            frappe.call({
                method: 'frappe.client.get_list',
                args:{
                    doctype: "Carrier",
                    fields: ["name", "carrier_name"]
                },
                callback: function(r) {
                        if(r.message===undefined){
                            me.carriers = [{"name": "", "carrier_name": ""}]
                        }
                        else{
                            r.message.unshift({"name": "", "carrier_name": ""})
                            me.carriers = r.message
                        }
                }
            });
        },
        get_breeder_codes: function(event){
            var me = this;
            frappe.call({
                method: 'avium_tournament.www.registration.get_breeder_codes',
                callback: function(r) {
                        me.breeder_codes = r.message
                }
            });
        },
        get_data: function(event){
            this.get_breeder_codes()
            this.get_registration()
            this.get_carriers()
            set_up_modals()
            $('#save_button').attr("disabled", false);
        },
        update: function(event){
            $('#save_button').attr("disabled", true);

            var me = this;

            if(me.validate()){
                frappe.call({
                    method: "avium_tournament.www.registration.save",
                    args:{
                        doc: me.registration,
                    },
                    freeze:true,
                    callback: function(r) {
                        frappe.show_message(__("Saving"))
                        setTimeout(function(){
                            frappe.hide_message();
                        },3000);
                        me.get_data();
                    },
                    always: function(){
                        $('#save_button').attr("disabled", false);
                    }
                });
            }
            else{
                $('#save_button').attr("disabled", false);
            }
        },
        set_selected_group: function(group, is_individual){
            if(is_individual){
                selected_group = $.grep(app.individual_groups, function( n, i ) {
                  return n.value == group;
                })[0]
                if(selected_group.registration_description!="Empty"){
                    selected_group.group_description= selected_group.label
                }
                app.last_individual = selected_group;
            }
            else{
                selected_group = $.grep(app.team_groups, function( n, i ) {
                  return n.value == group;
                })[0]
                if(selected_group.registration_description!="Empty"){
                    selected_group.group_description= selected_group.label
                }
                app.last_team = selected_group;
            }
        },
        add_individual: function(){
            var group_code = this.last_individual.value;
            var empty_item = {
                "registration_description":"",
                "taxed_a":"No",
                "tax_a":0,
                "breeder_code_a":app.breeder_codes.breeder_code,
                "location_status_a": "Tournament",
                "league_inscription_a":"No",
                "sex_a":"Male",
                "group":"",
                "group_description":"",
                "group_code":"",
                "idx":1,
                "federation_code_a":app.breeder_codes.federation_code,
                "birth_year_a":(new Date()).getFullYear()
            }
            
            if(this.individual_groups.filter(function(e) { return e.value === group_code; }).length == 0){                
                bootbox.confirm({
                        "message": __("You should select a group"),
                        "callback": function(result){
                        },
                        buttons: {
                            confirm: {
                                label: __('Yes'),
                                className: 'btn-success'
                            }
                        }
                });
                return;
            }
            else if(this.last_individual.registration_description=="Empty" & (this.last_individual.group_description==undefined | this.last_individual.group_description=="")){
                bootbox.confirm({
                    "message": __("Registration Description is mandatory for this group"),
                    "callback": function(result){
                    },
                    buttons: {
                        confirm: {
                            label: __('Yes'),
                            className: 'btn-success'
                        }
                    }
                });
                return;
            }

            empty_item.group = this.last_individual.id;
            empty_item.group_code = this.last_individual.value;
            empty_item.group_description = this.last_individual.label;
            empty_item.registration_description = this.last_individual.group_description;

            if(this.registration.individual_table.length>0){
                empty_item.idx = Math.max.apply(Math,this.registration.individual_table.map(function(o){return o.idx;}))+1;
            }
            else{
                empty_item.idx = 0;
            }

            this.registration.individual_table.push(empty_item)

            setTimeout(function(){
                $('#lineind'+empty_item.idx).modal('show');
            }, 500)
        },
        add_team: function(){
            var group_code = this.last_team.value;
            var empty_item = {
                "team_size":4
                ,"league_inscription_a":"No"
                ,"league_inscription_c":"No"
                ,"league_inscription_b":"No"
                ,"league_inscription_d":"No"

                ,"location_status_a":"Tournament"
                ,"location_status_b":"Tournament"
                ,"location_status_c":"Tournament"
                ,"location_status_d":"Tournament"

                ,"taxed_a":"No"
                ,"taxed_b":"No"
                ,"taxed_c":"No"
                ,"taxed_d":"No"

                ,"sex_a":"Male"
                ,"sex_b":"Male"
                ,"sex_c":"Male"
                ,"sex_d":"Male"

                ,"breeder_code_a":app.breeder_codes.breeder_code
                ,"breeder_code_b":app.breeder_codes.breeder_code
                ,"breeder_code_c":app.breeder_codes.breeder_code
                ,"breeder_code_d":app.breeder_codes.breeder_code

                ,"federation_code_a":app.breeder_codes.federation_code
                ,"federation_code_b":app.breeder_codes.federation_code
                ,"federation_code_c":app.breeder_codes.federation_code
                ,"federation_code_d":app.breeder_codes.federation_code

                ,"birth_year_a":(new Date()).getFullYear()
                ,"birth_year_b":(new Date()).getFullYear()
                ,"birth_year_c":(new Date()).getFullYear()
                ,"birth_year_d":(new Date()).getFullYear()

                ,"tax_a":0
                ,"tax_c":0
                ,"tax_b":0
                ,"tax_d":0

            }

            if(this.team_groups.filter(function(e) { return e.value === group_code; }).length == 0){                
                bootbox.confirm({
                        "message": __("You should select a group"),
                        "callback": function(result){
                        },
                        buttons: {
                            confirm: {
                                label: __('Yes'),
                                className: 'btn-success'
                            }
                        }
                })
                return;
            }
            else if(this.last_team.registration_description=="Empty" & (this.last_team.group_description==undefined | this.last_team.group_description=="")){
                bootbox.confirm({
                    "message": __("Registration Description is mandatory for this group"),
                    "callback": function(result){
                    },
                    buttons: {
                        confirm: {
                            label: __('Yes'),
                            className: 'btn-success'
                        }
                    }
                });
                return;
            }

            empty_item.group = this.last_team.id;
            empty_item.group_code = this.last_team.value;
            empty_item.group_description = this.last_team.label;
            empty_item.registration_description = this.last_team.group_description;
            if(this.registration.team_table.length>0){
                empty_item.idx = Math.max.apply(Math,this.registration.team_table.map(function(o){return o.idx;}))+1;
            }
            else{
                empty_item.idx = 0;
            }
            empty_item.team_size = this.last_team.team_size;

            this.registration.team_table.push(empty_item)

            setTimeout(function(){
                $('#lineteam'+empty_item.idx).modal('show');
            }, 500)
        },
        delete_item: function(type, idx){
            if(type=="ind"){
                var index = this.registration.individual_table.findIndex(x => x.idx==idx);
                this.registration.individual_table.splice(index, 1);
            }
            else if(type=="team"){
                var index = this.registration.team_table.findIndex(x => x.idx==idx);
                this.registration.team_table.splice(index, 1);
            }
        },
        print_pdf: function(){
            if(JSON.stringify(this.registration) != JSON.stringify(this.registration_original)){
                $("#printmodal").modal()
            }
            else{
                var args = {
                    "price": 0,
                    "cage": 0
                };
                var w = window.open(
                        "/api/method/avium_tournament.pdf_generation.pdf_doc_function?"
                            +"doctype="+encodeURIComponent("Registration")
                            +"&name="+encodeURIComponent(this.registration.name)
                            +"&function="+encodeURIComponent("print_registration_summary")
                            +"&arguments="+encodeURIComponent(JSON.stringify(args))
                            +"&donwload_name="+encodeURIComponent(__("Registration Summary"))
                    );
    
                if(!w) {
                    frappe.show_message(__("Please enable pop-ups"));
                    setTimeout(function(){
                                frappe.hide_message();
                            },3000);
                }
            }
        }
      },
      delimiters: ['[[', ']]']
    });

    var set_up_groups = function(selector, data) {
            var Latinise = {};
            Latinise.LATIN_MAP = {
                    "Á":"A","Ă":"A","Ắ":"A","Ặ":"A","Ằ":"A","Ẳ":"A","Ẵ":"A","Ǎ":"A","Â":"A","Ấ":"A","Ậ":"A","Ầ":"A","Ẩ":"A","Ẫ":"A","Ä":"A","Ǟ":"A","Ȧ":"A","Ǡ":"A","Ạ":"A","Ȁ":"A","À":"A","Ả":"A","Ȃ":"A","Ā":"A","Ą":"A","Å":"A","Ǻ":"A","Ḁ":"A","Ⱥ":"A","Ã":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ḃ":"B","Ḅ":"B","Ɓ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ć":"C","Č":"C","Ç":"C","Ḉ":"C","Ĉ":"C","Ċ":"C","Ƈ":"C","Ȼ":"C","Ď":"D","Ḑ":"D","Ḓ":"D","Ḋ":"D","Ḍ":"D","Ɗ":"D","Ḏ":"D","ǲ":"D","ǅ":"D","Đ":"D","Ƌ":"D","Ǳ":"DZ","Ǆ":"DZ","É":"E","Ĕ":"E","Ě":"E","Ȩ":"E","Ḝ":"E","Ê":"E","Ế":"E","Ệ":"E","Ề":"E","Ể":"E","Ễ":"E","Ḙ":"E","Ë":"E","Ė":"E","Ẹ":"E","Ȅ":"E","È":"E","Ẻ":"E","Ȇ":"E","Ē":"E","Ḗ":"E","Ḕ":"E","Ę":"E","Ɇ":"E","Ẽ":"E","Ḛ":"E","Ꝫ":"ET","Ḟ":"F","Ƒ":"F","Ǵ":"G","Ğ":"G","Ǧ":"G","Ģ":"G","Ĝ":"G","Ġ":"G","Ɠ":"G","Ḡ":"G","Ǥ":"G","Ḫ":"H","Ȟ":"H","Ḩ":"H","Ĥ":"H","Ⱨ":"H","Ḧ":"H","Ḣ":"H","Ḥ":"H","Ħ":"H","Í":"I","Ĭ":"I","Ǐ":"I","Î":"I","Ï":"I","Ḯ":"I","İ":"I","Ị":"I","Ȉ":"I","Ì":"I","Ỉ":"I","Ȋ":"I","Ī":"I","Į":"I","Ɨ":"I","Ĩ":"I","Ḭ":"I","Ꝺ":"D","Ꝼ":"F","Ᵹ":"G","Ꞃ":"R","Ꞅ":"S","Ꞇ":"T","Ꝭ":"IS","Ĵ":"J","Ɉ":"J","Ḱ":"K","Ǩ":"K","Ķ":"K","Ⱪ":"K","Ꝃ":"K","Ḳ":"K","Ƙ":"K","Ḵ":"K","Ꝁ":"K","Ꝅ":"K","Ĺ":"L","Ƚ":"L","Ľ":"L","Ļ":"L","Ḽ":"L","Ḷ":"L","Ḹ":"L","Ⱡ":"L","Ꝉ":"L","Ḻ":"L","Ŀ":"L","Ɫ":"L","ǈ":"L","Ł":"L","Ǉ":"LJ","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ń":"N","Ň":"N","Ņ":"N","Ṋ":"N","Ṅ":"N","Ṇ":"N","Ǹ":"N","Ɲ":"N","Ṉ":"N","Ƞ":"N","ǋ":"N","Ñ":"N","Ǌ":"NJ","Ó":"O","Ŏ":"O","Ǒ":"O","Ô":"O","Ố":"O","Ộ":"O","Ồ":"O","Ổ":"O","Ỗ":"O","Ö":"O","Ȫ":"O","Ȯ":"O","Ȱ":"O","Ọ":"O","Ő":"O","Ȍ":"O","Ò":"O","Ỏ":"O","Ơ":"O","Ớ":"O","Ợ":"O","Ờ":"O","Ở":"O","Ỡ":"O","Ȏ":"O","Ꝋ":"O","Ꝍ":"O","Ō":"O","Ṓ":"O","Ṑ":"O","Ɵ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Õ":"O","Ṍ":"O","Ṏ":"O","Ȭ":"O","Ƣ":"OI","Ꝏ":"OO","Ɛ":"E","Ɔ":"O","Ȣ":"OU","Ṕ":"P","Ṗ":"P","Ꝓ":"P","Ƥ":"P","Ꝕ":"P","Ᵽ":"P","Ꝑ":"P","Ꝙ":"Q","Ꝗ":"Q","Ŕ":"R","Ř":"R","Ŗ":"R","Ṙ":"R","Ṛ":"R","Ṝ":"R","Ȑ":"R","Ȓ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꜿ":"C","Ǝ":"E","Ś":"S","Ṥ":"S","Š":"S","Ṧ":"S","Ş":"S","Ŝ":"S","Ș":"S","Ṡ":"S","Ṣ":"S","Ṩ":"S","Ť":"T","Ţ":"T","Ṱ":"T","Ț":"T","Ⱦ":"T","Ṫ":"T","Ṭ":"T","Ƭ":"T","Ṯ":"T","Ʈ":"T","Ŧ":"T","Ɐ":"A","Ꞁ":"L","Ɯ":"M","Ʌ":"V","Ꜩ":"TZ","Ú":"U","Ŭ":"U","Ǔ":"U","Û":"U","Ṷ":"U","Ü":"U","Ǘ":"U","Ǚ":"U","Ǜ":"U","Ǖ":"U","Ṳ":"U","Ụ":"U","Ű":"U","Ȕ":"U","Ù":"U","Ủ":"U","Ư":"U","Ứ":"U","Ự":"U","Ừ":"U","Ử":"U","Ữ":"U","Ȗ":"U","Ū":"U","Ṻ":"U","Ų":"U","Ů":"U","Ũ":"U","Ṹ":"U","Ṵ":"U","Ꝟ":"V","Ṿ":"V","Ʋ":"V","Ṽ":"V","Ꝡ":"VY","Ẃ":"W","Ŵ":"W","Ẅ":"W","Ẇ":"W","Ẉ":"W","Ẁ":"W","Ⱳ":"W","Ẍ":"X","Ẋ":"X","Ý":"Y","Ŷ":"Y","Ÿ":"Y","Ẏ":"Y","Ỵ":"Y","Ỳ":"Y","Ƴ":"Y","Ỷ":"Y","Ỿ":"Y","Ȳ":"Y","Ɏ":"Y","Ỹ":"Y","Ź":"Z","Ž":"Z","Ẑ":"Z","Ⱬ":"Z","Ż":"Z","Ẓ":"Z","Ȥ":"Z","Ẕ":"Z","Ƶ":"Z","Ĳ":"IJ","Œ":"OE","ᴀ":"A","ᴁ":"AE","ʙ":"B","ᴃ":"B","ᴄ":"C","ᴅ":"D","ᴇ":"E","ꜰ":"F","ɢ":"G","ʛ":"G","ʜ":"H","ɪ":"I","ʁ":"R","ᴊ":"J","ᴋ":"K","ʟ":"L","ᴌ":"L","ᴍ":"M","ɴ":"N","ᴏ":"O","ɶ":"OE","ᴐ":"O","ᴕ":"OU","ᴘ":"P","ʀ":"R","ᴎ":"N","ᴙ":"R","ꜱ":"S","ᴛ":"T","ⱻ":"E","ᴚ":"R","ᴜ":"U","ᴠ":"V","ᴡ":"W","ʏ":"Y","ᴢ":"Z","á":"a","ă":"a","ắ":"a","ặ":"a","ằ":"a","ẳ":"a","ẵ":"a","ǎ":"a","â":"a","ấ":"a","ậ":"a","ầ":"a","ẩ":"a","ẫ":"a","ä":"a","ǟ":"a","ȧ":"a","ǡ":"a","ạ":"a","ȁ":"a","à":"a","ả":"a","ȃ":"a","ā":"a","ą":"a","ᶏ":"a","ẚ":"a","å":"a","ǻ":"a","ḁ":"a","ⱥ":"a","ã":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ḃ":"b","ḅ":"b","ɓ":"b","ḇ":"b","ᵬ":"b","ᶀ":"b","ƀ":"b","ƃ":"b","ɵ":"o","ć":"c","č":"c","ç":"c","ḉ":"c","ĉ":"c","ɕ":"c","ċ":"c","ƈ":"c","ȼ":"c","ď":"d","ḑ":"d","ḓ":"d","ȡ":"d","ḋ":"d","ḍ":"d","ɗ":"d","ᶑ":"d","ḏ":"d","ᵭ":"d","ᶁ":"d","đ":"d","ɖ":"d","ƌ":"d","ı":"i","ȷ":"j","ɟ":"j","ʄ":"j","ǳ":"dz","ǆ":"dz","é":"e","ĕ":"e","ě":"e","ȩ":"e","ḝ":"e","ê":"e","ế":"e","ệ":"e","ề":"e","ể":"e","ễ":"e","ḙ":"e","ë":"e","ė":"e","ẹ":"e","ȅ":"e","è":"e","ẻ":"e","ȇ":"e","ē":"e","ḗ":"e","ḕ":"e","ⱸ":"e","ę":"e","ᶒ":"e","ɇ":"e","ẽ":"e","ḛ":"e","ꝫ":"et","ḟ":"f","ƒ":"f","ᵮ":"f","ᶂ":"f","ǵ":"g","ğ":"g","ǧ":"g","ģ":"g","ĝ":"g","ġ":"g","ɠ":"g","ḡ":"g","ᶃ":"g","ǥ":"g","ḫ":"h","ȟ":"h","ḩ":"h","ĥ":"h","ⱨ":"h","ḧ":"h","ḣ":"h","ḥ":"h","ɦ":"h","ẖ":"h","ħ":"h","ƕ":"hv","í":"i","ĭ":"i","ǐ":"i","î":"i","ï":"i","ḯ":"i","ị":"i","ȉ":"i","ì":"i","ỉ":"i","ȋ":"i","ī":"i","į":"i","ᶖ":"i","ɨ":"i","ĩ":"i","ḭ":"i","ꝺ":"d","ꝼ":"f","ᵹ":"g","ꞃ":"r","ꞅ":"s","ꞇ":"t","ꝭ":"is","ǰ":"j","ĵ":"j","ʝ":"j","ɉ":"j","ḱ":"k","ǩ":"k","ķ":"k","ⱪ":"k","ꝃ":"k","ḳ":"k","ƙ":"k","ḵ":"k","ᶄ":"k","ꝁ":"k","ꝅ":"k","ĺ":"l","ƚ":"l","ɬ":"l","ľ":"l","ļ":"l","ḽ":"l","ȴ":"l","ḷ":"l","ḹ":"l","ⱡ":"l","ꝉ":"l","ḻ":"l","ŀ":"l","ɫ":"l","ᶅ":"l","ɭ":"l","ł":"l","ǉ":"lj","ſ":"s","ẜ":"s","ẛ":"s","ẝ":"s","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ᵯ":"m","ᶆ":"m","ń":"n","ň":"n","ņ":"n","ṋ":"n","ȵ":"n","ṅ":"n","ṇ":"n","ǹ":"n","ɲ":"n","ṉ":"n","ƞ":"n","ᵰ":"n","ᶇ":"n","ɳ":"n","ñ":"n","ǌ":"nj","ó":"o","ŏ":"o","ǒ":"o","ô":"o","ố":"o","ộ":"o","ồ":"o","ổ":"o","ỗ":"o","ö":"o","ȫ":"o","ȯ":"o","ȱ":"o","ọ":"o","ő":"o","ȍ":"o","ò":"o","ỏ":"o","ơ":"o","ớ":"o","ợ":"o","ờ":"o","ở":"o","ỡ":"o","ȏ":"o","ꝋ":"o","ꝍ":"o","ⱺ":"o","ō":"o","ṓ":"o","ṑ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","õ":"o","ṍ":"o","ṏ":"o","ȭ":"o","ƣ":"oi","ꝏ":"oo","ɛ":"e","ᶓ":"e","ɔ":"o","ᶗ":"o","ȣ":"ou","ṕ":"p","ṗ":"p","ꝓ":"p","ƥ":"p","ᵱ":"p","ᶈ":"p","ꝕ":"p","ᵽ":"p","ꝑ":"p","ꝙ":"q","ʠ":"q","ɋ":"q","ꝗ":"q","ŕ":"r","ř":"r","ŗ":"r","ṙ":"r","ṛ":"r","ṝ":"r","ȑ":"r","ɾ":"r","ᵳ":"r","ȓ":"r","ṟ":"r","ɼ":"r","ᵲ":"r","ᶉ":"r","ɍ":"r","ɽ":"r","ↄ":"c","ꜿ":"c","ɘ":"e","ɿ":"r","ś":"s","ṥ":"s","š":"s","ṧ":"s","ş":"s","ŝ":"s","ș":"s","ṡ":"s","ṣ":"s","ṩ":"s","ʂ":"s","ᵴ":"s","ᶊ":"s","ȿ":"s","ɡ":"g","ᴑ":"o","ᴓ":"o","ᴝ":"u","ť":"t","ţ":"t","ṱ":"t","ț":"t","ȶ":"t","ẗ":"t","ⱦ":"t","ṫ":"t","ṭ":"t","ƭ":"t","ṯ":"t","ᵵ":"t","ƫ":"t","ʈ":"t","ŧ":"t","ᵺ":"th","ɐ":"a","ᴂ":"ae","ǝ":"e","ᵷ":"g","ɥ":"h","ʮ":"h","ʯ":"h","ᴉ":"i","ʞ":"k","ꞁ":"l","ɯ":"m","ɰ":"m","ᴔ":"oe","ɹ":"r","ɻ":"r","ɺ":"r","ⱹ":"r","ʇ":"t","ʌ":"v","ʍ":"w","ʎ":"y","ꜩ":"tz","ú":"u","ŭ":"u","ǔ":"u","û":"u","ṷ":"u","ü":"u","ǘ":"u","ǚ":"u","ǜ":"u","ǖ":"u","ṳ":"u","ụ":"u","ű":"u","ȕ":"u","ù":"u","ủ":"u","ư":"u","ứ":"u","ự":"u","ừ":"u","ử":"u","ữ":"u","ȗ":"u","ū":"u","ṻ":"u","ų":"u","ᶙ":"u","ů":"u","ũ":"u","ṹ":"u","ṵ":"u","ᵫ":"ue","ꝸ":"um","ⱴ":"v","ꝟ":"v","ṿ":"v","ʋ":"v","ᶌ":"v","ⱱ":"v","ṽ":"v","ꝡ":"vy","ẃ":"w","ŵ":"w","ẅ":"w","ẇ":"w","ẉ":"w","ẁ":"w","ⱳ":"w","ẘ":"w","ẍ":"x","ẋ":"x","ᶍ":"x","ý":"y","ŷ":"y","ÿ":"y","ẏ":"y","ỵ":"y","ỳ":"y","ƴ":"y","ỷ":"y","ỿ":"y","ȳ":"y","ẙ":"y","ɏ":"y","ỹ":"y","ź":"z","ž":"z","ẑ":"z","ʑ":"z","ⱬ":"z","ż":"z","ẓ":"z","ȥ":"z","ẕ":"z","ᵶ":"z","ᶎ":"z","ʐ":"z","ƶ":"z","ɀ":"z","ﬀ":"ff","ﬃ":"ffi","ﬄ":"ffl","ﬁ":"fi","ﬂ":"fl","ĳ":"ij","œ":"oe","ﬆ":"st","ₐ":"a","ₑ":"e","ᵢ":"i","ⱼ":"j","ₒ":"o","ᵣ":"r","ᵤ":"u","ᵥ":"v","ₓ":"x"
            };
            String.prototype.latinise = function() {
                return this.replace(/[^A-Za-z0-9\[\] ]/g, function(a) {
                    return Latinise.LATIN_MAP[a] || a;
                });
            };
            var awesomplete = new Awesomplete(selector[0],{
                item: function (item, input) {
                    var html = "<strong>" + item.value + "</strong>";
                    html += '<span class="small">  ' + item.label + '</span>';

                    return $('<li></li>')
                        .prop('aria-selected', 'false')
                        .html('<a><p>' + html + '</p></a>')
                        .get(0);
                },
                filter: function(item, input) {
                    return item.label.latinise().toLowerCase().indexOf(input.toLowerCase())!=-1 || item.value.latinise().toLowerCase().indexOf(input.toLowerCase())!=-1 ;
                },
                data: function (item, input) {
                    return {
                        label: item.label || item.value,
                        value: item.value
                    };
                },
                list: data,
                minChars: 0,
                maxItems: 99,
                autoFirst: true,
                sort: function(a, b){
                    return (a.value == b.value ? 0 : (a.value < b.value ? -1 : 1));
                }
            });

            return awesomplete;
    }

    set_up_modals = function(){
        frappe.call({
            method: "avium_tournament.utils.get_groups_select",
            callback: function(r) {
                app.individual_groups = r.message.results_individual;

                set_up_groups($("#individual_add_select"), r.message.results_individual)
                $("#individual_add_select").on('awesomplete-selectcomplete', function() {
                    inputVal = $(this).val();
                    app.set_selected_group(inputVal, true);
                })

                app.team_groups = r.message.results_team;

                set_up_groups($("#team_add_select"), r.message.results_team)
                $("#team_add_select").on('awesomplete-selectcomplete', function() {
                    inputVal = $(this).val();
                    app.set_selected_group(inputVal, false);
                })
            }
        });
    }

    Vue.component('ind_registration_line', {
        props: ['line'],
        data: function() {
            return {
                type: "ind",
                tournament: app.tournament
            }
        },
        template: '#registration_line-template',
        delimiters: ['[[', ']]'],
        computed:{
            description_blocked: function(){
                var me = this.line;
                current_group = $.grep(app.individual_groups, function( n, i ) {
                    return n.value == me.group_code;
                })[0]
                return current_group.registration_description == "Blocked";
            }
        },
        methods:{
            delete_item: function(event){
                var me = this.line;
                bootbox.confirm({
                        "message": __("Delete group") + " " + me.group_code + "?",
                        "callback": function(result){
                            if(result){
                                app.delete_item("ind", me.idx)
                            }
                        },
                        buttons: {
                            confirm: {
                                label: __('Yes'),
                                className: 'btn-success'
                            },
                            cancel: {
                                label: __('No'),
                                className: 'btn-danger'
                            }
                        }
                })
            }
        }
      // options
    })
    Vue.component('team_registration_line', {
        props: ['line'],
        data: function() {
            return {type: "team"}
        },
        computed:{
            description_blocked: function(){
                var me = this.line;
                current_group = $.grep(app.team_groups, function( n, i ) {
                    return n.value == me.group_code;
                })[0]
                return current_group.registration_description == "Blocked";
            }
        },
        template: '#registration_line-template',
        delimiters: ['[[', ']]'],
        methods:{
            delete_item: function(event){
                var me = this.line;
                bootbox.confirm({
                        "message": __("Delete group") + " " + me.group_code + "?",
                        "callback": function(result){
                            if(result){
                                app.delete_item("team", me.idx)
                            }
                        },
                        buttons: {
                            confirm: {
                                label: __('Yes'),
                                className: 'btn-success'
                            },
                            cancel: {
                                label: __('No'),
                                className: 'btn-danger'
                            }
                        }
                })
            }
        }
      // options
    })

    app.get_data();

    frappe._messages["is not a number"] = "no es un número"
    frappe._messages["Books"] = "Catálogos"
    frappe._messages["Meals"] = "Comidas"
    frappe._messages["Saving"] = "Guardando"
    frappe._messages["Delete group"] = "Eliminar grupo"
    frappe._messages["Yes"] = "Si"
    frappe._messages["No"] = "No"
    frappe._messages["Registration Summary"] = "Incripción"
    frappe._messages["Print PDF"] = "Imprimir PDF"
    frappe._messages["must be unique"] = "ya esta en el sistema registrado."
    frappe._messages["Membership Discount"] = "Aplicar Descuento Socio"
    frappe._messages["The birth year of the specimen A in the group {0}, row {1}, is incorrect."] = "El año de nacimiento del ejemplar A en el grupo {0}, fila {1}, es incorrecto."
    frappe._messages["The birth year of the specimen {0} in the group {1}, row {2}, is incorrect."] = "El año de nacimiento del ejemplar {0} en el grupo {1}, fila {2}, es incorrecto."
    frappe._messages["Registration Description is mandatory for this group"] = "La denominacion para este grupo es obligatoria"

})
