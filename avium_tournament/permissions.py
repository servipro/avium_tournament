# Solo se permite acceso a una entidad si esta a tu nivel o por debajo
import frappe


def scoring_template_query_conditions(user):
    roles = frappe.get_roles()

    if "AVIUM Association" in roles or "System Manager" in roles:
        return ""
    else:
        judge = frappe.db.get_value("User", user, ["judge"])
        return u"""(judge_1  = "{judge}" OR judge_2  = "{judge}")""".format(judge=judge)

def scoring_template_has_permission(doc, user):
    roles = frappe.get_roles()

    if "AVIUM Association" in roles or "System Manager" in roles:
        return True
    else:
        judge = frappe.db.get_value("User", user, ["judge"])
        return doc.judge_1 ==judge or doc.judge_2==judge