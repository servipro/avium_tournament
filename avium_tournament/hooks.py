# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version
from frappe import _

app_name = "avium_tournament"
app_title = "Avium Tournament"
app_publisher = "SERVIPRO SL"
app_description = "AVIUM Tournament App"
app_icon = "fa fa-twitter"
app_color = "orange"
app_email = "pau@servipro.eu"
app_license = "All Rights Reserved"

# Includes in <head>
# ------------------
fixtures = ["Translation"]

# include js, css files in header of desk.html
# app_include_css = "/assets/avium_tournament/css/avium_tournament.css"
# app_include_js = "/assets/avium_tournament/js/avium_tournament.js"

app_include_js = [
	"assets/js/avium_tournament.js",
	"/assets/avium_tournament/js/dist/vue.js",
	"/assets/avium_tournament/js/dist/date_fns.min.js",
	"/assets/avium_tournament/js/dist/vuetiful.min.js",
	"/assets/avium_tournament/js/dist/ag-grid.js",
	"/assets/avium_tournament/js/dist/sentry.js",
	"/assets/avium_tournament/js/sentry_client.js"
]

app_include_css = [
	"assets/css/avium_tournament.css",
	"assets/css/vue.css"
]

# include js, css files in header of web template
web_include_css = [
	"/assets/css/vue.css",
	"/assets/css/custom_avium.css"
]

web_include_js = [
	"/assets/avium_tournament/js/dist/vue.js",
	"/assets/avium_tournament/js/dist/date_fns.min.js",
	"/assets/avium_tournament/js/dist/vuetiful.min.js",
	"/assets/avium_tournament/js/dist/ag-grid.js",
	"/assets/avium_tournament/js/dist/sentry.js",
	"/assets/avium_tournament/js/sentry_client.js"
]

boot_session = [
	"avium_tournament.sentry.init_sentry"
]
# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
home_page = "tournament_info"

# website user home page (by Role)
role_home_page = {
	"AVIUM Exhibitor": "registration"
}

# Website user home page (by function)
# get_website_user_home_page = "avium_tournament.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "avium_tournament.install.before_install"
# after_install = "avium_tournament.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "avium_tournament.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
permission_query_conditions = {
	"Scoring Template": "avium_tournament.permissions.scoring_template_query_conditions"
}

has_permission = {
	"Scoring Template": "avium_tournament.permissions.scoring_template_has_permission"
}

#on_login = ['avium_tournament.utils.on_login']

# standard_portal_menu_items = [
# 	{"title": _("Exhibitor Profile"), "route": "/exhibitor_profile", "reference_doctype": "Registration", "role": "AVIUM Exhibitor"},
# 	{"title": _("Registration"), "route": "/registration", "reference_doctype": "Registration", "role": "AVIUM Exhibitor"}
# ]

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
#  	"User": {
# 		"before_insert": "avium_tournament.utils.import_users_central.on_create_user"
# # 		"on_update": "method",
# # 		"on_cancel": "method",
# # 		"on_trash": "method"
# 	}
}

# Scheduled Tasks
# ---------------

scheduler_events = {
# 	"all": [
# 		"avium_tournament.tasks.all"
# 	],
	"daily": [
		"avium_tournament.tasks.daily"
	]
# 	"hourly": [
# 		"avium_tournament.tasks.hourly"
# 	],
# 	"weekly": [
# 		"avium_tournament.tasks.weekly"
# 	]
# 	"monthly": [
# 		"avium_tournament.tasks.monthly"
# 	]
}

# Testing
# -------

# before_tests = "avium_tournament.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# on_login = "avium_tournament.api.on_login"

update_website_context = 'avium_tournament.utils.update_website_context'

after_login = ['avium_tournament.utils.after_login']
before_login = ['avium_tournament.utils.before_login']

override_whitelisted_methods = {
 	"frappe.utils.print_format.download_pdf": "avium_tournament.pdf_generation.download_pdf_custom",
    "printview": "printview"
}

