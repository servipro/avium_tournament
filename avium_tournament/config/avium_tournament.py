from __future__ import unicode_literals

import frappe

from frappe import _
import frappe

def get_data():

    tournaments = frappe.get_all("Tournament", fields=["name",])
    if len(tournaments) == 0:
        tournament_route = "List/Tournament/List"
    else:
        tournament_route = "Form/Tournament/" + tournaments[0]["name"]

    return [
        {
            "label": _("Auxiliary"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Federations"),
                    "name": "Federation"
                },
                {
                    "type": "doctype",
                    "label": _("Associations"),
                    "name": "Association"
                },
                {
                    "type": "doctype",
                    "label": _("Judge Associations"),
                    "name": "Judge Association"
                },
                {
                    "type": "doctype",
                    "label": _("Judges"),
                    "name": "Judge"
                },
                {
                    "type": "doctype",
                    "label": _("Cages"),
                    "name": "Cage"
                },
                {
                    "type": "doctype",
                    "label": _("Prizes Description"),
                    "name": "Tournament Prizes Description"
                },
                {
                    "type": "doctype",
                    "label": _("Territory"),
                    "name": "Territory"
                }
            ]
        },
        {
            "label": _("Tournament Management"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Tournament"),
                    "name": "Tournament",
                    "route": tournament_route,
                },
                {
                    "type": "doctype",
                    "label": _("Carrier"),
                    "name": "Carrier"
                },
                {
                    "type": "doctype",
                    "label": _("Registrations"),
                    "name": "Registration"
                },
                {
                    "type": "doctype",
                    "label": _("Payments"),
                    "name": "Payment"
                },
                {
                    "type": "doctype",
                    "label": _("Scoring Template"),
                    "name": "Scoring Template"
                }
            ]
        },
        {
            "label": _("Templates"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Family"),
                    "name": "Family"
                },
                {
                    "type": "doctype",
                    "label": _("Judging Template"),
                    "name": "Judging Template"
                }
            ]
        },
        {
            "label": _("Tools"),
            "items": [
                {
                    "type": "page",
                    "name": "assign-judges",
                    "label": _("Assign Judges")
                },
                {
                    "type": "page",
                    "name": "ring-update",
                    "label": _("Ring Updater")
                },
                {
                    "type": "page",
                    "name": "score-update",
                    "label": _("Score Updater")
                },
                {
                    "type": "doctype",
                    "label": _("Scoring Templates Mass Mail"),
                    "name": "Scoring Templates Mass Mail"
                }
            ]
        },
        {
            "label": _("Reports"),
            "items": [
                {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Judging Status",
                    "doctype": "Scoring Template"
                },
                {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Sing Registrations",
                    "doctype": "Registration"
                },
                {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Ring Inspection",
                    "doctype": "Tournament"
                },
                {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Food",
                    "doctype": "Tournament"
                },
                {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Ring Search",
                    "doctype": "Scoring Template"
                },
                {
                    "type": "report",
                    "is_query_report": True,
                    "name": "Cages By Group",
                    "doctype": "Tournament Group"
                }

            ]
        },
        {
            "label": _("Report Configurations"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Report Configuration"),
                    "name": "Report Configuration"
                },
                {
                    "type": "doctype",
                    "label": _("Judging Stickers Configuration"),
                    "name": "Judging Stickers Configuration"
                },
                {
                    "type": "doctype",
                    "label": _("Marks Stickers Configuration"),
                    "name": "Marks Stickers Configuration"
                },
                {
                    "type": "doctype",
                    "label": _("Exhibitor Stickers Configuration"),
                    "name": "Exhibitor Stickers Configuration"
                },
                {
                    "type": "doctype",
                    "label": _("Certificate Configuration"),
                    "name": "Certificate Configuration"
                }
            ]
        }
    ]