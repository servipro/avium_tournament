# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Avium Tournament",
			"color": "orange",
			"icon": "fa fa-twitter",
			"type": "module",
			"label": _("Avium Tournament Menu")
		},
		{
			"module_name": "Registration",
			"_doctype": "Registration",
			"color": "orange",
			"icon": "fa fa-file-text-o",
			"type": "link",
			"link": "List/Registration",
			"label": _("Registrations")
		}
	]
