from __future__ import unicode_literals
from frappe import _


def get_data():

    return [
        {
            "label": _("Geography"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Territory"),
                    "name": "Territory"
                }
            ]
        }
    ]