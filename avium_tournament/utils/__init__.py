import copy

import frappe
import json

from avium_tournament.reports.commonFunctions import blank_undefined
from frappe import _
from frappe.utils.password import update_password
from frappe.frappeclient import FrappeClient

@frappe.whitelist()
def set_password_judge(password, judge, email):
    users = frappe.get_all("User", {"name": email, "judge":judge})
    if len(users)!=1:
        frappe.throw(_("More than one user with this parameters"))

    update_password(users[0]['name'], password)

def update_website_context(context):
    if "AVIUM Judge" in frappe.get_roles(frappe.session.user):
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": "/judging",
            "label": _("Judging Interface")
        })
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": 'https://www.youtube.com/playlist?list=PLb036NSNLGC_p0RFwxgytFie5o8MSMHfR" target="_blank',
            "label": _("Tutorials")
        })
    if "AVIUM Exhibitor" in frappe.get_roles(frappe.session.user):
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": "/tournament_info",
            "label": _("Tournament Information")
        })
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": "/exhibitor_profile",
            "label": _("Personal Information")
        })
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": "/registration",
            "label": _("Registration")
        })
        context["top_bar_items"].append({
            "right": 0,
            "target": "",
            "url": 'https://www.youtube.com/playlist?list=PLb036NSNLGC9-9vNVvZrwMecm34zq6esm" target="_blank',
            "label": _("Tutorials")
        })

    context["top_bar_items"].append({
        "right": 1,
        "target": "",
        "url": "/contact",
        "label": _("Contact")
    })

def before_login():
    email = frappe.request.form["usr"]
    if email=="Administrator" or frappe.db.exists("User", email):
        return
    else:
        tournament = get_tournament()
            
        dmc = frappe.get_doc("Data Migration Connector", "AVIUM Central")
        client = FrappeClient(dmc.hostname, dmc.username, dmc.get_password(), verify=True)
        
        remote_user = client.get_doc("User", email)
        if not remote_user:
            return

        remote_roles = list(map(lambda x: x["role"], remote_user["roles"]))
        if "AVIUM Organization" in remote_roles:
            tournament_settings = frappe.get_doc("Tournament Settings")
            if tournament_settings.association:
                organizations = client.get_list("Organization", filters={"user": email, "association":tournament_settings.association})
                if len(organizations)>0:
                    local_user = import_user(email)
                    local_user.append("roles", {"role": "AVIUM Association"})
                    local_user.save(ignore_permissions=True)
                    frappe.db.commit()
                    return
                else:
                    frappe.throw(_("You are not an association for this tournament"))
        elif "AVIUM Exhibitor" in remote_roles:
            if tournament!=None and tournament.get_exhibitors_registration_open():
                local_user = import_user(email)
                local_user.append("roles", {"role": "AVIUM Exhibitor"})
                local_user.save(ignore_permissions=True)
                import_registration(local_user, client)
                frappe.db.commit()
            else:
                frappe.throw(_("This tournament does not accept registrations at the current moment"))

def import_user(email):
    dmc = frappe.get_doc("Data Migration Connector", "AVIUM Central")
    client = FrappeClient(dmc.hostname, dmc.username, dmc.get_password(), verify=True)

    try:
        user_info = client.get_api("avium_web.utils.get_user_details", params={
            "email": email,
            "auth_key": "4e37eb14-7930-4b97-8190-b6c00564cc67"
        })
    except:
        frappe.throw("Recupera tu contraseña en avium.eu")

    user = frappe.new_doc("User")
    user.email = user_info["email"]
    user.username = user_info["username"]
    user.first_name = user_info["first_name"]
    user.last_name = user_info["last_name"]
    user.language = "es"
    user.enabled = 1
    user.flags.no_welcome_mail = True
    user.db_insert()

    from frappe.utils.password import LegacyPassword
    pwd = LegacyPassword.hash(user_info["passsalt"], salt=user_info["salt"].encode('UTF-8'))
    
    frappe.db.sql("""insert into __Auth (doctype, name, fieldname, `password`, encrypted)
        values (%(doctype)s, %(name)s, %(fieldname)s, %(passsalt)s, 0)""",
        { 'doctype': "User", 'name': user.email, 'fieldname': "password", 'passsalt': pwd})
    frappe.db.commit()
    return user

def import_registration(local_user,client):
    username = local_user.email
    tournament = get_tournament()

    exhibitor = client.get_list("Exhibitor", filters={"user": username})
    if len(exhibitor)>0:
        registration = frappe.new_doc("Registration")
        fields_to_import = ["exhibitor_name", "exhibitor_surname", "dni", "association", "territory", "postal_code",
                            "address", "city", "postal_code", "telephone", "mobile_phone"]
        for field in fields_to_import:
            setattr(registration, field, exhibitor[0][field])

        registration.books = 0
        registration.comunity = frappe.get_doc("Territory", registration.territory).parent_territory
        registration.country = frappe.get_doc("Territory", registration.comunity).parent_territory
        registration.federation = frappe.get_doc("Association", registration.association).federation
        registration.user = username
        registration.email = username
        registration.owner = username
        registration.db_update()

        breeder_code = frappe.new_doc("Exhibitor Breeder")
        breeder_code.federation_name = registration.federation
        breeder_code.federation_code = frappe.get_value("Federation", registration.federation, "federation_code")
        breeder_code.breeder_code = exhibitor[0]["breeder_code"]
        breeder_code.parent = registration.name
        breeder_code.parenttype = "Registration"
        breeder_code.parentfield = "breeder_code"
        breeder_code.db_update()
    else:
        frappe.throw("User does not have exhibitor in avium.eu")

def after_login():
    roles = frappe.permissions.get_roles(frappe.session.user)

    if "System Manager" in roles:
        return
    elif "AVIUM Judge" in roles:
        frappe.local.response["home_page"] = "/judging"
    elif "AVIUM Exhibitor" in roles:
        frappe.local.response["home_page"] = "/registration"
    return

def get_exhibitor():
    exhibitor_list = frappe.get_list("Registration", {"user": frappe.session.user})

    if len(exhibitor_list) == 0:
        frappe.throw(_("No exhibitor found"))
    else:
        exhibitor = frappe.get_doc("Registration", exhibitor_list[0].name)

    return exhibitor

@frappe.whitelist()
def get_tournament(attribute=None):
    tournaments = frappe.get_all("Tournament")
    if len(tournaments)==0:
        return None
    elif attribute == None:
        return frappe.get_doc("Tournament",tournaments[0].name)
    else:
        return frappe.get_value("Tournament",tournaments[0].name,attribute)

@frappe.whitelist()
def get_tournament_value(fields):
    tournaments = frappe.get_all("Tournament", fields=fields)
    if len(tournaments)==0:
        return None
    else:
        return tournaments[0]

@frappe.whitelist()
def get_territory_parents(territory):

    territory_raw_data = frappe.db.sql(""" 
        SELECT  ter.territory_name as territory,
                parter.territory_name as comunity,
                parparter.territory_name as country
        FROM `tabTerritory` as ter
        LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
        LEFT JOIN `tabTerritory` as parparter on parparter.territory_name = parter.parent_territory
        """, as_dict=True)

    territory_cache = {
        territory['territory']: (territory['comunity'], territory['country'])
        for territory
        in territory_raw_data
    }

    return territory_cache.get(territory, (None, None))

@frappe.whitelist()
def get_comunities():

    territory_raw_data = frappe.db.sql(""" 
        SELECT DISTINCT parter.territory_name as comunity
        FROM `tabTerritory` as ter
        LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
        LEFT JOIN `tabTerritory` as parparter on parparter.territory_name = parter.parent_territory
        ORDER BY comunity
        """, as_dict=True)

    comunity_list = list()

    for territory in territory_raw_data:
        if territory['comunity'] not in comunity_list and territory['comunity']:
            comunity_list.append(territory['comunity'])

    return comunity_list

def untie_group(group, commit=False):
    scoring_templates = frappe.get_list("Scoring Template",
                                            filters=[["group_code", "=", group], ["status", "!=", "Pending"], ["classification", "<", 5]],
                                            fields=["name", "total_mark", "tie_break_mark", "classification", "prize"],
                                            order_by="classification ASC")

    if len(scoring_templates)<=1:
        return

    one_none = []
    for st in scoring_templates:
        one_none.append(st)
        if st.prize==None:
            break

    sorted_sts = sorted(one_none, key=lambda st: -st.classification)

    fail_group = []

    for i, st in enumerate(sorted_sts[:len(sorted_sts)-1]):
        if sorted_sts[i].total_mark + sorted_sts[i].tie_break_mark == sorted_sts[i+1].total_mark + sorted_sts[i+1].tie_break_mark:
            for item in sorted_sts[i+1:]:
                if item.total_mark + item.tie_break_mark <= sorted_sts[i+1].total_mark + sorted_sts[i+1].tie_break_mark:
                    item.tie_break_mark +=1
                    if group not in fail_group:
                        fail_group.append(group)


    if commit:
        for item in sorted_sts:
            frappe.db.set_value("Scoring Template", item.name, "tie_break_mark", item.tie_break_mark)


    # if initial!=sorted_sts:
    #     print "---------Initial Status " + group
    #     for item in sorted_sts:
    #         print str(item.total_mark) + " " + str(item.tie_break_mark)
    #     print "---------Finish Status " + group
    #     for item in sorted_sts:
    #         print str(item.total_mark) + " " + str(item.tie_break_mark)

@frappe.whitelist()
def get_groups_select():

    tournament_groups = frappe.get_all(
        "Tournament Group",
        fields={
            'individual_code',
            'block_individual_resitration',
            'team_code',
            'block_team_registration',
            'description',
            'name',
            'team_size',
            'judging_template',
        }
    )

    results_individual = []
    results_team = []

    judging_templates_cache = {judging_template["name"]: judging_template["registration_description"]
                               for judging_template
                               in frappe.get_all("Judging Template",
                                                 fields=["name","registration_description"]
                                                 )
                               }

    for tournament_group in tournament_groups:
        if blank_undefined(tournament_group['individual_code']) != "Undefined" and not tournament_group['block_individual_resitration']:
            results_individual.append({
                "value": tournament_group['individual_code'],
                "label": tournament_group['description'],
                "id": tournament_group['name'],
                "registration_description": judging_templates_cache[tournament_group['judging_template']],
            })
        if blank_undefined(tournament_group['team_code']) != "Undefined" and not tournament_group['block_team_registration']:
            results_team.append({
                "value": tournament_group['team_code'],
                "label": tournament_group['description'],
                "id": tournament_group['name'],
                "team_size": tournament_group['team_size'],
                "registration_description": judging_templates_cache[tournament_group['judging_template']],
            })

    return {
        "results_individual": results_individual,
        "results_team": results_team
    }

@frappe.whitelist()
def set_judges_by_scoring_template(scoring_templates, judge1=None, judge2=None):
    templates = json.loads(scoring_templates)

    for sc in templates:
        sc = frappe.get_doc("Scoring Template", sc["name"])
        if judge1!=None and judge1!="":
            sc.judge_1 = judge1
        if judge2!=None and judge2!="":
            sc.judge_2 = judge2
        sc.db_update()

    return "OK"

@frappe.whitelist()
def enable_disable_judges(judges, action, check_templates=1):
    judges = json.loads(judges)

    check_templates = check_templates==1

    for judge in judges:
        judge = frappe.get_doc("Judge", judge["name"])
        if action=="Enable":
            judge.enable_judge()
        else:
            judge.finish_judge(check_templates)

    return "OK"

@frappe.whitelist()
def get_ungenerated_scoring_templates():
    individual = frappe.db.sql("""
        SELECT count(*)
        FROM `tabIndividual Registration`
        LEFT JOIN `tabScoring Template` ON `tabIndividual Registration`.name = `tabScoring Template`.registration
        WHERE `tabScoring Template`.registration is NULL
    """)[0][0]

    groups = frappe.db.sql("""
        SELECT count(*)
        FROM `tabTeam Registration`
        LEFT JOIN `tabScoring Template` ON `tabTeam Registration`.name = `tabScoring Template`.registration
        WHERE `tabScoring Template`.registration is NULL
    """)[0][0]

    return individual+groups

def get_taxes(exhibitor_tax, extra_tax=None, add_deduct_tax=None, fix_percentage_tax=None):

    if None in [extra_tax, add_deduct_tax, fix_percentage_tax]:
        tournament_data = get_tournament_value(["extra_tax", "add_deduct_tax", "fix_percentage_tax"])
        extra_tax = tournament_data["extra_tax"]
        add_deduct_tax = tournament_data["add_deduct_tax"]
        fix_percentage_tax = tournament_data["fix_percentage_tax"]

    if fix_percentage_tax == "Percentage":
        extra_tax = round(float(exhibitor_tax) * float(extra_tax) / 100.0, 2)

    if add_deduct_tax == "Deduct":
        tax = exhibitor_tax
        if extra_tax > exhibitor_tax:
            extra_tax = exhibitor_tax
            exhibitor_tax = 0
        else:
            exhibitor_tax -= extra_tax
    else:
        tax = exhibitor_tax + extra_tax
    
    return exhibitor_tax, extra_tax, tax

def on_login():
    return
