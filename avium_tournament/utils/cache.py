import frappe
from avium_tournament.utils.lru_cache import lru_cache


def clear_all_cache():
    get_doc.cache_clear()
    get_list.cache_clear()
    get_all.cache_clear()
    get_parent_territory.cache_clear()

@lru_cache(2000, 120)
def get_doc(doctype, docname):
    return frappe.get_doc(doctype, docname)

@lru_cache(2000, 120)
def get_list(*args, **kwargs):
    return frappe.get_list(*args, **kwargs)

@lru_cache(2000, 120)
def get_value(*args, **kwargs):
    return frappe.get_value(*args, **kwargs)

@lru_cache(2000, 120)
def get_all(*args, **kwargs):
    return frappe.get_all(*args, **kwargs)

@lru_cache(500, 15)
def get_parent_territory(territory):
    return frappe.db.get_value("Territory", territory, "parent_territory")

# def pdf_cache_get_list(cache, doctype, fields):
#     list_key = doctype+"_"+"-".join(fields)
#     if cache is None:
#         cache={}
#     if not "lists" in cache:
#         cache["lists"]={}
#     if not list_key in cache["lists"]:
#         cache["lists"][list_key] = frappe.get_all(doctype, fields=fields)
#
#     return cache["lists"][list_key], cache

# def pdf_cache_get(cache, doctype, docname):
#     if cache is None:
#         cache={}
#     if not doctype in cache:
#         cache[doctype] = {}
#     if not docname in cache[doctype]:
#         cache[doctype][docname] = frappe.get_doc(doctype, docname)
#
#     return cache[doctype][docname], cache

# def pdf_cache_get(doctype, docname):
#     doc = frappe.cache().hget('avium_pdf_cache_'+doctype, docname)
#     if doc == None:
#        doc = frappe.get_doc(doctype, docname)
#        frappe.cache().hset('avium_pdf_cache_'+doctype, docname, doc)
#     return doc

# def pdf_cache_delete(doctype, docname):
#     pass