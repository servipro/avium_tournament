import sqlite3
import frappe

@frappe.whitelist()
def migrate():
    db_file = "/home/pau/Documents/PlanillesExport/Pla.db"

    conn = sqlite3.connect(db_file)
    planillas = conn.execute('SELECT * FROM tablas_planilla').fetchall()

    for i, planilla in enumerate(planillas):
        planilla_id = planilla[0]
        clase_id = planilla[1]
        tamanio = planilla[2].replace("Talla: ", "")
        monudo = planilla[3]
        international = planilla[4]
        if international==1:
            continue
        factor_rojo = planilla[5]
        if planilla[6]==0:
            optional = "Optional"
        else:
            optional = "Mandatory"

        clase = conn.execute('SELECT * FROM tablas_clase WHERE codigo = ?', [clase_id]).fetchone()
        familia = clase[1]
        desc = clase[2]

        st = frappe.new_doc("Judging Template")
        st.family = familia
        st.template_name = unicode(desc)
        st.code = clase_id
        st.cage = "Undefined"
        st.description_extension = optional
        st.size = tamanio
        st.tufted = monudo
        st.red_factor = factor_rojo
        st.insert()

        #sep = ";"
        #print familia + sep + clase_id  + sep + unicode(desc) + sep + "Undefined" + sep + sep + optional + sep + tamanio + sep + str(monudo) + sep + str(factor_rojo)

        conceptos = conn.execute('SELECT * FROM tablas_concepto WHERE plan_id = ? order by ordre', [planilla_id]).fetchall()
        for concepto in conceptos:
            pmin = concepto[2]
            pmax = concepto[3]
            pind = concepto[4]
            neg = concepto[5]
            desc = concepto[7]

            st.append("concepts", {
                "concept_name":unicode(desc),
                "min_value": pmin,
                "max_value":pmax,
                "default_value":pind,
                "negative":neg==1
            })

        st.save()
            #print unicode(desc) + sep + unicode(pmin) + sep + unicode(pmax) + sep + unicode(pind) + sep + unicode(neg)

        frappe.db.commit()