import frappe
import json

from frappe.desk.doctype.desktop_icon.desktop_icon import set_hidden
from frappe.utils.password import update_password


@frappe.whitelist(allow_guest=True)
def register(doc):
    form_data = json.loads(doc)

    password = form_data["password"]

    exhibitor = frappe.new_doc("Exhibitor")
    exhibitor.dni = form_data["dni"]
    exhibitor.exhibitor_name = form_data["first_name"]
    exhibitor.exhibitor_surname = form_data["surname"]
    exhibitor.association = form_data["association"]
    exhibitor.federation = form_data["federation"]
    exhibitor.address = form_data["address"]
    exhibitor.city = form_data["city"]
    exhibitor.territory = form_data["territory"]
    exhibitor.postal_code = form_data["postal_code"]
    exhibitor.telephone = form_data["telephone"]
    exhibitor.email = form_data["email"]
    exhibitor.mobile_phone = form_data["mobile_phone"]
    exhibitor.allow = form_data["allow"]

    federation = frappe.get_doc("Federation", exhibitor.federation)

    exhibitor.append("breeder_code", {"breeder_code":form_data["breeder_1"], "federation_name": exhibitor.federation, "federation_code":federation.federation_code})
    if form_data["breeder_2"] != "":
        exhibitor.append("breeder_code", {"breeder_code": form_data["breeder_2"],"federation_name": exhibitor.federation, "federation_code":federation.federation_code})

    exhibitor.validate()
    exhibitor.flags.in_registration = True

    user = frappe.new_doc("User")
    user.email = form_data["email"]
    user.first_name = form_data["first_name"] + " " + form_data["surname"]
    user.flags.no_welcome_mail = True
    user.insert(ignore_permissions=True)

    user.add_roles("AVIUM Exhibitor")
    user.language = "es"

    exhibitor.user = user.name
    exhibitor.insert(ignore_permissions=True)

    update_password(user.name, password)

    set_hidden('Avium Tournament', user.name, 1)

    frappe.local.login_manager.login_as(user.name)

    return "ok"
