import frappe
from avium_tournament.utils import get_tournament_value,get_tournament
from frappe.translate import get_full_dict
from frappe import local


@frappe.whitelist()
def judge_interface_data():
    non_judjable_status = get_tournament("remove_trophy_location").split(",")
    judge = frappe.db.get_value("User", frappe.session.user, ["judge"])

    scoring_templates = frappe.db.sql("""
    		SELECT  `tabScoring Template`.status,
    		        `tabScoring Template`.group_code,
    		        `tabScoring Template`.cage_number,
    		        `tabScoring Template`.total_mark,
    		        `tabScoring Template`.tie_break_mark,
    		        `tabScoring Template`.classification,
    		        `tabScoring Template`.name,
    		        `tabScoring Template`.prize_visualization,
    		        `tabScoring Template`.location_status_a,
    		        `tabScoring Template`.location_status_b,
    		        `tabScoring Template`.location_status_c,
    		        `tabScoring Template`.location_status_d
    		FROM `tabScoring Template`
    		WHERE judge_1=%s OR judge_2=%s
    		ORDER BY group_code, classification, cage_number
    		""", [judge, judge], as_dict=True)

    add_tie_break_marks = get_tournament_value(["add_tie_break_marks",])['add_tie_break_marks']

    if add_tie_break_marks == "Yes":
        for scoring_template in scoring_templates:
            scoring_template['total_mark'] = scoring_template['total_mark'] + scoring_template['tie_break_mark']

    for scoring_template in scoring_templates[:]:
        for status in [scoring_template['location_status_a'],scoring_template['location_status_b'],scoring_template['location_status_c'],scoring_template['location_status_d']]:
            if status in non_judjable_status and scoring_template in scoring_templates:
                scoring_templates.remove(scoring_template)
    
    groups = frappe.db.sql("""
    		SELECT DISTINCT group_code
    		FROM `tabScoring Template`
    		WHERE judge_1=%s OR judge_2=%s
    		ORDER BY group_code
    		""", [judge, judge], as_dict=True)

    if not hasattr(local, 'lang'):
        local.lang = 'en'

    lang = local.lang

    status_tournament = get_tournament("tournament_status")
    if status_tournament != "Started":
        scoring_templates = []

    return {
        "add_tie_break_marks": add_tie_break_marks,
        "scoring_templates": scoring_templates,
        "_messages": get_full_dict(lang),
        "groups": groups
    }
