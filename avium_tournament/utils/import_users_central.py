import frappe
from frappe.frappeclient import FrappeClient
from avium_tournament.utils import get_tournament
from frappe import _
from frappe.utils.oauth import login_via_oauth2
import json


@frappe.whitelist(allow_guest=True)
def oauth_callback(code, state):
    login_via_oauth2("frappe", code, state, decoder=json.loads)
    frappe.local.response["type"] = "redirect"
    frappe.local.response["location"] = "/registration"

