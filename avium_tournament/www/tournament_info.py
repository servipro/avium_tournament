from __future__ import unicode_literals
import frappe
from avium_tournament.utils import get_tournament
from frappe import _

no_cache = 1
no_sitemap = 1

def get_context(context):
    context.no_cache = 1

    tournament = get_tournament()
    error = None

    if tournament is None:
        error = _("No tournament created, go to <a href='/desk'> Desk </a>")

    return {"tournament": tournament, "error": error}