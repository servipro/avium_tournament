from __future__ import unicode_literals
import frappe

no_cache = 1
no_sitemap = 1

# _("Group")

def get_context(context):
    roles = frappe.get_roles()
    if not "AVIUM Judge" in roles:
        context.error = "denied"
    else:
        context.error = ""

    context.no_cache = 1

    return context
