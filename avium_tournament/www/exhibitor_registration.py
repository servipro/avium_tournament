from __future__ import unicode_literals
import frappe
from avium_tournament.utils import get_tournament

no_cache = 1
no_sitemap = 1

def get_context(context):
    context.no_cache = 1

    associations = frappe.get_all("Association", fields=["name","association_name"], order_by="association_name")
    territories = frappe.get_all("Territory", fields=["name", "territory_name"], filters={"has_subterritories": 0}, order_by="territory_name")
    tournament = get_tournament()

    if tournament==None:
        frappe.throw(_("No tournament created"))

    return {
        "associations": associations,
        "territories": territories,
        "tournament": tournament
    }