from __future__ import unicode_literals

import json

import frappe
import sys
from avium_tournament.utils import get_tournament, get_exhibitor

no_cache = 0
no_sitemap = 0

PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3
PY34 = sys.version_info[0:2] >= (3, 4)

if PY3:
    string_types = str
else:
    string_types = basestring

def get_context(context):
    roles = frappe.get_roles(frappe.session.user)

    tournament = get_tournament()

    if tournament==None:
        frappe.throw(_("No tournament created"))

    if not "AVIUM Exhibitor" in roles:
        frappe.logger().error(roles)
        context["scoring_templates"] = []
        context["error"] = "denied"
        return context

    context.show_sidebar = False
    context.no_cache = 1

    context["tournament"] = tournament
    context["error"] = ""
    return context

@frappe.whitelist()
def save(doc):
    if isinstance(doc, string_types):
        doc = json.loads(doc)
    doc = frappe.get_doc(doc)
    for reg in doc.individual_table:
        reg.location_status_a = "Tournament"
    for reg in doc.team_table:
        if not reg.location_status_a:
            reg.location_status_a = "Tournament"
        if not reg.location_status_b:
            reg.location_status_b = "Tournament"
        if not reg.location_status_c:
            reg.location_status_c = "Tournament"
        if not reg.location_status_d:
            reg.location_status_d = "Tournament"
    doc.save(ignore_permissions=True)
    frappe.db.commit()
    return doc.as_dict()

@frappe.whitelist()
def get_registration(tournament=None):
    registration_list =  frappe.get_list("Registration", {"owner": frappe.session.user})
    tournament_doc = get_tournament()

    if len(registration_list) == 0:
        registration = frappe.new_doc("Registration")
        registration.tournament = tournament_doc.name
    else:
        registration = frappe.get_doc("Registration", registration_list[0].name)

    return {"registration": registration, "tournament": tournament_doc}


@frappe.whitelist()
def get_breeder_codes():
    exhibitor = get_exhibitor()

    if len(exhibitor.breeder_code)>0:
        return {
            "breeder_code": exhibitor.breeder_code[0].breeder_code,
            "federation_code": exhibitor.breeder_code[0].federation_code
        }
    else:
        return {
            "breeder_code": "",
            "federation_code": ""
        }
