from __future__ import unicode_literals
import frappe
import json
from avium_tournament.utils import get_tournament

no_cache = 1
no_sitemap = 1

def get_context(context):
	context.no_cache = 1
	roles = frappe.get_roles()
	if not "AVIUM Judge" in roles:
		return {"doc": [], "doc_url": "",  "error":"Denied"}

	if get_tournament("tournament_status")!="Started":
		return {"doc": [], "doc_url": "",  "error":"Torneo pausado"}

	doc = frappe.get_doc("Scoring Template", frappe.form_dict.st)

	registration_doc = frappe.get_doc(doc.template_type, doc.registration)
	tournament_group = frappe.get_doc("Tournament Group", registration_doc.group)
	judging_template = frappe.get_doc("Judging Template", tournament_group.judging_template)
	is_manual_harmony = doc.is_manual_harmony()
	is_manual_trophy = doc.is_manual_trophy()
	if is_manual_trophy:
		available_trophy = doc.get_available_trophies()
		available_trophy.insert(0, {
			"name":"",
			"description":""
		})
	else:
		available_trophy = []

	return { "doc": doc, "doc_url":judging_template.additional_information_file, "error":"", "is_manual_harmony": is_manual_harmony, "is_manual_trophy": is_manual_trophy, "available_trophy": available_trophy}

@frappe.whitelist()
def get_data(st):
	doc = frappe.get_doc("Scoring Template", st)

	registration_doc = frappe.get_doc(doc.template_type, doc.registration)
	tournament_group = frappe.get_doc("Tournament Group", registration_doc.group)
	judging_template = frappe.get_doc("Judging Template", tournament_group.judging_template)
	is_manual_harmony = doc.is_manual_harmony()
	is_manual_trophy = doc.is_manual_trophy()
	if is_manual_trophy:
		available_trophy = doc.get_available_trophies()
		available_trophy.insert(0, {
			"name":"",
			"description":""
		})
	else:
		available_trophy = []
	tournament = get_tournament()

	return { "doc": doc, "doc_url":judging_template.additional_information_file, "error":"", "is_manual_harmony": is_manual_harmony, "is_manual_trophy": is_manual_trophy, "available_trophy": available_trophy, "tournament": tournament}

