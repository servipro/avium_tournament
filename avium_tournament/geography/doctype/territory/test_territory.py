# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
from frappe.test_runner import make_test_records
from avium_tournament.tests.CustomTestCase import CustomTestCase

test_records = frappe.get_test_records('Territory')

class TestTerritory(CustomTestCase):

	def test_root_child(self):
		from avium_tournament.geography.doctype.territory.territory import _get_children
		root = _get_children(None, self.doctype_name, 'parent_territory')

		self.assertEquals(len(root), 1)
		self.assertEquals(root[0]["expandable"], 1)
		self.assertEquals(root[0]["value"], test_records[0]["territory_name"])
	
	def test_second_level(self):
		from avium_tournament.geography.doctype.territory.territory import _get_children
		root = _get_children(test_records[0]["territory_name"], self.doctype_name, 'parent_territory')

		self.assertEquals(len(root), 2)
		self.assertEquals(root[0]["expandable"], 1)
		self.assertEquals(root[1]["expandable"], 1)
		self.assertTrue(root[0]["value"] in list(map(lambda x: x["territory_name"], test_records)))
		self.assertTrue(root[1]["value"] in list(map(lambda x: x["territory_name"], test_records)))

	def test_third_level(self):
		from avium_tournament.geography.doctype.territory.territory import _get_children
		root = _get_children(test_records[1]["territory_name"], self.doctype_name, 'parent_territory')

		self.assertEquals(len(root), 1)
		self.assertEquals(root[0]["expandable"], 0)
		self.assertEquals(root[0]["value"], test_records[3]["territory_name"])

	def test_parents_root(self):
		from avium_tournament.utils import get_territory_parents
		comunity, country = get_territory_parents(test_records[0]["territory_name"])

		self.assertEquals(country, None)
		self.assertEquals(comunity, None)

	def test_comunity(self):
		from avium_tournament.utils import get_territory_parents
		comunity, country = get_territory_parents(test_records[1]["territory_name"])

		self.assertEquals(country, None)
		self.assertEquals(comunity, test_records[0]["territory_name"])

	def test_city(self):
		from avium_tournament.utils import get_territory_parents
		comunity, country = get_territory_parents(test_records[3]["territory_name"])

		self.assertEquals(country, test_records[0]["territory_name"])
		self.assertEquals(comunity, test_records[1]["territory_name"])