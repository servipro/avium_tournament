/* eslint-disable */
// rename this file from _test_[name] to test_[name] to activate
// and remove above this line

QUnit.module('avium');

QUnit.test("test: Create Territory", function(assert) {
	assert.expect(2);
	assert.setTimeout(1000); // Timeout of 1 second

	let done = assert.async();
	let random = "3";

	frappe.run_serially([
		() => frappe.set_route('List', 'Territory'),
		() => frappe.new_doc('Territory'),
		() => cur_frm.set_value('territory_name', "_Test Territory "+random),
		() => cur_frm.save(),
		(doc) => {
			assert.ok(doc)
			return frappe.set_route('Form', 'Territory', doc.name);
		},
		() => {
			assert.ok(cur_frm.doc.territory_name.includes(random));
			return done();
		}
	]);
});