QUnit.module('avium');

QUnit.test("test tournament", function(assert) {
	assert.expect(2);
	let done = assert.async();
	let random = frappe.utils.get_random(10);

	frappe.run_serially([
		() => frappe.set_route('List', 'Tournament'),
		() => frappe.new_doc('Tournament'),
		() => cur_frm.set_value('tournament_name', "Tournament "+random),
		() => cur_frm.save(),
		(doc) => {
			assert.ok(doc && !doc.__islocal);
			return frappe.set_route('Form', 'Tournament', doc.name);
		},
		() => {
			assert.ok(cur_frm.doc.tournament_name.includes(random));
			return done();
		}
	]);
});