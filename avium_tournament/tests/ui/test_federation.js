QUnit.module('avium');

QUnit.test("test federation", function(assert) {
	assert.expect(2);
	let done = assert.async();
	let random = frappe.utils.get_random(10);

	frappe.run_serially([
		() => frappe.set_route('List', 'Federation'),
		() => frappe.new_doc('Federation'),
		() => cur_frm.set_value('federation_name', "Test Federation "+random),
		() => cur_frm.set_value('territory', "Alicante"),
		() => cur_frm.save(),
		() => {
			console.log(cur_frm.doc.federation_name)
			assert.ok(cur_frm.doc.federation_name.includes(random));
			return done();
		}
	]);
});