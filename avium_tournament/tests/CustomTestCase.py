from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.test_runner import make_test_records
import unittest
import re
class CustomTestCase(unittest.TestCase):
    doctype_name = ""

    def __new__(cls, *args, **kwargs):
        cls.doctype_name = re.sub(r"(\w)([A-Z])", r"\1 \2", cls.__name__.replace("Test", ""))
        return object.__new__(cls, *args, **kwargs)

    @classmethod
    def setUpClass(cls):
        make_test_records(cls.doctype_name)