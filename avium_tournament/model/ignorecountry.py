from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class IgnoreCountry(Document):
    
	def before_insert(self):
		if hasattr(self, 'country') and not frappe.db.exists("Territory", self.country):
			self.country = ""
