import frappe

def execute():
    i=0
    individual_registrations = frappe.get_all("Individual Registration")
    team_registrations = frappe.get_all("Team Registration")

    for individual_registration_name in individual_registrations:
        individual_registration = frappe.get_doc("Individual Registration", individual_registration_name["name"])
        i+=1
        if individual_registration.registration_description == None or individual_registration.registration_description == "":
            individual_registration.registration_description = individual_registration.group_description
            individual_registration.save()

    i=0
    for team_registration_name in team_registrations:
        team_registration = frappe.get_doc("Team Registration", team_registration_name["name"])
        i+=1
        if team_registration.registration_description == None or team_registration.registration_description == "":
            team_registration.registration_description = team_registration.group_description
            team_registration.save()