import frappe

def execute():
    scoring_templates = frappe.get_all("Scoring Template")
    i=0

    for st_name in scoring_templates:
        st = frappe.get_doc("Scoring Template", st_name.name)
        st.big_prize_a = "No"
        st.big_prize_b = "No"
        st.big_prize_c = "No"
        st.big_prize_d = "No"
        st.db_update()
        i+=1