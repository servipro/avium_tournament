import frappe

from datetime import date

def execute():
    i=0
    scoring_templates = frappe.get_all("Scoring Template")
    for scoring_template in scoring_templates:
        scoring_template_doctype = frappe.get_doc("Scoring Template", scoring_template.name)
        scoring_template_doctype.judgment_date = date.today()
        i+=1