import frappe
from avium_tournament.reports.commonFunctions import blank_undefined

def execute():
    frappe.reload_doc("Avium Tournament", "doctype", "Tournament")
    frappe.reload_doc("Avium Tournament", "doctype", "Carrier")
    frappe.reload_doc("Avium Tournament", "doctype", "Judge")
    frappe.reload_doc("Avium Tournament", "doctype", "Association")
    frappe.reload_doc("Avium Tournament", "doctype", "Federation")
    frappe.reload_doc("Avium Tournament", "doctype", "Judge Association")

    territory_raw_data = frappe.db.sql(""" 
        SELECT  ter.territory_name as territory,
                parter.territory_name as comunity,
                parparter.territory_name as country
        FROM `tabTerritory` as ter
        LEFT JOIN `tabTerritory` as parter on parter.territory_name = ter.parent_territory
        LEFT JOIN `tabTerritory` as parparter on parparter.territory_name = parter.parent_territory
        """, as_dict=True)

    territory_cache = {
        territory['territory']: (territory['comunity'], territory['country'])
        for territory
        in territory_raw_data
    }

    tournament_raw_data = frappe.db.sql(""" 
        SELECT  tour.name as name, 
                tour.region as region
        FROM `tabTournament` as tour
        """, as_dict=True)

    for tournament in tournament_raw_data:
        if blank_undefined(tournament['region']) == "Undefined":
            territory = ""
        else:
            territory = tournament['region'].title()
        territory_parents = territory_cache.get(territory, ("", ""))
        frappe.db.sql(
            """UPDATE `tabTournament`
            SET territory=%(territory)s,
                comunity=%(comunity)s,
                country=%(country)s
            WHERE name = %(name)s""",
            {
                'name': tournament['name'],
                'territory': territory,
                'comunity': territory_parents[0],
                'country': territory_parents[1],
            }
        )

    registration_raw_data = frappe.db.sql(""" 
        SELECT  reg.name as name, 
                reg.territory as territory
        FROM `tabRegistration` as reg
        """, as_dict=True)

    for registration in registration_raw_data:
        if blank_undefined(registration['territory']) == "Undefined":
            territory = ""
        else:
            territory = registration['territory'].title()
        territory_parents = territory_cache.get(territory, ("", ""))
        frappe.db.sql(
            """UPDATE `tabRegistration`
            SET territory=%(territory)s,
                comunity=%(comunity)s,
                country=%(country)s
            WHERE name = %(name)s""",
            {
                'name': registration['name'],
                'territory': territory,
                'comunity': territory_parents[0],
                'country': territory_parents[1],
            }
        )

    carrier_raw_data = frappe.db.sql(""" 
        SELECT  car.name as name, 
                car.region as region
        FROM `tabCarrier` as car
        """, as_dict=True)

    for carrier in carrier_raw_data:
        if blank_undefined(carrier['region']) == "Undefined":
            territory = ""
        else:
            territory = carrier['region'].title()
        territory_parents = territory_cache.get(territory, ("", ""))
        frappe.db.sql(
            """UPDATE `tabCarrier`
            SET territory=%(territory)s,
                comunity=%(comunity)s,
                country=%(country)s
            WHERE name = %(name)s""",
            {
                'name': carrier['name'],
                'territory': territory,
                'comunity': territory_parents[0],
                'country': territory_parents[1],
            }
        )

    judge_raw_data = frappe.db.sql(""" 
        SELECT  jud.name as name, 
                jud.territory as territory
        FROM `tabJudge` as jud
        """, as_dict=True)

    for judge in judge_raw_data:
        if blank_undefined(judge['territory']) == "Undefined":
            territory = ""
        else:
            territory = judge['territory'].title()
        territory_parents = territory_cache.get(territory, ("", ""))
        frappe.db.sql(
            """UPDATE `tabFederation`
            SET territory=%(territory)s,
                comunity=%(comunity)s,
                country=%(country)s
            WHERE name = %(name)s""",
            {
                'name': judge['name'],
                'territory': territory,
                'comunity': territory_parents[0],
                'country': territory_parents[1],
            }
        )

    association_raw_data = frappe.db.sql(""" 
        SELECT  aso.name as name, 
                aso.territory as territory
        FROM `tabAssociation` as aso
        """, as_dict=True)

    for association in association_raw_data:
        if blank_undefined(association['territory']) == "Undefined":
            territory = ""
        else:
            territory = association['territory'].title()
        territory_parents = territory_cache.get(territory, ("", ""))
        frappe.db.sql(
            """UPDATE `tabAssociation`
            SET territory=%(territory)s,
                comunity=%(comunity)s,
                country=%(country)s
            WHERE name = %(name)s""",
            {
                'name': association['name'],
                'territory': territory,
                'comunity': territory_parents[0],
                'country': territory_parents[1],
            }
        )

    federation_raw_data = frappe.db.sql(""" 
        SELECT  fed.name as name, 
                fed.territory as territory
        FROM `tabFederation` as fed
        """, as_dict=True)

    for federation in federation_raw_data:
        if blank_undefined(federation['territory']) == "Undefined":
            territory = ""
        else:
            territory = federation['territory'].title()
        territory_parents = territory_cache.get(territory, ("", ""))
        frappe.db.sql(
            """UPDATE `tabFederation`
            SET territory=%(territory)s,
                comunity=%(comunity)s,
                country=%(country)s
            WHERE name = %(name)s""",
            {
                'name': federation['name'],
                'territory': territory,
                'comunity': territory_parents[0],
                'country': territory_parents[1],
            }
        )

    judge_association_raw_data = frappe.db.sql(""" 
        SELECT  judaso.name as name, 
                judaso.territory as territory
        FROM `tabJudge Association` as judaso
        """, as_dict=True)

    for judge_association in judge_association_raw_data:
        if blank_undefined(judge_association['territory']) == "Undefined":
            territory = ""
        else:
            territory = judge_association['territory'].title()
        territory_parents = territory_cache.get(territory, ("", ""))
        frappe.db.sql(
            """UPDATE `tabFederation`
            SET territory=%(territory)s,
                comunity=%(comunity)s,
                country=%(country)s
            WHERE name = %(name)s""",
            {
                'name': judge_association['name'],
                'territory': territory,
                'comunity': territory_parents[0],
                'country': territory_parents[1],
            }
        )