import frappe

def execute():

        frappe.reload_doc("Avium Tournament", "doctype", "Registration")

        exhibitors_breeder_raw_data = frappe.db.sql("""
            SELECT  exhbr.name as name,
                    exhbr.parent as parent
            FROM `tabExhibitor Breeder` as exhbr
            """, as_dict=True)

        exhibitors_raw_data = frappe.db.sql("""
            SELECT  exh.name as exhibitor,
                    exh.exhibitor_name as exhibitor_name,
                    exh.exhibitor_surname as exhibitor_surname,
                    exh.dni as dni,
                    exh.user as user,
                    exh.association as association,
                    exh.federation as federation,
                    exh.address as address,
                    exh.postal_code as postal_code,
                    exh.city as city,
                    exh.territory as territory,
                    exh.country as country,
                    exh.telephone as telephone,
                    exh.mobile_phone as mobile_phone,
                    exh.email as email,
                    exh.allow as allow
            FROM `tabExhibitor` as exh
            """, as_dict=True)

        exhibitors_data = {
            exhibitor['exhibitor']: exhibitor
            for exhibitor
            in exhibitors_raw_data
        }

        registrations_raw_data = frappe.db.sql("""
            SELECT  reg.name as name,
                    reg.exhibitor as exhibitor
            FROM `tabRegistration` as reg
            """, as_dict=True)

        registrations_data = {
            registration['exhibitor']: registration['name']
            for registration
            in registrations_raw_data
        }

        for registration in registrations_raw_data:
            exhibitor_data = exhibitors_data[registration['exhibitor']]
            frappe.db.set_value("Registration", registration['name'], "exhibitor_name", exhibitor_data['exhibitor_name'])
            frappe.db.set_value("Registration", registration['name'], "exhibitor_surname", exhibitor_data['exhibitor_surname'])
            frappe.db.set_value("Registration", registration['name'], "dni", exhibitor_data['dni'])
            frappe.db.set_value("Registration", registration['name'], "user", exhibitor_data['user'])
            frappe.db.set_value("Registration", registration['name'], "association", exhibitor_data['association'])
            frappe.db.set_value("Registration", registration['name'], "federation", exhibitor_data['federation'])
            frappe.db.set_value("Registration", registration['name'], "address", exhibitor_data['address'])
            frappe.db.set_value("Registration", registration['name'], "postal_code", exhibitor_data['postal_code'])
            frappe.db.set_value("Registration", registration['name'], "city", exhibitor_data['city'])
            frappe.db.set_value("Registration", registration['name'], "territory", exhibitor_data['territory'])
            frappe.db.set_value("Registration", registration['name'], "country", exhibitor_data['country'])
            frappe.db.set_value("Registration", registration['name'], "telephone", exhibitor_data['telephone'])
            frappe.db.set_value("Registration", registration['name'], "mobile_phone", exhibitor_data['mobile_phone'])
            frappe.db.set_value("Registration", registration['name'], "email", exhibitor_data['email'])
            frappe.db.set_value("Registration", registration['name'], "allow", exhibitor_data['allow'])

        for exhibitor_code in exhibitors_breeder_raw_data:
            registration_name = registrations_data.get(exhibitor_code['parent'], None)
            if registration_name is not None:
                frappe.db.set_value("Exhibitor Breeder", exhibitor_code['name'], "parent", registration_name)
                frappe.db.set_value("Exhibitor Breeder", exhibitor_code['name'], "parenttype", "Registration")

        frappe.db.commit()