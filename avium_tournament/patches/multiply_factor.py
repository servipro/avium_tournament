import frappe

def execute():
    frappe.reload_doc("Avium Tournament", "doctype", "Scoring Template Concept")
    frappe.reload_doc("Avium Tournament", "doctype", "Judging Template Concept")

    frappe.db.sql(
        """UPDATE `tabScoring Template Concept`
        SET multiply_factor=1"""
    )
    frappe.db.sql(
        """UPDATE `tabJudging Template Concept`
        SET multiply_factor=1"""
    )