import frappe

def execute():
    i=0
    sts = frappe.get_all("Scoring Template")
    for st_name in sts:
        st = frappe.get_doc("Scoring Template", st_name.name)
        st.update_rings()
        i+=1