# -*- coding: utf-8 -*-
import frappe
from avium_tournament.reports.commonFunctions import blank_undefined


def execute():
    frappe.reload_doc("Avium Tournament", "doctype", "Scoring Template")
    frappe.reload_doc("Avium Tournament", "doctype", "Judging Template")

    judgement_type_cache = {
        "A": "Canto Roller",
        "B": "Canto Malinois",
        "C": "Canto Timbrado Español",
        "D": "Pose/Color",
        "E": "Pose/Color",
        "F": "Pose/Color",
        "F1": "Pose/Color",
        "F2": "Pose/Color",
        "G": "Pose/Color",
        "H": "Pose/Color",
        "I": "Pose/Color",
        "J": "Pose/Color",
        "K": "Canto Fringilidos",
        "L": "Pose/Color",
        "M": "Canto Cantor Español",
    }

    judging_templates = frappe.get_all(
        'Judging Template',
        fields=['name', 'family']
    )

    for judging_template in judging_templates:
        frappe.db.sql(
            """UPDATE `tabJudging Template`
            SET judgement_type=%(judgement_type)s
            WHERE name = %(name)s""",
            {
                'name': judging_template['name'],
                'judgement_type': judgement_type_cache[judging_template['family']],
            }
        )

    individual_groups_cache = {
        blank_undefined(group['individual_code']): group['family']
        for group
        in frappe.get_all('Tournament Group', fields=['family', 'individual_code'])
    }

    team_groups_cache = {
        blank_undefined(group['team_code']): group['family']
        for group
        in frappe.get_all('Tournament Group', fields=['family', 'team_code'])
    }

    scoring_templates = [
        (
            scoring_template['name'],
            judgement_type_cache[
                individual_groups_cache.get(
                    scoring_template['group_code'],
                    team_groups_cache.get(
                        scoring_template['group_code'],
                        "D"
                    )
                )
            ],
        )
        for scoring_template
        in frappe.get_all('Scoring Template', fields=['name', 'group_code'])
    ]

    for scoring_template_name, judgement_type in scoring_templates:
        frappe.db.sql(
            """UPDATE `tabScoring Template`
            SET judgement_type=%(judgement_type)s
            WHERE name = %(name)s""",
            {
                'name': scoring_template_name,
                'judgement_type': judgement_type,
            }
        )