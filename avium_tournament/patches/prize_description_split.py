import frappe
from collections import OrderedDict
from avium_tournament.utils import get_tournament

def execute():
    frappe.reload_doc("Avium Tournament", "doctype", "Tournament Prizes")
    frappe.reload_doc("Avium Tournament", "doctype", "Tournament Prizes Description")
    frappe.reload_doc("Avium Tournament", "doctype", "Scoring Template")
    frappe.reload_doc("Avium Tournament", "doctype", "Scoring Template Concept")

    raw_data = frappe.db.sql("""
        SELECT  toupri.name as name,
                toupri.individual_mark as individual_mark,
                toupri.duo_mark as duo_mark,
                toupri.trio_mark as trio_mark,
                toupri.team_mark as team_mark,
                toupri.description as description_1,
                toupri.description_2 as description_2,
                toupri.description_3 as description_3,
                toupri.description_4 as description_4
        FROM `tabTournament Prizes` as toupri
        ORDER BY (individual_mark + duo_mark + trio_mark + team_mark) DESC
        """,
        as_dict=True
    )

    data = OrderedDict()
    for item in raw_data:
        if item['description_1'] not in data:
            data[item['description_1']] = dict()
            data[item['description_1']]['description_1'] = item['description_1']
            data[item['description_1']]['description_2'] = item['description_2']
            data[item['description_1']]['description_3'] = item['description_3']
            data[item['description_1']]['description_4'] = item['description_4']

    for index, key in enumerate(data.keys()):
        frappe.delete_doc("Tournament Prizes Description", key)
        new_doc = frappe.new_doc("Tournament Prizes Description")
        new_doc.assignment_order = index + 1
        new_doc.description_1 = data[key]['description_1']
        new_doc.description_2 = data[key]['description_2']
        new_doc.description_3 = data[key]['description_3']
        new_doc.description_4 = data[key]['description_4']
        new_doc.save()

    for prize in raw_data:
        doc = frappe.get_doc('Tournament Prizes', prize['name'])
        doc.prize_description = doc.description
        doc.save()

    tournament = get_tournament()
    if tournament:
        tournament.update_all_classifications()