import frappe
from collections import OrderedDict
from avium_tournament.utils import get_tournament

def execute():
    tournament = get_tournament()
    if tournament:
        tournament.update_all_classifications()