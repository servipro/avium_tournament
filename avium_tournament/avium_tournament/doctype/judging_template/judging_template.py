# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class JudgingTemplate(Document):

    def validate(self):
        # Check there are no scoring templates generated
        scoring_templates = frappe.db.sql("""
            SELECT sco_temp.name as name
            FROM `tabScoring Template` AS sco_temp
            LEFT JOIN `tabTournament Group` AS tougro ON tougro.name = sco_temp.group
            LEFT JOIN `tabJudging Template` AS judtem ON judtem.name = tougro.judging_template
            WHERE judtem.name = %(judging_template)s
            """,
            {"judging_template": self.name},
            as_dict=True
        )

        if len(scoring_templates) > 0:
            frappe.throw(frappe._("This Judging Template cannot be modified because there are Scoring Templates generated based on it."))

        # Check team size is in range
        if self.team_size < 1 or self.team_size > 4:
            frappe.throw(frappe._("Team size of judging template is incorrect. It should be between 1 and 4."))

        for concept in self.concepts:
            # Check the ranges for each concept are correct
            concept.validate()
            # Check the concepts are not repeated
            for comparing_concept in self.concepts:
                if concept.name != comparing_concept.name and concept.concept_name.lower() == comparing_concept.concept_name.lower():
                    frappe.throw((frappe._("Description of concepts {} and {} are the same.")).format(concept.idx, comparing_concept.idx))

        # Check the concepts are not repeated
        for specialisation in self.specialisations:
            for comparing_specialisation in self.specialisations:
                if specialisation.name != comparing_specialisation.name and specialisation.specialisation_name.lower() == comparing_specialisation.specialisation_name.lower():
                    frappe.throw((frappe._("Description of specialisation {} and {} are the same.")).format(specialisation.idx, comparing_specialisation.idx))
            if specialisation.team_size < 1 or specialisation.team_size > 4:
                frappe.throw((frappe._("Team size of the specialization {} is incorrect. It should be between 1 and 4.")).format(specialisation.specialisation_name))

        # Check printing order
        judging_templates = frappe.get_all("Judging Template", fields=["name", "family"])
        family_cache = {judging_template["name"]: judging_template["family"] for judging_template in judging_templates}

        specializations = frappe.get_all("Judging Template Specialisation", fields=["name", "parent", "specialisation_name", "printing_order"])
        for specialization in self.specialisations:
            printing_orders = [specialization_item["printing_order"] for specialization_item in specializations
                               if specialization_item["name"] != specialization.name and family_cache[specialization_item["parent"]]==self.family]
            specialization_cache = {specialization_item["printing_order"]: (specialization_item["name"], specialization_item["specialisation_name"], specialization_item["parent"])
                                    for specialization_item in specializations
                                    if specialization_item["name"] != specialization.name and family_cache[specialization_item["parent"]]==self.family}
            if specialization.printing_order in printing_orders:
                frappe.msgprint(frappe._("Specialization '{}' in Judging Template '{}' has the same printing code").format(specialization_cache[specialization.printing_order][1],specialization_cache[specialization.printing_order][2]))