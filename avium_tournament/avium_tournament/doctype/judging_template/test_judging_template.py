# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
from frappe.test_runner import make_test_records
from avium_tournament.tests.CustomTestCase import CustomTestCase
from avium_tournament.utils import get_tournament

test_records = frappe.get_test_records('Judging Template')
test_dependencies = ["Cage", "Family"]

class TestJudgingTemplate(CustomTestCase):
	def test_territories(self):
		pass