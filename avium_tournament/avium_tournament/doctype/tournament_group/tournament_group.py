# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class TournamentGroup(Document):
	def validate(self):
		if not(self.individual==1 or self.team==1):
			frappe.throw(_("Tournament Group {0} must be individual or team").format(self.description))
		if (self.team==1 and self.team_size <= 1):
			frappe.throw(_("Tournament Group {0} team size must be greater than 1").format(self.description))

