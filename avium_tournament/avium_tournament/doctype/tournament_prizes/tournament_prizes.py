# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class TournamentPrizes(Document):
    def update_prize_visualization(self):
        description = frappe.get_value("Tournament Prizes Description", self.prize_description, "description_1")
        frappe.db.sql(
            "UPDATE `tabScoring Template` SET prize_visualization = %s WHERE prize = %s",
            [description, self.name]
        )