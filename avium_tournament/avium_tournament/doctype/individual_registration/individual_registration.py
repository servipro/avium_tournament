# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe.model.document import Document
from frappe import _

class IndividualRegistration(Document):
    ## Overwritten
    def validate(self):
        if self.birth_year_a < (datetime.datetime.now().year - 3) or self.birth_year_a > datetime.datetime.now().year:
            frappe.throw(_('The birth year of the specimen A in the group {0}, row {1}, is incorrect.').format(self.group_code,self.idx))

    ## Custom
    def generate_scoring_template(self):
        if not frappe.db.exists("Scoring Template", {"registration": self.name}):
            st = frappe.new_doc("Scoring Template")
            st.template_type = "Individual Registration"
            st.registration = self.name
            st.team_size = 1
            st.group_code = self.group_code
            st.group = self.group
            st.group_description = self.group_description
            st.registration_description = self.registration_description
            st.workflow_state = "Judges Undefined"
            st.flags.generating_templates = True
            # Retrieve Concepts
            st.update_rings(group=self)
            st.update_location_status(group=self)
            st.gen_concepts(doc=self)
            st.db_insert()
            st.set_parent_in_children()
            for d in st.get_all_children():
                d.db_insert()
