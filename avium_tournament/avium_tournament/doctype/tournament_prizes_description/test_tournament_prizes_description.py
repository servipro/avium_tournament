# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
from frappe.test_runner import make_test_records
from avium_tournament.tests.CustomTestCase import CustomTestCase

test_records = frappe.get_test_records('Tournament Prizes Description')

class TestTournamentPrizesDescription(CustomTestCase):
	def test_duplicate_order(self):
		prize = frappe.new_doc(self.doctype_name)
		prize.assignment_order = 1
		prize.description_1 = "Repeated"
		with self.assertRaises(frappe.ValidationError):
			prize.insert()