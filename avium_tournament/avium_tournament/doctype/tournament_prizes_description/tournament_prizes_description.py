# -*- coding: utf-8 -*-
# Copyright (c) 2018, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class TournamentPrizesDescription(Document):

    def validate(self):
        #Check if the Assignment Order already exists
        existing_prizes_doctypes = frappe.get_list(
            "Tournament Prizes Description",
            filters={"name": ("!=", self.name)},
            fields=["name", "assignment_order"],
            order_by="assignment_order ASC"
        )

        existing_assignment_orders = [prize['assignment_order'] for prize in existing_prizes_doctypes]

        if self.assignment_order in existing_assignment_orders:
            existing_prizes_doctypes_cache = {
                str(prize['assignment_order']): prize['name']
                for prize in existing_prizes_doctypes
            }

            frappe.throw(
                frappe._(
                    "The assignment order has been already assigned to Tournament Prizes Description <b>{0}</b>"
                ).format(
                    existing_prizes_doctypes_cache[str(self.assignment_order)]
                )
            )

