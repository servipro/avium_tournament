// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.listview_settings['Tournament Prizes Description'] = {
    hide_name_column: true
};