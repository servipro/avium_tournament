# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from avium_tournament import validation
from frappe import _

from avium_tournament.model.ignorecountry import IgnoreCountry
from avium_tournament.utils import get_territory_parents

class Association(IgnoreCountry):
	def before_save(self):
		if self.territory:
			self.comunity, self.country = get_territory_parents(self.territory)

		if self.cif:
			self.cif=self.cif.upper()

		# If this attributes are not mandatory, lets not validate them
		# if self.telephone!=None and self.telephone!='' and not validation.valid_spanish_phone(self.telephone):
		# 	frappe.throw(_("Telephone is not valid"))

		# if self.email!=None and self.email!='' and not validation.valid_email(self.email):
		# 	frappe.throw(_("Email is not valid"))

		# if self.fax!=None and self.fax!='' and not validation.valid_spanish_phone(self.fax):
		# 	frappe.throw(_("Fax is not valid"))

		# if self.webpage!=None and self.webpage!='' and not validation.valid_website(self.webpage):
		# 	frappe.throw(_("Webpage is not valid"))

		# if self.mobile_phone!=None and self.mobile_phone!='' and not validation.valid_spanish_mobile(self.mobile_phone):
		# 	frappe.throw(_("Mobile phone is not valid"))

		# for contact_person in self.contact_person:
		# 	contact_person.validate()