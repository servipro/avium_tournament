# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
from frappe.test_runner import make_test_records
from avium_tournament.tests.CustomTestCase import CustomTestCase

test_records = frappe.get_test_records('Association')
test_dependencies = ["Territory"]

class TestAssociation(CustomTestCase):

	def test_cif_upper(self):
		assoc = frappe.get_doc(self.doctype_name, test_records[0]["association_name"])
		self.assertEquals(assoc.cif, test_records[0]["cif"].upper())

	def test_territories(self):
		assoc = frappe.get_doc(self.doctype_name, test_records[0]["association_name"])
		self.assertIsNot(assoc.comunity, None)
		self.assertIsNot(assoc.country, None)