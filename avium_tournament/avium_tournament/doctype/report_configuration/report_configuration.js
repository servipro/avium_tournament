// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Report Configuration', {
	reset_configuration: function(frm){
	    frm.set_value("report_type", "Simplified")
	    frm.set_value("upper_margin", 0.3)
	    frm.set_value("lower_margin", 0.2)
	    frm.set_value("left_margin", 0.5)
	    frm.set_value("right_margin", 0.4)
	    frm.set_value("left_logo", "Association Logo")
	    frm.set_value("logo_size", 100)
	    frm.set_value("only_first_page", 1)
	    frm.set_value("right_logo", "Federation Logo")
	    frm.set_value("judge_logo", "Right")
	    frm.set_value("title_background", "#3366cc")
	    frm.set_value("first_level_background", "#d2d2d2")
	    frm.set_value("second_level_background", "#ffffff")
	    frm.set_value("content_background", "#f2f2f2")
	    frm.set_value("title_text", "#ffffff")
	    frm.set_value("first_level_text", "#000000")
	    frm.set_value("second_level_text", "#000000")
	    frm.set_value("footer_height", 0.6)
	    frm.set_value("no_report_name", 0)
	    frm.set_value("footer_text_size", 7)
	    frm.set_value("no_page_number", 0)
        cur_frm.save()
	}
});