# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from frappe import _

def get_data():
	return {
		'fieldname': 'registration',
		#Field name if not contact
		'non_standard_fieldnames': {
			#EX 'Delivery Note': 'against_sales_invoice',
		},
		'internal_links': {
			#'Receivables': ['items', 'contact'],
			#'Delivery Note': ['items', 'delivery_note'],
		},
		'transactions': [
			{
				'label': _('Payments'),
				'items': ['Payment']
			}
		]
	}