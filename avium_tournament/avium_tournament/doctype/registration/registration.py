# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

from avium_tournament.reports import get_report_type
from avium_tournament.reports.commonFunctions import blank_undefined
from avium_tournament.reports.judging_stickers_report import JudgingStickersReport
from avium_tournament.reports.exhibitor_stickers_report import ExhibitorStickersReport
from avium_tournament.reports.extended.scoring_templates_report import ExtendedScoringTemplatesReport
from avium_tournament.reports.registration_summary_report import RegistrationSummaryReport
from avium_tournament.reports.simplified.scoring_templates_report import SimplifiedScoringTemplatesReport
from avium_tournament.utils import get_tournament
from avium_tournament.validation import valid_email
import frappe
from frappe import _
from frappe.model.document import Document
from itertools import groupby
import re
from unidecode import unidecode

class Registration(Document):

    ## Overwritten
    def onload(self):
        self.onload_data = {
            "extra": "extra_data",
            "other": [1,2,3]
        }

    def validate(self):

        # if not validation.valid_DNI(self.dni):
        #     frappe.throw(_("DNI is not valid"))

        # if self.telephone and not self.telephone.isdigit():
        #     frappe.throw(_("Telephone is not valid"))

        if self.email != None and self.email != '' and not valid_email(self.email):
            frappe.throw(_("Email is not valid"))

        # if self.mobile_phone and not self.mobile_phone.isdigit():
        #     frappe.throw(_("Mobile phone is not valid"))

        #self.check_min_registrations()
        self.check_min_books()
        self.check_specimen_leage()

        # Validate Breeder Codes
        #self.check_duplicated_breeder_codes()
        # TODO Review this checking if it's needed
        #self.check_changed_breeder_codes()

        # Validate Individual Registrations
        for individual in self.individual_table:
            individual.validate()

        # Validate Team Registrations
        for team in self.team_table:
            team.validate()

        # Validate Batch Registrations
        self.check_batch_individual_registrations()
        #self.check_batch_team_registrations()

    def before_save(self):
        tournament = get_tournament()

        try:
            frappe.only_for(["AVIUM Association", "AVIUM Exhibitor"])
        except:
            frappe.throw(_("Permission Denied"))

        status = tournament.get_exhibitors_registration_open()

        roles = frappe.get_roles()
        if "AVIUM Association" not in roles:
            if not tournament.get_exhibitors_registration_open():
                frappe.throw(_("Registrations can only be modified when the tournament is Published"))

        self.order_groups()

        for ex_code in self.breeder_code:
            if ex_code.federation_code is not None:
                ex_code.federation_code = re.sub('[-\.\ ]', '', ex_code.federation_code)
                ex_code.federation_code = ex_code.federation_code.upper()
            if ex_code.breeder_code is not None:
                ex_code.breeder_code = re.sub('[-\.\ ]', '', ex_code.breeder_code)
                ex_code.breeder_code = ex_code.breeder_code.upper()

        for ex_code in self.individual_table:
            if ex_code.federation_code_a is not None:
                ex_code.federation_code_a = re.sub('[-\.\ ]', '', ex_code.federation_code_a)
                ex_code.federation_code_a = ex_code.federation_code_a.upper()
            if ex_code.breeder_code_a is not None:
                ex_code.breeder_code_a = re.sub('[-\.\ ]', '', ex_code.breeder_code_a)
                ex_code.breeder_code_a = ex_code.breeder_code_a.upper()

        for ex_code in self.team_table:
            if ex_code.federation_code_a is not None:
                ex_code.federation_code_a = re.sub('[-\.\ ]', '', ex_code.federation_code_a)
                ex_code.federation_code_a = ex_code.federation_code_a.upper()
            if ex_code.breeder_code_a is not None:
                ex_code.breeder_code_a = re.sub('[-\.\ ]', '', ex_code.breeder_code_a)
                ex_code.breeder_code_a = ex_code.breeder_code_a.upper()
            if ex_code.federation_code_b is not None:
                ex_code.federation_code_b = re.sub('[-\.\ ]', '', ex_code.federation_code_b)
                ex_code.federation_code_b = ex_code.federation_code_b.upper()
            if ex_code.breeder_code_b is not None:
                ex_code.breeder_code_b = re.sub('[-\.\ ]', '', ex_code.breeder_code_b)
                ex_code.breeder_code_b = ex_code.breeder_code_b.upper()
            if ex_code.federation_code_c is not None:
                ex_code.federation_code_c = re.sub('[-\.\ ]', '', ex_code.federation_code_c)
                ex_code.federation_code_c = ex_code.federation_code_c.upper()
            if ex_code.breeder_code_c is not None:
                ex_code.breeder_code_c = re.sub('[-\.\ ]', '', ex_code.breeder_code_c)
                ex_code.breeder_code_c = ex_code.breeder_code_c.upper()
            if ex_code.federation_code_d is not None:
                ex_code.federation_code_d = re.sub('[-\.\ ]', '', ex_code.federation_code_d)
                ex_code.federation_code_d = ex_code.federation_code_d.upper()
            if ex_code.breeder_code_d is not None:
                ex_code.breeder_code_d = re.sub('[-\.\ ]', '', ex_code.breeder_code_d)
                ex_code.breeder_code_d = ex_code.breeder_code_d.upper()

        self.fix_breeder_federation_codes()
        self.set_increasing_idx()
        self.check_existing_breeder_federation_codes()
        self.check_remove_scoring_templates()
        self.update_total_price()
        self.payments_update(update_payment_date=False)
        self.update_scoring_templates()

        self.exhibitor_name = self.exhibitor_name.title()
        self.exhibitor_surname = self.exhibitor_surname.title()
        if self.email:
            self.email = self.email.lower()
        if self.address:
            self.address = self.address.title()
        if self.country:
            self.country = self.country.title()
        if self.city:
            self.city = self.city.title()

    def order_groups(self):
        self.individual_table.sort(key=sort_code_description, reverse=False)
        n=1
        for item in self.individual_table:
            item.idx=n
            n+=1
        self.team_table.sort(key=sort_code_description, reverse=False)
        n=1
        for item in self.team_table:
            item.idx=n
            n+=1

    def before_insert(self):
        # Check the registration is not duplicated
        self.check_duplicate()

        # Check the selected tournament is Published
        tournament = get_tournament()
        if tournament.tournament_status!="Published":
            frappe.throw(_("New registrations can only be created when the tournament is Published"))

        # Remove -, . and set letters to upper letters in the DNI
        if self.dni:
            self.dni=re.sub('[-\.\ ]', '', self.dni)
            self.dni=self.dni.upper()

        # Assign the registration ownership tho the exhibitor user
        self.owner = self.user

    def on_trash(self):
        tournament = get_tournament()
        if tournament.tournament_status in ["Finished"]:
            frappe.throw(_("You cannot delete a Registration if the tournament is {0} or {1}").format(
                ["Finished"]))

        individual_scoring_templates = self.get_ind_scoring_templates()
        team_scoring_templates = self.get_team_scoring_templates()

        for scoring_template in individual_scoring_templates:
            if scoring_template["status"]!="Pending":
                frappe.throw(_("Scoring Template {0} has been judged already, the registration cannot be deleted").format(scoring_template["name"]))

        for scoring_template in team_scoring_templates:
            if scoring_template["status"]!="Pending":
                frappe.throw(_("Scoring Template {0} has been judged already, the registration cannot be deleted").format(scoring_template["name"]))

        for scoring_template in team_scoring_templates:
            doc = frappe.get_doc("Scoring Template", scoring_template["name"])
            doc.flags.ignore_permissions=True
            doc.delete()

        for scoring_template in individual_scoring_templates:
            doc = frappe.get_doc("Scoring Template", scoring_template["name"])
            doc.flags.ignore_permissions=True
            doc.delete()

    def on_update(self):
        tournament_status = get_tournament("tournament_status")
        if self.total_price > 0 and tournament_status=="Published":
            self.send_email_notification()
        #self.generate_scoring_templates(assign_judge=True)
        self.update_scores_and_classification()

    def after_delete(self):
        if self.user != None:
            user = frappe.get_doc("User", self.user)
            user.flags.ignore_permissions=True
            user.delete()

    ## Auxiliar
    def get_ind_scoring_templates(self):
        scoring_templates = frappe.db.sql("""SELECT sco_temp.registration, sco_temp.name, sco_temp.status
                                        FROM `tabScoring Template` AS sco_temp
                                        JOIN `tabIndividual Registration` AS reg ON sco_temp.registration=reg.name
                                        WHERE reg.parent = %(registration)s""", {"registration": self.name}, as_dict=True)
        return scoring_templates

    def get_team_scoring_templates(self):
        scoring_templates = frappe.db.sql("""SELECT sco_temp.registration, sco_temp.name, sco_temp.status
                                        FROM `tabScoring Template` AS sco_temp
                                        JOIN `tabTeam Registration` AS reg ON sco_temp.registration=reg.name
                                        WHERE reg.parent = %(registration)s""", {"registration": self.name}, as_dict=True)
        return scoring_templates

    ## Custom
    def set_increasing_idx(self):
        for idx, item in enumerate(self.individual_table):
            item.idx = idx + 1

        for idx, item in enumerate(self.team_table):
            item.idx = idx + 1

    def fix_breeder_federation_codes(self):
        for code in self.breeder_code:
            code.federation_code = self.clean_federation_registration_code(code.federation_code)
            
        for individual_registration in self.individual_table:
            for attr_name in ["federation_code_a", "breeder_code_a"]:
                setattr(individual_registration, attr_name,
                        self.clean_federation_registration_code(getattr(individual_registration, attr_name)))

        for team_registration in self.team_table:
            for attr_name in ["federation_code_a", "federation_code_b", "federation_code_c", "federation_code_d",
                              "breeder_code_a", "breeder_code_b", "breeder_code_c", "breeder_code_d"]:
                setattr(team_registration, attr_name, self.clean_federation_registration_code(getattr(team_registration, attr_name)))

    def clean_federation_registration_code(self, code):
        if type(code) not in (str,):
            return code
        code = re.sub('[-\.\ ]', '', code)
        code = code.upper()
        return code

    def check_min_registrations(self):
        # Check minimum one registration
        if len(self.individual_table) + len(self.team_table) < 1:
            frappe.throw(_('One individual or team registration is needed to complete the registration.'))

    def check_min_books(self):
        tournament = get_tournament()

        if not tournament.enable_book:
            return

        specimens = len(self.individual_table)
        for team_registration in self.team_table:
            specimens += team_registration.team_size

        if specimens < tournament.minimum_specimens_to_be_applicable:
            return

        if self.books < tournament.minimum_book_quantity or blank_undefined(self.books) == 'Undefined':
            self.books = tournament.minimum_book_quantity
            frappe.msgprint(_("The minimum number of books is {0}.").format(tournament.minimum_book_quantity))

    def check_duplicate(self):
        duplicate_registrations = frappe.get_all('Registration',
                                                 filters={'exhibitor_name': self.exhibitor_name,
                                                          'exhibitor_surname': self.exhibitor_surname,
                                                          },
                                                 fields=['name']
                                                 )
        if len(duplicate_registrations)>0:
            frappe.throw(_('This registration already exists'))

    def check_specimen_leage(self):
        tournament = get_tournament()
        # Check there are minimum specimen of the same group inscribed in the league
        specimens_league_by_group = 0
        for individual in self.individual_table:
            if individual.league_inscription_a == 'Yes':
                specimens_league_by_group += 1
        for team in self.team_table:
            if team.league_inscription_a == 'Yes':
                specimens_league_by_group += 1
            if team.league_inscription_b == 'Yes':
                specimens_league_by_group += 1
            if team.league_inscription_c == 'Yes':
                specimens_league_by_group += 1
            if team.league_inscription_d == 'Yes':
                specimens_league_by_group += 1

        if specimens_league_by_group > 0 and specimens_league_by_group < tournament.minimum_league_inscriptions:
            frappe.throw(_(
                "There are only {0} specimens inscribed in the league. It's needed {1} specimens inscribed in the league to participate in it.").format(
                specimens_league_by_group, tournament.minimum_league_inscriptions))

    def check_remove_scoring_templates(self):
        individual_scoring_templates = self.get_ind_scoring_templates()
        team_scoring_templates = self.get_team_scoring_templates()

        current_individual_registrations = [individual_registration.name for individual_registration in self.individual_table]
        current_team_registrations = [team_registration.name for team_registration in self.team_table]

        for scoring_template in individual_scoring_templates:
            if scoring_template["registration"] not in current_individual_registrations:
                if scoring_template["status"]=="Pending":
                    template = frappe.get_doc("Scoring Template", scoring_template.name)
                    template.flags.ignore_permissions=True
                    template.delete()
                else:
                    frappe.throw(_("A group cannot be deleted if the template is not Pending"))

        for scoring_template in team_scoring_templates:
            if scoring_template["registration"] not in current_team_registrations:
                if scoring_template["status"]=="Pending":
                    template = frappe.get_doc("Scoring Template", scoring_template.name)
                    template.flags.ignore_permissions=True
                    template.delete()
                else:
                    frappe.throw(_("A group cannot be deleted if the template is not Pending"))

    def check_existing_breeder_federation_codes(self):
        exhibitor_breeder_codes = [(breeder_code.federation_code, breeder_code.breeder_code)
                                   for breeder_code
                                   in self.breeder_code]

        for individual_registration in self.individual_table:
            if (individual_registration.federation_code_a,
                individual_registration.breeder_code_a) not in exhibitor_breeder_codes:
                frappe.msgprint(_(
                    "Individual registration {0} federation code or breeder code is not inserted in exhibitor document").format(
                    individual_registration.idx))

        for team_registration in self.team_table:
            pairs =  [
                (team_registration.federation_code_a, team_registration.breeder_code_a),
                (team_registration.federation_code_b, team_registration.breeder_code_b),
                (team_registration.federation_code_c, team_registration.breeder_code_c),
                (team_registration.federation_code_d, team_registration.breeder_code_d)
            ]

            for pair in pairs[:team_registration.team_size]:
                if pair not in exhibitor_breeder_codes:
                    frappe.msgprint(_(
                        "Team registration {0} federation code or breeder code is not inserted in exhibitor document").format(
                        team_registration.idx))

    def check_duplicated_breeder_codes(self):
        for breeder_code in self.breeder_code:
            #Registrations with same combination of breeder and federation codes
            registrations = frappe.get_all("Exhibitor Breeder",
                                           filters={"breeder_code": breeder_code.breeder_code,
                                                    "federation_code": breeder_code.federation_code,
                                                    },
                                           fields=["parent"]
                                           )

            for registration in registrations:
                if registration["parent"]!=self.name:
                    frappe.throw(_("Breeder code {1} {0} is defined in registration {2}").format(breeder_code.breeder_code,breeder_code.federation_code, registration["parent"].replace("REG0","")))

    def check_changed_breeder_codes(self):
         #Check if breeder codes have been modified
         breeder_codes = frappe.db.sql("""SELECT bre_cod.name, bre_cod.federation_code, bre_cod.breeder_code
                                         FROM `tabExhibitor Breeder` AS bre_cod
                                         WHERE bre_cod.parent = %(exhibitor)s""", {"exhibitor": self.name}, as_dict=True)
         breeder_code_cache = {breeder_code["name"]: (breeder_code["federation_code"],breeder_code["breeder_code"])
                               for breeder_code in breeder_codes}
         if self.name!=None:
             registrations = frappe.get_all("Registration", filters={"exhibitor": self.name})

             if len(registrations) > 0:
                 for breeder_code in self.breeder_code:
                     if breeder_code.name != None:
                         if breeder_code.federation_code!=breeder_code_cache[breeder_code.name][0]:
                             frappe.msgprint(_("A federation code has been modified, please modify the registration {} according to this modification").format(registrations[0]["name"]))
                         if breeder_code.breeder_code!=breeder_code_cache[breeder_code.name][1]:
                             frappe.msgprint(_("A breeder code has been modified, please modify the registration {} according to this modification").format(registrations[0]["name"]))

    def check_batch_individual_registrations(self):

        specimens_by_group = dict()

        for registration in self.individual_table:
            if registration.group not in specimens_by_group:
                specimens_by_group[registration.group] = dict()
                specimens_by_group[registration.group]["group_code"] = registration.group_code
                group_batch_size = frappe.db.sql(
                    """SELECT tougro.individual_batch_size as batch_size
                    FROM `tabTournament Group` AS tougro
                    WHERE tougro.name = %(group)s""",
                    {"group": registration.group},
                    as_dict=True
                )
                specimens_by_group[registration.group]["batch_size"] = int(group_batch_size[0]["batch_size"])
                specimens_by_group[registration.group]["specimens"] = 0

            specimens_by_group[registration.group]["specimens"] += 1

        for group_name in specimens_by_group.keys():
            group = specimens_by_group[group_name]
            if group["batch_size"] > 0:
                specimens_in_incomplete_batch = group["specimens"] - (group["specimens"]/group["batch_size"]) * group["batch_size"]
                if specimens_in_incomplete_batch > 0:
                    frappe.throw(
                        _("Individual registrations for group {0} are in batch of {1}. {2} more specimens should be registered to complete the batch.").format(
                            group["group_code"], group["batch_size"], group["batch_size"] - specimens_in_incomplete_batch
                        )
                    )

    def check_batch_team_registrations(self):

        specimens_by_group = dict()

        for registration in self.team_table:
            if registration.group not in specimens_by_group:
                specimens_by_group[registration.group] = dict()
                specimens_by_group[registration.group]["group_code"] = registration.group_code
                group_batch_size = frappe.db.sql(
                    """SELECT tougro.team_batch_size as batch_size
                    FROM `tabTournament Group` AS tougro
                    WHERE tougro.name = %(group)s""",
                    {"group": registration.group},
                    as_dict=True
                )
                specimens_by_group[registration.group]["batch_size"] = int(group_batch_size[0]["batch_size"])
                specimens_by_group[registration.group]["specimens"] = 0

            specimens_by_group[registration.group]["specimens"] += 1

        for group_name in specimens_by_group.keys():
            group = specimens_by_group[group_name]
            if group["batch_size"] > 0:
                specimens_in_incomplete_batch = group["specimens"] - (group["specimens"]/group["batch_size"]) * group["batch_size"]
                if specimens_in_incomplete_batch > 0:
                    frappe.throw(
                        _("Team registrations for group {0} are in batch of {1}. {2} more teams should be registered to complete the batch.").format(
                            group["group_code"], group["batch_size"], group["batch_size"] - specimens_in_incomplete_batch
                        )
                    )

    def update_total_price(self):
        ## Specimen registration prices and units
        tournament = get_tournament()
        prices_and_units_data = tournament.get_registration_prices_and_units(self)

        self.individual_registrations = prices_and_units_data.get('individual_registrations', 0)
        self.team_registrations = prices_and_units_data.get('team_registrations', 0)
        self.batch_registrations = prices_and_units_data.get('batch_registrations', 0)
        self.total_specimen = prices_and_units_data.get('total_specimen', 0)

        individual_amount = self.individual_registration_price = prices_and_units_data.get('individual_registration_price', 0)
        team_amount = self.team_registration_price = prices_and_units_data.get('team_registration_price', 0)
        batch_amount = self.batch_registration_price = prices_and_units_data.get('batch_registration_price', 0)

        if prices_and_units_data.get('individual_registrations', 0) == 0:
            self.price_per_individual_registration = 0
        else:
            self.price_per_individual_registration = individual_amount/prices_and_units_data['individual_registrations']
        if prices_and_units_data.get('team_registrations', 0) == 0:
            self.price_per_team_registration = 0
        else:
            self.price_per_team_registration = team_amount/prices_and_units_data['team_registrations']
        if prices_and_units_data.get('batch_registrations', 0) == 0:
            self.price_per_batch_registration = 0
        else:
            self.price_per_batch_registration = batch_amount/prices_and_units_data['batch_registrations']

        ## Extras prices and units
        self.league_inscribed_specimens = prices_and_units_data.get('league_inscribed_specimens', 0)
        self.summary_books = self.books
        self.summary_meals = self.meals

        self.league_price = tournament.league_price
        self.price_per_book = tournament.book_price
        self.price_per_meal = tournament.meal_price

        league_amount = self.league_inscription_price = prices_and_units_data.get('league_inscribed_specimens', 0) * tournament.league_price
        books_amount = self.books_price = self.books * float(tournament.book_price)
        meals_amount = self.meals_price = self.meals * tournament.meal_price

        # Total price
        self.total_price = individual_amount + team_amount + batch_amount + league_amount + books_amount + meals_amount

    def payments_update(self,update_payment_date):
        updated_paid_amount = 0.0
        payments = frappe.get_all("Payment", filters={"registration": self.name}, fields=["payment_amount","payment_date"])

        for payment in payments:
            updated_paid_amount += payment["payment_amount"]
        self.paid_amount = updated_paid_amount
        self.remaining_amount = self.total_price - updated_paid_amount

        if update_payment_date:
            if len(payments)>0:
                payment_dates = [payment["payment_date"] for payment in payments if payment["payment_date"]!=None]
                self.payment_date = max(payment_dates)
            else:
                self.payment_date = None

    def generate_scoring_templates(self, assign_cage_number=True, assign_judge=False):
        for ind_registration in self.individual_table:
            ind_registration.generate_scoring_template()

        for team_registration in self.team_table:
            team_registration.generate_scoring_template()

        if assign_cage_number:
            tournament = get_tournament()
            groups = []
            for ind_registration in self.individual_table:
                if ind_registration.group_code not in groups:
                    tournament.assign_cage_number_group(group=ind_registration.group_code)
                    groups.append(ind_registration.group_code)

            for team_registration in self.team_table:
                if team_registration.group_code not in groups:
                    tournament.assign_cage_number_group(group=team_registration.group_code)
                    groups.append(team_registration.group_code)

        if assign_judge:
            tournament = get_tournament()
            tournament.assign_judges_empty()

        return "ok"

    def update_scoring_templates(self):
        individual_registrations = list(map(
            lambda registration: registration.name,
            self.individual_table
        ))

        team_registrations = list(map(
            lambda registration: registration.name,
            self.team_table
        ))

        scoring_templates_raw_data = []
        if len(individual_registrations)>0:
            scoring_templates_raw_data += frappe.db.sql("""
                SELECT  ireg.name as reg_name,
                        scote.name as st_name
                FROM `tabIndividual Registration` as ireg
                LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
                WHERE ireg.name IN %(individual_registrations)s
                """,
                {
                    'individual_registrations': individual_registrations,
                },
                as_dict=True
                )

        if len(team_registrations)>0:
            scoring_templates_raw_data += frappe.db.sql("""
                SELECT  treg.name as reg_name,
                        scote.name as st_name
                FROM `tabTeam Registration` as treg
                LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
                WHERE treg.name IN %(team_registrations)s
                """,
                {
                    'team_registrations': team_registrations,
                },
                as_dict=True
                )

        scoring_templates_name_cache = {
            scoring_template['reg_name']: scoring_template['st_name']
            for scoring_template
            in scoring_templates_raw_data
        }

        for individual_registration in self.individual_table:
            if individual_registration.name in scoring_templates_name_cache and scoring_templates_name_cache[individual_registration.name] is not None:
                st = frappe.get_doc("Scoring Template", scoring_templates_name_cache[individual_registration.name])
                st.update_rings(group=individual_registration)
                st.update_location_status(group=individual_registration)
                st.db_update()
                

        for team_registration in self.team_table:
            if team_registration.name in scoring_templates_name_cache and scoring_templates_name_cache[team_registration.name] is not None:
                st = frappe.get_doc("Scoring Template", scoring_templates_name_cache[team_registration.name])
                st.update_rings(group=team_registration)
                st.update_location_status(group=team_registration)
                st.db_update()

    def update_scores_and_classification(self):
        individual_registrations = list(map(
            lambda registration: registration.name,
            self.individual_table
        ))

        team_registrations = list(map(
            lambda registration: registration.name,
            self.team_table
        ))

        scoring_templates_raw_data = []
        if len(individual_registrations)>0:
            scoring_templates_raw_data += frappe.db.sql("""
                SELECT  ireg.name as reg_name,
                        scote.name as st_name
                FROM `tabIndividual Registration` as ireg
                LEFT JOIN `tabScoring Template` as scote on scote.registration = ireg.name
                WHERE ireg.name IN %(individual_registrations)s
                """,
                {
                    'individual_registrations': individual_registrations,
                },
                as_dict=True
                )

        if len(team_registrations)>0:
            scoring_templates_raw_data += frappe.db.sql("""
                SELECT  treg.name as reg_name,
                        scote.name as st_name
                FROM `tabTeam Registration` as treg
                LEFT JOIN `tabScoring Template` as scote on scote.registration = treg.name
                WHERE treg.name IN %(team_registrations)s
                """,
                {
                    'team_registrations': team_registrations,
                },
                as_dict=True
                )

        scoring_templates_name_cache = {
            scoring_template['reg_name']: scoring_template['st_name']
            for scoring_template
            in scoring_templates_raw_data
        }

        tournament = get_tournament()
        updated_groups_scoring = []

        for individual_registration in self.individual_table:
            if individual_registration.name in scoring_templates_name_cache and scoring_templates_name_cache[individual_registration.name] is not None:
                st = frappe.get_doc("Scoring Template", scoring_templates_name_cache[individual_registration.name])
                if st.status=="Judged":
                    st.update_scores()
                    st.db_update()
                    if st.group_code not in updated_groups_scoring:
                        updated_groups_scoring.append(st.group_code)                

        for team_registration in self.team_table:
            if team_registration.name in scoring_templates_name_cache and scoring_templates_name_cache[team_registration.name] is not None:
                st = frappe.get_doc("Scoring Template", scoring_templates_name_cache[team_registration.name])
                if st.status=="Judged":
                    st.update_scores()
                    st.db_update()
                    if st.group_code not in updated_groups_scoring:
                        updated_groups_scoring.append(st.group_code)
        
        for group in updated_groups_scoring:
            tournament.update_classification_group(group)

    def print_judging_stickers(self, arguments):
        arguments["registration"] = self.name

        with JudgingStickersReport(arguments) as report:
            return report.get_pdf()

    def print_exhibitor_stickers(self, arguments):
        arguments['sorted'] = "Registration"
        arguments['start_registration'] = self.name
        arguments['end_registration'] = self.name
        with ExhibitorStickersReport(arguments) as report:
            return report.get_pdf()

    def print_registration_summary(self, arguments):
        try:
            frappe.only_for(["System Manager", "AVIUM Association"])
        except:
            if frappe.session.user != self.owner:
                frappe.throw(_("Permission Denied"))

        ## Generate PDF
        arguments["registration"] = self.name
        try:
            frappe.only_for(["System Manager", "AVIUM Association"])
        except:
            arguments["cage"] = 0

        with RegistrationSummaryReport(arguments) as report:
            return report.get_pdf()

    def send_email_notification(self):
        frappe.db.set_default("lang", "es")
        frappe.local.lang = "es"

        if len(self.individual_table)==0 and len(self.team_table)==0:
            return

        tournament = get_tournament()

        arguments = dict()
        arguments["cage"] = 0
        arguments["carrier"] = 1

        pdf_file = self.print_registration_summary(arguments)

        body = _("Estimado {},<br><br> Gracias por su inscripción al {}. En el documento adjunto encontrarás toda la información relativa a su inscripción.<br><br><br><br> {} <br><br>Reciba un cordial saludo.").format(
                self.exhibitor_name,
                tournament.tournament_name,
                tournament.registration_payment_information)

        frappe.sendmail(
                    recipients=[self.email],
                    subject=_("Registration", "es") + " " + tournament.tournament_name,
                    reply_to= tournament.reply_to_mail or "info@avium.eu",
                    message=body,
                    reference_doctype=self.doctype,
                    reference_name=self.name,
                    attachments=[{
                        "fname": unidecode(_("Registration")) + "_" + unidecode(tournament.tournament_name).replace(" ", "_").replace("-", "_").replace(".", "_") + ".pdf",
                        "fcontent": pdf_file
                    }]
            )
        
        return "ok"
        
    def send_scoring_templates_to_expositor(self, body=""):
        frappe.db.set_default("lang", "es")
        frappe.local.lang = "es"

        if blank_undefined(self.email) == "Undefined":
            return

        if len(self.individual_table)==0 and len(self.team_table)==0:
            return

        tournament = get_tournament()

        arguments = dict()
        arguments['sorted'] = "Registration"
        arguments['start_registration'] = self.name
        arguments['end_registration'] = self.name
        arguments['empty'] = 0
        arguments['wo_exhibitor'] = 0
        arguments['only_judged'] = False

        report_type = get_report_type()
        if report_type == "Extended":
            with ExtendedScoringTemplatesReport(arguments) as report:
                pdf_file = report.get_pdf()

                frappe.sendmail(
                    recipients=[self.email],
                    subject=_("Results", "es") + " " + tournament.tournament_name,
                    reply_to= tournament.reply_to_mail or "info@avium.eu",
                    message=body,
                    reference_doctype=self.doctype,
                    reference_name=self.name,
                    attachments=[{
                        "fname": unidecode(tournament.tournament_name).replace(" ", "_").replace("-", "_").replace(".", "_") + ".pdf",
                        "fcontent": pdf_file
                    }]
                )

                return "ok"
        else:
            with SimplifiedScoringTemplatesReport(arguments) as report:
                pdf_file = report.get_pdf()

                frappe.sendmail(
                    recipients=[self.email],
                    subject=_("Results", "es") + " " + tournament.tournament_name,
                    reply_to= tournament.reply_to_mail or "info@avium.eu",
                    message=body,
                    reference_doctype=self.doctype,
                    reference_name=self.name,
                    attachments=[{
                        "fname": unidecode(tournament.tournament_name).replace(" ", "_").replace("-", "_").replace(".", "_") + ".pdf",
                        "fcontent": pdf_file
                    }]
                )

                return "ok"

def sort_code_description(elem):
    return elem.group_code+elem.registration_description

@frappe.whitelist()
def check_remove_group_line(dct, dcn):
    templates = frappe.get_list("Scoring Template", {"registration": dcn})

    if len(templates)>0:
        template = frappe.get_doc("Scoring Template", templates[0].name)
        if template.status != "Pending":
            return {"code": "st_judged", "template": templates[0].name}

    return {"code":"ok"}

@frappe.whitelist()
def update_ring(line_name, type, attr, value, st_name):

    if type=="I" and attr not in["ring_a", "tax_a", "location_status_a"]:
        return "not_group"

    if type=="I":
        doc = frappe.get_doc("Individual Registration", line_name)
    if type=="T":
        doc = frappe.get_doc("Team Registration", line_name)

    if attr.startswith("ring"):
        if getattr(doc, attr.replace("ring", "location_status")) == "Not Presented":
            return "not_presented"

    if attr.startswith('tax'):
        if value!="":
            value=float(value)
        else:
            value=0
        setattr(doc, attr, value)

    st = frappe.get_doc("Scoring Template", st_name)
    change_location = False
    
    if attr.startswith('location_status'):
        change_location = True
        setattr(doc, attr, value)
        if value == "Not Presented":
            if (st.status=="Judged" or st.status=="Tied") and st.is_judjable():
                return "not_change_judged"
            setattr(doc, attr.replace("location_status", "ring"), "NP")
            st.status = "Judged"
        else:
            if getattr(doc, attr.replace("location_status", "ring")) != "NP":
                return "not_change_judged"
            setattr(doc, attr.replace("location_status", "ring"), "")
            st.status = "Pending"
    else:
        setattr(doc, attr, value)

    doc.db_update()

    st.update_rings()
    if change_location:
        st.update_location_status()
        st.update_scores()
        for d in st.get_all_children():
            d.db_update()
    st.db_update()

    if change_location:
        tournament = get_tournament()
        tournament.update_classification_group(st.group_code)

    return "ok"

@frappe.whitelist()
def update_carrier(registration, carrier):
    reg = frappe.get_doc("Registration", registration)
    reg.carrier = carrier
    reg.save()

    return "ok"

@frappe.whitelist()
def get_registrations_and_rings(hide_assigned="true", registration=""):
    filter_ind = filter_group = ""

    if hide_assigned == "true":
        filter_ind = "WHERE (`tabIndividual Registration`.ring_a IS NULL or `tabIndividual Registration`.ring_a = '') "
        filter_group = "WHERE (`tabTeam Registration`.ring_a IS NULL or `tabTeam Registration`.ring_a = '' or " \
                       "(`tabTeam Registration`.team_size >= 2 AND (`tabTeam Registration`.ring_b IS NULL or `tabTeam Registration`.ring_b = '')) or " \
                       "(`tabTeam Registration`.team_size >= 3 AND (`tabTeam Registration`.ring_c IS NULL or `tabTeam Registration`.ring_c = '')) or " \
                       "(`tabTeam Registration`.team_size >= 4 AND (`tabTeam Registration`.ring_d IS NULL or `tabTeam Registration`.ring_d = ''))) "

    if registration != "":
        if hide_assigned == "true":
            filter_ind+= " AND tabRegistration.name = '{0}' ".format(registration)
            filter_group+= " AND tabRegistration.name = '{0}' ".format(registration)
        else:
            filter_ind = filter_group = "WHERE tabRegistration.name = '{0}' ".format(registration)

    regs = frappe.db.sql("""
            (SELECT     tabRegistration.name as reg_name,
                        tabRegistration.exhibitor_name,
                        tabRegistration.exhibitor_surname,
                        `tabIndividual Registration`.name,
                        `tabIndividual Registration`.group_code,
                        `tabIndividual Registration`.ring_a,
                        "-" as "ring_b",
                        "-" as "ring_c",
                        "-" as "ring_d",
                        `tabIndividual Registration`.location_status_a,
                        "-" as "location_status_b",
                        "-" as "location_status_c",
                        "-" as "location_status_d",
                        `tabIndividual Registration`.tax_a,
                        "-" as "tax_b",
                        "-" as "tax_c",
                        "-" as "tax_d",
                        `tabIndividual Registration`.idx,
                        `tabScoring Template`.cage_number,
                        `tabScoring Template`.name as "st_name",
                        "I" as type
            FROM tabRegistration
            INNER JOIN `tabIndividual Registration` ON tabRegistration.name = `tabIndividual Registration`.parent
            INNER JOIN `tabScoring Template` ON `tabScoring Template`.registration = `tabIndividual Registration`.name
            """ + filter_ind + """)
         UNION ALL
            (SELECT     tabRegistration.name as reg_name,
                    tabRegistration.exhibitor_name,
                    tabRegistration.exhibitor_surname,
                    `tabTeam Registration`.name,
                    `tabTeam Registration`.group_code,
                    `tabTeam Registration`.ring_a,
                    IF (`tabTeam Registration`.team_size >= 2, `tabTeam Registration`.ring_b,"-") as "ring_b",
                    IF (`tabTeam Registration`.team_size >= 3, `tabTeam Registration`.ring_c,"-") as "ring_c",
                    IF (`tabTeam Registration`.team_size >= 4, `tabTeam Registration`.ring_d,"-") as "ring_d",
                    `tabTeam Registration`.location_status_a,
                    IF (`tabTeam Registration`.team_size >= 2, `tabTeam Registration`.location_status_b,"-") as "location_status_b",
                    IF (`tabTeam Registration`.team_size >= 3, `tabTeam Registration`.location_status_c,"-") as "location_status_c",
                    IF (`tabTeam Registration`.team_size >= 4, `tabTeam Registration`.location_status_d,"-") as "location_status_d",
                    `tabTeam Registration`.tax_a,
                    IF (`tabTeam Registration`.team_size >= 2, `tabTeam Registration`.tax_b,"-") as "tax_b",
                    IF (`tabTeam Registration`.team_size >= 3, `tabTeam Registration`.tax_c,"-") as "tax_c",
                    IF (`tabTeam Registration`.team_size >= 4, `tabTeam Registration`.tax_d,"-") as "tax_d",
                    `tabTeam Registration`.idx + 1000,
                    `tabScoring Template`.cage_number,
                    `tabScoring Template`.name as "st_name",
                    "T" as type
                FROM tabRegistration
                INNER JOIN `tabTeam Registration` ON tabRegistration.name = `tabTeam Registration`.parent
                INNER JOIN `tabScoring Template` ON `tabScoring Template`.registration = `tabTeam Registration`.name
                """ + filter_group + """)
                
                ORDER BY type, SUBSTR(group_code FROM 1 FOR 1), CAST(SUBSTR(group_code FROM 3) AS UNSIGNED), cage_number, idx
    """, as_dict=1)

    regs.sort(key=lambda x: x.exhibitor_surname + ", " + x.exhibitor_name)

    results = []
    for key, group in groupby(regs, lambda x: x.exhibitor_surname + ", " + x.exhibitor_name):
        lines = []

        carrier = ""

        lines_group = list(group)

        for line in lines_group:
            carrier=line.carrier
            reg_name=line.reg_name
            lines.append({
                "name":line.name,
                "group_code": line.group_code,
                "group_description":line.group_description,
                "cage_number":line.cage_number,
                "ring_a":line.ring_a,
                "ring_b":line.ring_b,
                "ring_c":line.ring_c,
                "ring_d":line.ring_d,
                "tax_a": str(line.tax_a).rstrip("0").rstrip('.'),
                "tax_b": str(line.tax_b).rstrip("0").rstrip('.'),
                "tax_c": str(line.tax_c).rstrip("0").rstrip('.'),
                "tax_d": str(line.tax_d).rstrip("0").rstrip('.'),
                "st_name": line.st_name,
                "type": line.type
            })

        results.append({
            "exhibitor_name": key,
            "lines": lines,
            "carrier": carrier,
            "reg_name": reg_name
        })

    return results
