// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

// Load tournament doctype
var previous_group_team = ""
var previous_group_ind = ""
var previous_registration_description_team = ""
var previous_registration_description_ind = ""
var values_complete = ""
var league_inscription = ""
var fields = ""
var result_list = ""
var item = ""
var registration_codes = ""

var tournament_doctype
frappe.call({
    method:"avium_tournament.utils.get_tournament",
    args: {
    },
    async: false,
    callback: function(r) {
        if(r.message){
            tournament_doctype = r.message;
        }
    }
});

var set_group_dialog = function(frm, individual){
    frappe.call({
        method: "avium_tournament.utils.get_groups_select",
        args: {
        },
        callback: function(r) {
            values_complete = r.message;

            // Store first breeder code
            if (frm.doc.breeder_code.length>0) {
                window.federation = frm.doc.breeder_code[0].federation_code
                window.breeder = frm.doc.breeder_code[0].breeder_code
            } else {
                window.breeder = "";
                window.federation = "";
            };

            if(individual){
                league_inscription = cur_frm.doc.league_inscription
                fields = [
                    {'fieldname': 'ht', 'fieldtype': 'HTML'},
                    {'fieldname': 'group_description', 'fieldtype': 'Data', 'label': "Group Description", 'read_only': 1},
                    {'fieldname': 'registration_description', 'fieldtype': 'Data', 'label': "Registration Description", 'reqd':1, 'hidden':1, 'description': null},
                    {'fieldname': 'sbra', 'fieldtype': 'Section Break', "label": "Specimen A", 'hidden': 1},
                    {'fieldname': 'federation_code_a', 'fieldtype': 'Data', 'label': "Federation Code", 'reqd':1},
                    {'fieldname': 'birth_year_a', 'fieldtype': 'Int', 'label': "Birth Year"},
                    {'fieldname': 'cbr1', 'fieldtype': 'Column Break'},
                    {'fieldname': 'breeder_code_a', 'fieldtype': 'Data', 'label': "Breeder Code", 'reqd':1},
                    {'default': 'Male', 'fieldname': 'sex_a', 'fieldtype': 'Select', 'label': "Sex", 'options': "Male\nFemale"},
                    {'fieldname': 'cbr2', 'fieldtype': 'Column Break'},
                    {'fieldname': 'ring_a', 'fieldtype': 'Data', 'label': "Ring"},
                    {'default': 0.00, 'fieldname': 'tax_a', 'fieldtype': 'Currency', 'label': "Tax", 'hidden':!tournament_doctype.enable_tax},
                    {'fieldname': 'cbr3', 'fieldtype': 'Column Break'},
                    {'default': league_inscription, 'fieldname': 'league_inscription_a', 'fieldtype': 'Select', 'label': "League Inscription", 'options': "Yes\nNo", 'hidden':!tournament_doctype.enable_league_inscriptions}
                ]
                result_list = values_complete.results_individual
            }
            else{
                league_inscription = cur_frm.doc.league_inscription
                fields = [
                    {'fieldname': 'ht', 'fieldtype': 'HTML'},
                    {'fieldname': 'group_description', 'fieldtype': 'Data', 'label': "Group Description", 'read_only': 1},
                    {'fieldname': 'registration_description', 'fieldtype': 'Data', 'label': "Registration Description", 'reqd':1, 'hidden':1, 'description': null},
                    {'fieldname': 'sbra', 'fieldtype': 'Section Break', "label": "Specimen A", 'hidden': 1},
                    {'fieldname': 'federation_code_a', 'fieldtype': 'Data', 'label': "Federation Code", 'reqd':1},
                    {'fieldname': 'birth_year_a', 'fieldtype': 'Int', 'label': "Birth Year"},
                    {'fieldname': 'cbr1', 'fieldtype': 'Column Break'},
                    {'fieldname': 'breeder_code_a', 'fieldtype': 'Data', 'label': "Breeder Code", 'reqd':1},
                    {'default': 'Male', 'fieldname': 'sex_a', 'fieldtype': 'Select', 'label': "Sex", 'options': "Male\nFemale"},
                    {'fieldname': 'cbr2', 'fieldtype': 'Column Break'},
                    {'fieldname': 'ring_a', 'fieldtype': 'Data', 'label': "Ring"},
                    {'default': 0.00, 'fieldname': 'tax_a', 'fieldtype': 'Currency', 'label': "Tax", 'hidden':!tournament_doctype.enable_tax},
                    {'fieldname': 'cbr3', 'fieldtype': 'Column Break'},
                    {'default': league_inscription, 'fieldname': 'league_inscription_a', 'fieldtype': 'Select', 'label': "League Inscription", 'options': "Yes\nNo", 'hidden':!tournament_doctype.enable_league_inscriptions},
                    {'fieldname': 'sbrb', 'fieldtype': 'Section Break', "label": "Specimen B", 'hidden': 1},
                    {'fieldname': 'federation_code_b', 'fieldtype': 'Data', 'label': "Federation Code", 'reqd':1},
                    {'fieldname': 'birth_year_b', 'fieldtype': 'Int', 'label': "Birth Year"},
                    {'fieldname': 'cbr4', 'fieldtype': 'Column Break'},
                    {'fieldname': 'breeder_code_b', 'fieldtype': 'Data', 'label': "Breeder Code", 'reqd':1},
                    {'default': 'Male', 'fieldname': 'sex_b', 'fieldtype': 'Select', 'label': "Sex", 'options': "Male\nFemale"},
                    {'fieldname': 'cbr5', 'fieldtype': 'Column Break'},
                    {'fieldname': 'ring_b', 'fieldtype': 'Data', 'label': "Ring"},
                    {'default': 0.00, 'fieldname': 'tax_b', 'fieldtype': 'Currency', 'label': "Tax", 'hidden':!tournament_doctype.enable_tax},
                    {'fieldname': 'cbr6', 'fieldtype': 'Column Break'},
                    {'default': league_inscription, 'fieldname': 'league_inscription_b', 'fieldtype': 'Select', 'label': "League Inscription", 'options': "Yes\nNo", 'hidden':!tournament_doctype.enable_league_inscriptions},
                    {'fieldname': 'sbrc', 'fieldtype': 'Section Break', "label": "Specimen C", 'hidden': 1},
                    {'fieldname': 'federation_code_c', 'fieldtype': 'Data', 'label': "Federation Code", 'reqd':1},
                    {'fieldname': 'birth_year_c', 'fieldtype': 'Int', 'label': "Birth Year"},
                    {'fieldname': 'cbr7', 'fieldtype': 'Column Break'},
                    {'fieldname': 'breeder_code_c', 'fieldtype': 'Data', 'label': "Breeder Code", 'reqd':1},
                    {'default': 'Male', 'fieldname': 'sex_c', 'fieldtype': 'Select', 'label': "Sex", 'options': "Male\nFemale"},
                    {'fieldname': 'cbr8', 'fieldtype': 'Column Break'},
                    {'fieldname': 'ring_c', 'fieldtype': 'Data', 'label': "Ring"},
                    {'default': 0.00, 'fieldname': 'tax_c', 'fieldtype': 'Currency', 'label': "Tax", 'hidden':!tournament_doctype.enable_tax},
                    {'fieldname': 'cbr9', 'fieldtype': 'Column Break'},
                    {'default': league_inscription, 'fieldname': 'league_inscription_c', 'fieldtype': 'Select', 'label': "League Inscription", 'options': "Yes\nNo", 'hidden':!tournament_doctype.enable_league_inscriptions},
                    {'fieldname': 'sbrd', 'fieldtype': 'Section Break', "label": "Specimen D", 'hidden': 1},
                    {'fieldname': 'federation_code_d', 'fieldtype': 'Data', 'label': "Federation Code", 'reqd':1},
                    {'fieldname': 'birth_year_d', 'fieldtype': 'Int', 'label': "Birth Year"},
                    {'fieldname': 'cbr10', 'fieldtype': 'Column Break'},
                    {'fieldname': 'breeder_code_d', 'fieldtype': 'Data', 'label': "Breeder Code", 'reqd':1},
                    {'default': 'Male', 'fieldname': 'sex_d', 'fieldtype': 'Select', 'label': "Sex", 'options': "Male\nFemale"},
                    {'fieldname': 'cbr11', 'fieldtype': 'Column Break'},
                    {'fieldname': 'ring_d', 'fieldtype': 'Data', 'label': "Ring"},
                    {'default': 0.00, 'fieldname': 'tax_d', 'fieldtype': 'Currency', 'label': "Tax", 'hidden':!tournament_doctype.enable_tax},
                    {'fieldname': 'cbr12', 'fieldtype': 'Column Break'},
                    {'default': league_inscription, 'fieldname': 'league_inscription_d', 'fieldtype': 'Select', 'label': "League Inscription", 'options': "Yes\nNo", 'hidden':!tournament_doctype.enable_league_inscriptions}
                ]
                result_list = values_complete.results_team
            }

            if(result_list.length==0){
                frappe.msgprint("<b>"+__("No groups available")+"</b>", 'Info')
                return;
            }

            var d = new frappe.ui.Dialog({
                'title': __('Add New Registration'),
                'fields': fields,
                primary_action: function(){
                    d.hide();

                    var selector = d.fields_dict.ht.$wrapper.find("#select_group")

                    var group = null;
                    if(individual){
                        for (item in values_complete.results_individual) {
                            if(values_complete.results_individual[item].value===selector[0].value){
                                group=values_complete.results_individual[item];
                            }
                        }
                    }
                    else{
                        for (item in values_complete.results_team) {
                            if(values_complete.results_team[item].value===selector[0].value){
                                group=values_complete.results_team[item];
                            }
                        }
                    }

                    var values = d.get_values()

                    if (group != null){
                        if(individual){
                            var row = frappe.model.add_child(frm.doc, "Individual Registration", "individual_table");
                            row.federation_code_a = values.federation_code_a;
                            row.breeder_code_a = values.breeder_code_a;
                            row.ring_a = values.ring_a;
                            row.league_inscription_a = values.league_inscription_a;
                            row.birth_year_a = values.birth_year_a;
                            row.sex_a = values.sex_a;
                            row.tax_a = values.tax_a;
                            row.taxed_a = "No";
                            previous_group_ind = group;
                            previous_registration_description_ind = values.registration_description;
                        }
                        else{
                            var row = frappe.model.add_child(frm.doc, "Team Registration", "team_table");
                            row.federation_code_a = values.federation_code_a;
                            row.breeder_code_a = values.breeder_code_a;
                            row.ring_a = values.ring_a;
                            row.league_inscription_a = values.league_inscription_a;
                            row.birth_year_a = values.birth_year_a;
                            row.sex_a = values.sex_a;
                            row.tax_a = values.tax_a;
                            row.taxed_a = "No";
                            row.federation_code_b = values.federation_code_b;
                            row.breeder_code_b = values.breeder_code_b;
                            row.ring_b = values.ring_b;
                            row.league_inscription_b = values.league_inscription_b;
                            row.birth_year_b = values.birth_year_b;
                            row.sex_b = values.sex_b;
                            row.tax_b = values.tax_b;
                            row.taxed_b = "No";
                            row.federation_code_c = values.federation_code_c;
                            row.breeder_code_c = values.breeder_code_c;
                            row.ring_c = values.ring_c;
                            row.league_inscription_c = values.league_inscription_c;
                            row.birth_year_c = values.birth_year_c;
                            row.sex_c = values.sex_c;
                            row.tax_c = values.tax_c;
                            row.taxed_c = "No";
                            row.federation_code_d = values.federation_code_d;
                            row.breeder_code_d = values.breeder_code_d;
                            row.ring_d = values.ring_d;
                            row.league_inscription_d = values.league_inscription_d;
                            row.birth_year_d = values.birth_year_d;
                            row.sex_d = values.sex_d;
                            row.tax_d = values.tax_d;
                            row.taxed_d = "No";
                            row.team_size = group.team_size;
                            previous_group_team = group;
                            previous_registration_description_team = values.registration_description;
                        }
                        row.group = group.id;
                        row.group_code = group.value;
                        row.group_description = group.label;
                        row.registration_description = values.registration_description;
                    }

                    //Ordenem taules d'inscripcions individuals i per equips segons codi de grup

                    if (typeof frm.doc.individual_table !== 'undefined') {
                        registration_codes = [];
                        for (var i = 0; i < frm.doc.individual_table.length; i++) {
                            registration_codes.push([cur_frm.doc.individual_table[i].group_code, cur_frm.doc.individual_table[i]]);
                        }

                        registration_codes.sort((function(index){
                            return function(a, b){
                                return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
                            };
                        })(0));

                        for (var i = 0; i < registration_codes.length; i++) {
                            registration_codes[i][1].idx = i + 1;
                        };
                    };

                    if (typeof frm.doc.team_table !== 'undefined') {
                        registration_codes = [];
                        for (var i = 0; i < frm.doc.team_table.length; i++) {
                            registration_codes.push([cur_frm.doc.team_table[i].group_code, cur_frm.doc.team_table[i]]);
                        }

                        registration_codes.sort((function(index){
                            return function(a, b){
                                return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
                            };
                        })(0));

                        for (var i = 0; i < registration_codes.length; i++) {
                            registration_codes[i][1].idx = i + 1;
                        };
                    };

                    //Refresquem  vista de les taules
                    frm.refresh_field("individual_table")
                    frm.refresh_field("team_table")
                }
            });
            d.fields_dict.ht.$wrapper.html('<h5>'+__("Group")+'</h5> <input id="select_group" class="form-control"></input>');
            d.fields_dict.ht.$wrapper.addClass("has-error")

            var Latinise = {};
            Latinise.LATIN_MAP = {
                    "Á":"A","Ă":"A","Ắ":"A","Ặ":"A","Ằ":"A","Ẳ":"A","Ẵ":"A","Ǎ":"A","Â":"A","Ấ":"A","Ậ":"A","Ầ":"A","Ẩ":"A","Ẫ":"A","Ä":"A","Ǟ":"A","Ȧ":"A","Ǡ":"A","Ạ":"A","Ȁ":"A","À":"A","Ả":"A","Ȃ":"A","Ā":"A","Ą":"A","Å":"A","Ǻ":"A","Ḁ":"A","Ⱥ":"A","Ã":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ḃ":"B","Ḅ":"B","Ɓ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ć":"C","Č":"C","Ç":"C","Ḉ":"C","Ĉ":"C","Ċ":"C","Ƈ":"C","Ȼ":"C","Ď":"D","Ḑ":"D","Ḓ":"D","Ḋ":"D","Ḍ":"D","Ɗ":"D","Ḏ":"D","ǲ":"D","ǅ":"D","Đ":"D","Ƌ":"D","Ǳ":"DZ","Ǆ":"DZ","É":"E","Ĕ":"E","Ě":"E","Ȩ":"E","Ḝ":"E","Ê":"E","Ế":"E","Ệ":"E","Ề":"E","Ể":"E","Ễ":"E","Ḙ":"E","Ë":"E","Ė":"E","Ẹ":"E","Ȅ":"E","È":"E","Ẻ":"E","Ȇ":"E","Ē":"E","Ḗ":"E","Ḕ":"E","Ę":"E","Ɇ":"E","Ẽ":"E","Ḛ":"E","Ꝫ":"ET","Ḟ":"F","Ƒ":"F","Ǵ":"G","Ğ":"G","Ǧ":"G","Ģ":"G","Ĝ":"G","Ġ":"G","Ɠ":"G","Ḡ":"G","Ǥ":"G","Ḫ":"H","Ȟ":"H","Ḩ":"H","Ĥ":"H","Ⱨ":"H","Ḧ":"H","Ḣ":"H","Ḥ":"H","Ħ":"H","Í":"I","Ĭ":"I","Ǐ":"I","Î":"I","Ï":"I","Ḯ":"I","İ":"I","Ị":"I","Ȉ":"I","Ì":"I","Ỉ":"I","Ȋ":"I","Ī":"I","Į":"I","Ɨ":"I","Ĩ":"I","Ḭ":"I","Ꝺ":"D","Ꝼ":"F","Ᵹ":"G","Ꞃ":"R","Ꞅ":"S","Ꞇ":"T","Ꝭ":"IS","Ĵ":"J","Ɉ":"J","Ḱ":"K","Ǩ":"K","Ķ":"K","Ⱪ":"K","Ꝃ":"K","Ḳ":"K","Ƙ":"K","Ḵ":"K","Ꝁ":"K","Ꝅ":"K","Ĺ":"L","Ƚ":"L","Ľ":"L","Ļ":"L","Ḽ":"L","Ḷ":"L","Ḹ":"L","Ⱡ":"L","Ꝉ":"L","Ḻ":"L","Ŀ":"L","Ɫ":"L","ǈ":"L","Ł":"L","Ǉ":"LJ","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ń":"N","Ň":"N","Ņ":"N","Ṋ":"N","Ṅ":"N","Ṇ":"N","Ǹ":"N","Ɲ":"N","Ṉ":"N","Ƞ":"N","ǋ":"N","Ñ":"N","Ǌ":"NJ","Ó":"O","Ŏ":"O","Ǒ":"O","Ô":"O","Ố":"O","Ộ":"O","Ồ":"O","Ổ":"O","Ỗ":"O","Ö":"O","Ȫ":"O","Ȯ":"O","Ȱ":"O","Ọ":"O","Ő":"O","Ȍ":"O","Ò":"O","Ỏ":"O","Ơ":"O","Ớ":"O","Ợ":"O","Ờ":"O","Ở":"O","Ỡ":"O","Ȏ":"O","Ꝋ":"O","Ꝍ":"O","Ō":"O","Ṓ":"O","Ṑ":"O","Ɵ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Õ":"O","Ṍ":"O","Ṏ":"O","Ȭ":"O","Ƣ":"OI","Ꝏ":"OO","Ɛ":"E","Ɔ":"O","Ȣ":"OU","Ṕ":"P","Ṗ":"P","Ꝓ":"P","Ƥ":"P","Ꝕ":"P","Ᵽ":"P","Ꝑ":"P","Ꝙ":"Q","Ꝗ":"Q","Ŕ":"R","Ř":"R","Ŗ":"R","Ṙ":"R","Ṛ":"R","Ṝ":"R","Ȑ":"R","Ȓ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꜿ":"C","Ǝ":"E","Ś":"S","Ṥ":"S","Š":"S","Ṧ":"S","Ş":"S","Ŝ":"S","Ș":"S","Ṡ":"S","Ṣ":"S","Ṩ":"S","Ť":"T","Ţ":"T","Ṱ":"T","Ț":"T","Ⱦ":"T","Ṫ":"T","Ṭ":"T","Ƭ":"T","Ṯ":"T","Ʈ":"T","Ŧ":"T","Ɐ":"A","Ꞁ":"L","Ɯ":"M","Ʌ":"V","Ꜩ":"TZ","Ú":"U","Ŭ":"U","Ǔ":"U","Û":"U","Ṷ":"U","Ü":"U","Ǘ":"U","Ǚ":"U","Ǜ":"U","Ǖ":"U","Ṳ":"U","Ụ":"U","Ű":"U","Ȕ":"U","Ù":"U","Ủ":"U","Ư":"U","Ứ":"U","Ự":"U","Ừ":"U","Ử":"U","Ữ":"U","Ȗ":"U","Ū":"U","Ṻ":"U","Ų":"U","Ů":"U","Ũ":"U","Ṹ":"U","Ṵ":"U","Ꝟ":"V","Ṿ":"V","Ʋ":"V","Ṽ":"V","Ꝡ":"VY","Ẃ":"W","Ŵ":"W","Ẅ":"W","Ẇ":"W","Ẉ":"W","Ẁ":"W","Ⱳ":"W","Ẍ":"X","Ẋ":"X","Ý":"Y","Ŷ":"Y","Ÿ":"Y","Ẏ":"Y","Ỵ":"Y","Ỳ":"Y","Ƴ":"Y","Ỷ":"Y","Ỿ":"Y","Ȳ":"Y","Ɏ":"Y","Ỹ":"Y","Ź":"Z","Ž":"Z","Ẑ":"Z","Ⱬ":"Z","Ż":"Z","Ẓ":"Z","Ȥ":"Z","Ẕ":"Z","Ƶ":"Z","Ĳ":"IJ","Œ":"OE","ᴀ":"A","ᴁ":"AE","ʙ":"B","ᴃ":"B","ᴄ":"C","ᴅ":"D","ᴇ":"E","ꜰ":"F","ɢ":"G","ʛ":"G","ʜ":"H","ɪ":"I","ʁ":"R","ᴊ":"J","ᴋ":"K","ʟ":"L","ᴌ":"L","ᴍ":"M","ɴ":"N","ᴏ":"O","ɶ":"OE","ᴐ":"O","ᴕ":"OU","ᴘ":"P","ʀ":"R","ᴎ":"N","ᴙ":"R","ꜱ":"S","ᴛ":"T","ⱻ":"E","ᴚ":"R","ᴜ":"U","ᴠ":"V","ᴡ":"W","ʏ":"Y","ᴢ":"Z","á":"a","ă":"a","ắ":"a","ặ":"a","ằ":"a","ẳ":"a","ẵ":"a","ǎ":"a","â":"a","ấ":"a","ậ":"a","ầ":"a","ẩ":"a","ẫ":"a","ä":"a","ǟ":"a","ȧ":"a","ǡ":"a","ạ":"a","ȁ":"a","à":"a","ả":"a","ȃ":"a","ā":"a","ą":"a","ᶏ":"a","ẚ":"a","å":"a","ǻ":"a","ḁ":"a","ⱥ":"a","ã":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ḃ":"b","ḅ":"b","ɓ":"b","ḇ":"b","ᵬ":"b","ᶀ":"b","ƀ":"b","ƃ":"b","ɵ":"o","ć":"c","č":"c","ç":"c","ḉ":"c","ĉ":"c","ɕ":"c","ċ":"c","ƈ":"c","ȼ":"c","ď":"d","ḑ":"d","ḓ":"d","ȡ":"d","ḋ":"d","ḍ":"d","ɗ":"d","ᶑ":"d","ḏ":"d","ᵭ":"d","ᶁ":"d","đ":"d","ɖ":"d","ƌ":"d","ı":"i","ȷ":"j","ɟ":"j","ʄ":"j","ǳ":"dz","ǆ":"dz","é":"e","ĕ":"e","ě":"e","ȩ":"e","ḝ":"e","ê":"e","ế":"e","ệ":"e","ề":"e","ể":"e","ễ":"e","ḙ":"e","ë":"e","ė":"e","ẹ":"e","ȅ":"e","è":"e","ẻ":"e","ȇ":"e","ē":"e","ḗ":"e","ḕ":"e","ⱸ":"e","ę":"e","ᶒ":"e","ɇ":"e","ẽ":"e","ḛ":"e","ꝫ":"et","ḟ":"f","ƒ":"f","ᵮ":"f","ᶂ":"f","ǵ":"g","ğ":"g","ǧ":"g","ģ":"g","ĝ":"g","ġ":"g","ɠ":"g","ḡ":"g","ᶃ":"g","ǥ":"g","ḫ":"h","ȟ":"h","ḩ":"h","ĥ":"h","ⱨ":"h","ḧ":"h","ḣ":"h","ḥ":"h","ɦ":"h","ẖ":"h","ħ":"h","ƕ":"hv","í":"i","ĭ":"i","ǐ":"i","î":"i","ï":"i","ḯ":"i","ị":"i","ȉ":"i","ì":"i","ỉ":"i","ȋ":"i","ī":"i","į":"i","ᶖ":"i","ɨ":"i","ĩ":"i","ḭ":"i","ꝺ":"d","ꝼ":"f","ᵹ":"g","ꞃ":"r","ꞅ":"s","ꞇ":"t","ꝭ":"is","ǰ":"j","ĵ":"j","ʝ":"j","ɉ":"j","ḱ":"k","ǩ":"k","ķ":"k","ⱪ":"k","ꝃ":"k","ḳ":"k","ƙ":"k","ḵ":"k","ᶄ":"k","ꝁ":"k","ꝅ":"k","ĺ":"l","ƚ":"l","ɬ":"l","ľ":"l","ļ":"l","ḽ":"l","ȴ":"l","ḷ":"l","ḹ":"l","ⱡ":"l","ꝉ":"l","ḻ":"l","ŀ":"l","ɫ":"l","ᶅ":"l","ɭ":"l","ł":"l","ǉ":"lj","ſ":"s","ẜ":"s","ẛ":"s","ẝ":"s","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ᵯ":"m","ᶆ":"m","ń":"n","ň":"n","ņ":"n","ṋ":"n","ȵ":"n","ṅ":"n","ṇ":"n","ǹ":"n","ɲ":"n","ṉ":"n","ƞ":"n","ᵰ":"n","ᶇ":"n","ɳ":"n","ñ":"n","ǌ":"nj","ó":"o","ŏ":"o","ǒ":"o","ô":"o","ố":"o","ộ":"o","ồ":"o","ổ":"o","ỗ":"o","ö":"o","ȫ":"o","ȯ":"o","ȱ":"o","ọ":"o","ő":"o","ȍ":"o","ò":"o","ỏ":"o","ơ":"o","ớ":"o","ợ":"o","ờ":"o","ở":"o","ỡ":"o","ȏ":"o","ꝋ":"o","ꝍ":"o","ⱺ":"o","ō":"o","ṓ":"o","ṑ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","õ":"o","ṍ":"o","ṏ":"o","ȭ":"o","ƣ":"oi","ꝏ":"oo","ɛ":"e","ᶓ":"e","ɔ":"o","ᶗ":"o","ȣ":"ou","ṕ":"p","ṗ":"p","ꝓ":"p","ƥ":"p","ᵱ":"p","ᶈ":"p","ꝕ":"p","ᵽ":"p","ꝑ":"p","ꝙ":"q","ʠ":"q","ɋ":"q","ꝗ":"q","ŕ":"r","ř":"r","ŗ":"r","ṙ":"r","ṛ":"r","ṝ":"r","ȑ":"r","ɾ":"r","ᵳ":"r","ȓ":"r","ṟ":"r","ɼ":"r","ᵲ":"r","ᶉ":"r","ɍ":"r","ɽ":"r","ↄ":"c","ꜿ":"c","ɘ":"e","ɿ":"r","ś":"s","ṥ":"s","š":"s","ṧ":"s","ş":"s","ŝ":"s","ș":"s","ṡ":"s","ṣ":"s","ṩ":"s","ʂ":"s","ᵴ":"s","ᶊ":"s","ȿ":"s","ɡ":"g","ᴑ":"o","ᴓ":"o","ᴝ":"u","ť":"t","ţ":"t","ṱ":"t","ț":"t","ȶ":"t","ẗ":"t","ⱦ":"t","ṫ":"t","ṭ":"t","ƭ":"t","ṯ":"t","ᵵ":"t","ƫ":"t","ʈ":"t","ŧ":"t","ᵺ":"th","ɐ":"a","ᴂ":"ae","ǝ":"e","ᵷ":"g","ɥ":"h","ʮ":"h","ʯ":"h","ᴉ":"i","ʞ":"k","ꞁ":"l","ɯ":"m","ɰ":"m","ᴔ":"oe","ɹ":"r","ɻ":"r","ɺ":"r","ⱹ":"r","ʇ":"t","ʌ":"v","ʍ":"w","ʎ":"y","ꜩ":"tz","ú":"u","ŭ":"u","ǔ":"u","û":"u","ṷ":"u","ü":"u","ǘ":"u","ǚ":"u","ǜ":"u","ǖ":"u","ṳ":"u","ụ":"u","ű":"u","ȕ":"u","ù":"u","ủ":"u","ư":"u","ứ":"u","ự":"u","ừ":"u","ử":"u","ữ":"u","ȗ":"u","ū":"u","ṻ":"u","ų":"u","ᶙ":"u","ů":"u","ũ":"u","ṹ":"u","ṵ":"u","ᵫ":"ue","ꝸ":"um","ⱴ":"v","ꝟ":"v","ṿ":"v","ʋ":"v","ᶌ":"v","ⱱ":"v","ṽ":"v","ꝡ":"vy","ẃ":"w","ŵ":"w","ẅ":"w","ẇ":"w","ẉ":"w","ẁ":"w","ⱳ":"w","ẘ":"w","ẍ":"x","ẋ":"x","ᶍ":"x","ý":"y","ŷ":"y","ÿ":"y","ẏ":"y","ỵ":"y","ỳ":"y","ƴ":"y","ỷ":"y","ỿ":"y","ȳ":"y","ẙ":"y","ɏ":"y","ỹ":"y","ź":"z","ž":"z","ẑ":"z","ʑ":"z","ⱬ":"z","ż":"z","ẓ":"z","ȥ":"z","ẕ":"z","ᵶ":"z","ᶎ":"z","ʐ":"z","ƶ":"z","ɀ":"z","ﬀ":"ff","ﬃ":"ffi","ﬄ":"ffl","ﬁ":"fi","ﬂ":"fl","ĳ":"ij","œ":"oe","ﬆ":"st","ₐ":"a","ₑ":"e","ᵢ":"i","ⱼ":"j","ₒ":"o","ᵣ":"r","ᵤ":"u","ᵥ":"v","ₓ":"x"
            };
            String.prototype.latinise = function() {
                return this.replace(/[^A-Za-z0-9\[\] ]/g, function(a) {
                    return Latinise.LATIN_MAP[a] || a;
                });
            };

            var selector = d.fields_dict.ht.$wrapper.find("#select_group")
            var awesomplete = new Awesomplete(selector[0],{
                item: function (item, input) {
                    var html = "<strong>" + item.value + "</strong>";
                    html += '<span class="small">  ' + __(item.label) + '</span>';

                    return $('<li></li>')
                        .data('item.autocomplete', d)
                        .prop('aria-selected', 'false')
                        .html('<a><p>' + html + '</p></a>')
                        .get(0);
                },
                filter: function(item, input) {
                    return item.label.latinise().toLowerCase().indexOf(input.toLowerCase())!=-1 || item.value.latinise().toLowerCase().indexOf(input.toLowerCase())!=-1 ;
                },
                data: function (item, input) {
                    return {
                        label: item.label || item.value,
                        value: item.value
                    };
                },
                list: result_list,
                minChars: 0,
                maxItems: 2000,
                autoFirst: true,
                sort: function(a, b){
                    return (a.value == b.value ? 0 : (a.value < b.value ? -1 : 1));
                }
            });

            if(individual && previous_group_ind!=""){
                selector.val(previous_group_ind.value);
                set_values_dialog(d, selector[0].value, individual);
                d.fields_dict.registration_description.set_value(previous_registration_description_ind);
            }
            else if(!individual && previous_group_team!=""){
                selector.val(previous_group_team.value);
                set_values_dialog(d, selector[0].value, individual);
                d.fields_dict.registration_description.set_value(previous_registration_description_team);
            }

            selector.on("awesomplete-open", function(e) {
                me.autocomplete_open = true;
            });

            selector.on("awesomplete-close", function(e) {
                selector.css({"z-index": 1});
                me.autocomplete_open = false;
                set_values_dialog(d, selector[0].value, individual);
            });

            selector.on("focus", function() {
                    if(!selector.val()) {
                        awesomplete.evaluate();
                    }
            });

            d.show();
        }
    });
}

var set_values_dialog = function(d, group_name, individual){
    var group = null;
    if(individual){
        for (item in values_complete.results_individual) {
            if(values_complete.results_individual[item].value===group_name){
                group=values_complete.results_individual[item];
                d.fields_dict.group_description.set_value(group.label);
                switch (group.registration_description) {
                    case "Blocked":
                        d.fields_dict.registration_description.df.read_only = 1;
                        d.fields_dict.registration_description.df.hidden = 0;
                        d.fields_dict.registration_description.df.description = null;
                        d.fields_dict.registration_description.set_value(group.label);
                        break;
                    case "Editable":
                        d.fields_dict.registration_description.df.hidden = 0;
                        d.fields_dict.registration_description.df.read_only = 0;
                        d.fields_dict.registration_description.df.description = __("Please, modify registration description if needed");
                        d.fields_dict.registration_description.set_value(group.label);
                        break;
                    case "Empty":
                        d.fields_dict.registration_description.df.hidden = 0;
                        d.fields_dict.registration_description.df.read_only = 0;
                        d.fields_dict.registration_description.df.description = __("Please, modify registration description if needed");
                        d.fields_dict.registration_description.set_value("");
                        break;
                };
                d.fields_dict.sbra.df.hidden = 0;
                d.fields_dict.birth_year_a.set_value(new Date().getFullYear());
                d.fields_dict.breeder_code_a.set_value(window.breeder);
                d.fields_dict.federation_code_a.set_value(window.federation);
                d.refresh();
            }
        }
        if (group==null) {
            d.fields_dict.group_description.set_value(null);
            d.fields_dict.registration_description.df.read_only = 0;
            d.fields_dict.registration_description.set_value("");
            d.fields_dict.sbra.df.hidden = 1;
            d.refresh();
        }
    }
    else{
        for (item in values_complete.results_team) {
            if(values_complete.results_team[item].value===group_name){
                group=values_complete.results_team[item];
                d.fields_dict.group_description.set_value(group.label);
                switch (group.registration_description) {
                    case "Blocked":
                        d.fields_dict.registration_description.df.read_only = 1;
                        d.fields_dict.registration_description.df.hidden = 0;
                        d.fields_dict.registration_description.df.description = null;
                        d.fields_dict.registration_description.set_value(group.label);
                        break;
                    case "Editable":
                        d.fields_dict.registration_description.df.hidden = 0;
                        d.fields_dict.registration_description.df.read_only = 0;
                        d.fields_dict.registration_description.df.description = __("Please, modify registration description if needed");
                        d.fields_dict.registration_description.set_value(group.label);
                        break;
                    case "Empty":
                        d.fields_dict.registration_description.df.hidden = 0;
                        d.fields_dict.registration_description.df.read_only = 0;
                        d.fields_dict.registration_description.df.description = __("Please, modify registration description if needed");
                        d.fields_dict.registration_description.set_value(group.null);
                        break;
                };
                d.fields_dict.sbra.df.hidden = 0;
                d.fields_dict.sbrb.df.hidden = 0;
                switch (group.team_size) {
                    case 2:
                        d.fields_dict.sbrc.df.hidden = 1;
                        d.fields_dict.sbrd.df.hidden = 1;
                        d.fields_dict.ring_c.set_value(null);
                        d.fields_dict.league_inscription_c.set_value("No");
                        d.fields_dict.breeder_code_c.set_value(null);
                        d.fields_dict.birth_year_c.set_value(null);
                        d.fields_dict.ring_d.set_value(null);
                        d.fields_dict.league_inscription_d.set_value("No");
                        d.fields_dict.breeder_code_d.set_value(null);
                        d.fields_dict.birth_year_d.set_value(null);
                        break;
                    case 3:
                        d.fields_dict.sbrc.df.hidden = 0;
                        d.fields_dict.sbrd.df.hidden = 1;
                        d.fields_dict.ring_d.set_value(null);
                        d.fields_dict.league_inscription_d.set_value("No");
                        d.fields_dict.breeder_code_d.set_value(null);
                        d.fields_dict.birth_year_d.set_value(null);
                        break;
                    case 4:
                        d.fields_dict.sbrc.df.hidden = 0;
                        d.fields_dict.sbrd.df.hidden = 0;
                        break;
                }

                //Set default year
                d.fields_dict.birth_year_a.set_value(new Date().getFullYear())
                d.fields_dict.birth_year_b.set_value(new Date().getFullYear())
                d.fields_dict.birth_year_c.set_value(new Date().getFullYear())
                d.fields_dict.birth_year_d.set_value(new Date().getFullYear())

                d.fields_dict.breeder_code_a.set_value(window.breeder)
                d.fields_dict.breeder_code_b.set_value(window.breeder)
                d.fields_dict.breeder_code_c.set_value(window.breeder)
                d.fields_dict.breeder_code_d.set_value(window.breeder)

                d.fields_dict.federation_code_a.set_value(window.federation)
                d.fields_dict.federation_code_b.set_value(window.federation)
                d.fields_dict.federation_code_c.set_value(window.federation)
                d.fields_dict.federation_code_d.set_value(window.federation)

                d.refresh();
            }
        }
        if (group==null) {
            d.fields_dict.group_description.set_value(null);
            d.fields_dict.registration_description.df.read_only = 0;
            d.fields_dict.registration_description.set_value(group.null);
            d.fields_dict.sbra.df.hidden = 1;
            d.fields_dict.sbrb.df.hidden = 1;
            d.fields_dict.sbrc.df.hidden = 1;
            d.fields_dict.sbrd.df.hidden = 1;
            d.refresh();
        }
    }
}

frappe.ui.form.on("Registration", {
    refresh: function(frm) {
        // Filter Territory field
        cur_frm.set_query("territory", function() {
            return {
                "filters": {
                    "has_subterritories": 0
                }
            };
        });

        // Add New Specimen buttons to individual_table and team_table
        $('[data-fieldname="individual_table"] .grid-add-row').html(__("New specimen"))
        $('[data-fieldname="team_table"] .grid-add-row').html(__("New specimen"))

        // Add dashboard buttons
        if (!frm.doc.__islocal) {
            frm.add_custom_button(__("Send Registration Email"), function() {
                frappe.call({
                    method: "send_email_notification",
                    doc: cur_frm.doc,
                    freeze: true,
                    args:{
                    },
                    callback: function(r) {
                        switch(r.message) {
                            case "ok":
                                 frappe.show_alert(__("Registration sent succesfully"));
                                 break;
                            default:
                                 frappe.show_alert(__("There was an error generating sending the registration"));
                        }
                    }
                });
            },__("Actions"));
            frm.add_custom_button(__("Generate Scoring Templates"), function() {
                if (cur_frm.doc.__unsaved==1){
                    frappe.msgprint(__("Save before generating scoring templates"));
                }
                else {
                    frappe.call({
                        method: "generate_scoring_templates",
                        doc: cur_frm.doc,
                        freeze: true,
                        args:{
                            assign_cage_number: true,
                            assign_judge: true
                        },
                        callback: function(r) {
                            switch(r.message) {
                                case "ok":
                                    frappe.show_alert(__("Scoring Templates generated correctly"));
                                    break;
                                default:
                                    frappe.show_alert(__("There was an error generating the Scoring Templates"));
                            }
                        }
                    });
                }
            },__("Actions"));
            frm.add_custom_button(__("Send Scoring Templates to Expositor"), function() {
                if (frappe.user_roles.includes("AVIUM Association") || frappe.user_roles.includes("Administrator")) {
                    frappe.call({
                        method: "send_scoring_templates_to_expositor",
                        doc: cur_frm.doc,
                        args:{
                        },
                        callback: function() {
                        }
                    });
                } else {
                    frappe.msgprint(__("You are not allowed to perform this action."));

                }
            },__("Actions"));
            frm.add_custom_button(__("Judging Stickers"), function(frm) {
                var groups_list=[]
                $.each(cur_frm.doc.individual_table, function(item){
                        groups_list.push(cur_frm.doc.individual_table[item].group_code)
                })
                $.each(cur_frm.doc.team_table, function(item){
                        groups_list.push(cur_frm.doc.team_table[item].group_code)
                })

                groups_list = $.unique(groups_list).sort();

                frappe.prompt([{
                       //This field has been added to avoid showing the following field's list once the prompt appears
                       "fieldname": "dummy",
                       "fieldtype": "Int",
                       "read_only": 1,
                       "hidden": 1,
                    },{
                       "collapsible": 0,
                       "fieldname": "sbr1",
                       "fieldtype": "Section Break",
                       "label": __("Options"),
                    },{
                       "fieldname": "column",
                       "fieldtype": "Int",
                       "label": __("Column Start Position"),
                       "default": "1",
                       "reqd": 1,
                    },{
                       "fieldname": "direction",
                       "fieldtype": "Select",
                       "label": __("Print Direction"),
                       "options": [
                           { "value": "Vertical", "label": __("Vertical") },
                           { "value": "Horizontal", "label": __("Horizontal") }
                       ],
                       "default": "Horizontal",
                    },{
                       "fieldname": "cbr1",
                       "fieldtype": "Column Break",
                    },{
                       "fieldname": "row",
                       "fieldtype": "Int",
                       "label": __("Row Start Position"),
                       "default": "1",
                       "reqd": 1,
                    },{
                       "collapsible": 0,
                       "fieldname": "sbr2",
                       "fieldtype": "Section Break",
                       "label": __("Filters"),
                    },{
                       "fieldname": "start_group",
                       "fieldtype": "Select",
                       "label": __("Starting Group"),
                       "options": groups_list,
                       "default": groups_list[0],
                       "reqd": 1,
                    },{
                       "fieldname": "start_cage",
                       "fieldtype": "Int",
                       "label": __("Starting Cage"),
                    },{
                       "fieldname": "individual",
                       "fieldtype": "Select",
                       "label": __("Individual/Team Groups"),
                       "options": [
                           { "value": "All", "label": __("All") },
                           { "value": "Individual", "label": __("Individual") },
                           { "value": "Team", "label": __("Team") }
                       ],
                       "default": "Individual",
                    },{
                       "fieldname": "cbr2",
                       "fieldtype": "Column Break",
                    },{
                       "fieldname": "end_group",
                       "fieldtype": "Select",
                       "label": __("Ending Group"),
                       "options": groups_list,
                       "default": groups_list[groups_list.length-1],
                       "reqd": 1,
                    },{
                       "fieldname": "end_cage",
                       "fieldtype": "Int",
                       "label": __("Ending Cage"),
                    }],
                    function(values){
                        values.filter_registration = cur_frm.doc.name;
                        avium_tournament.pdf.pdf_doc_function(cur_frm.doc.doctype, cur_frm.doc.name, "print_judging_stickers", values, __("Judging Stickers"))
                    },
                    __('Judging Stickers'),
                    __('Print')
                );
            },__("Print"));
            frm.add_custom_button(__("Exhibitor Stickers"), function(frm) {
                frappe.prompt([
                    {
                       "fieldname": "column",
                       "fieldtype": "Int",
                       "label": __("Column Start Position"),
                       "default": "1",
                       "reqd": 1,
                    },{
                       "fieldname": "cbr1",
                       "fieldtype": "Column Break",
                    },{
                       "fieldname": "row",
                       "fieldtype": "Int",
                       "label": __("Row Start Position"),
                       "default": "1",
                       "reqd": 1,
                    }],
                    function(values){
                        avium_tournament.pdf.pdf_doc_function(cur_frm.doc.doctype, cur_frm.doc.name, "print_exhibitor_stickers", values, __("Exhibitor Stickers"))
                    },
                    __('Exhibitor Stickers'),
                    __('Print')
                );
            },__("Print"));
            frm.add_custom_button(__("Registration Summary"), function() {
                frappe.prompt([{
                        "fieldname": "cage",
                        "fieldtype": "Check",
                        "label": __("Print Cage Numbers"),
                    },{
                        "fieldname": "price",
                        "fieldtype": "Check",
                        "label": __("Do Not Print Price Summary"),
                    },{
                        "fieldname": "carrier",
                        "fieldtype": "Check",
                        "label": __("Print Carrier"),
                    }],
                    function(values){
                        avium_tournament.pdf.pdf_doc_function(cur_frm.doc.doctype, cur_frm.doc.name, "print_registration_summary", values, __("Registration Summary"));
                    },
                    __('Registration Summary'),
                    __('Print')
               );
            },__("Print"));

            // Set buttons background color blue
            $("button:contains("+__('Print')+")").addClass( "btn-primary" ).removeClass("btn-default");
            $("button:contains("+__('Actions')+")").addClass( "btn-primary" ).removeClass("btn-default");

            // Show summary sections
            frm.toggle_display("sbr_registration_summary_1", true);
            frm.toggle_display("sbr_registration_summary_2", true);
            frm.toggle_display("sbr_payments_summary", true);
        } else {
            // Hide summary sections
            frm.toggle_display("sbr_registration_summary_1", false);
            frm.toggle_display("sbr_registration_summary_2", false);
            frm.toggle_display("sbr_payments_summary", false);
        }

        // Variables for the groups selections cache
        previous_group_ind = previous_group_team = "";
        previous_registration_description_ind = previous_registration_description_team = "";

        // Hide/show Extra fields
        frm.toggle_display("books", tournament_doctype.enable_book);
        frm.toggle_display("meals", tournament_doctype.enable_meal);
        frm.toggle_display("league_inscription", tournament_doctype.enable_league_inscriptions);
        frm.toggle_display("summary_books", tournament_doctype.enable_book);
        frm.toggle_display("price_per_book", tournament_doctype.enable_book);
        frm.toggle_display("books_price", tournament_doctype.enable_book);
        frm.toggle_display("summary_meals", tournament_doctype.enable_meal);
        frm.toggle_display("price_per_meal", tournament_doctype.enable_meal);
        frm.toggle_display("meals_price", tournament_doctype.enable_meal);
        frm.toggle_display("league_inscribed_specimens", tournament_doctype.enable_league_inscriptions);
        frm.toggle_display("league_price", tournament_doctype.enable_league_inscriptions);
        frm.toggle_display("league_inscription_price", tournament_doctype.enable_league_inscriptions);
        frm.toggle_display("membership_prices", tournament_doctype.enable_membership_price);
        frm.toggle_display("sbr_carrier", tournament_doctype.enable_carrier);
        frm.fields_dict["individual_table"].grid.set_column_disp('tax_a', tournament_doctype.enable_tax)
        frm.fields_dict["individual_table"].grid.set_column_disp('taxed_a', tournament_doctype.enable_tax)
        frm.fields_dict["individual_table"].grid.set_column_disp('league_inscription_a', tournament_doctype.enable_league_inscriptions)
        frm.fields_dict["team_table"].grid.set_column_disp('tax_a', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('taxed_a', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('league_inscription_a', tournament_doctype.enable_league_inscriptions)
        frm.fields_dict["team_table"].grid.set_column_disp('tax_b', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('taxed_b', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('league_inscription_b', tournament_doctype.enable_league_inscriptions)
        frm.fields_dict["team_table"].grid.set_column_disp('tax_c', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('taxed_c', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('league_inscription_c', tournament_doctype.enable_league_inscriptions)
        frm.fields_dict["team_table"].grid.set_column_disp('tax_d', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('taxed_d', tournament_doctype.enable_tax)
        frm.fields_dict["team_table"].grid.set_column_disp('league_inscription_d', tournament_doctype.enable_league_inscriptions)

    },
    onload_post_render: function(frm){

        $("[data-fieldname='individual_table'] .btn.grid-add-row").off();
        $("[data-fieldname='individual_table'] .btn.grid-add-row").click(function(){
            set_group_dialog(frm,true)
        });

        $("[data-fieldname='team_table'] .btn.grid-add-row").off();
        $("[data-fieldname='team_table'] .btn.grid-add-row").click(function(){
            set_group_dialog(frm,false)
        });
    },
    onload: function(frm) {
        // Habilitem l'edició del camp taxed
        if (frappe.user_roles.includes("AVIUM Association")){
            cur_frm.get_field("individual_table").grid.toggle_enable("taxed_a", true);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_a", true);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_b", true);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_c", true);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_d", true);
        } else {
            cur_frm.get_field("individual_table").grid.toggle_enable("taxed_a", false);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_a", false);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_b", false);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_c", false);
            cur_frm.get_field("team_table").grid.toggle_enable("taxed_d", false);
        }
    },
    association: function(frm) {
        frappe.call({
            method:"frappe.client.get_value",
            args: {
                'doctype': "Association",
				'filters': {'name': frm.doc.association},
				'fieldname':[
					'federation'
				]
            },
            callback: function(r) {
                if(r.message){
                    frappe.call({
                        method:"frappe.client.get_value",
                        args: {
                            'doctype': "Federation",
                            'filters': {'name': r.message.federation},
                            'fieldname':[
                                'federation_name',
                                'federation_code'
                            ]
                        },
                        callback: function(r) {
                            if(r.message){
                                frm.set_value("federation", r.message.federation_name);
                                if(frm.get_field("breeder_code").grid.grid_rows.length>0){
                                    $.each(frm.doc.breeder_code || [], function(i, v) {
                                        if (v.federation_name==undefined) {
                                            frappe.model.set_value(v.doctype, v.name, "federation_name", r.message.federation_name)
                                            frappe.model.set_value(v.doctype, v.name, "federation_code", r.message.federation_code)
                                        }
                                    })
                                }
                            }
                        }
                    });
                }
            }
        });
    },
    territory: function(frm) {
        frm.set_value("comunity", "");
        frm.set_value("country", "");

        if (frm.doc.territory) {
            frappe.call({
                method:"avium_tournament.utils.get_territory_parents",
                args: {
                    'territory': frm.doc.territory
                },
                callback: function(r) {
                    if(r.message[0]!=null){
                        frm.set_value("comunity", r.message[0]);
                    }
                    if(r.message[1]!=null){
                        frm.set_value("country", r.message[1]);
                    }
                }
            });
        }
    },
});

frappe.ui.form.on("Exhibitor Breeder", {
    breeder_code_add: function(frm , dct, dcn){
        if (frm.doc.association) {
            frappe.call({
                method:"frappe.client.get_value",
                args: {
                    'doctype': "Association",
                    'filters': {'name': frm.doc.association},
                    'fieldname':[
                        'federation'
                    ]
                },
                callback: function(r) {
                    if(r.message){
                        frappe.call({
                            method:"frappe.client.get_value",
                            args: {
                                'doctype': "Federation",
                                'filters': {'name': r.message.federation},
                                'fieldname':[
                                    'federation_name',
                                    'federation_code'
                                ]
                            },
                            callback: function(r) {
                                if(r.message){
                                    frappe.model.set_value(dct, dcn, "federation_name", r.message.federation_name)
                                    frappe.model.set_value(dct, dcn, "federation_code", r.message.federation_code)
                                }
                            }
                        });
                    }
                }
            });
        }
    },
    federation_name: function(frm , dct, dcn){
        if (frappe.model.get_value(dct, dcn, "federation_name") == undefined) {
            frappe.model.set_value(dct, dcn, "federation_code", "")
        }
    },
});

frappe.ui.form.on("Individual Registration", {
    before_individual_table_remove: function(frm , dct, dcn){
        frappe.call({
			method: "avium_tournament.avium_tournament.doctype.registration.registration.check_remove_group_line",
			args: {
				dct: dct,
				dcn: dcn
			},
			async: false,
			always: function(r){
			    if(r.message.code == "st_judged"){
			       	frappe.throw(__("You cannot delete this row, as the template {0} has been judged already.", [r.message.template]))
			    }
			}
		})
    },
});

frappe.ui.form.on("Team Registration", {
    before_team_table_remove: function(frm, dct, dcn){
        frappe.call({
			method: "avium_tournament.avium_tournament.doctype.registration.registration.check_remove_group_line",
			args: {
				dct: dct,
				dcn: dcn
			},
			async: false,
			always: function(r){
			    if(r.message.code == "st_judged"){
			       	frappe.throw(__("You cannot delete this row, as the template {0} has been judged already.", [r.message.template]))
			    }
			}
		})
    },
});
