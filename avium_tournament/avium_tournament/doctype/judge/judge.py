# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, re
from frappe.model.document import Document
from avium_tournament import validation
from frappe import _

class Judge(Document):
    def on_trash(self):
        if self.email!="Administrator":
            frappe.delete_doc(doctype="User", name=self.email, ignore_permissions=True)

    def enable_judge(self):
        user = frappe.get_doc("User", self.email)
        user.enabled = 1
        user.save(ignore_permissions=True)

        self.status = "Enabled"
        self.save()

        return "OK"

    def check_all_templates_judged(self):
        sts = frappe.get_list("Scoring Template", filters={"judge_1": self.name, "status": "Pending"})
        if len(sts) > 0:
            frappe.throw(_("There are {0} Scoring Templates pending to judge").format(len(sts)))

    def finish_judge(self, check_templates=True):
        if check_templates:
            self.check_all_templates_judged()

        user = frappe.get_doc("User", self.email)
        user.enabled = 0
        user.save(ignore_permissions=True)

        self.status = "Disabled"
        self.save()

        if getattr(frappe.local, "login_manager", None):
            frappe.local.login_manager.logout(user=self.name)

        return "OK"

    def before_insert(self):

        if self.dni is not None:
            self.dni=re.sub('[-]', '', self.dni)
            self.dni=self.dni.upper()

    def after_insert(self):
        user = frappe.new_doc("User")
        user.email = self.email
        user.first_name = self.judge_name
        user.judge = self.name
        user.language = "es"
        user.append_roles("AVIUM Judge")
        user.flags.no_welcome_mail = True
        user.save(ignore_permissions=True)

    def validate(self):
        # if not validation.valid_DNI(self.dni):
        #     frappe.throw(frappe._("DNI is not valid"))

        if self.telephone != None and self.telephone != '' and not validation.valid_spanish_phone(self.telephone):
            frappe.msgprint(frappe._("Telephone is not valid"))

        if self.email != None and self.email != '' and not validation.valid_email(self.email):
            frappe.throw(frappe._("Email is not valid"))

        if self.mobile_phone != None and self.mobile_phone != '' and not validation.valid_spanish_mobile(
                self.mobile_phone):
            frappe.msgprint(frappe._("Mobile phone is not valid"))

    def before_save(self):
        self.judge_name = self.judge_name.title()
