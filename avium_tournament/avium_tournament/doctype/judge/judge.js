// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Judge', {
	refresh: function(frm) {
        // Filter Territory field
        cur_frm.set_query("territory", function() {
            return {
                "filters": {
                    "has_subterritories": 0
                }
            };
        });

        frm.add_custom_button(__("Change Password"), function() {
            frappe.prompt(
                [
                    {'fieldname': 'password', 'fieldtype': 'Password', 'label': 'Password', 'reqd': 1}
                ],
                function(values){
                    frappe.call({
                        method: "avium_tournament.utils.set_password_judge",
                        args: {
                            'judge': frm.doc.name,
                            'email': frm.doc.email,
                            'password': values.password
                        },
                        callback: function(r) {
                            show_alert(__('Password Changed'));
                        }
                    });
                },
                __('Change Password'),
                __('Change Password')
                );
        });

	    if(frm.doc.status=="Enabled"){
            frm.add_custom_button(__("Finish Judge"), function() {
                frappe.call({
                    method: "finish_judge",
                    doc:frm.doc,
                    callback: function(r) {
                        show_alert(__('Judge deactivated'))
                        show_alert(__('Groups closed'))
                        frm.reload_doc()
                    }
                })
            })
        }
	    else if(frm.doc.status=="Disabled"){
            frm.add_custom_button(__("Enable Judge"), function() {
                frappe.call({
                    method: "enable_judge",
                    doc:frm.doc,
                    callback: function(r) {
                        show_alert(__('Judge activated'))
                        frm.reload_doc()
                    }
                })
            })
        }
	},
    territory: function(frm) {
        frm.set_value("comunity", "");
        frm.set_value("country", "");

        if (frm.doc.territory) {
            frappe.call({
                method:"avium_tournament.utils.get_territory_parents",
                args: {
                    'territory': frm.doc.territory
                },
                callback: function(r) {
                    if(r.message[0]!=null){
                        frm.set_value("comunity", r.message[0]);
                    }
                    if(r.message[1]!=null){
                        frm.set_value("country", r.message[1]);
                    }
                }
            });
        }
    },
});