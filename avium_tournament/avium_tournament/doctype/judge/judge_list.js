frappe.listview_settings['Judge'] = {
    colwidths: {"indicator":1, "subject": 2},
    add_fields: ["status"],
    onload: function(me){
	    me.page.add_menu_item(__("Enable/Disable Judges"), function(){
	        frappe.prompt([
                {
                   "fieldname": "action",
                   "fieldtype": "Select",
                   "label": __("Action"),
                   "options": "Habilitar\nDeshabilitar",
                   "default": "Habilitar"
                },
                {
                   "fieldname": "check_templates",
                   "fieldtype": "Check",
                   "label": __("Check all Scoring Templates are judged"),
                   "default": 1,
                   "depends_on": "eval:doc.action==\"Deshabilitar\""
                }
                ],
                function(values){
                    var judges = me.get_checked_items();
                    var action = values.action == "Habilitar" ? "Enable":"Disable";

                    frappe.call({
                        method:"avium_tournament.utils.enable_disable_judges",
                        args:{
                            judges: judges,
                            action: action,
                            check_templates: values.check_templates
                        },
                        callback: function(r) {
                            frappe.msgprint(__("Judges Updated"))
                            cur_list.refresh()
                        }
                    });

                },
                __('Assign Judges'),
                __('Save')
            );


	    })
	},
	get_indicator: function(doc) {
	    if(doc.status==="Enabled") {
	        return [__(doc.status), "green", "status,=," + doc.status];
		}
		else if(doc.status==="Disabled"){
        	return [__(doc.status), "red", "status,=," + doc.status];
		}
	}
};
