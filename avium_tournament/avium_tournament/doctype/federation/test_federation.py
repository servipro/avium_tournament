# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
from frappe.test_runner import make_test_records
from avium_tournament.tests.CustomTestCase import CustomTestCase

test_records = frappe.get_test_records('Federation')
test_dependencies = ["Territory"]

class TestFederation(CustomTestCase):

	def test_cif_upper(self):
		fed = frappe.get_doc(self.doctype_name, test_records[0]["federation_name"])
		self.assertEquals(fed.cif, test_records[0]["cif"].upper())

	def test_territories(self):
		fed = frappe.get_doc(self.doctype_name, test_records[0]["federation_name"])
		self.assertIsNot(fed.comunity, None)
		self.assertIsNot(fed.country, None)