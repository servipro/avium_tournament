// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Certificate Configuration', {
	reset_configuration: function(frm){
	    frm.set_value("upper_margin", 0.3)
	    frm.set_value("lower_margin", 0.2)
	    frm.set_value("left_margin", 0.5)
	    frm.set_value("right_margin", 0.4)
	    frm.set_value("table_left_margin", 3.0)
	    frm.set_value("table_right_margin", 3.0)
	    frm.set_value("print_header", "Yes")
	    frm.set_value("header_height", 7.0)
	    frm.set_value("logo_size", 100)
	    frm.set_value("left_logo", "Association Logo")
	    frm.set_value("right_logo", "Federation Logo")
	    frm.set_value("print_signatures", "Yes")
	    frm.set_value("signature_height", 5.0)
	    frm.set_value("footer_height", 0.6)
	    frm.set_value("footer_text_size", 7)
        cur_frm.save()
	}
});