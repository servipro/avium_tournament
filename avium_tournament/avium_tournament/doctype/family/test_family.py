# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
from frappe.test_runner import make_test_records
from avium_tournament.tests.CustomTestCase import CustomTestCase

test_records = frappe.get_test_records('Family')
test_dependencies = ["Cage"]

class TestFamily(CustomTestCase):
	
	def test_duplicate_order(self):
		family = frappe.new_doc(self.doctype_name)
		family.family_name = "_Test Repeated Family"
		family.code = "F"
		family.printing_order = 1
		with self.assertRaises(frappe.ValidationError):
			family.insert()

	def test_update_scoring_templates(self):
		#TODO test
		pass
