// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Scoring Template', {
	refresh: function(frm) {
        // Filter Manual Prizes
        frappe.call({
            method: "avium_tournament.avium_tournament.doctype.tournament.tournament.get_prizes",
            args: {
                'group_code': frm.doc.group_code
            },
            async: false,
            callback: function(r){
                if(r.message){
                    var idx = 0;
                    var prize_list = [];
                    while (idx < r.message.length) {
                        prize_list.push( r.message[idx].description );
                        idx++
                    }

                    cur_frm.set_query("manual_prize", function() {
                        return {
                            "filters": {
                                "name": ["in", prize_list]
                            }
                        };
                    });
                } else {
                    cur_frm.set_query("manual_prize", function() {
                        return {
                            "filters": {
                                "name": ["in", []]
                            }
                        };
                    });
                }
            }
        })

        // Add dashboard buttons
	    frm.add_custom_button(__("Open Registration"), function() {
            frappe.call({
                method: "get_registration",
                doc:frm.doc,
                callback: function(r) {
                    frappe.set_route("Form", "Registration", r.message.parent);
                }
            })
        });

        // Add dashboard buttons
	    frm.add_custom_button(__("Disqualify Puntuated Specimen"), function() {
            frappe.prompt([{
                    "fieldname": "specimen",
                    "fieldtype": "Select",
                    "label": __("Select specimen"),
                    "options": "A\nB\nC\nD",
                    'reqd': 1
                },{
                    "fieldname": "observations",
                    "fieldtype": "Text",
                    "label": __("Observations"),
                    "default": frm.doc.observations,
                    'reqd': 1
                }],
                function(values){
                    frappe.call({
                        method: "disqualify_puntuated_specimen",
                        args:{
                            "specimen": values.specimen,
                            "reason": values.observations
                        },
                        doc:frm.doc,
                        callback: function(r) {
                            show_alert(__('Specimen Disqualified Puntuated'))
                            frm.refresh()
                        }
                    })
                },
                __('Disqualify Puntuated Specimen'),
                __('Disqualify Puntuated')
           );
        },__("Actions"));

        // Add dashboard buttons
	    frm.add_custom_button(__("Disqualify Specimen"), function() {
            frappe.prompt([{
                    "fieldname": "specimen",
                    "fieldtype": "Select",
                    "label": __("Select specimen"),
                    "options": "A\nB\nC\nD",
                    'reqd': 1
                },{
                    "fieldname": "observations",
                    "fieldtype": "Text",
                    "label": __("Observations"),
                    "default": frm.doc.observations,
                    'reqd': 1
                }],
                function(values){
                    frappe.call({
                        method: "disqualify_specimen",
                        args:{
                            "specimen": values.specimen,
                            "reason": values.observations
                        },
                        doc:frm.doc,
                        callback: function(r) {
                            show_alert(__('Specimen Disqualified'))
                            frm.refresh()
                        }
                    })
                },
                __('Disqualify Specimen'),
                __('Disqualify')
           );
        },__("Actions"));

        frm.add_custom_button(__("Send PDF to Expositor"), function() {
            frappe.call({
                method: "send_pdf_to_expositor",
                args:{
                },
                doc:frm.doc
            });
        },__("Actions"));
        frm.add_custom_button(__("Print Scoring Template"), function() {
            frappe.prompt([{
                   "fieldname": "empty",
                   "fieldtype": "Check",
                   "label": __("Print empty scoring template")
                },{
                   "fieldname": "wo_exhibitor",
                   "fieldtype": "Check",
                   "label": __("Print without exhibitor information"),
                   "depends_on": "eval:doc.empty==0"
                }],
                function(values){
                    avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_scoring_template", values, __("Scoring Template"))
                },
                __('Scoring Template'),
                __('Print')
            );
        },__("Print"));


        // Set buttons background color blue
        $("button:contains("+__('Print')+")").addClass( "btn-primary" ).removeClass("btn-default");
        $("button:contains("+__('Actions')+")").addClass( "btn-primary" ).removeClass("btn-default");

	    // Total mark visualization update
        frappe.call({
            method:"avium_tournament.utils.get_tournament",
            args: {
            },
            async: false,
            callback: function(r) {
                if(r.message){
                    if (r.message.add_tie_break_marks=="Yes") {
                        frm.set_value("total_mark_visualization", cur_frm.doc.total_mark+cur_frm.doc.tie_break_mark);
                    } else{
                        frm.set_value("total_mark_visualization", cur_frm.doc.total_mark);
                    }
                }
            }
        });

	// Prize update
//        if (frm.doc.prize==undefined) {
//            cur_frm.set_value("prize_visualization", "-");
//        } else {
//            frappe.call({
//                method:"frappe.client.get_value",
//                args: {
//                    doctype:"Tournament Prizes",
//                    filters: {
//                        name:cur_frm.doc.prize
//                    },
//                    fieldname:["description"]
//                },
//                callback: function(r) {
//                    if (cur_frm.doc.prize_visualization!=r.message.description) {
//                        cur_frm.set_value("prize_visualization", r.message.description);
//                    }
//                }
//            })
//        };
	},
    onload: function(frm) {
        // Set read-only on tufted field
        cur_frm.set_df_property("tufted", "read_only", cur_frm.doc.tufted_read_only);

        //Hide/Show Prize Visualization field
        if (frm.doc.manual_prize) {
            frm.toggle_display("prize_visualization", false);
        } else {
            frm.toggle_display("prize_visualization", true);
        }
    },
    manual_prize: function(frm) {
        if (frm.doc.manual_prize) {
            //Check if the prize has been already assigned
            frappe.call({
                method:"frappe.client.get_value",
                args:{
                    'doctype': "Scoring Template",
                    'filters': {
                        'name': ["!=", frm.doc.name],
                        'group': frm.doc.group,
                        'manual_prize': frm.doc.manual_prize
                    },
                    'fieldname':[
                        'cage_number'
                    ]
                },
                callback: function(r) {
                    if (r.message) {
                        frappe.msgprint(
                            __("Prize ") + frm.doc.manual_prize + __(" has been already assigned to cage ") + r.message.cage_number.replace(/^0+/, "")
                        );
                    }
                }
            });

            //Hide Prize Visualization field
            frm.toggle_display("prize_visualization", false);
        }
    },
});
