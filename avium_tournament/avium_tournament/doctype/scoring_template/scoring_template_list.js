frappe.listview_settings['Scoring Template'] = {
    hide_name_column: true,
    onload: function(me){
	    me.page.add_menu_item(__("Assign Judges"), function(){
	        frappe.prompt([
                {
                   "fieldname": "judge1",
                   "fieldtype": "Link",
                   "label": __("Judge 1"),
                   "options": "Judge"
                }, {
                   "fieldname": "judge2",
                   "fieldtype": "Link",
                   "label": __("Judge 2"),
                   "options": "Judge"
                }],
                function(values){
                    var scoring_templates = me.get_checked_items();
                    frappe.call({
                        method:"avium_tournament.utils.set_judges_by_scoring_template",
                        args:{
                            scoring_templates: scoring_templates,
                            judge1: values.judge1,
                            judge2: values.judge2
                        },
                        callback: function(r) {
                            frappe.msgprint(__("Judges Assigned"))
                            cur_list.refresh()
                        }
                    });

                },
                __('Assign Judges'),
                __('Save')
            );


	    })
	}
};