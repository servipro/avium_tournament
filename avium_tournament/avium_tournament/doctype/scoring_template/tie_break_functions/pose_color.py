# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from avium_tournament.utils import get_tournament

def pose_color_tie_break(scoring_template_1, scoring_template_2):


    #Check Concepts Tie Break Order
    for concept_1, concept_2 in zip(scoring_template_1.concepts, scoring_template_2.concepts):
        total_1 = concept_1.get_values(scoring_template_1.team_size)
        total_2 = concept_2.get_values(scoring_template_1.team_size)

        if total_1 > total_2:
            return 1
        elif total_2 > total_1:
            return -1

    #Check Tie Break Points Given By The Judge
    total_1 = (scoring_template_1.total_mark*1.0 + scoring_template_1.tie_break_mark*0.001)
    total_2 = (scoring_template_2.total_mark*1.0 + scoring_template_2.tie_break_mark*0.001)
    if total_1 > total_2:
        return 1
    elif total_2 > total_1:
        return -1

    return 0