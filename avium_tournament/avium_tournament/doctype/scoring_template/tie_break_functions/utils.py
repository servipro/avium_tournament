# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

def check_negative_concepts(scoring_template_1, scoring_template_2):
    negativa_marks_1 = sum([
        sum(concept.get_values(scoring_template_1.team_size))
        for concept
        in scoring_template_1.concepts
        if concept.negative
    ])
    negativa_marks_2 = sum([
        sum(concept.get_values(scoring_template_2.team_size))
        for concept
        in scoring_template_2.concepts
        if concept.negative
    ])
    if negativa_marks_1 < negativa_marks_2:
        return 1
    elif negativa_marks_2 < negativa_marks_1:
        return -1
    else:
        return 0

def check_harmony(scoring_template_1, scoring_template_2):
    if scoring_template_1.harmony > scoring_template_2.harmony:
        return 1
    elif scoring_template_2.harmony > scoring_template_1.harmony:
        return -1
    else:
        return 0

def check_concepts_tie_break_order(scoring_template_1, scoring_template_2):
    tie_break_concepts_1 = [
        concept
        for concept
        in scoring_template_1.concepts
        if concept.tie_break_order != 0
    ]

    tie_break_concepts_2 = [
        concept
        for concept
        in scoring_template_2.concepts
        if concept.tie_break_order != 0
    ]

    if len(tie_break_concepts_1)>0 and len(tie_break_concepts_2)>0:
        max_tie_break_order = max(
            [
                concept.tie_break_order
                for concept
                in tie_break_concepts_1
            ]
        )
        tie_break_values_1 = [0]*(max_tie_break_order + 1)
        for concept in tie_break_concepts_1:
            tie_break_values_1[concept.tie_break_order] += sum(concept.get_values(scoring_template_1.team_size))
        tie_break_values_2 = [0]*(max_tie_break_order + 1)
        for concept in tie_break_concepts_2:
            tie_break_values_2[concept.tie_break_order] += sum(concept.get_values(scoring_template_2.team_size))

    else:
        tie_break_concepts_1 = [
            concept
            for concept
            in scoring_template_1.concepts
        ]
        tie_break_concepts_2 = [
            concept
            for concept
            in scoring_template_2.concepts
        ]
        tie_break_concepts_1.sort(key=lambda concept: concept.idx)
        tie_break_concepts_2.sort(key=lambda concept: concept.idx)
        tie_break_values_1 = [
            sum(concept.get_values(scoring_template_1.team_size))
            for concept
            in tie_break_concepts_1
        ]
        tie_break_values_2 = [
            sum(concept.get_values(scoring_template_2.team_size))
            for concept
            in tie_break_concepts_2
        ]

    if tie_break_values_1 > tie_break_values_2:
        return 1
    elif tie_break_values_2 > tie_break_values_1:
        return -1
    else:
        return 0

def check_judge_tie_break_points(scoring_template_1, scoring_template_2):
    total_1 = (scoring_template_1.total_mark*1.0 + scoring_template_1.tie_break_mark*0.001)
    total_2 = (scoring_template_2.total_mark*1.0 + scoring_template_2.tie_break_mark*0.001)
    if total_1 > total_2:
        return 1
    elif total_2 > total_1:
        return -1
    else:
        return 0