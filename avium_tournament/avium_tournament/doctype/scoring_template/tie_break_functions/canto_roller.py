# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from avium_tournament.avium_tournament.doctype.scoring_template.tie_break_functions.utils import check_negative_concepts, check_concepts_tie_break_order, check_judge_tie_break_points

def canto_roller_tie_break(scoring_template_1, scoring_template_2):

    cmp_value = check_negative_concepts(scoring_template_1, scoring_template_2)
    if cmp_value != 0:
        return cmp_value

    cmp_value = check_concepts_tie_break_order(scoring_template_1, scoring_template_2)
    if cmp_value != 0:
        return cmp_value

    cmp_value = check_judge_tie_break_points(scoring_template_1, scoring_template_2)
    if cmp_value != 0:
        return cmp_value

    return 0