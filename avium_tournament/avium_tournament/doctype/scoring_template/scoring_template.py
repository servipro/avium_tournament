# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

import json

import frappe
import avium_tournament.utils.cache as cache
from avium_tournament.reports import get_report_type
from avium_tournament.utils import cache, get_tournament
from avium_tournament.reports.extended.scoring_templates_report import ExtendedScoringTemplatesReport
from avium_tournament.reports.simplified.scoring_templates_report import SimplifiedScoringTemplatesReport
from avium_tournament.avium_tournament.doctype.scoring_template.tie_break_functions.pose_color import pose_color_tie_break
from avium_tournament.avium_tournament.doctype.scoring_template.tie_break_functions.canto_roller import canto_roller_tie_break
from avium_tournament.avium_tournament.doctype.scoring_template.tie_break_functions.canto_malinois import canto_malinois_tie_break
from avium_tournament.avium_tournament.doctype.scoring_template.tie_break_functions.canto_timbrado_espanol import canto_timbrado_espanol
from avium_tournament.avium_tournament.doctype.scoring_template.tie_break_functions.canto_cantor_espanol import canto_cantor_espanol
from avium_tournament.avium_tournament.doctype.scoring_template.tie_break_functions.canto_fringilidos import canto_fringilidos
from avium_tournament.avium_tournament.doctype.tournament.tournament import get_prizes
from frappe.model.document import Document
from frappe import _
from datetime import date


class ScoringTemplate(Document):

    def onload(self):
        self.get("__onload").import_types = ["1", "2", "3"]

    ## Overwritten
    def validate(self):
        if not self.flags.generating_templates:
            group = frappe.get_doc("Tournament Group", self.group)

            if self.template_type == "Team Registration":
                self.group_code = group.team_code
            else:
                self.group_code = group.individual_code

            # Check there is no team with a size less than 2
            if self.template_type == "Team Registration" and self.team_size <= 1:
                frappe.throw(_("Team Size must be bigger than 1"))

            # if self.check_all_zero() == True:
            #     frappe.throw(_("All values cannot be 0"))

            for concept in self.concepts:
                # Check the ranges for each concept are correct
                concept.validate()

    def before_insert(self):
        self.update_rings()

    def get_registration(self):
        return frappe.get_doc(self.template_type, self.registration)

    def is_manual_harmony(self):
        section = frappe.db.sql("""
            SELECT family.manual_harmony
            FROM `tabScoring Template` as st
            LEFT JOIN `tabTournament Group` as tgroup on st.group = tgroup.name
            LEFT JOIN `tabFamily` as family on family.name = tgroup.family
            WHERE st.name = %s
        """, [self.name])

        return section[0][0]==1

    def is_manual_trophy(self):
        section = frappe.db.sql("""
            SELECT family.manual_trophy
            FROM `tabScoring Template` as st
            LEFT JOIN `tabTournament Group` as tgroup on st.group = tgroup.name
            LEFT JOIN `tabFamily` as family on family.name = tgroup.family
            WHERE st.name = %s
        """, [self.name])

        return section[0][0]==1

    def get_available_trophies(self):
        return get_prizes(self.group_code)

    def before_save(self):
        roles = frappe.get_roles()

        # Mark once it is saved
        if "AVIUM Association" not in roles and self.status == "Pending" and not self.flags.generating_templates and not self.flags.assing_cage:
            judge = frappe.get_value("User", frappe.session.user, "judge")
            if (judge!=None and self.judge_1==judge) :
                self.status = "Judged"
                self.judgment_date = date.today()
            else:
                frappe.throw(_("You are not a judge assigned to this template"))

        self.check_observation_disqualified()
        self.update_scores()


    def on_update(self):
        if not self.flags.generating_templates:
            tournament = get_tournament()
            tournament.update_classification_group(self.group_code)
            if tournament.tournament_status == "Paused":
                frappe.throw(_("Tourment is paused, contact the organization"))

    def check_observation_disqualified(self):
        if not self.judge_status_tournament() and not self.observations:
            frappe.throw(_("If a specimen is not in Tournament, please fill observations"))

    def judge_status_tournament(self):
        status = [self.status_a, self.status_b, self.status_c, self.status_d]
        correct_status = ["Judjable"]
        for specimen in status[:self.team_size]:
            if specimen not in correct_status:
                return False
        return True

    def update_scores(self):
        # Be sure that individual templates have a team size of 1
        if self.template_type == "Individual Registration":
            self.team_size = 1

        self.set_concept_values_by_status()

        self.total_a, self.total_b, self.total_c, self.total_d = self.get_subtotals()
        self.sum_mark, self.harmony, self.total_mark = self.get_totals()

    def on_trash(self):
        tournament = get_tournament()
        if tournament.tournament_status in ["Started", "Finished"]:
            frappe.throw(_("You cannot delete a Scoring Template if the tournament is {0} or {1}").format(["Started", "Finished"]))



    ## Custom
    def set_concept_values_by_status(self):
        mark_status = ["Judjable", "Declassified Puntuated", "Disqualified Puntuated"]
        location_remove_status = get_tournament("remove_trophy_location").split(",")

        for concept in self.concepts:
            if not self.status_a in mark_status:
                concept.value_a = 0
            if not self.status_b in mark_status:
                concept.value_b = 0
            if not self.status_c in mark_status:
                concept.value_c = 0
            if not self.status_d in mark_status:
                concept.value_d = 0

        for concept in self.concepts:
            if self.location_status_a in location_remove_status:
                concept.value_a = 0
            if self.location_status_b in location_remove_status:
                concept.value_b = 0
            if self.location_status_c in location_remove_status:
                concept.value_c = 0
            if self.location_status_d in location_remove_status:
                concept.value_d = 0

    def get_status(self):
        status = (self.status_a, self.status_b, self.status_c, self.status_d,)
        if "Not Judjable" in status:
            return "Not Judjable"
        elif "Declassified" in status:
            return "Declassified"
        elif "Declassified Puntuated" in status:
            return "Declassified Puntuated"
        elif "Disqualified" in status:
            return "Disqualified"
        elif "Disqualified Puntuated" in status:
            return "Disqualified Puntuated"
        elif "Not Presented" in status:
            return "Not Presented"
        elif "Did Not Sing" in status:
            return "Did Not Sing"
        elif "Insuficient Sing" in status:
            return "Insuficient Sing"
        else:
            return "Judjable"

    def get_harmony(self):
        if self.team_size == 1:
            return 0
        
        if self.manual_scoring:
            return self.manual_harmony

        if self.is_manual_harmony():
            return self.harmony

        # consider subtotals that are being judged
        subtotals = self.get_subtotals()[:self.team_size]

        difference = (max(subtotals) - min(subtotals))

        if difference >= 6:
            return 0

        return 6 - difference

    def get_subtotals(self):
        total_a = total_b = total_c = total_d = 0

        if self.status == "Pending":
            return 0,0,0,0

        if self.manual_scoring:
            return self.manual_score_a, self.manual_score_b, self.manual_score_c, self.manual_score_d
        else:
            for concept in self.concepts:
                if concept.negative == 0:
                    total_a += (concept.value_a * concept.multiply_factor or 0)
                    total_b += (concept.value_b * concept.multiply_factor or 0)
                    total_c += (concept.value_c * concept.multiply_factor or 0)
                    total_d += (concept.value_d * concept.multiply_factor or 0)
                else:
                    total_a -= (concept.value_a * concept.multiply_factor or 0)
                    total_b -= (concept.value_b * concept.multiply_factor or 0)
                    total_c -= (concept.value_c * concept.multiply_factor or 0)
                    total_d -= (concept.value_d * concept.multiply_factor or 0)

        return total_a, total_b, total_c, total_d

    def get_totals(self):
        if not self.is_judjable():
            return 0,0,0
        
        sum_mark = self.total_a + self.total_b + self.total_c + self.total_d
        harmony = self.get_harmony()
        total_mark = sum_mark + harmony

        return sum_mark, harmony, total_mark

    def get_cage(self):
        registration = frappe.get_doc(self.template_type, self.registration)

        tournament_group = frappe.get_doc("Tournament Group", registration.group)

        return frappe.get_doc("Cage", tournament_group.cage)

    def gen_concepts(self, doc=None):
        if doc is None:
            registration_doc = cache.get_doc(self.template_type, self.registration)
        else:
            registration_doc = doc
        tournament_group = cache.get_doc("Tournament Group", registration_doc.group)
        judging_template = cache.get_doc("Judging Template", tournament_group.judging_template)

        if tournament_group.tufted == "Optional":
            self.tufted_read_only = 0
            self.tufted = "Corona"
        else:
            self.tufted_read_only = 1
            self.tufted = tournament_group.tufted
        self.red_factor = judging_template.red_factor

        for concept in judging_template.concepts:
            new_concept = {
                "parent": self.name,
                "doctype": "Scoring Template Concept",
                "parent_field": "concepts",
                "concept": concept.name,
                "concept_name": concept.concept_name,
                "min_value": concept.min_value,
                "max_value": concept.max_value,
                "default_value": concept.default_value,
                "multiply_factor": concept.multiply_factor,
                "tie_break_order": concept.tie_break_order,
                "negative": concept.negative
            }

            if self.team_size >= 1:
                new_concept["value_a"] = concept.default_value
            if self.team_size >= 2:
                new_concept["value_b"] = concept.default_value
            if self.team_size >= 3:
                new_concept["value_c"] = concept.default_value
            if self.team_size == 4:
                new_concept["value_d"] = concept.default_value

            self.append("concepts", new_concept)

    def print_scoring_template(self, arguments):

        arguments['scoring_template'] = self.name

        report_type = get_report_type()
        if report_type == "Extended":
            with ExtendedScoringTemplatesReport(arguments) as report:
                return report.get_pdf()
        else:
            with SimplifiedScoringTemplatesReport(arguments) as report:
                return report.get_pdf()

    def is_judjable(self, specimen="all"):
        remove_trophy_location = get_tournament("remove_trophy_location")

        if  cache.get_value("Family", self.group_code[0], "sing_family"):
            return True

        if specimen=="all":
            status_values = [self.status_a, self.status_b, self.status_c, self.status_d]
            for item in status_values[:self.team_size]:
                if item!="Judjable":
                    return False
            location_values = [self.location_status_a, self.location_status_b, self.location_status_c, self.location_status_d]
            for item in location_values[:self.team_size]:
                if item!="Tournament":
                    return False
            return True
        elif specimen.upper()=="A" and self.status_a=="Judjable" and self.location_status_a=="Tourment":
            return True
        elif specimen.upper()=="B" and self.status_b=="Judjable" and self.location_status_b=="Tourment":
            return True
        elif specimen.upper()=="C" and self.status_c=="Judjable" and self.location_status_c=="Tourment":
            return True
        elif specimen.upper()=="D" and self.status_d=="Judjable" and self.location_status_d=="Tourment":
            return True
        else:
            return False

    def get_value_ring(self, ring_code, group):
        if self.team_size == 1:
            ring_values = {
                "A": [group.federation_code_a, group.breeder_code_a, group.ring_a]
            }

        else:
            ring_values = {
                "A": [group.federation_code_a, group.breeder_code_a, group.ring_a],
                "B": [group.federation_code_b, group.breeder_code_b, group.ring_b],
                "C": [group.federation_code_c, group.breeder_code_c, group.ring_c],
                "D": [group.federation_code_d, group.breeder_code_d, group.ring_d]
            }

        federation_code = ring_values[ring_code][0] or ""
        breeder_code = ring_values[ring_code][1] or ""
        ring = ring_values[ring_code][2] or ""

        return federation_code + "/" + breeder_code + "/" + ring

    def update_rings(self, group=None):
        if group is None:
            group = frappe.get_doc(self.template_type, self.registration)

        if self.team_size >= 1:
            self.ring_a = self.get_value_ring("A", group)
        else:
            self.ring_a = ""

        if self.team_size >= 2:
            self.ring_b = self.get_value_ring("B", group)
        else:
            self.ring_b = ""

        if self.team_size >= 3:
            self.ring_c = self.get_value_ring("C", group)
        else:
            self.ring_c = ""

        if self.team_size == 4:
            self.ring_d = self.get_value_ring("D", group)
        else:
            self.ring_d = ""

    def update_location_status(self, group=None):
        if group is None:
            group = frappe.get_doc(self.template_type, self.registration)

        if self.team_size >= 1:
            self.location_status_a = group.location_status_a
            self.location_status_a_comment = group.location_status_a_comment
        else:
            self.location_status_a = "Tournament"
            self.location_status_a_comment = ""

        if self.team_size >= 2:
            self.location_status_b = group.location_status_b
            self.location_status_b_comment = group.location_status_b_comment
        else:
            self.location_status_b = "Tournament"
            self.location_status_b_comment = ""

        if self.team_size >= 3:
            self.location_status_c = group.location_status_c
            self.location_status_c_comment = group.location_status_c_comment
        else:
            self.location_status_c = "Tournament"
            self.location_status_c_comment = ""

        if self.team_size == 4:
            self.location_status_d = group.location_status_d
            self.location_status_d_comment = group.location_status_d_comment
        else:
            self.location_status_d = "Tournament"
            self.location_status_d_comment = ""

    def set_status_specimen(self, specimen, new_status):
        if specimen=="A":
            self.status_a = new_status
        elif specimen=="B":
            self.status_b = new_status
        elif specimen == "C":
            self.status_c = new_status
        elif specimen == "D":
            self.status_d = new_status
        self.save()

    def disqualify_specimen(self, specimen, reason):
        new_status = "Disqualified"
        self.observations = reason
        self.set_status_specimen(specimen, new_status)

    def disqualify_puntuated_specimen(self, specimen, reason):
        new_status = "Disqualified Puntuated"
        self.observations = reason
        self.set_status_specimen(specimen, new_status)

    def requalify_specimen(self, specimen):
        new_status = "Judjable"
        self.set_status_specimen(specimen, new_status)

    def send_pdf_to_expositor(self):
        frappe.db.set_default("lang", "es")
        frappe.local.lang = "es"

        group = frappe.get_doc(self.template_type, self.registration)
        registration = frappe.get_doc("Registration", group.parent)
        tournament = get_tournament()

        pdf_file = self.print_scoring_template({"empty":0, "exhibitor":1})

        frappe.sendmail(
            recipients=[registration.email],
            subject=_("Results", "es") + " " + tournament.tournament_name+".pdf",
            reply_to=tournament.reply_to_mail or "info@avium.eu",
            message=_("You can find the results for the tournament {0} in this email", "es").format(tournament.tournament_name),
            reference_doctype=self.doctype,
            reference_name=self.name,
            attachments=[{
                "fname": tournament.tournament_name+".pdf",
                "fcontent": pdf_file
            }]
        )

        return "ok"

    def check_all_zero(self):
        if not self.is_judjable():
            return False

        for concept in self.concepts:
            if concept.value_a > 0:
                return False
            elif self.team_size>=2 and concept.value_b>0:
                return False
            elif self.team_size>=3 and concept.value_c>0:
                return False
            elif self.team_size>=4 and concept.value_d>0:
                return False
        return True

    def __lt__(self, other):
        return self.compare(other) == -1

    def __get__(self, other):
        return self.compare(other) == 1

    def __eq__(self, other):
        return self.compare(other) == 0

    ## Python 3 migrate to lt and eq
    def compare(self, other):
        if self.total_mark == other.total_mark:

            if self.get_status() == "Judjable" and other.get_status() != "Judjable":
                return 1
            if self.get_status() != "Judjable" and other.get_status() == "Judjable":
                return -1

            if self.judgement_type == "Canto Roller":
                return canto_roller_tie_break(self, other)
            if self.judgement_type == "Canto Malinois":
                return canto_malinois_tie_break(self, other)
            if self.judgement_type == "Canto Timbrado Español":
                return canto_timbrado_espanol(self, other)
            if self.judgement_type == "Canto Fringilidos":
                return canto_fringilidos(self, other)
            if self.judgement_type == "Canto Cantor Español":
                return canto_cantor_espanol(self, other)
            else:
                return pose_color_tie_break(self, other)
        else:
            if self.total_mark > other.total_mark:
                return 1
            elif other.total_mark > self.total_mark:
                return -1

        return 0

@frappe.whitelist()
def get_next_template(current_template):
    st_list = frappe.get_list("Scoring Template",
                              fields=["name"],
                              order_by="group_code, cage_number")

    if len(st_list) == 0:
        frappe.throw("There are no templates assigned to you")

    for index, item in enumerate(st_list):
        if item.name == current_template:
            if index < len(st_list) - 1:
                return st_list[index + 1].name
            else:
                return st_list[0].name

@frappe.whitelist()
def get_judging_groups(hide_assigned="true"):
    filter = ""
    if hide_assigned == "true":
        filter = "WHERE judge_1 IS NULL or judge_1 = ''"

    groups = frappe.db.sql("""
        SELECT 
            group_code,
            group_description,
            CASE team_size 
				WHEN 1 THEN "I"
				ELSE "T"
				END as type, 
            judge_1, 
            judge_2,
            count(*) * team_size as specimens,
            CASE WHEN judge_1 IS NOT NULL THEN 'template_assigned' ELSE 'template_not_assigned' END as assigned
        FROM `tabScoring Template`
        """ + filter + """
        GROUP BY group_code
    """, as_dict=1)

    return groups

@frappe.whitelist()
def get_judging_assignments():
    judges = frappe.db.sql("""
            SELECT 
                judge_1 as judge,
                sum(team_size) as specimens
            FROM `tabScoring Template`
            GROUP by judge_1
        """, as_dict=1)

    return judges

@frappe.whitelist()
def get_assigned_judging_groups(judge1):
    groups = frappe.db.sql("""
        SELECT 
            group_code,
            group_description,
            team_size=1 as individual, 
            judge_1, 
            judge_2,
            count(*) * team_size as specimens,
            CASE WHEN judge_1 IS NOT NULL THEN 'template_assigned' ELSE 'template_not_assigned' END as assigned
        FROM `tabScoring Template`
        WHERE judge_1 = %s
        GROUP BY group_code
    """, (judge1), as_dict=1)

    return groups

@frappe.whitelist()
def set_judges_group(groups, judge1=None, judge2=None):
    group_codes = list(map(lambda x: x["group_code"], json.loads(groups)))

    if judge1:
        query = "judge_1=%s"
        values = (judge1, group_codes)
    if judge2:
        query = "judge_2=%s"
        values = (judge2, group_codes)
    if judge1 and judge2:
        query = "judge_1=%s, judge_2=%s"
        values = (judge1, judge2, group_codes)
    if judge1 == "" and judge2 == "":
        query = "judge_1=%s, judge_2=%s"
        values = ("", "", group_codes)

    frappe.db.sql(
        """UPDATE `tabScoring Template` SET """ + query + """ WHERE `tabScoring Template`.group_code IN %s""",
        values)

    return "ok"

@frappe.whitelist()
def set_manual_points(name, field, value):
    st = frappe.get_doc("Scoring Template", name)
    st.status = "Judged"
    st.manual_scoring = True
    setattr(st, field, int(value))
    if "manual_score" in field:
        setattr(st, field.replace("manual_score", "total"), int(value))
    if "harmony" in field:
        setattr(st, field.replace("manual_", ""), int(value))
    st.update_scores()
    st.db_update()

    tournament = get_tournament()
    tournament.update_classification_group(st.group_code)

    return "ok"
