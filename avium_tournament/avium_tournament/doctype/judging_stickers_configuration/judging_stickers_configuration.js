// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Judging Stickers Configuration', {
	reset_configuration: function(frm){
	    frm.set_value("page_upper_margin", 0)
	    frm.set_value("page_lower_margin", 0)
	    frm.set_value("page_left_margin", 0)
	    frm.set_value("page_right_margin", 0)
	    frm.set_value("columns_number", 4)
	    frm.set_value("column_width", 5.25)
	    frm.set_value("rows_number", 10)
	    frm.set_value("rows_height", 2.97)
	    frm.set_value("label_upper_margin", 0.3)
	    frm.set_value("label_lower_margin", 0.3)
	    frm.set_value("label_left_margin", 0.3)
	    frm.set_value("label_right_margin", 0.3)
	    frm.set_value("text_size", 12)
	    frm.set_value("print_border", 0)
        cur_frm.save()
	}
});
