# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class ScoringTemplateConcept(Document):

	def validate(self):
		message = "Concept {0} for specimen {1} with value {2} is not between {3} and {4}"

		if not self.check_range(self.value_a):
			frappe.throw(_(message).format(self.concept_name, "A", self.value_a, self.min_value, self.max_value))
		if not self.check_range(self.value_b):
			frappe.throw(_(message).format(self.concept_name, "B", self.value_b, self.min_value, self.max_value))
		if not self.check_range(self.value_c):
			frappe.throw(_(message).format(self.concept_name, "C", self.value_c, self.min_value, self.max_value))
		if not self.check_range(self.value_d):
			frappe.throw(_(message).format(self.concept_name, "D", self.value_d, self.min_value, self.max_value))

	def check_range(self, val):
		return val==0 or self.min_value <= val <= self.max_value

	def get_values(self, team_size):
		values = [self.value_a * self.multiply_factor]
		if team_size>=2:
			values.append(self.value_b * self.multiply_factor)
		if team_size>=3:
			values.append(self.value_c * self.multiply_factor)
		if team_size==4:
			values.append(self.value_d * self.multiply_factor)
		return values