// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Scoring Templates Mass Mail', {
	refresh: function(frm) {
            frm.page.set_primary_action(__('Send Emails'), function () {
                    frappe.confirm(
                        __('Are you sure you want to send the templates to all exhibitors?'),
                        function(){
                            frappe.call({
                                method: "send_mass_mail",
                                doc:frm.doc,
                                callback: function(r) {
                                    show_message(__('Emails are being sent, please, wait to finnish'))
                                    frm.reload_doc()
                                }
                            })
                        },
                        function(){
                            window.close();
                        }
                    )

			}, 'octicon octicon-trashcan')
				.addClass('btn-warning');
	}
});
