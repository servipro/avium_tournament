# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
from avium_tournament.utils import get_tournament

class ScoringTemplatesMassMail(Document):
    def send_mass_mail(self):
        frappe.db.set_default("lang", "es")
        frappe.local.lang = "es"
        # tournament = get_tournament()
        # if tournament.tournament_status != "Finished":
        #     frappe.throw(_("Cannot send mass mail of scoring templates if the tournament is not finished"))

        body = _("Estimado {},<br><br>Gracias por su participación en el {}. En el documento adjunto encontrarás toda la información relativa a sus planillas, en breve podrá consultar la clasificación y palmarés en la página web y redes sociales de AVIUM.eu <br><br>Reciba un cordial saludo")
        registrations = frappe.get_list("Registration", fields=["name","exhibitor_name"])
        tournament = get_tournament()

        self.user = frappe.session.user
        self.date_sent = frappe.utils.now()
        self.times_sent += 1
        self.message = body
        self.save()

        for registration in registrations:
            frappe.enqueue('avium_tournament.avium_tournament.doctype.scoring_templates_mass_mail.scoring_templates_mass_mail.enqueue_send_docs',
                           queue='short',
                           st_name=registration.name,
                           body=body.format(registration.exhibitor_name, tournament.tournament_name)
            )
        return "ok"

def enqueue_send_docs(st_name, body):
    frappe.db.set_default("lang", "es")
    frappe.local.lang = "es"
    registration = frappe.get_doc("Registration", st_name)
    registration.send_scoring_templates_to_expositor(body=body)
    return
