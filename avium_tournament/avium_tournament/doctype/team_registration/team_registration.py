# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe.model.document import Document

class TeamRegistration(Document):
	## Overwritten
	def validate(self):
		for idx, date_birth in enumerate([self.birth_year_a, self.birth_year_b, self.birth_year_c, self.birth_year_d]):
			if date_birth<(datetime.datetime.now().year - 3) or date_birth>datetime.datetime.now().year:
				frappe.throw(frappe._('The birth year of the specimen {0} in the group {1}, row {2}, is incorrect.').format(chr(65+idx), self.group_code, self.idx))

	## Custom
	def generate_scoring_template(self):
		if not frappe.db.exists("Scoring Template", {"registration": self.name}):
			team_size = frappe.get_value("Tournament Group", self.group, "team_size")
			st = frappe.new_doc("Scoring Template")
			st.template_type = "Team Registration"
			st.registration = self.name
			st.team_size = team_size
			st.group_code = self.group_code
			st.group = self.group
			st.group_description = self.group_description
			st.registration_description = self.registration_description
			st.workflow_state = "Judges Undefined"
			st.flags.generating_templates = True
			st.update_rings(group=self)
			st.update_location_status(group=self)
			st.gen_concepts(doc=self)
			st.db_insert()
			st.set_parent_in_children()
			for d in st.get_all_children():
				d.db_insert()
