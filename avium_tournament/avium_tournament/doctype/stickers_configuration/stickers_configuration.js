// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Stickers Configuration', {
	reset_configuration: function(frm){
	    frm.set_value("upper_margin", 1)
	    frm.set_value("lower_margin", 1)
	    frm.set_value("left_margin", 1)
	    frm.set_value("right_margin", 1)
	    frm.set_value("columns_number", 4)
	    frm.set_value("column_width", 4.1)
	    frm.set_value("rows_number", 10)
	    frm.set_value("rows_height", 2.5)
	    frm.set_value("text_size", 9)
	    frm.set_value("print_border", 0)
        cur_frm.save()
	}
});
