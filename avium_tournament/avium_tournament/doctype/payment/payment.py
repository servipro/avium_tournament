# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
from unidecode import unidecode

from avium_tournament.utils import get_tournament
from avium_tournament.reports.registration_summary_report import RegistrationSummaryReport

class Payment(Document):

	def update_registration(self):
		registration_doctype = frappe.get_doc("Registration", self.registration)
		if self.payment_date==None:
			registration_doctype.payments_update(False)
		else:
			registration_doctype.payments_update(True)

		registration_doctype.db_update()
		return registration_doctype.remaining_amount

	def on_update(self):
		remaining_amount = self.update_registration()
		if remaining_amount != self.remaining_amount_visualization:
			self.remaining_amount_visualization = remaining_amount
			self.save()

	def send_email_notification_fn(self):
		frappe.db.set_default("lang", "es")
		frappe.local.lang = "es"

		registration = frappe.get_doc("Registration", self.registration)
		tournament = get_tournament()
		settings = frappe.get_doc("System Settings")

		tournament = get_tournament()

		arguments = dict()
		arguments["registration"] = self.registration
		arguments["cage"] = 0
		arguments["carrier"] = 1

		with RegistrationSummaryReport(arguments) as report:
			pdf_file = report.get_pdf()

			body = _("Estimado {},<br> Gracias por su inscripción al {}. En el documento adjunto encontrarás toda la información relativa a su inscripción y pago.<br><br>Reciba un cordial saludo.").format(
				registration.exhibitor_name,
				tournament.tournament_name)

			frappe.sendmail(
					recipients=[registration.email],
					subject=_("Registration Payment", "es") + " " + tournament.tournament_name,
					reply_to=tournament.reply_to_mail or "info@avium.eu",
					message=body,
					reference_doctype=self.doctype,
					reference_name=self.name,
					attachments=[{
						"fname": unidecode(_("Registration")) + "_" + unidecode(tournament.tournament_name).replace(" ", "_").replace("-", "_").replace(".", "_") + ".pdf",
						"fcontent": pdf_file
					}]
			)
			self.notifications_sent+=1
			self.save()

	def after_delete(self):
		self.update_registration()
