// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

function update_registration_information(frm){
    if (frm.doc.registration==undefined) {
        frm.set_value('exhibitor_name_visualization', "-");
    } else {
        frappe.call({
            method:'frappe.client.get_value',
            args: {
                doctype:'Registration',
                filters: {
                    name:frm.doc.registration
                },
                fieldname:['exhibitor_name',
                    'exhibitor_surname',
                    'remaining_amount',
                    'total_price',
                ]
            },
            callback: function(r) {
                if (cur_frm.doc.exhibitor_name_visualization!=r.message.exhibitor_name) {
                    frm.set_value('exhibitor_name_visualization', r.message.exhibitor_surname + ", " + r.message.exhibitor_name);
                };
                if (cur_frm.doc.remaining_amount_visualization!=r.message.remaining_amount) {
                    frm.set_value('remaining_amount_visualization', r.message.remaining_amount);
                };
                if (cur_frm.doc.total_price_visualization!=r.message.total_price) {
                    frm.set_value('total_price_visualization', r.message.total_price);
                };
            }
        })
    }
}

frappe.ui.form.on('Payment', {
    // TODO update visualization fields when openning document
    refresh: function(frm) {
        // Update total_price_visualization and remaining_amount_visualization fields
        frm.add_fetch('registration','total_price','total_price_visualization');
        frm.add_fetch('registration','remaining_amount','remaining_amount_visualization');

        // Update registration information
        update_registration_information(frm);
    },
    registration: function(frm) {
        // Update registration information
        update_registration_information(frm);
    },
});
