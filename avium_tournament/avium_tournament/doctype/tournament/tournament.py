# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

import frappe
import math
import json
from avium_tournament.reports import get_report_type, get_marks_stickers_type
from avium_tournament.reports.commonFunctions import blank_undefined
from avium_tournament.utils import cache, get_tournament
from frappe import _
from frappe.model.document import Document

from avium_tournament.reports.box_report import BoxReport
from avium_tournament.reports.cages_report import CagesReport
from avium_tournament.reports.certificates_report import CertificatesReport
from avium_tournament.reports.carrier_prizes_report import CarrierPrizesReport
from avium_tournament.reports.carriers_report import CarriersReport
from avium_tournament.reports.delivery_report import DeliveryReport
from avium_tournament.reports.exhibitor_classification_best_punctuations_report import ExhibitorClassificationBestPunctuationsReport
from avium_tournament.reports.exhibitor_stickers_report import ExhibitorStickersReport
from avium_tournament.reports.extended.classification_report import ExtendedClassificationReport
from avium_tournament.reports.extended.exhibitors_list_report import ExtendedExhibitorsListReport
from avium_tournament.reports.extended.marks_stickers_report import ExtendedMarksStickersReport
from avium_tournament.reports.extended.record_book_report import ExtendedRecordBookReport
from avium_tournament.reports.extended.scoring_templates_report import ExtendedScoringTemplatesReport
from avium_tournament.reports.groups_list_report import GroupsListReport
from avium_tournament.reports.judges_report import JudgesReport
from avium_tournament.reports.judgement_report import JudgementReport
from avium_tournament.reports.judging_stickers_report import JudgingStickersReport
from avium_tournament.reports.league_punctuations_report import LeaguePunctuationsReport
from avium_tournament.reports.location_status_report import LocationStatusReport
from avium_tournament.reports.payments_report import PaymentsReport
from avium_tournament.reports.tax_report import TaxReport
from avium_tournament.reports.registration_report import RegistrationReport
from avium_tournament.reports.registration_summary_report import RegistrationSummaryReport
from avium_tournament.reports.simplified.exhibitors_list_report import SimplifiedExhibitorsListReport
from avium_tournament.reports.simplified.individual_classification_report import SimplifiedIndividualClassificationReport
from avium_tournament.reports.simplified.marks_stickers_report import SimplifiedMarksStickersReport
from avium_tournament.reports.simplified.record_book_report import SimplifiedRecordBookReport
from avium_tournament.reports.simplified.scoring_templates_report import SimplifiedScoringTemplatesReport
from avium_tournament.reports.simplified.team_classification_report import SimplifiedTeamClassificationReport
from avium_tournament.reports.statistics_report import StatisticsReport
from avium_tournament.utils import get_territory_parents

class Tournament(Document):

        ## Overwritten
        def on_update(self):
            for tournament_prize in self.tournament_prizes:
                tournament_prize.update_prize_visualization()

        def validate(self):
            existing_group = list()
            for group in self.group_table:
                group.validate()
                if (group.description, group.family) in existing_group:
                    frappe.throw(frappe._("Two groups with the same description and section not allowed. <br> <b>{0}</b> is duplicated").format(group.description))
                else:
                    existing_group.append((group.description, group.family))

            # Comprovar que els limits dels rangs siguin correctes i no hi hagi solapament entre rangs de preus individuals
            if len(self.specimen_price)>0:
                for price in self.specimen_price:
                    if price.range_start == 0:
                        price.range_start = 1
                    if price.range_start > price.range_end:
                        frappe.throw((frappe._("Range start is higher than the range end in the price range {}.")).format(price.idx))
                    for comparing_price in self.specimen_price:
                        if price.name!=comparing_price.name and (comparing_price.range_start<=price.range_start<=comparing_price.range_end or comparing_price.range_start<=price.range_end<=comparing_price.range_end):
                            frappe.throw((frappe._("Prices ranges {} and {} overlapping.")).format(price.idx, comparing_price.idx))

            # Comprovar que els limits dels rangs siguin correctes i no hi hagi solapament entre rangs de preus individuals
            if len(self.individual_price)>0:
                for individual_price in self.individual_price:
                    if individual_price.range_start == 0:
                        individual_price.range_start = 1
                    if individual_price.range_start > individual_price.range_end:
                        frappe.throw((frappe._("Range start is higher than the range end in the Individual price range {}.")).format(individual_price.idx))
                    for comparing_individual_price in self.individual_price:
                        if individual_price.name!=comparing_individual_price.name and (comparing_individual_price.range_start<=individual_price.range_start<=comparing_individual_price.range_end or comparing_individual_price.range_start<=individual_price.range_end<=comparing_individual_price.range_end):
                            frappe.throw((frappe._("Individual prices ranges {} and {} overlapping.")).format(individual_price.idx, comparing_individual_price.idx))

            # Comprovar que els limits dels rangs siguin correctes i no hi hagi solapament entre rangs de preus equips
            if len(self.team_price)>0:
                for team_price in self.team_price:
                    if team_price.range_start == 0:
                        team_price.range_start = 1
                    if team_price.range_start > team_price.range_end:
                        frappe.throw((frappe._("Range start is higher than the range end in the Team price range {}.")).format(individual_price.idx))
                    for comparing_team_price in self.team_price:
                        if team_price.name!=comparing_team_price.name and (comparing_team_price.range_start<=team_price.range_start<=comparing_team_price.range_end or comparing_team_price.range_start<=team_price.range_end<=comparing_team_price.range_end):
                            frappe.throw((frappe._("Team prices ranges {} and {} overlapping.")).format(team_price.idx, comparing_team_price.idx))

            # Check batch_size in price is greater than 0 and it's not repeated
            batch_sizes = list()
            for batch_price in self.batch_price:
                if batch_price.batch_size < 1:
                    frappe.throw(_("Batch size {0} in the batch prices table is not correct, it should be greater than 0").format(batch_price.batch_size,))
                if batch_price.batch_size in batch_sizes:
                    frappe.throw(
                        _("Batch size {0} is repeated in position {1} and {2} in the batch prices table. Batch size cannot be repeated").format(
                            batch_price.batch_size,
                            batch_sizes.index(batch_price.batch_size) + 1,
                            batch_price.idx,
                        )
                    )
                else:
                    batch_sizes.append(batch_price.batch_size)

            # Check batch_size in groups exists in price batch
            available_batch_sizes = [
                batch.batch_size
                for batch
                in self.batch_price
            ]

            for tournament_group in self.group_table:
                if tournament_group.individual_batch_size > 0 and not tournament_group.individual_batch_size in available_batch_sizes:
                    frappe.throw(
                        _("Individual batch size in group {0}, position {1}, is not defined in batch prices table").format(
                            tournament_group.individual_code,
                            tournament_group.idx,
                        )
                    )

                if tournament_group.team_batch_size > 0 and not tournament_group.team_batch_size in available_batch_sizes:
                    frappe.throw(
                        _("Team batch size in group {0}, position {1}, is not defined in batch prices table").format(
                            tournament_group.team_code,
                            tournament_group.idx,
                        )
                    )

        def before_insert(self):
            count = frappe.db.count("Tournament")
            if count>1:
                frappe.throw(_("Only 1 tournament is supported"))

        def before_save(self):
            self.check_registered_groups()
            
            if self.territory:
                self.comunity, self.country = get_territory_parents(self.territory)
            
            if not self.enable_book:
                self.minimum_book_quantity = 0
            
            if self.mandatory_carrier:
                self.enable_carrier = True

            self.tournament_prizes = sorted(self.tournament_prizes, key=lambda prize: (-prize.individual_mark, prize.idx))
            for idx, item in enumerate(self.tournament_prizes):
                item.idx = idx+1

        def delete_all_groups(self):
            ## TODO check status tournament, only allow if there is no registration done
            registrations = frappe.get_list("Registration")
            if len(registrations)==0:
                frappe.db.sql("Delete from `tabTournament Group` where parent=%s", [self.name])
                return "ok"
            else:
                frappe.throw(_("{0} registrations exist for this tournament. You can only delete groups individually").format(len(registrations)))

        def get_registration_prices_and_units(self, registration):

            data = dict()

            registration, data = self.get_batch_price(registration, data)

            registration, data = self.get_individual_team_price(registration, data)

            return data

        def get_exhibitors_registration_open(self):
            from avium_tournament.api import get_statistics
            statistics = get_statistics()
            current_specimens=statistics["no_sing_individual_specimens"] + statistics["no_sing_team_specimens"]

            return (self.tournament_status == "Published") and (frappe.utils.get_datetime() < self.end_registration_date) and (self.max_specimen_amount==0 or (current_specimens < self.max_specimen_amount))

        def get_individual_team_price(self, registration, data):

            individual = 0
            team = 0
            team_specimens = 0
            specimens = 0

            for individual_registration in registration.individual_table:
                if not individual_registration.flags.batch:
                    individual += 1
                specimens += 1
                if individual_registration.league_inscription_a == 'Yes':
                    data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1

            for team_registration in registration.team_table:
                if not team_registration.flags.batch:
                    team += 1
                    team_specimens += team_registration.team_size
                specimens += team_registration.team_size
                if team_registration.league_inscription_a == 'Yes':
                    data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                if team_registration.league_inscription_b == 'Yes':
                    data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                if team_registration.league_inscription_c == 'Yes':
                    data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                if team_registration.league_inscription_d == 'Yes':
                    data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1

            data['individual_registrations'] = data.get('individual_registrations', 0) + individual
            data['team_registrations'] = data.get('team_registrations', 0) + team
            data['total_specimen'] = data.get('total_specimen', 0) + specimens

            if self.independent_price == "Yes":
                range_not_defined = True
                if individual == 0:
                    range_not_defined = False
                else:
                    for item in self.individual_price:
                        if self.price_apply == "Unique":
                            if individual >= item.range_start and individual <= item.range_end:
                                if registration.membership_discount == "Yes":
                                    data['individual_registration_price'] = item.member_price * individual
                                else:
                                    data['individual_registration_price'] = item.non_member_price * individual
                                range_not_defined = False
                        else:
                            if individual >= item.range_start and individual <= item.range_end:
                                data['individual_registration_price'] = data.get('individual_registration_price', 0)
                                if registration.membership_discount == "Yes":
                                    data['individual_registration_price'] += (individual - item.range_start + 1) * item.member_price
                                else:
                                    data['individual_registration_price'] += (individual - item.range_start + 1) * item.non_member_price
                                range_not_defined = False
                            elif individual >= item.range_end:
                                data['individual_registration_price'] = data.get('individual_registration_price', 0)
                                if registration.membership_discount == "Yes":
                                    data['individual_registration_price'] += (item.range_end - item.range_start + 1) * item.member_price
                                else:
                                    data['individual_registration_price'] += (item.range_end - item.range_start + 1) * item.non_member_price
                                range_not_defined = False
                if range_not_defined:
                    frappe.throw(_("Individual range not defined for this amount of specimens"))

                range_not_defined = True
                if team == 0:
                    range_not_defined = False
                else:
                    for item in self.team_price:
                        if self.price_apply == "Unique":
                            if team >= item.range_start and team <= item.range_end:
                                if registration.membership_discount == "Yes":
                                    data['team_registration_price'] = item.member_price * team
                                else:
                                    data['team_registration_price'] = item.non_member_price * team
                                range_not_defined = False
                        else:
                            if team >= item.range_start and team <= item.range_end:
                                data['team_registration_price'] = data.get('team_registration_price', 0)
                                if registration.membership_discount == "Yes":
                                    data['team_registration_price'] += (team - item.range_start + 1) * item.member_price
                                else:
                                    data['team_registration_price'] += (team - item.range_start + 1) * item.non_member_price
                                range_not_defined = False
                            elif team >= item.range_end:
                                data['team_registration_price'] = data.get('team_registration_price', 0)
                                if registration.membership_discount == "Yes":
                                    data['team_registration_price'] += (item.range_end - item.range_start + 1) * item.member_price
                                else:
                                    data['team_registration_price'] += (item.range_end - item.range_start + 1) * item.non_member_price
                                range_not_defined = False
                if range_not_defined:
                    frappe.throw(_("Team range not defined for this amount of specimens"))

            else:
                total_price = 0
                range_not_defined = True
                if (individual + team_specimens) == 0:
                    range_not_defined = False
                else:
                    for item in self.specimen_price:
                        if self.price_apply == "Unique":
                            if item.range_start <= (individual + team_specimens) <= item.range_end:
                                if registration.membership_discount == "Yes":
                                    data['individual_registration_price'] = item.member_price * individual
                                    data['team_registration_price'] = item.member_price * team_specimens
                                else:
                                    data['individual_registration_price'] = item.non_member_price * individual
                                    data['team_registration_price'] = item.non_member_price * team_specimens
                                range_not_defined = False
                        else:
                            if item.range_start <= (individual + team_specimens) <= item.range_end:
                                if registration.membership_discount == "Yes":
                                    total_price += ((individual + team_specimens) - item.range_start + 1) * item.member_price
                                    range_not_defined = False
                                else:
                                    total_price += ((individual + team_specimens) - item.range_start + 1) * item.non_member_price
                                    range_not_defined = False
                            elif (individual + team_specimens) >= item.range_end:
                                if registration.membership_discount == "Yes":
                                    total_price += (item.range_end - item.range_start + 1) * item.member_price
                                    range_not_defined = False
                                else:
                                    total_price += (item.range_end - item.range_start + 1) * item.non_member_price
                                    range_not_defined = False
                            data['individual_registration_price'] = total_price * (float(individual)/specimens)
                            data['team_registration_price'] = total_price * (1 - float(individual)/specimens)

                if range_not_defined:
                    frappe.throw(_("Range not defined for this amount of specimens"))

            return registration, data

        def get_batch_price(self, registration, data):
            batch_price_cache = {
                batch.batch_size: {
                    "Yes": batch.member_price,
                    "No": batch.non_member_price
                }
                for batch
                in self.batch_price
            }
            
            if len(batch_price_cache) < 1:
                for index, individual_registration in enumerate(registration.individual_table):
                    registration.individual_table[index].flags.batch = False
                for index, team_registration in enumerate(registration.team_table):
                    registration.team_table[index].flags.batch = False
                return registration, data

            individual_group_with_batch = {
                group.individual_code:
                    {
                        "batch_size": group.individual_batch_size,
                        "specimens": 0,
                    }
                for group
                in self.group_table
                if group.individual_batch_size > 0
            }
            for index, individual_registration in enumerate(registration.individual_table):
                if individual_registration.group_code in individual_group_with_batch:
                    individual_group_with_batch[individual_registration.group_code]["specimens"] += 1
                    if individual_registration.league_inscription_a == "Yes":
                        data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                    registration.individual_table[index].flags.batch = True
                else:
                    registration.individual_table[index].flags.batch = False

            team_group_with_batch = {
                group.team_code:
                    {
                        "batch_size": group.team_batch_size,
                        "specimens": 0,
                    }
                for group
                in self.group_table
                if group.team_batch_size > 0
            }
            for index, team_registration in enumerate(registration.team_table):
                if team_registration.group_code in team_group_with_batch:
                    team_group_with_batch[team_registration.group_code]["specimens"] += team_registration.team_size
                    if team_registration.league_inscription_a == "Yes":
                        data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                    if team_registration.league_inscription_b == "Yes":
                        data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                    if team_registration.league_inscription_c == "Yes":
                        data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                    if team_registration.league_inscription_d == "Yes":
                        data['league_inscribed_specimens'] = data.get('league_inscribed_specimens', 0) + 1
                    registration.team_table[index].flags.batch = True
                else:
                    registration.team_table[index].flags.batch = False

            for group_code in individual_group_with_batch.keys():
                batch_size = float(individual_group_with_batch[group_code]["batch_size"])
                batch_number = math.ceil(individual_group_with_batch[group_code]["specimens"]/batch_size)
                batch_price = batch_price_cache[batch_size][registration.membership_discount]
                data['batch_registrations'] = data.get('batch_registrations', 0) + batch_number
                data['batch_registration_price'] = data.get('batch_registration_price', 0) + batch_number * batch_price

            for group_code in team_group_with_batch.keys():
                batch_size = float(team_group_with_batch[group_code]["batch_size"])
                batch_number = math.ceil(team_group_with_batch[group_code]["specimens"]/batch_size)
                batch_price = batch_price_cache[batch_size][registration.membership_discount]
                data['batch_registrations'] = data.get('batch_registrations', 0) + batch_number
                data['batch_registration_price'] = data.get('batch_registration_price', 0) + batch_number * batch_price

            return registration, data

        def group_exists(self, description, family):
            for item in self.group_table:
                if item.description == description and item.family == family:
                    return True
            return False

        def generate_groups(self, families):
            if len(self.group_table)>0:
                idx = max([x.idx for x in self.group_table]) + 1
            else:
                idx = 1

            if families == "All":
                sorted_families = sorted(frappe.get_all("Family", fields=['name', 'cage']), key=lambda family: family['name'])
            else:
                family_code = families.split()[0]
                sorted_families = frappe.get_all("Family", filters={"code": family_code}, fields=['name', 'cage'])

            for family in sorted_families:
                groups = list()
                sorted_templates = sorted(frappe.get_all("Judging Template", filters={"family":family.name}), key=lambda template: template['name'])
                for template_name in sorted_templates:
                    template = frappe.get_doc("Judging Template", template_name.name)
                    if len(template.specialisations)==0 and not self.group_exists(template.template_name, family.name):
                        group=dict()
                        group["description"]=template.template_name
                        group["size"]=template.size
                        group["tufted"]=template.tufted
                        group["red_factor"]=template.red_factor
                        group["team_size"]=template.team_size
                        group["cage"]=template.cage
                        group["family"]=family.name
                        group["judging_template"]=template.name
                        group["printing_order"]=template.printing_order
                        groups.append(group)
                    else:
                        for specialisation in template.specialisations:
                            if not self.group_exists(specialisation.specialisation_name, family.name):
                                group = dict()
                                group["description"]=specialisation.specialisation_name
                                group["size"]=template.size
                                group["tufted"]=specialisation.tufted
                                group["red_factor"]=template.red_factor
                                group["team_size"]=specialisation.team_size
                                group["cage"]=specialisation.cage
                                group["family"]=family.name
                                group["judging_template"]=template.name
                                group["printing_order"]=specialisation.printing_order
                                group["individual_batch_size"]=specialisation.individual_batch_size
                                group["team_batch_size"]=specialisation.team_batch_size
                                groups.append(group)

                sorted_groups = sorted(groups, key=lambda group: group["printing_order"])

                for group in sorted_groups:
                    new_group = {
                        "idx": idx,
                        "description": group["description"],
                        "size": group["size"],
                        "tufted": group["tufted"],
                        "red_factor": group["red_factor"],
                        "individual": 1,
                        "team": 1,
                        "team_size": group["team_size"],
                        "cage": group["cage"],
                        "family": group["family"],
                        "judging_template": group["judging_template"],
                        "individual_batch_size": group["individual_batch_size"],
                        "team_batch_size": group["team_batch_size"]
                    }
                    idx+=1
                    self.append("group_table", new_group)
            self.save()
            return "OK"

        def assign_group_codes(self, pair_groups):
                assignations = {}

                for t_group in self.group_table:
                        family = t_group.family
                        if not family in assignations:
                                if pair_groups == 1:
                                        assignations[family] = {
                                                "count_ind": 1,
                                                "count_team": 2
                                        }
                                else:
                                        assignations[family] = {
                                                "count_ind": 2,
                                                "count_team": 1
                                        }

                        if t_group.individual==1:
                                t_group.individual_code = family + '-' + str(assignations[family]["count_ind"]).zfill(3)
                        else:
                                t_group.individual_code = None

                        if t_group.team==1:
                                t_group.team_code = family + '-' + str(assignations[family]["count_team"]).zfill(3)
                        else:
                                t_group.team_code = None

                        assignations[family]["count_ind"] += 2
                        assignations[family]["count_team"] += 2

                self.save()

                return "OK"

        def get_ordered_groups(self):
            groups_data = frappe.db.sql("""
                        SELECT  tgro.team_code as team,
                                tgro.individual_code as individual
                                FROM `tabTournament Group` as tgro
                                        LEFT JOIN `tabFamily` as fami on fami.name = tgro.family
                                        LEFT JOIN `tabJudging Template Specialisation` as jutespe on jutespe.specialisation_name = tgro.description
                                ORDER BY
                                        fami.printing_order ASC,
                                        IF(tgro.individual_code IS NOT NULL, tgro.individual_code, tgro.team_code) ASC
                    """)

            groups = []
            for item in groups_data:
                if item[0] is not None and item[1] is not None:
                    if item[0]<item[1]:
                        groups.append(item[0])
                        groups.append(item[1])
                    else:
                        groups.append(item[1])
                        groups.append(item[0])
                elif item[0] is not None:
                    groups.append(item[0])
                elif item[1] is not None:
                    groups.append(item[1])

            return groups

        def assign_cage_number(self):
            groups = self.get_ordered_groups()
            for group in groups:
                self.assign_cage_number_group(group)

        def assign_cage_number_group(self, group):
            if self.consecutive_cage_numbers:
                max_cage = frappe.db.sql("select max(cage_number) from `tabScoring Template`")[0][0]
            else:
                max_cage = frappe.db.sql("select max(cage_number) from `tabScoring Template` WHERE group_code=%s", [group])[0][0]


            if max_cage == None:
                counter = 1
            else:
                counter = int(max_cage) + 1

            scoring_templates = frappe.db.sql("select name from `tabScoring Template` where (cage_number is null or cage_number='') and group_code=%s order by name", [group], as_dict=True)
            if self.randomize_cage_numbers:
                import random
                random.shuffle(scoring_templates)

            for st in scoring_templates:
                frappe.db.set_value("Scoring Template", st.name, "cage_number", "00000"[:-len(str(counter))] + str(counter), update_modified=False)
                counter += 1

        def generate_scoring_templates(self):
            # #Check all ring codes have been introduced
            # individual_registrations = [(individual_registration["idx"], individual_registration["parent"])
            #                             for individual_registration in frappe.get_all("Individual Registration", fields=["ring_a", "idx", "parent"])
            #                             if individual_registration["ring_a"]==None or individual_registration["ring_a"]==""]
            #
            # if len(individual_registrations)>0:
            #     frappe.throw(_("Ring code from the individual registration {} in registration {} is missing.").format(individual_registrations[0][0], individual_registrations[0][1]))
            #
            # team_registrations = [(team_registration["idx"], team_registration["parent"])
            #                       for team_registration in frappe.get_all("team Registration", fields=["ring_a", "ring_b", "ring_c", "ring_d", "idx", "parent"])
            #                       if team_registration["ring_a"] == None or team_registration["ring_a"] == "" or team_registration["ring_b"] == None or team_registration["ring_b"] == "" or team_registration["ring_c"] == None or team_registration["ring_c"] == "" or team_registration["ring_d"] == None or team_registration["ring_d"] == ""]
            #
            # if len(team_registrations) > 0:
            #     frappe.throw(_("A ring code from the team registration {} in registration {} is missing.").format(team_registrations[0][0], team_registrations[0][1]))

            #Generate scoring templates
            registrations = frappe.get_all("Registration")
            frappe.publish_realtime("generate_scoring_templates", {"progress": [0, len(registrations)]}, user=frappe.session.user)
            for i, registration in enumerate(registrations):
                    registration_doc = frappe.get_doc("Registration", registration.name)
                    registration_doc.generate_scoring_templates(assign_cage_number=False, assign_judge=False)
                    frappe.publish_realtime("generate_scoring_templates", {"progress": [i, len(registrations)]}, user=frappe.session.user)

            self.assign_cage_number()
            self.assign_judges_empty()

        def update_all_classifications(self):
            groups = frappe.db.sql("SELECT DISTINCT group_code from `tabScoring Template`", as_list=1)
            frappe.publish_realtime("update_all_classifications", {"progress": [0, len(groups)]}, user=frappe.session.user)
            for index, group in enumerate(groups):
                self.update_classification_group(group[0])
                frappe.publish_realtime("update_all_classifications", {"progress": [index, len(groups)]}, user=frappe.session.user)

        def update_classification_group(self, group_code):

            frappe.db.sql(
                """UPDATE `tabScoring Template`
                SET classification=9999
                WHERE group_code = %(group_code)s AND "Not Presented"  IN (location_status_a, location_status_b, location_status_c, location_status_d)""",
                {"group_code": group_code}
            )

            # Reset scoring templates prizes and status
            frappe.db.sql(
                """UPDATE `tabScoring Template`
                SET prize=NULL, prize_visualization=NULL, status='Judged'
                WHERE group_code = %(group_code)s AND status NOT IN ('Pending','Closed')""",
                {"group_code": group_code}
            )

            # Get group scoring templates
            scoring_templates_ids = frappe.get_all(
                "Scoring Template",
                filters=[["group_code","=", group_code],["status","!=","Pending"],["status","!=","Closed"]],
                fields=["name"]
            )

            if len(scoring_templates_ids) < 1:
                return True

            scoring_templates = list(map(
                lambda template: frappe.get_doc("Scoring Template", template['name']),
                scoring_templates_ids
            ))

            for scoring_template in scoring_templates:
                scoring_template.flags.tie_break_mark_used = False

            # Sort scoring templates
            scoring_templates = sorted(scoring_templates, reverse=True)

            scoring_templates_manual_prizes = [
                scoring_template_doctype
                for scoring_template_doctype
                in scoring_templates
                if blank_undefined(scoring_template_doctype.manual_prize) != "Undefined"
            ]

            if len(scoring_templates_manual_prizes) > 0:
                prizes_list = get_prizes(group_code)
                prizes_cache = {
                    prize['description']: int(prize['assignment_order'])
                    for prize
                    in prizes_list
                }

                scoring_templates = sorted(
                    scoring_templates,
                    key=lambda scoring_template: prizes_cache.get(scoring_template.manual_prize, len(scoring_templates))
                )

            for idx, st in enumerate(scoring_templates):
                frappe.db.set_value("Scoring Template", st.name, "classification", idx + 1, update_modified=False)

            # Assign prizes
            if len(scoring_templates_manual_prizes) > 0:
                scoring_templates = self.assign_manual_prizes(group_code, scoring_templates)
            else:
                scoring_templates = self.assign_automatic_prizes(group_code, scoring_templates)

            # Check tied and error status scoring templates
            scoring_templates = self.check_tied_error_scoring_templates(group_code, scoring_templates)

            # Disable Tie Break Mark fields
            flagged_scoring_templates = [
                scoring_template.name
                for scoring_template
                in scoring_templates
                if scoring_template.flags.tie_break_mark_used
            ]

            if flagged_scoring_templates:
                frappe.db.sql(
                    """UPDATE `tabScoring Template`
                    SET block_tie_break_mark=0
                    WHERE group_code = %(group_code)s AND name IN %(flagged_scoring_templates)s""",
                    {
                        "flagged_scoring_templates": flagged_scoring_templates,
                        "group_code": group_code
                    }
                )

                frappe.db.sql(
                    """UPDATE `tabScoring Template`
                    SET block_tie_break_mark=1, tie_break_mark=0
                    WHERE group_code = %(group_code)s AND name NOT IN %(flagged_scoring_templates)s""",
                    {
                        "flagged_scoring_templates": flagged_scoring_templates,
                        "group_code": group_code
                    }
                )

            return True

        def assign_automatic_prizes(self, group_code, scoring_templates):

            group = None
            for item in self.group_table:
                if item.individual_code == group_code:
                    group = item
                    individual = True
                if item.team_code == group_code:
                    group = item
                    individual = False

            if individual:
                punctuation_key = 'individual_mark'
            elif group.team_size == 2:
                punctuation_key = 'duo_mark'
            elif group.team_size == 3:
                punctuation_key = 'trio_mark'
            else:
                punctuation_key = 'team_mark'

            prizes_list = get_prizes(group_code)

            remaining_prizes = [
                {
                    'prize': prize['prize'],
                    'description': prize['description'],
                    'points': prize[punctuation_key],
                }
                for prize
                in prizes_list
            ]

            #Assignació dels premis individuals
            if len(scoring_templates) > 0:
                for scoring_template in scoring_templates:
                    remaining_prizes, prized_template = assign_prize(scoring_template, remaining_prizes)
                    if prized_template:
                        scoring_template.flags.tie_break_mark_used = True

            return scoring_templates

        def assign_manual_prizes(self, group_code, scoring_templates):
            prizes_list = get_prizes(group_code)
            prizes_cache = {
                prize['description']: {
                    'prize': prize['prize'],
                    'description': prize['description'],
                }
                for prize
                in prizes_list
            }

            for scoring_template in scoring_templates:
                if blank_undefined(scoring_template.manual_prize) != "Undefined":
                    assign_prize(
                        scoring_template,
                        [prizes_cache[scoring_template.manual_prize]],
                        manual_assign=True
                    )
                    scoring_template.flags.tie_break_mark_used = True

            return scoring_templates

        def check_tied_error_scoring_templates(self, group_code, scoring_templates):

            prized_templates = [
                scoring_template['name']
                for scoring_template
                in frappe.get_all(
                    "Scoring Template",
                    filters=[["group_code", "=", group_code], ["prize", "!=", ""]],
                    fields=["name"],
                    order_by="classification ASC"
                )
            ]

            if len(prized_templates) > 0:
                last_template = frappe.get_doc("Scoring Template", prized_templates[-1])

                same_points = [
                    scoring_template['name']
                    for scoring_template
                    in frappe.get_all(
                        "Scoring Template",
                        filters=[
                            ["group_code", "=", group_code],
                            ["total_mark", "=", last_template.total_mark],
                            ["tie_break_mark", "=", last_template.tie_break_mark],
                            ["name", "not in", prized_templates],
                            ["status", "=","Judged"],
                        ],
                        fields=["name"],
                        order_by="classification ASC"
                    )
                ]

                for template_index, template in enumerate(scoring_templates):
                    if template.name in prized_templates:
                        for other_item_index, other_item in enumerate(scoring_templates[template_index+1:len(prized_templates + same_points)]):
                            template_total = template.total_mark + template.tie_break_mark
                            other_item_total = other_item.total_mark + other_item.tie_break_mark
                            if self.add_tie_break_marks == "Yes" and self.minimum_distance_one_point == "Yes":
                                if template_total == other_item_total:
                                    if scoring_templates[template_index].status != "Error":
                                        frappe.db.set_value("Scoring Template", template.name, "status", "Tied", update_modified=False)
                                        scoring_templates[template_index].flags.tie_break_mark_used = True
                                    if scoring_templates[template_index + other_item_index + 1].status != "Error":
                                        frappe.db.set_value("Scoring Template", other_item.name, "status", "Tied", update_modified=False)
                                        scoring_templates[template_index + other_item_index + 1].flags.tie_break_mark_used = True
                            else:
                                if template == other_item:
                                    if scoring_templates[template_index].status != "Error":
                                        frappe.db.set_value("Scoring Template", template.name, "status", "Tied")
                                        scoring_templates[template_index].flags.tie_break_mark_used = True
                                    if scoring_templates[template_index + other_item_index + 1].status != "Error":
                                        frappe.db.set_value("Scoring Template", other_item.name, "status", "Tied", update_modified=False)
                                        scoring_templates[template_index + other_item_index + 1].flags.tie_break_mark_used = True
                            if template_total < other_item_total and self.add_tie_break_marks == "Yes":
                                frappe.db.set_value("Scoring Template", template.name, "status", "Error", update_modified=False)
                                frappe.db.set_value("Scoring Template", other_item.name, "status", "Error", update_modified=False)
                                scoring_templates[template_index].status = "Error"
                                scoring_templates[template_index + other_item_index + 1].status = "Error"
                                scoring_templates[template_index].flags.tie_break_mark_used = True
                                scoring_templates[template_index + other_item_index + 1].flags.tie_break_mark_used = True

            return scoring_templates
            
        def check_registered_groups(self):
            individual_registered_groups = self.get_ind_registered_groups()
            team_registered_groups = self.get_team_registered_groups()
            current_individual_groups = [group.name for group in self.group_table if group.individual == 1]
            current_team_groups = [group.name for group in self.group_table if group.team == 1]
            for registration in individual_registered_groups:
                if registration.group not in current_individual_groups:
                    frappe.throw(_("Group {0} cannot be deleted as it contains some specimens registered, please refresh window").format(registration.group_code))
            for registration in team_registered_groups:
                if registration.group not in current_team_groups:
                    frappe.throw(_("Group {0} cannot be deleted as it contains some specimens registered, please refresh window").format(registration.group_code))

        def get_ind_registered_groups(self):
            registered_groups = frappe.db.sql("""SELECT distinct reg_group.`group`, reg_group.group_code
                                            FROM `tabIndividual Registration` AS reg_group
                                            JOIN `tabRegistration` AS reg ON reg_group.parent=reg.name""",
                                            as_dict=True)
            return registered_groups

        def get_team_registered_groups(self):
            registered_groups = frappe.db.sql("""SELECT distinct reg_group.`group`, reg_group.group_code
                                            FROM `tabTeam Registration` AS reg_group
                                            JOIN `tabRegistration` AS reg ON reg_group.parent=reg.name""",
                                            as_dict=True)
            return registered_groups

        def get_group_codes(self):
            groups = []
            for group in self.group_table:
                if group.individual:
                    groups.append(group.individual_code)
                if group.team:
                    groups.append(group.team_code)
            return groups

        def print_judges_user_pass(self):
            judges_list = frappe.get_all("Judge", fields=["name"], order_by="creation")

            for item in judges_list:
                judge = frappe.get_doc("Judge", item)
                users = frappe.get_list("User", filters={"judge": judge.name})
                user = frappe.get_doc("User", users[0].name)

                try:
                    print(judge.judge_name + ";" + user.username + ";" + judge.get_password("password"))
                except:
                    print(judge.judge_name or "" + ";" + user.username or "" + ";")

        ##Stickers
        def print_judging_stickers(self, arguments):
            with JudgingStickersReport(arguments) as report:
                return report.get_pdf()

        #@profileit("print_marks_stickers")
        def print_marks_stickers(self, arguments):
            stickers_type = get_marks_stickers_type()
            if stickers_type == "Extended":
                with ExtendedMarksStickersReport(arguments) as report:
                    return report.get_pdf()
            else:
                with SimplifiedMarksStickersReport(arguments) as report:
                    return report.get_pdf()

        def print_exhibitor_stickers(self, arguments):
            with ExhibitorStickersReport(arguments) as report:
                return report.get_pdf()

        ##Reports
        def print_groups_list_report(self, arguments):
            with GroupsListReport(arguments) as report:
                return report.get_pdf()

        def print_exhibitors_list_report(self, arguments):
            report_type = get_report_type()
            if report_type == "Extended":
                with ExtendedExhibitorsListReport(arguments) as report:
                    return report.get_pdf()
            else:
                with SimplifiedExhibitorsListReport(arguments) as report:
                    return report.get_pdf()

        def print_registration_report(self, arguments):
            with RegistrationReport(arguments) as report:
                return report.get_pdf()

        def print_location_status_report(self, arguments):
            with LocationStatusReport(arguments) as report:
                return report.get_pdf()

        #@profileit("print_registration_summaries_actual")
        def print_registration_summaries(self, arguments):
            with RegistrationSummaryReport(arguments) as report:
                return report.get_pdf()

        #@profileit("print_cages_report")
        def print_cages_report(self, arguments):
            with CagesReport(arguments) as report:
                return report.get_pdf()

        def print_payments_report(self, arguments):
            with PaymentsReport(arguments) as report:
                return report.get_pdf()

        def print_judges_report(self, arguments):
            with JudgesReport(arguments) as report:
                return report.get_pdf()

        def print_carriers_report(self, arguments):
            with CarriersReport(arguments) as report:
                return report.get_pdf()

        def print_judgement_report(self, arguments):
            with JudgementReport(arguments) as report:
                return report.get_pdf()

        #@profileit("print_scoring_templates_cache_var_all")
        def print_scoring_templates(self, arguments):
            report_type = get_report_type()
            if report_type == "Extended":
                with ExtendedScoringTemplatesReport(arguments) as report:
                    return report.get_pdf()
            else:
                with SimplifiedScoringTemplatesReport(arguments) as report:
                    return report.get_pdf()

        #@profileit("print_classification_report")
        def print_classification_report(self, arguments):
            report_type = get_report_type()
            if report_type == "Extended":
                with ExtendedClassificationReport(arguments) as report:
                    return report.get_pdf()
            else:
                with SimplifiedIndividualClassificationReport(arguments) as report:
                    with SimplifiedTeamClassificationReport(arguments, canvas=report.canvas) as report:
                        return report.get_pdf()

        #@profileit("print_record_book_report")
        def print_record_book_report(self, arguments):
            report_type = get_report_type()
            if report_type == "Extended":
                with ExtendedRecordBookReport(arguments) as report:
                    return report.get_pdf()
            else:
                with SimplifiedRecordBookReport(arguments) as report:
                    return report.get_pdf()

        #@profileit("print_league_punctuations_report_actual")
        def print_league_punctuations_report(self, arguments):
            with LeaguePunctuationsReport(arguments) as report:
                return report.get_pdf()

        def print_exhibitor_classification_best_punctuations_report(self, arguments):
            with ExhibitorClassificationBestPunctuationsReport(arguments) as report:
                return report.get_pdf()

        def print_statistics_report(self, arguments):
            with StatisticsReport(arguments) as report:
                return report.get_pdf()

        def print_certificates(self, arguments):
            with CertificatesReport(arguments) as report:
                return report.get_pdf()

        #@profileit("print_delivery_report")
        def print_delivery_report(self, arguments):
            with DeliveryReport(arguments) as report:
                return report.get_pdf()

        #@profileit("print_box_report")
        def print_box_report(self, arguments):
            with BoxReport(arguments) as report:
                return report.get_pdf()

        def print_tax_report(self, arguments):
            with TaxReport(arguments) as report:
                return report.get_pdf()

        def mark_all_notpresented_judged(self):
            frappe.db.sql(
                    """UPDATE `tabScoring Template`
                    SET status = "Judged", classification = 9999
                    WHERE location_status_a="Not Presented" OR
                     location_status_b="Not Presented" OR
                     location_status_c="Not Presented" OR
                     location_status_d="Not Presented"
                     """
                )
            frappe.db.sql(
                    """UPDATE `tabScoring Template`
                    SET status_a = "Not Presented"
                    WHERE location_status_a="Not Presented" 
                     """
                )
            frappe.db.sql(
                    """UPDATE `tabScoring Template`
                    SET status_b = "Not Presented"
                    WHERE location_status_b="Not Presented" 
                     """
                )
            frappe.db.sql(
                    """UPDATE `tabScoring Template`
                    SET status_c = "Not Presented"
                    WHERE location_status_c="Not Presented" 
                     """
                )
            frappe.db.sql(
                    """UPDATE `tabScoring Template`
                    SET status_d = "Not Presented"
                    WHERE location_status_d="Not Presented" 
                     """
                )
            return "ok"

        # Not used (Pending to be deleted)
        # def print_family_exhibitor_report(self, arguments):
        #
        #     ## Internal variables
        #     get_parent = itemgetter(0)
        #
        #     ## Collect data
        #     data = dict()
        #
        #     # Report configuration data
        #     data["report_configuration"] = frappe.get_doc("Report Configuration", "Report Configuration")
        #
        #     # Header data
        #     data["header"] = header_data(self, _("Family Exhibitor Report"))
        #
        #     # Family
        #     data["family"] = arguments["family"]
        #     family_code = arguments["family"].split()[0]
        #
        #     # Tournament days
        #     data["tournament_days"] = ("Saturday", "Sunday", "Undefined")
        #
        #     # Exhibitors data
        #
        #     #registration_cache: Registration -> [Exhibitor,Sing_judgement_date,0,0]
        #     registration_cache = {registration["name"]: [registration["exhibitor"], blank_undefined(registration["sing_judgement_date"]), 0, 0]
        #                           for registration
        #                           in frappe.get_all("Registration", fields=['name', 'exhibitor', "sing_judgement_date"])
        #                           }
        #
        #     #individual_registration_cache: Individual Registration -> (Parent,1,0)
        #     individual_registration_cache = [(registration["parent"],1,0)
        #                                      for registration
        #                                      in frappe.get_all("Individual Registration", fields=['parent', 'group_code'])
        #                                      if registration["parent"] in registration_cache.keys() and re.sub(r'-[0-9]*', r'', registration["group_code"])==family_code
        #                                      ]
        #
        #     #team_registration_cache: Team Registration list -> (Parent,0,Team_size)
        #     team_registration_cache = [(registration["parent"], 0,registration["team_size"])
        #                                for registration
        #                                in frappe.get_all("Team Registration", fields=['parent', 'group_code','team_size'])
        #                                if registration["parent"] in registration_cache.keys() and re.sub(r'-[0-9]*', r'', registration["group_code"])==family_code]
        #
        #     #exhibitor_cache
        #     exhibitor_cache = dict()
        #     for registration in individual_registration_cache+team_registration_cache:
        #         if get_parent(registration) not in exhibitor_cache:
        #             exhibitor_cache[get_parent(registration)] = registration_cache[get_parent(registration)]
        #         exhibitor_cache[get_parent(registration)][2] += registration[1]
        #         exhibitor_cache[get_parent(registration)][3] += registration[2]
        #
        #     sorted_exhibitor_list = [registration['exhibitor_surname'] + ", " + registration['exhibitor_name']
        #                              for registration
        #                              in frappe.get_all(
        #                                  "Registration",
        #                                  fields={'exhibitor_name', 'exhibitor_surname'},
        #                                  order_by='exhibitor_name ASC, exhibitor_surname ASC'
        #                                 )
        #                              ]
        #
        #     data["exhibitors"] = dict()
        #     for day in data["tournament_days"]:
        #         data["exhibitors"][day] = [(frappe.get_doc("Registration", exhibitor_cache[registration][0]),exhibitor_cache[registration][2],exhibitor_cache[registration][3]) for registration in exhibitor_cache.keys() if exhibitor_cache[registration][1]==day]
        #         data["exhibitors"][day] = sorted(data["exhibitors"][day], key=lambda registration: sorted_exhibitor_list.index(registration[0].exhibitor_surname + ", " + registration[0].exhibitor_name))
        #
        #     ## Generate PDF
        #     with tempfile.NamedTemporaryFile() as temp:
        #             c = canvas.Canvas(temp)
        #             family_exhibitor_reportlab.drawpdf(c, data)
        #             return c.getpdfdata()

        ##Development Reports (Pending to be deleted)
        # def print_specialization_cages_report(self, arguments):
        #
        #     ## Collect data
        #     data = dict()
        #
        #     # Report configuration data
        #     data["report_configuration"] = frappe.get_doc("Report Configuration", "Report Configuration")
        #
        #     # Header data
        #     data["header"] = header_data(self, _("Specialization Cages Report"))
        #
        #     #family_description_cache: family_name -> family_description
        #     family_description_cache = {family["name"]: family["family_name"]
        #                                 for family in frappe.get_all("Family", fields=['name', 'family_name'])}
        #
        #     #judging_template_family_cache: judging_template_name -> judging_template_family
        #     judging_template_family_cache = {judging_template["name"]: judging_template["family"]
        #                                      for judging_template in frappe.get_all("Judging Template", fields=['name', 'family'])}
        #
        #     # Cages data
        #     cages = dict()
        #     judging_templates_specialization = frappe.get_all("Judging Template Specialisation", fields=['parent', 'specialisation_name', 'cage', 'printing_order'])
        #     sorted_judging_templates_specialization = sorted(judging_templates_specialization, key=lambda judging_templates_specialization: judging_templates_specialization["printing_order"])
        #     for judging_templates_specialization in sorted_judging_templates_specialization:
        #         if judging_template_family_cache[judging_templates_specialization["parent"]] not in cages:
        #             cages[judging_template_family_cache[judging_templates_specialization["parent"]]] = dict()
        #             cages[judging_template_family_cache[judging_templates_specialization["parent"]]]["description"] = family_description_cache[judging_template_family_cache[judging_templates_specialization["parent"]]]
        #             cages[judging_template_family_cache[judging_templates_specialization["parent"]]]["specializations"] = list()
        #         cages[judging_template_family_cache[judging_templates_specialization["parent"]]]["specializations"].append((judging_templates_specialization["specialisation_name"],judging_templates_specialization["cage"]))
        #     data["cages"] = cages
        #
        #     ## Generate PDF
        #     with tempfile.NamedTemporaryFile() as temp:
        #         c = canvas.Canvas(temp)
        #         specialization_cages_reportlab.drawpdf(c, data)
        #         c.save()
        #         return c.getpdfdata()

        def print_carrier_prizes_report(self, arguments):
            with CarrierPrizesReport(arguments) as report:
                return report.get_pdf()
        
        def assign_judges_empty(self):
            for group in self.group_table:
                if group.individual and group.default_individual_judge:
                    frappe.db.sql(
                        """UPDATE `tabScoring Template` SET judge_1=%s WHERE `tabScoring Template`.judge_1 is NULL AND `tabScoring Template`.group_code = %s""",
                        (group.default_individual_judge, group.individual_code))

                if group.team and group.default_team_judge:
                    frappe.db.sql(
                        """UPDATE `tabScoring Template` SET judge_1=%s WHERE `tabScoring Template`.judge_1 is NULL AND `tabScoring Template`.group_code = %s""",
                        (group.default_team_judge, group.team_code))

@frappe.whitelist()
def get_prizes(group_code):

    prizes_list = list()

    group = frappe.db.sql("""
        SELECT  tgro.name as name,
                tgro.family
        FROM `tabTournament Group` as tgro
        WHERE tgro.individual_code = %(group_code)s OR tgro.team_code = %(group_code)s
        """,
        {
            'group_code': group_code,
        },
        as_dict=True
    )

    if len(group) < 1:
        return prizes_list

    family_doctype = frappe.get_doc("Family", group[0]['family'])
    if len(family_doctype.prizes) > 0:
        parent = family_doctype.name
    else:
        parent = get_tournament().name

    prizes_raw_data = frappe.db.sql("""
        SELECT  toupri.name as prize,
                toupri.individual_mark as individual_mark,
                toupri.duo_mark as duo_mark,
                toupri.trio_mark as trio_mark,
                toupri.team_mark as team_mark,
                touprides.description_1 as description,
                touprides.assignment_order as assignment_order
        FROM `tabTournament Prizes` as toupri
        LEFT JOIN `tabTournament Prizes Description` as touprides on touprides.name = toupri.prize_description
        WHERE toupri.parent = %(parent)s
        ORDER BY assignment_order ASC
        """,
        {
            'parent': parent,
        },
        as_dict=True
    )

    return prizes_raw_data

@frappe.whitelist()
def tax_enabled():
    tournaments = frappe.get_list("Tournament", fields=["enable_tax"])

    if len(tournaments)==0:
        return {"tax_enabled": False}

    return {"tax_enabled": tournaments[0].enable_tax}

@frappe.whitelist()
def set_judges_group(groups, judge1):

    group_codes = list(map(lambda x: x["group_code"], json.loads(groups)))

    frappe.db.sql(
            """UPDATE `tabTournament Group` SET default_individual_judge=%s WHERE `tabTournament Group`.individual_code IN %s""",
        (judge1, group_codes))

    frappe.db.sql(
            """UPDATE `tabTournament Group` SET default_team_judge=%s WHERE `tabTournament Group`.team_code IN %s""",
        (judge1, group_codes))

    return "ok"

def assign_prize(scoring_template, prizes, manual_assign=False):
    for i, prize in enumerate(prizes):
        if manual_assign or scoring_template.total_mark >= prize['points']:
            frappe.db.set_value("Scoring Template", scoring_template.name, "prize", prize['prize'], update_modified=False)
            frappe.db.set_value("Scoring Template", scoring_template.name, "prize_visualization", prize['description'], update_modified=False)
            return prizes[i+1:], True
    return [], False

@frappe.whitelist()
def get_groups():
    return get_tournament().get_ordered_groups()

@frappe.whitelist()
def get_scoring_templates_updater(selected_group=None):
    filter_group=""
    if selected_group:
        filter_group = "WHERE `tabScoring Template`.group_code = '{0}' ".format(selected_group)

    regs = frappe.db.sql("""
            SELECT     
                name,
                group_code,
                cage_number,
                manual_scoring,
                manual_score_a,
                IF (team_size >= 2, manual_score_b,"-") as "manual_score_b",
                IF (team_size >= 3, manual_score_c,"-") as "manual_score_c",
                IF (team_size >= 4, manual_score_d,"-") as "manual_score_d",
                manual_harmony,
                IF (team_size > 1, "T", "I") as type
            FROM `tabScoring Template`
            """ + filter_group, 
            as_dict=1)

    regs.sort(key=lambda x: x.group_code + ", " + x.cage_number)

    return regs
