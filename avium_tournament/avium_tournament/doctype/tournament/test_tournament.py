# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
from frappe.test_runner import make_test_records
from avium_tournament.tests.CustomTestCase import CustomTestCase
from avium_tournament.utils import get_tournament

test_records = frappe.get_test_records('Tournament')
test_dependencies = ["Territory", "Association", "Judge Association", "Federation", "Tournament Prizes Description"]

class TestTournament(CustomTestCase):
	def test_territories(self):
		tournament = frappe.get_doc(self.doctype_name, test_records[0]["tournament_name"])
		self.assertIsNot(tournament.comunity, None)
		self.assertIsNot(tournament.country, None)

	def test_create_second_tournament(self):
		#TODO test
		pass

	def test_get_tournament(self):
		tournament_fn = get_tournament()
		tournament = frappe.get_doc(self.doctype_name, test_records[0]["tournament_name"])
		self.assertEquals(tournament_fn.name, tournament.name)

	def test_generate_groups(self):
		tournament = frappe.get_doc(self.doctype_name, test_records[0]["tournament_name"])
		tournament.generate_groups(families="All")
		self.assertEquals(len(tournament.group_table), 5)

	def test_doublegenerate_groups(self):
		tournament = frappe.get_doc(self.doctype_name, test_records[0]["tournament_name"])
		tournament.generate_groups(families="All")
		tournament.generate_groups(families="All")
		self.assertEquals(len(tournament.group_table), 5)

	def test_groups_order(self):
		tournament = frappe.get_doc(self.doctype_name, test_records[0]["tournament_name"])
		tournament.generate_groups(families="All")
		self.assertEquals(tournament.group_table[0].description, "CANARIOS CANTO ROLLER")
		self.assertEquals(tournament.group_table[1].description, "Lipocromos Rojos Intensos")

	def test_cage_inheritance(self):
		tournament = frappe.get_doc(self.doctype_name, test_records[0]["tournament_name"])
		tournament.generate_groups(families="All")
		self.assertEquals(tournament.group_table[0].cage, "_Test Cage 2")
		self.assertEquals(tournament.group_table[1].cage, "_Test Cage 1")
		self.assertEquals(tournament.group_table[2].cage, "_Test Cage 3")

	def test_group_exhists(self):
		tournament = frappe.get_doc(self.doctype_name, test_records[0]["tournament_name"])
		exists = tournament.group_exists(description="CANARIOS CANTO ROLLER", family="A")
		self.assertEquals(exists, True)
		exists = tournament.group_exists(description="CANARIOS CANTO ROLLER", family="B")
		self.assertEquals(exists, False)
		exists = tournament.group_exists(description="Jilguero", family="A")
		self.assertEquals(exists, False)