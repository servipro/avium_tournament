// Copyright (c) 2017, SERVIPRO SL and contributors
// For license information, please see license.txt

frappe.ui.form.on('Tournament', {
	refresh: function(frm) {
        // Filter Territory field
        cur_frm.set_query("territory", function() {
            return {
                "filters": {
                    "has_subterritories": 0
                }
            };
        });

        // Filter Judging Template in group field
        frm.set_query("judging_template","group_table", function (doc, cdt, cdn) {
             return {
                 filters: {
                      family: locals[cdt][cdn].family
                 }
             }
        });

        frappe.call({
            method:"avium_tournament.utils.get_ungenerated_scoring_templates",
            args: {
            },
            async: false,
            callback: function(r) {
                if(r.message>0 && (frm.doc.tournament_status != "Published")){
                    frappe.msgprint(__("There are some missing scoring templates, please generate them beforehand"), __("Error"))
                }
            }
        });

        frappe.realtime.on("generate_scoring_templates", function(data) {
             if(data.progress) {
                frappe.hide_msgprint(true);
                console.log(data.progress[0] + "/" + data.progress[1]);
                frappe.show_progress(__("Generating Scoring Templates"), data.progress[0], data.progress[1]);
             }
             if(data.progress[0]===data.progress[1]-1){
                setTimeout(frappe.hide_progress, 1000);
                frappe.show_alert(__("Scoring Templates generated correctly"));
             }
        });

        //Block fields in sections Price and Extra if there are registrations
        frappe.call({
            method:"frappe.client.get_list",
            args:{
                doctype:"Registration",
                filters: [
                ],
                fields: [
                    "name"
                ]
            },
            callback: function(r) {
                if (r.message.length > 0) {
                    frm.toggle_enable("independent_price", false)
                    frm.toggle_enable("price_apply", false)
                    frm.get_field("specimen_price").grid.docfields[0].read_only = 1
                    frm.get_field("specimen_price").grid.docfields[1].read_only = 1
                    frm.get_field("specimen_price").grid.docfields[2].read_only = 1
                    frm.get_field("specimen_price").grid.docfields[3].read_only = 1
                    frm.get_field("individual_price").grid.docfields[0].read_only = 1
                    frm.get_field("individual_price").grid.docfields[1].read_only = 1
                    frm.get_field("individual_price").grid.docfields[2].read_only = 1
                    frm.get_field("individual_price").grid.docfields[3].read_only = 1
                    frm.get_field("team_price").grid.docfields[0].read_only = 1
                    frm.get_field("team_price").grid.docfields[1].read_only = 1
                    frm.get_field("team_price").grid.docfields[2].read_only = 1
                    frm.get_field("team_price").grid.docfields[3].read_only = 1
                    frm.get_field("batch_price").grid.docfields[0].read_only = 1
                    frm.get_field("batch_price").grid.docfields[1].read_only = 1
                    frm.get_field("batch_price").grid.docfields[2].read_only = 1
                    frm.toggle_enable("specimen_price", false)
                    frm.toggle_enable("individual_price", false)
                    frm.toggle_enable("team_price", false)
                    frm.toggle_enable("batch_price", false)
                    frm.toggle_enable("enable_book", false)
                    frm.toggle_enable("enable_league_inscriptions", false)
                    frm.toggle_enable("enable_meal", false)
                    frm.toggle_enable("enable_tax", false)
                    frm.toggle_enable("enable_carrier", false)
                    frm.toggle_enable("enable_membership_price", false)
                    frm.toggle_enable("book_price", false)
                    frm.toggle_enable("league_price", false)
                    frm.toggle_enable("meal_price", false)
                    frm.toggle_enable("add_deduct_tax", false)
                    frm.toggle_enable("fix_percentage_tax", false)
                    frm.toggle_enable("extra_tax", false)
                    frm.toggle_enable("minimum_specimens_to_be_applicable", false)
                    frm.toggle_enable("minimum_book_quantity", false)
                    frm.toggle_enable("minimum_league_inscriptions", false)
                };
            },
        });

        //Block fields in sections Price and Extra if there are registrations
        frappe.call({
            method:"frappe.client.get_list",
            args:{
                doctype:"Scoring Template",
                filters: [
                ],
                fields: [
                    "name"
                ]
            },
            callback: function(r) {
                if (r.message.length > 0) {
                    frm.toggle_enable("randomize_cage_numbers", false)
                    frm.toggle_enable("consecutive_cage_numbers", false)
                };
            },
        });

        //Hide/Show price fields according to the selected price type
        if (frm.doc.independent_price=="Yes") {
            frm.toggle_display("specimen_price", false);
            frm.toggle_display("individual_price", true);
            frm.toggle_display("team_price", true);
            frm.toggle_reqd("specimen_price", false);
            frm.toggle_reqd("individual_price", true);
            frm.toggle_reqd("team_price", true);
            frm.set_value("specimen_price",[]);
        } else {
            frm.toggle_display("specimen_price", true);
            frm.toggle_display("individual_price", false);
            frm.toggle_display("team_price", false);
            frm.toggle_reqd("specimen_price", true);
            frm.toggle_reqd("individual_price", false);
            frm.toggle_reqd("team_price", false);
            frm.set_value("individual_price",[]);
            frm.set_value("team_price",[]);
        };

        //Assign association's Federation to Federation field in the tournament
        frm.add_fetch("association", "federation", "federation");
	},

	independent_price: function(frm){
        //Hide/Show price fields according to the selected price type
        if (frm.doc.independent_price=="Yes") {
            frm.toggle_display("specimen_price", false);
            frm.toggle_display("individual_price", true);
            frm.toggle_display("team_price", true);
            frm.toggle_reqd("specimen_price", false);
            frm.toggle_reqd("individual_price", true);
            frm.toggle_reqd("team_price", true);
            frm.set_value("specimen_price",[]);
        } else {
            frm.toggle_display("specimen_price", true);
            frm.toggle_display("individual_price", false);
            frm.toggle_display("team_price", false);
            frm.toggle_reqd("specimen_price", true);
            frm.toggle_reqd("individual_price", false);
            frm.toggle_reqd("team_price", false);
            frm.set_value("individual_price",[]);
            frm.set_value("team_price",[]);
        };
    },

    territory: function(frm) {
        frm.set_value("comunity", "");
        frm.set_value("country", "");

        if (frm.doc.territory) {
            frappe.call({
                method:"avium_tournament.utils.get_territory_parents",
                args: {
                    'territory': frm.doc.territory
                },
                callback: function(r) {
                    if(r.message[0]!=null){
                        frm.set_value("comunity", r.message[0]);
                    }
                    if(r.message[1]!=null){
                        frm.set_value("country", r.message[1]);
                    }
                }
            });
        }
    },

    onload_post_render: function(frm){
        $("[data-fieldname='group_table'] .btn.grid-add-row").click(function(){
            new_row = true
            old_family = null
            old_individual_code = null
            old_team_code = null
        });
        $("[data-fieldname='group_table'] .close.btn-open-row").click(function(){
            if (frm.cur_grid.doc.individual==1||frm.cur_grid.doc.team==1) {
                new_row = false
                old_family = frm.cur_grid.doc.family
                if (frm.cur_grid.doc.individual==1) {
                    old_individual_code = Number(frm.cur_grid.doc.individual_code.slice(-3))
                } else {
                    old_team_code = null
                }
                if (frm.cur_grid.doc.team==1) {
                    old_team_code = Number(frm.cur_grid.doc.team_code.slice(-3))
                } else {
                    old_individual_code = null
                }
            } else {
                new_row = true
                old_family = null
                old_individual_code = null
                old_team_code = null
            }
        });
    },

	generate_groups: function(frm){

        frappe.call({
            method:"frappe.client.get_list",
            args:{
                doctype:"Registration",
                filters: {
                },
                fields: [
                    "exhibitor_name"
                ]
            },
            callback: function(r) {
                if (r.message.length!=0) {
                    frappe.msgprint(__("Group generation cannot be performed because there are registration."));
                } else {
                    var families_list=get_families_code_description();
                    frappe.prompt([
                        {
                           "fieldname": "families",
                           "fieldtype": "Select",
                           "label": __("Generate groups from family"),
                           "options": families_list,
                           "reqd": 1,
                        }],
                        function(values){
                            frappe.call({
                                method: "generate_groups",
                                doc:frm.doc,
                                args: {
                                    families: values.families
                                },
                                callback: function(r) {
                                    switch(r.message) {
                                        case "OK":
                                            frappe.show_alert(__("Groups have been generated correctly"));
                                            break;
                                        case "NOT_FOUND":
                                            frappe.show_alert(__("Groups have been generated correctly"));
                                            break;
                                        default:
                                            frappe.show_alert(__("There was an error generating the groups"));
                                    }
                                    frm.refresh_field("group_table");
                                }
                            });
                        },
                        __('Generate Groups'),
                        __('Generate')
                    );
                }
            }
        })
	},

	reassign_group_codes: function(frm){
	    //Check if all rows has a defined family, judging template and description
	    var group_table_OK = true
        
	    $.each(frm.doc.group_table, function(i,d) {
            var row = d.idx;
            if(typeof d.family=='undefined'){
                frappe.msgprint(__("Family on the group in row ${row}, is not defined.").replace("${row}", row));
	            group_table_OK = false
            };
            if(d.judging_template==null){
                frappe.msgprint(__("Judging template on the group in row ${row}, is not defined.").replace("${row}", row));
	            group_table_OK = false
            };
            if(d.description==null){
                frappe.msgprint(__("Description on the group in row ${row}, is not defined.").replace("${row}", row));
	            group_table_OK = false
            };
        });
        if (group_table_OK == false) {
            return;
        }

        //Reassign codes
        frappe.call({
            method:"frappe.client.get_list",
            args:{
                doctype:"Registration",
                filters: {
                },
                fields: [
                    "exhibitor_name"
                ]
            },
            callback: function(r) {
                if (r.message.length!=0) {
                    frappe.msgprint(__("Reassign group codes cannot be performed because there are registration."));
                } else {
                    frappe.prompt([{
                           "fieldname": "pair_groups",
                           "fieldtype": "Check",
                           "label": __("Assign pair numbers to team groups"),
                           "options": ""
                        }],
                        function(values){
                            frappe.call({
                                method: "assign_group_codes",
                                doc:frm.doc,
                                args: {
                                    pair_groups: values.pair_groups
                                },
                                callback: function(r) {
                                    if(r.message=="OK"){
                                        frappe.show_alert(__("Group codes have been assigned correctly"));
                                    }
                                    else{
                                        frappe.show_alert(__("There was an error assigning group codes"));
                                    }
                                    frm.refresh_field("group_table")
                                    sort_group_table(frm)
                                }
                            })
                        },
                        __('Reassign Group Codes'),
                        __('Reassign'),
                    );
                }
            }
        })
    },

	judging_stickers: function(frm){

	    var groups_list = get_group_codes(frm);

	    frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "collapsible": 0,
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
               "label": __("Options"),
            },{
               "fieldname": "column",
               "fieldtype": "Int",
               "label": __("Column Start Position"),
               "default": "1",
               "reqd": 1,
            },{
               "fieldname": "direction",
               "fieldtype": "Select",
               "label": __("Print Direction"),
               "options": [
                   { "value": "Vertical", "label": __("Vertical") },
                   { "value": "Horizontal", "label": __("Horizontal") }
               ],
               "default": "Horizontal",
            },{
               "fieldname": "cbr1",
               "fieldtype": "Column Break",
            },{
               "fieldname": "row",
               "fieldtype": "Int",
               "label": __("Row Start Position"),
               "default": "1",
               "reqd": 1,
            },{
               "collapsible": 0,
               "fieldname": "sbr2",
               "fieldtype": "Section Break",
               "label": __("Filters"),
            },{
               "fieldname": "start_group",
               "fieldtype": "Select",
               "label": __("Starting Group"),
               "options": groups_list,
               "default": groups_list[0],
               "reqd": 1,
            },{
               "fieldname": "start_cage",
               "fieldtype": "Int",
               "label": __("Starting Cage"),
            },{
               "fieldname": "registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Registration")
            },{
               "fieldname": "individual",
               "fieldtype": "Select",
               "label": __("Individual/Team Groups"),
               "options": [
                   { "value": "All", "label": __("All") },
                   { "value": "Individual", "label": __("Individual") },
                   { "value": "Team", "label": __("Team") }
               ],
               "default": "Individual",
            },{
               "fieldname": "cbr2",
               "fieldtype": "Column Break",
            },{
               "fieldname": "end_group",
               "fieldtype": "Select",
               "label": __("Ending Group"),
               "options": groups_list,
               "default": groups_list[groups_list.length-1],
               "reqd": 1,
            },{
               "fieldname": "end_cage",
               "fieldtype": "Int",
               "label": __("Ending Cage"),
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_judging_stickers", values, __("Judging Stickers"))
            },
            __('Judging Stickers'),
            __('Print')
        );
	},

	marks_stickers: function(frm){

	    var groups_list = get_group_codes(frm);

	    frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "collapsible": 0,
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
               "label": __("Options"),
            },{
               "fieldname": "column",
               "fieldtype": "Int",
               "label": __("Column Start Position"),
               "default": "1",
               "reqd": 1,
            },{
               "fieldname": "direction",
               "fieldtype": "Select",
               "label": __("Print Direction"),
               "options": [
                   { "value": "Vertical", "label": __("Vertical") },
                   { "value": "Horizontal", "label": __("Horizontal") }
               ],
               "default": "Horizontal",
            },{
               "fieldname": "information",
               "fieldtype": "Select",
               "label": __("Exhibitor information"),
               "options": [
                   { "value": "Phone and Email", "label": __("Phone and Email") },
                   { "value": "City", "label": __("City") }
               ],
               "default": "Phone and Email",
            },{
               "fieldname": "cbr2",
               "fieldtype": "Column Break",
            },{
               "fieldname": "row",
               "fieldtype": "Int",
               "label": __("Row Start Position"),
               "default": "1",
               "reqd": 1,
            },{
               "fieldname": "prize",
               "fieldtype": "Select",
               "label": __("Prize Description"),
               "options": "1\n2\n3\n4",
               "default": "1",
            },{
               "collapsible": 0,
               "fieldname": "sbr2",
               "fieldtype": "Section Break",
               "label": __("Filters"),
            },{
               "fieldname": "start_group",
               "fieldtype": "Select",
               "label": __("Starting Group"),
               "options": groups_list,
               "default": groups_list[0],
               "reqd": 1,
            },{
               "fieldname": "starting_cage",
               "fieldtype": "Int",
               "label": __("Starting Cage"),
            },{
               "fieldname": "registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Registration")
            },{
               "fieldname": "individual",
               "fieldtype": "Select",
               "label": __("Individual/Team Groups"),
               "options": [
                   { "value": "All", "label": __("All") },
                   { "value": "Individual", "label": __("Individual") },
                   { "value": "Team", "label": __("Team") }
               ],
               "default": "Individual",
            },{
               "fieldname": "cbr1",
               "fieldtype": "Column Break",
            },{
               "fieldname": "end_group",
               "fieldtype": "Select",
               "label": __("Ending Group"),
               "options": groups_list,
               "default": groups_list[groups_list.length-1],
               "reqd": 1,
            },{
               "fieldname": "end_cage",
               "fieldtype": "Int",
               "label": __("Ending Cage"),
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_marks_stickers", values, __("Marks Stickers"))
            },
            __('Marks Stickers'),
            __('Print')
        );
	},

	exhibitor_stickers: function(frm){
	    frappe.prompt([
            {
               "collapsible": 0,
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
               "label": __("Options"),
            },{
               "fieldname": "column",
               "fieldtype": "Int",
               "label": __("Column Start Position"),
               "default": "1",
               "reqd": 1,
            },{
               "fieldname": "direction",
               "fieldtype": "Select",
               "label": __("Print Direction"),
               "options": [
                   { "value": "Vertical", "label": __("Vertical") },
                   { "value": "Horizontal", "label": __("Horizontal") }
               ],
               "default": "Horizontal",
            },{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Registration", "label": __("Registration") }
               ],
               "default": "Exhibitor",
            },{
               "fieldname": "cbr1",
               "fieldtype": "Column Break",
            },{
               "fieldname": "row",
               "fieldtype": "Int",
               "label": __("Row Start Position"),
               "default": "1",
               "reqd": 1,
            },{
               "fieldname": "registration",
               "fieldtype": "Select",
               "label": __("Show Regitsration Number"),
               "options": [
                   { "value": "Yes", "label": __("Yes") },
                   { "value": "No", "label": __("No") }
               ],
               "default": "Yes",
            },{
               "collapsible": 0,
               "fieldname": "sbr2",
               "fieldtype": "Section Break",
               "label": __("Filters"),
            },{
               "fieldname": "start_exhibitor",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Starting Exhibitor"),
               "depends_on": "eval:doc.sorted!='Registration'",
            },{
               "fieldname": "start_registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Starting Registration"),
               "depends_on": "eval:doc.sorted=='Registration'",
            },{
               "fieldname": "cbr2",
               "fieldtype": "Column Break",
            },{
               "fieldname": "end_exhibitor",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Ending Exhibitor"),
               "depends_on": "eval:doc.sorted!='Registration'",
            },{
               "fieldname": "end_registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Ending Registration"),
               "depends_on": "eval:doc.sorted=='Registration'",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_exhibitor_stickers", values, __("Exhibitor Stickers"))
            },
            __('Exhibitor Stickers'),
            __('Print')
        );
	},

    groups_list_report: function(frm){
	    avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_groups_list_report", {}, __("Groups List Report"))
	},

	exhibitors_list_report: function(frm){
        frappe.prompt([{
               "collapsible": 0,
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
               "label": __("Options"),
            },{
               "fieldname": "total",
               "fieldtype": "Select",
               "label": __("Print total"),
               "options": [
                   { "value": "Yes", "label": __("Yes") },
                   { "value": "No", "label": __("No") }
               ],
               "default": "Yes",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_exhibitors_list_report", values, __("Exhibitors List Report"))
            },
            __('Exhibitors List Report'),
            __('Print')
        );
	},

	registration_report: function(frm){
        var comunities = get_comunities()

        frappe.prompt([{
               "fieldname": "filter",
               "fieldtype": "Select",
               "label": __("Filter"),
               "options": [
                   { "value": "Family", "label": __("Family") },
                   { "value": "Group", "label": __("Group") },
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Association", "label": __("Association") },
                   { "value": "Federation", "label": __("Federation") },
                   { "value": "Territory", "label": __("Territory") },
                   { "value": "Comunity", "label": __("Comunity") }
               ],
               "default": "Family",
            },{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Name", "label": __("Name") },
                   { "value": "Registration", "label": __("Registration") },
                   { "value": "Payment", "label": __("Payment") },
                   { "value": "Comunity", "label": __("Comunity") }
               ],
               "default": "Name",
               "depends_on": "eval:doc.filter=='Exhibitor'"
            },{
                "fieldname": "comunity",
                "fieldtype": "Select",
                "label": __("Comunity"),
                "options": comunities,
                "default": "All",
                "depends_on": "eval:doc.sorted=='Comunity'"
             },{
               "fieldname": "no_total",
               "fieldtype": "Check",
               "label": __("Do not print total"),
               "options": ""
            },{
               "fieldname": "breakdown",
               "fieldtype": "Check",
               "label": __("Breakdown By Group"),
               "options": "",
               "depends_on": "eval:doc.filter!='Group'"
            },{
               "fieldname": "phone",
               "fieldtype": "Check",
               "label": __("Print contact information"),
               "options": "",
               "depends_on": "eval:doc.filter=='Exhibitor'"
            },{
               "fieldname": "pagebreak",
               "fieldtype": "Check",
               "label": __("One item per page"),
               "options": "",
               "depends_on": "eval:doc.filter!='Group'&doc.breakdown==1"
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_registration_report", values, __("Registration Report"))
            },
            __('Registration Report'),
            __('Print')
        );
	},

    location_status_report: function(frm){
        frappe.prompt([{
                "fieldname": "sbr0",
                "fieldtype": "Section Break",
                "label": __("Check the status to show in the report."),
            },{
                "fieldname": "not_presented",
                "fieldtype": "Check",
                "label": __("Not Presented"),
            },{
                "fieldname": "infirmary",
                "fieldtype": "Check",
                "label": __("Infirmary"),
                "default": true
            },{
                "fieldname": "cbr0",
                "fieldtype": "Column Break",
            },{
                "fieldname": "tournament",
                "fieldtype": "Check",
                "label": __("Tournament"),
            },{
                "fieldname": "deceased",
                "fieldtype": "Check",
                "label": __("Deceased"),
                "default": true
            },{
               "fieldname": "cbr1",
               "fieldtype": "Column Break",
            },{
                "fieldname": "flew",
                "fieldtype": "Check",
                "label": __("Flew"),
                "default": true
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_location_status_report", values, __("Location Status Report"))
            },
            __('Location Status Report'),
            __('Print')
       );
    },

    //REPORT NOT USED
	family_exhibitor_report: function(frm){

        var families_list=get_families_code_description();

        frappe.prompt([
            {
               "fieldname": "family",
               "fieldtype": "Select",
               "label": __("Print exhibitors from family"),
               "options": families_list,
               "reqd": 1,
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_family_exhibitor_report", values, __("Family Exhibitor Report"))
            },
            __('Group Exhibitor Report'),
            __('Print')
        );
	},

    registration_summaries: function(frm){
        frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "fieldname": "sbr0",
               "fieldtype": "Section Break",
            },{
               "fieldname": "registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Registration"),
               "description": __("Leave blank to print all registrations.")
            },{
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
            },{
                "fieldname": "cage",
                "fieldtype": "Check",
                "label": __("Print Cage Numbers"),
            },{
                "fieldname": "price",
                "fieldtype": "Check",
                "label": __("Do Not Print Price Summary"),
            },{
                "fieldname": "carrier",
                "fieldtype": "Check",
                "label": __("Print Carrier"),
            },{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Name", "label": __("Name") },
                   { "value": "Registration", "label": __("Registration") },
                   { "value": "Payment", "label": __("Payment") },
                   { "value": "Carrier", "label": __("Carrier") },
                   { "value": "Territory", "label": __("Territory") },
                   { "value": "Comunity", "label": __("Comunity") }
               ],
               "default": "Name"
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_registration_summaries", values, __("Registration Summaries"))
            },
            __('Registration Summaries'),
            __('Print')
       );
    },

	cages_report: function(frm){
        frappe.prompt([{
               "fieldname": "breakdown",
               "fieldtype": "Check",
               "label": __("Breakdown By Judging Template"),
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_cages_report", values, __("Cages Report"))
            },
            __('Cages Report'),
            __('Print')
        );
	},

    payments_report: function(frm){
        frappe.prompt([{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Registration", "label": __("Registration") }
               ],
               "default": "Exhibitor"
            },{
               "fieldname": "only_unpaid",
               "fieldtype": "Check",
               "label": __("Show only unpaid"),
               "options": ""
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_payments_report", values, __("Payments Report"))
            },
            __('Payments Report'),
            __('Print')
        );
	},

	judges_report: function(frm){
        frappe.prompt([{
               "fieldname": "breakdown",
               "fieldtype": "Check",
               "label": __("Breakdown By Group")
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_judges_report", values, __("Judges Report"))
            },
            __('Judges Report'),
            __('Print')
        );
	},

	carriers_report: function(frm){
        frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "fieldname": "judge",
               "fieldtype": "Link",
               "options": "Judge",
               "label": __("Judge"),
               "description": __("Leave blank to print all judges."),
            },{
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
            },{
               "fieldname": "results",
               "fieldtype": "Check",
               "label": __("Print Results")
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_carriers_report", values, __("Carriers Report"))
            },
            __('Carriers Report'),
            __('Print')
        );
	},

	judgement_report: function(frm){
        frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "fieldname": "judge",
               "fieldtype": "Link",
               "options": "Judge",
               "label": __("Judge"),
               "description": __("Leave blank to print all judges."),
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_judgement_report", values, __("Judgement Report"))
            },
            __('Judgement Report'),
            __('Print')
        );
	},

    scoring_templates: function(frm){

	    var groups_list = get_group_codes(frm);

        frappe.prompt([{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Group", "label": __("Group") },
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Judge", "label": __("Judge") },
                   { "value": "Registration", "label": __("Registration") }
               ],
               "default": "Exhibitor",
            },{
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
            },{
               "fieldname": "start_group",
               "fieldtype": "Select",
               "label": __("Starting Group"),
               "options": groups_list,
               "default": groups_list[0],
               "depends_on": "eval:doc.sorted=='Group'",
            },{
               "fieldname": "start_exhibitor",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Starting Exhibitor"),
               "depends_on": "eval:doc.sorted!='Group'&doc.sorted!='Judge'&doc.sorted!='Registration'",
            },{
               "fieldname": "start_judge",
               "fieldtype": "Link",
               "options": "Judge",
               "label": __("Starting Judge"),
               "depends_on": "eval:doc.sorted=='Judge'",
            },{
               "fieldname": "start_registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Starting Registration"),
               "depends_on": "eval:doc.sorted=='Registration'",
            },{
               "fieldname": "cbr1",
               "fieldtype": "Column Break",
            },{
               "fieldname": "end_group",
               "fieldtype": "Select",
               "label": __("Ending Group"),
               "options": groups_list,
               "default": groups_list[groups_list.length-1],
               "depends_on": "eval:doc.sorted=='Group'",
            },{
               "fieldname": "end_exhibitor",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Ending Exhibitor"),
               "depends_on": "eval:doc.sorted!='Group'&doc.sorted!='Judge'&doc.sorted!='Registration'",
            },{
               "fieldname": "end_judge",
               "fieldtype": "Link",
               "options": "Judge",
               "label": __("Ending Judge"),
               "depends_on": "eval:doc.sorted=='Judge'",
            },{
               "fieldname": "end_registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Ending Registration"),
               "depends_on": "eval:doc.sorted=='Registration'",
            },{
               "fieldname": "sbr2",
               "fieldtype": "Section Break",
            },{
               "fieldname": "empty",
               "fieldtype": "Check",
               "label": __("Print empty scoring template"),
               "default": 0
            },{
               "fieldname": "wo_exhibitor",
               "fieldtype": "Check",
               "label": __("Print without exhibitor information"),
               "depends_on": "eval:doc.empty==0",
               "default": 0
            },{
                "fieldname": "only_judged",
                "fieldtype": "Check",
                "label": __("Print Only Judged"),
                "depends_on": "eval:doc.empty==0",
                "default": 0
             },{
                "fieldname": "without_judge",
                "fieldtype": "Check",
                "label": __("Print Without Judge"),
                "depends_on": "eval:doc.empty==1",
                "default": 0
             },{
                "fieldname": "without_cage",
                "fieldtype": "Check",
                "label": __("Print Without Cage"),
                "depends_on": "eval:doc.empty==1",
                "default": 0
             },{
               "fieldname": "email",
               "fieldtype": "Check",
               "label": __("Print only exhibitors without email"),
               "default": 0
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_scoring_templates", values, __("Scoring Templates"))
            },
            __('Scoring Template'),
            __('Print')
        );
	},

	classification_report: function(frm){
        frappe.prompt([{
               "fieldname": "filter",
               "fieldtype": "Select",
               "label": __("Filtered By"),
               "options": [
                   { "value": "Group", "label": __("Group") },
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Association", "label": __("Association") }
               ],
               "default": "Group"
            },{
               "fieldname": "prize",
               "fieldtype": "Select",
               "label": __("Prize Description"),
               "options": "1\n2\n3",
               "default": "3",
            },{
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
            },{
               "fieldname": "print_individual",
               "fieldtype": "Check",
               "label": __("Individual Groups"),
               "default": "1",
            },{
               "fieldname": "cbr1",
               "fieldtype": "Column Break",
            },{
               "fieldname": "print_team",
               "fieldtype": "Check",
               "label": __("Team Groups"),
               "default": "1",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_classification_report", values, __("Classification Report"))
            },
            __('Classification Report'),
            __('Print')
        );
	},

	record_book_report: function(frm){
        frappe.prompt([{
               "fieldname": "filter",
               "fieldtype": "Select",
               "label": __("Filtered By"),
               "options": [
                   { "value": "Group", "label": __("Group") },
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Association", "label": __("Association") }
               ],
               "default": "Group"
            },{
               "fieldname": "prize",
               "fieldtype": "Select",
               "label": __("Prize Description"),
               "options": "1\n2\n3",
               "default": "3",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_record_book_report", values, __("Record Book Report"))
            },
            __('Record Book Report'),
            __('Print')
        );
	},

	league_punctuations_report: function(frm){
        frappe.prompt([{
               "fieldname": "filter",
               "fieldtype": "Select",
               "label": __("Filtered By"),
               "options": [
                   { "value": "Group", "label": __("Group") },
                   { "value": "Exhibitor", "label": __("Exhibitor") }
               ],
               "default": "Group"
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_league_punctuations_report", values, __("League Punctuations Report"))
            },
            __('League Punctuations Report'),
            __('Print')
        );
	},

	exhibitor_classification_best_punctuations_report: function(frm){

	    var groups_list = get_group_codes(frm);

        frappe.prompt([{
               "collapsible": 0,
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
               "label": __("Options"),
            },{
               "fieldname": "punctuations",
               "fieldtype": "Int",
               "label": __("Number of specimens"),
               "reqd": 1,
               "default": "10"
            },{
               "collapsible": 0,
               "fieldname": "sbr2",
               "fieldtype": "Section Break",
               "label": __("Filters"),
            },{
               "fieldname": "start_group",
               "fieldtype": "Select",
               "label": __("Starting Group"),
               "options": groups_list,
               "default": groups_list[0],
            },{
               "fieldname": "associated",
               "fieldtype": "Select",
               "label": __("Exhibitor Associated/Non Associated"),
               "options": [
                   { "value": "All", "label": __("All") },
                   { "value": "Associated", "label": __("Associated") },
                   { "value": "Non Associated", "label": __("Non Associated") }
               ],
               "default": "All",
            },{
               "fieldname": "individual",
               "fieldtype": "Select",
               "label": __("Individual Specimens/Team Specimens/Team Group"),
               "options": [
                   { "value": "All", "label": __("All") },
                   { "value": "Individual Specimens", "label": __("Individual Specimens") },
                   { "value": "Team Specimens", "label": __("Team Specimens") },
                   { "value": "Team Group", "label": __("Team Group") }
               ],
               "default": "All",
            },{
               "fieldname": "male",
               "fieldtype": "Select",
               "label": __("Specimens Male/Female"),
               "options": [
                   { "value": "All", "label": __("All") },
                   { "value": "Male", "label": __("Male") },
                   { "value": "Female", "label": __("Female") }
               ],
               "default": "All",
            },{
               "fieldname": "cbr1",
               "fieldtype": "Column Break",
            },{
               "fieldname": "end_group",
               "fieldtype": "Select",
               "label": __("Ending Group"),
               "options": groups_list,
               "default": groups_list[groups_list.length-1],
            }],
            function(values){
                if (values.puntuations<1) {
                    frappe.throw(__("Number of specimens must be greater than 0"));
                } else {
                    avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_exhibitor_classification_best_punctuations_report", values, __("Exhibitor Classification Best Punctuations Report"))
                }
            },
            __('Exhibitor Classification Best Punctuations Report'),
            __('Print')
        );
	},

	statistics_report: function(frm){
	    frappe.prompt([{
               "collapsible": 0,
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
               "label": __("Options"),
            },{
               "fieldname": "grouped",
               "fieldtype": "Select",
               "label": __("Grouped by"),
               "options": [
                   { "value": "Family", "label": __("Family") },
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Association", "label": __("Association") },
                   { "value": "Federation", "label": __("Federation") },
                   { "value": "Territory", "label": __("Territory") },
                   { "value": "Comunity", "label": __("Comunity") },
                   { "value": "Carrier", "label": __("Carrier") }
               ],
               "default": "Family",
            },{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Alphabetical order", "label": __("Alphabetical order") },
                   { "value": "Total prizes", "label": __("Total prizes") }
               ],
               "default": "Total prizes",
            },{
               "fieldname": "prize",
               "fieldtype": "Select",
               "label": __("Prize Description"),
               "options": "1\n2\n3",
               "default": "3",
            },{
               "fieldname": "print_statistics",
               "fieldtype": "Select",
               "label": __("Print Statistics"),
               "options": [
                   { "value": "Yes", "label": __("Yes") },
                   { "value": "No", "label": __("No") }
               ],
               "default": "Yes",
            },{
               "collapsible": 0,
               "fieldname": "sbr2",
               "fieldtype": "Section Break",
               "label": __("Filters"),
            },{
               "fieldname": "prized",
               "fieldtype": "Select",
               "label": __("All/Only Prized"),
               "options": [
                   { "value": "All", "label": __("All") },
                   { "value": "Only Prized", "label": __("Only Prized") }
               ],
               "default": "All",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_statistics_report", values, __("Statistics Report"))
            },
            __('Statistics Report'),
            __('Print')
        );
	},

	certificates: function(frm){
	    frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "fieldname": "registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Registration"),
               "description": __("Leave blank to print all certificates."),
            },{
               "fieldname": "prize",
               "fieldtype": "Select",
               "label": __("Prize Description"),
               "options": "1\n2\n3",
               "default": "1",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_certificates", values, __("Certificates"))
            },
            __('Certificates'),
            __('Print')
        );
	},

	delivery_report: function(frm){
        frappe.prompt([{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Exhibitor", "label": __("Exhibitor") },
                   { "value": "Registration", "label": __("Registration") }
               ],
               "default": "Exhibitor"
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_delivery_report", values, __("Delivery Report"))
            },
            __('Registration Summaries'),
            __('Print')
       );
    },

	box_report: function(frm){
	    frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "fieldname": "carrier",
               "fieldtype": "Link",
               "options": "Carrier",
               "label": __("Carrier"),
               "description": __("Leave blank to print all carriers."),
            },{
               "fieldname": "sorted",
               "fieldtype": "Select",
               "label": __("Sorted By"),
               "options": [
                   { "value": "Group", "label": __("Group") },
                   { "value": "Exhibitor", "label": __("Exhibitor") }
               ],
               "default": "Group",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_box_report", values, __("Box Report"))
            },
            __('Box Report'),
            __('Print')
        );
	},

	carrier_prizes_report: function(frm){
	    frappe.prompt([{
               //This field has been added to avoid showing the following field's list once the prompt appears
               "fieldname": "dummy",
               "fieldtype": "Int",
               "read_only": 1,
               "hidden": 1,
            },{
               "fieldname": "carrier",
               "fieldtype": "Link",
               "options": "Carrier",
               "label": __("Carrier"),
               "description": __("Leave blank to print all carriers."),
            },{
               "fieldname": "prize",
               "fieldtype": "Select",
               "label": __("Prize Description"),
               "options": "1\n2\n3",
               "default": "2",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_carrier_prizes_report", values, __("Carrier Prizes Report"))
            },
            __('Carrier Prizes Report'),
            __('Print')
        );
	},

	tax_report: function(frm){
	    frappe.prompt([{
               "collapsible": 0,
               "fieldname": "sbr1",
               "fieldtype": "Section Break",
               "label": __("Filters"),
            },{
               "fieldname": "registration",
               "fieldtype": "Link",
               "options": "Registration",
               "label": __("Registration"),
               "description": __("Leave blank to print all certificates."),
            },{
               "fieldname": "taxed",
               "fieldtype": "Select",
               "label": __("All/Only Taxed"),
               "options": [
                   { "value": "All", "label": __("All") },
                   { "value": "Only Taxed", "label": __("Only Taxed") }
               ],
               "default": "All",
            }],
            function(values){
                avium_tournament.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_tax_report", values, __("Tax Report"))
            },
            __('Tax Report'),
            __('Print')
        );
	},

    delete_all_groups: function(frm){
	    frappe.prompt([
            {
               "fieldname": "sure",
               "fieldtype": "Select",
               "label": __("Do you want to remove all groups?"),
               "options": [
                   { "value": "Yes", "label": __("Yes") },
                   { "value": "No", "label": __("No") }
               ],
               "default": "No",
            }],
            function(values){
                if(values.sure=='Yes'){
                    frappe.call({
                        method: "delete_all_groups",
                        doc:frm.doc,
                        callback: function(r) {
                            switch(r.message) {
                                case "ok":
                                    frappe.show_alert(__("Groups have been deleted correctly"));
                                    break;
                                default:
                                    frappe.show_alert(__("There was an error deleting the groups"));
                            }
                            frm.reload_doc();
                        }
                    })
                }
            },
            __('Are you sure?'),
            __('Send')
        );
	}
});

frappe.ui.form.on('Tournament Group', {
    family: function(frm){
        frm.cur_grid.doc.judging_template = null
        frm.cur_grid.doc.description = null
        set_group_codes(frm, new_row, old_family, old_individual_code, old_team_code)
        frm.cur_grid.refresh()
    },
    individual: function(frm){
        set_group_codes(frm, new_row, old_family, old_individual_code, old_team_code)
        frm.cur_grid.refresh()
    },
    team: function(frm){
        set_group_codes(frm, new_row, old_family, old_individual_code, old_team_code)
        frm.cur_grid.refresh()
    },
	"group_table_add": function(frm) {
        frm.add_fetch("family", "cage", "cage");
        frm.add_fetch("judging_template", "template_name", "description");
        frm.add_fetch("judging_template", "size", "size");
        frm.add_fetch("judging_template", "tufted", "tufted");
        frm.add_fetch("judging_template", "red_factor", "red_factor");
	},
	"before_group_table_remove": function(frm, dt, dn) {
        return false;
	}
});

var get_comunities = function(frm){
    var comunities_list=["All"];
    frappe.call({
        method:"avium_tournament.utils.get_comunities",
        args: {
        },
        callback: function(r) {
            if (r.message) {
                $.each(r.message, function(i,d) {
                    comunities_list.push(d);
                });
            };
        }
    });
    return comunities_list;
}

var set_group_codes = function(frm, new_row, old_family, old_individual_code, old_team_code){
    //Generate group codes
    //If there is no selected family or individual and team checks are not set, the codes are removed
    if (frm.cur_grid.doc.family==undefined||(frm.cur_grid.doc.individual==0&&frm.cur_grid.doc.team==0)) {
        frm.cur_grid.doc.individual_code = null
        frm.cur_grid.doc.team_code = null
    } else {
        //Variables inicialized
        individual_code = 0
        team_code = 0
        family_individual_in_table = 0
        family_team_in_table = 0
        team_pair = false
        //If the family selected is the same than when the row was opened, the old values are assigned
        //Old values are stored inside the frappe.ui.form.on('Tournament', {onload_post_render})
        if (new_row==false&&frm.cur_grid.doc.family==old_family) {
            if (old_individual_code==null) {
                individual_code = old_team_code - 1 + 2*old_team_code%2
            } else {
                individual_code = old_individual_code
            }
            if (old_team_code==null) {
                team_code = old_individual_code - 1 + 2*old_team_code%2
            } else {
                team_code = old_team_code
            }
            family_individual_in_table = 1
            family_team_in_table = 1
        //If the family selected is different than when the row was opened, new codes are generated searching for the higher code and adding 2
        } else {
            for (var i = 0; i < frm.doc.group_table.length; i++) {
                if (frm.doc.group_table[i].family==frm.cur_grid.doc.family && frm.doc.group_table[i].idx!=frm.cur_grid.doc.idx) {
                    if (frm.doc.group_table[i].individual==1 && Number(frm.doc.group_table[i].individual_code.slice(-3))+2>individual_code) {
                        individual_code = Number(frm.doc.group_table[i].individual_code.slice(-3)) + 2
                        family_individual_in_table = 1
                        if (individual_code%2==1) {
                            team_pair = true
                        } else {
                            team_pair = false
                        }
                    }
                    if (frm.doc.group_table[i].team==1 && Number(frm.doc.group_table[i].team_code.slice(-3))+2>team_code) {
                        team_code = Number(frm.doc.group_table[i].team_code.slice(-3)) + 2
                        family_team_in_table = 1
                        if (team_pair = team_code%2==0) {
                            team_pair = true
                        } else {
                            team_pair = false
                        }
                    }
                }
            }
        }
        //If the individual check is selected, the individual code generated before is assigned. If not, the individual code is removed.
        if (frm.cur_grid.doc.individual==1) {
            if (family_individual_in_table==0) {
                if (team_pair) {
                    frm.cur_grid.doc.individual_code = frm.cur_grid.doc.family + "-001"
                } else {
                    frm.cur_grid.doc.individual_code = frm.cur_grid.doc.family + "-002"
                }
            } else {
                frm.cur_grid.doc.individual_code = frm.cur_grid.doc.family + "-" + "000".slice(String(individual_code).length,3) + String(individual_code)
            }
        } else {
            frm.cur_grid.doc.individual_code = null
        }
        //If the team check is selected, the team code generated before is assigned. If not, the team code is removed.
        if (frm.cur_grid.doc.team==1) {
            if (family_team_in_table==0) {
                if (team_pair) {
                    frm.cur_grid.doc.team_code = frm.cur_grid.doc.family + "-002"
                } else {
                    frm.cur_grid.doc.team_code = frm.cur_grid.doc.family + "-001"
                }
            } else {
                frm.cur_grid.doc.team_code = frm.cur_grid.doc.family + "-" + "000".slice(String(team_code).length,3) + String(team_code)
            }
        } else {
            frm.cur_grid.doc.team_code = null
        }
    }
}

var sort_group_table = function(frm){
    //Collect all group codes
    groups = [];
    for (var i = 0; i < frm.doc.group_table.length; i++) {
        if (frm.doc.group_table[i].individual==1) {
            individual_number = Number(frm.doc.group_table[i].individual_code.slice(-3))
        } else {
            individual_number = 0
        }
        if (frm.doc.group_table[i].team==1) {
            team_number = Number(frm.doc.group_table[i].team_code.slice(-3))
        } else {
            team_number = 0
        }
        if (individual_number>team_number) {
            groups.push([frm.doc.group_table[i].individual_code, cur_frm.doc.group_table[i]]);
        } else {
            groups.push([frm.doc.group_table[i].team_code, cur_frm.doc.group_table[i]]);
        }
    }
    //Sort tournament groups by group codes
    groups.sort((function(index){
        return function(a, b){
            return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
        };
    })(0));
    //Reassign row index
    for (var i = 0; i < groups.length; i++) {
        groups[i][1].idx = i + 1;
    }
}

function get_group_codes(frm){
    var groups_list=[];
    $.each(frm.doc.group_table, function(item){
        var group = frm.doc.group_table[item];
        if(group.individual==1 && group.individual_code){
            groups_list.push(group.individual_code);
        }
        if(group.team==1 && group.team_code){
            groups_list.push(group.team_code);
        }
    });
    return groups_list.sort();
}

function get_families_code_description(){
    var families_list=[" ","All"];
    frappe.call({
        method:"frappe.client.get_list",
        args:{
            doctype:"Family",
            filters: [
            ],
            fields: [
                "code",
                "family_name"
            ]
        },
        async: false,
        callback: function(r) {
            if (r.message) {
                r.message.sort(function(a,b) {return (a.code > b.code) ? 1 : ((b.code > a.code) ? -1 : 0);} );
                $.each(r.message, function(i,d) {
                    families_list.push(d.code + " - " + d.family_name);
                });
            };
        },
    });
    return families_list;
}
