frappe.listview_settings['Tournament'] = {
    colwidths: {"indicator":1, "subject": 2},
    add_fields: ["tournament_status"],
	get_indicator: function(doc) {
	    if(doc.tournament_status==="Published") {
	        return [__(doc.tournament_status), "green", "tournament_status,=," + doc.status];
		}
		else {
        	return [__(doc.tournament_status), "grey", "tournament_status,=," + doc.status];
		}
	}
};