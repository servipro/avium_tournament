# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from avium_tournament import validation

class Transporter(Document):

    def validate(self):
        if self.telephone!=None and self.telephone!='' and not validation.valid_spanish_phone(self.contact_telephone):
            frappe.msgprint(frappe._("Telephone is not valid"))

        if self.email!=None and self.email!='' and not validation.valid_email(self.conatct_email):
            frappe.msgprint(frappe._("Email is not valid"))

        if self.mobile_phone!=None and self.mobile_phone!='' and not validation.valid_spanish_mobile(self.contact_mobile_phone):
            frappe.msgprint(frappe._("Mobile phone is not valid"))
