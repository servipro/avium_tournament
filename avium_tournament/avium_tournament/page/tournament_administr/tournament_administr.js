frappe.pages['tournament-administr'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Tournament Administration',
		single_column: true
	});

	$(frappe.render_template('tournament_administr')).appendTo(page.body);
	page.content = $(page.body).find('.filter-area');

    gen_button($("#generate_scoring_templates"),"Generate Scoring Templates", function(){
        console.log("hello")
    })
}

var gen_button = function(placement, label, callback){
    debugger;

    var button = $('<button class="btn btn-default btn-xs">'+ __(label) +'</button>')
		    .on("click", function() {
			    callback();
	        });

	placement.append(button)
}