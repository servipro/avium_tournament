frappe.pages['assign-judges'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('Assign Jugdes'),
		single_column: true
	});

	$(frappe.render_template("assign_judges")).appendTo(page.body);
	page.content = $(page.body).find('.filter-area');

    var judge1 = frappe.ui.form.make_control({
		parent: $(".judge1"),
		df: {
			fieldtype: "Link",
			options: "Judge",
			fieldname: "judge1",
			placeholder: __("Judge 1"),
            change: function(){
                set_assigned_judge(judge1.get_value())
            },
            get_query: function(){
                return {
                    "filters": {
                        "status": "Enabled"
                    }
                }
            }
		},
		render_input: true
	});

	frappe.pages['assign-judges'].judge1 = judge1;

	var judge2 = frappe.ui.form.make_control({
		parent: $(".judge2"),
		df: {
			fieldtype: "Link",
			options: "Judge",
			fieldname: "judge2",
			placeholder: __("Judge 2"),
			change: function(){
			},
            get_query: function(){
                return {
                    "filters": {
                        "status": "Enabled"
                    }
                }
            }
		},
        render_input: true
	});

	frappe.pages['assign-judges'].judge2 = judge2;

	$("#update").click(function(){
        var rows = assignmentGridOptions.api.getSelectedRows();
        var judge1 = frappe.pages['assign-judges'].judge1.get_value();
        var judge2 = frappe.pages['assign-judges'].judge2.get_value();

        if(rows.length==0){
            frappe.msgprint(__("Select at least one group"), __("Error"))
        }
        else {
            frappe.call({
                method: "avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.set_judges_group",
                args: {
                    groups: rows,
                    judge1: judge1,
                    judge2: judge2
                },
                callback: function (r) {
                    console.log(r)
                    frappe.msgprint(__("Judges Assigned"))
                    refresh_tables()
                }
            });
            frappe.call({
                method: "avium_tournament.avium_tournament.doctype.tournament.tournament.set_judges_group",
                args: {
                    groups: rows,
                    judge1: judge1
                },
                callback: function (r) {
                    console.log(r)
                }
            });
        }
	});

    $('#hide_assigned').change(function() {//do something when the user clicks the box
        refresh_tables();
    });

	init_tables();
}

frappe.pages['assign-judges'].on_page_show = function(wrapper) {
    refresh_tables();
    frappe.call({
        method:"avium_tournament.utils.get_ungenerated_scoring_templates",
        args: {
        },
        async: false,
        callback: function(r) {
            if(r.message>0){
                frappe.msgprint(__("There are some missing scoring templates, please generate them beforehand"), __("Error"))
            }
        }
    });
}

var set_assigned_judge = function(judge){
    frappe.call({
        method: 'avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.get_assigned_judging_groups',
        args:{
            judge1:judge
        },
        callback: function(r) {
            $.each(r.message, function (i) {
                console.log(r.message[i].group_code)
            });
        }
    });
}

var init_tables = function(){
    //Assignment Table
    var columnDefs = [
        {
            headerName: __("Group"), field: "group_code",
            headerCheckboxSelection: true,
            headerCheckboxSelectionFilteredOnly: true,
            checkboxSelection: true,
            width: 80,
            pinned: 'left'
        },
        {headerName: __("T."), field: "type",filter: "text", width: 50},
        {headerName: __("Group Description"), field: "group_description",filter: "text"},
        {headerName: __("N."), field: "specimens",filter: "text", width: 50},
        {
            headerName: __("Judge 1"),
            field: "judge_1",
            width: 120
        },
        {
            headerName: __("Judge 2"),
            field: "judge_2",
            width: 120,
        }
    ];
    var rowData = [];

    assignmentGridOptions = {
        columnDefs: columnDefs,
        rowData: rowData,
        onGridReady: function () {
            assignmentGridOptions.api.sizeColumnsToFit();
        },
        enableColResize: true,
        suppressRowClickSelection: true,
        rowSelection: 'multiple',
        enableFilter: true,
        onCellValueChanged: function(event) {
            console.log('onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue);
        },
        floatingFilter: true
    };


    var eGridDiv = document.querySelector('#assign_table');
    agGrid.LicenseManager.setLicenseKey("Other_MjQ3MzQ5NTYzNDAwMA==c2eb110b024f78f48d3f4e5cb896950f");
    new agGrid.Grid(eGridDiv, assignmentGridOptions);

    //Judges Table
    var judgesColumnDefs = [
        {headerName: "Judge", field: "judge",filter: "text"},
        {headerName: "N.", field: "specimens",filter: "text", width: 50}
    ];
    var judgesRowData = [];

    judgesGridOptions = {
        columnDefs: judgesColumnDefs,
        rowData: judgesRowData,
        onGridReady: function () {
            judgesGridOptions.api.sizeColumnsToFit();
        },
        enableColResize: true,
        suppressRowClickSelection: true,
        rowSelection: 'multiple',
        enableFilter: true,
        enableSorting: true
    };


    var judgeseGridDiv = document.querySelector('#judges_table');
    agGrid.LicenseManager.setLicenseKey("Other_MjQ3MzQ5NTYzNDAwMA==c2eb110b024f78f48d3f4e5cb896950f");
    new agGrid.Grid(judgeseGridDiv, judgesGridOptions);
}

function onQuickFilterChanged(text) {
    assignmentGridOptions.api.setQuickFilter(text);
}

function judgeOnQuickFilterChanged(text){
    judgesGridOptions.api.setQuickFilter(text);
}

var refresh_tables = function(){
    frappe.call({
        method: 'avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.get_judging_groups',
        args:{
            hide_assigned:$("#hide_assigned").is(":checked")
        },
        callback: function(r) {
            var groups_rows = r.message;
            assignmentGridOptions.api.setRowData(groups_rows);
            assignmentGridOptions.api.sizeColumnsToFit()
        }
    });
    frappe.call({
        method: 'avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.get_judging_assignments',
        callback: function(r) {
            var groups_rows = r.message;
            judgesGridOptions.api.setRowData(groups_rows);
            judgesGridOptions.api.sizeColumnsToFit()
        }
    });
}