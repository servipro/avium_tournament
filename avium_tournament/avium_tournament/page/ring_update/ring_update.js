frappe.pages['ring-update'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('Ring Update'),
		single_column: true
	});

	$(frappe.render_template("ring_update")).appendTo(page.body);
	page.content = $(page.body).find('.filter-area');

    var registration = frappe.ui.form.make_control({
		parent: $(".registration"),
		df: {
			fieldtype: "Link",
			options: "Registration",
			fieldname: "registration",
			placeholder: __("Registration"),
			label: __("Registration"),
            change: function(){
                refresh_tables();
                frappe.call({
                    method:"frappe.client.get_value",
                    args: {
                        doctype: "Registration",
                        filters: {
                            name:registration.get_value()
                        },
                        fieldname:["exhibitor_name", "exhibitor_surname", "carrier"]
                    },
                    callback: function(r) {
                        console.log(r.message)
                        if(jQuery.isEmptyObject(r)){
                            $("#reg_info").css("display", "none");
                            exhibitor_name.set_value("");
                            carrier.set_value("");
                        }
                        else{
                            $("#reg_info").css("display", "inline");
                            exhibitor_name.set_value(r.message.exhibitor_surname + ", " + r.message.exhibitor_name);
                            carrier.set_value(r.message.carrier);
                        }
                    }

                });
            }
		},
		render_input: true
	});
	frappe.pages['ring-update'].registration = registration;

    var exhibitor_name = frappe.ui.form.make_control({
		parent: $(".exhibitor_name"),
		df: {
			fieldtype: "Data",
			fieldname: "exhibitor_name",
			label: __("Exhibitor Name"),
			read_only: true
		},
		render_input: true
	});
	frappe.pages['ring-update'].exhibitor_name = exhibitor_name;

    var carrier = frappe.ui.form.make_control({
		parent: $(".carrier"),
		df: {
			fieldtype: "Link",
			options: "Carrier",
			fieldname: "carrier",
			placeholder: __("Carrier"),
			label: __("Carrier")
		},
		render_input: true
	});
	frappe.pages['ring-update'].carrier = carrier;

	$('#hide_assigned').change(function() {//do something when the user clicks the box
        refresh_tables();
    });

    $("#update_carrier").click(function(){
	    frappe.call({
            method:"avium_tournament.avium_tournament.doctype.registration.registration.update_carrier",
            args:{
                registration: registration.get_value(),
                carrier: carrier.get_value(),
            },
            callback: function(r) {
                frappe.msgprint(__("Carrier Updated"))
            }
        });
	});

    $("#print_reg").click(function(){
	    var w = window.open(
					frappe.urllib.get_full_url("/api/method/avium_tournament.pdf_generation.pdf_doc_function?"
                        +"doctype="+encodeURIComponent("Registration")
                        +"&name="+encodeURIComponent(registration.get_value())
                        +"&function="+encodeURIComponent("print_registration_summary")
                        +"&arguments="+encodeURIComponent(JSON.stringify({'price': 0, 'cage': 1, 'carrier': 1}))
                        +"&donwload_name="+encodeURIComponent(__("Registration")))
				);

		if(!w) {
			frappe.msgprint(__("Please enable pop-ups")); return;
		}
	});

    frappe.call({
        method:"avium_tournament.avium_tournament.doctype.tournament.tournament.tax_enabled",
        callback: function(r) {
            var tax_enabled = r.message.tax_enabled==1;
            init_tables(tax_enabled);
            refresh_tables();
        }
    });
};

frappe.pages['ring-update'].on_page_show = function(wrapper) {
    if (frappe.pages['ring-update'].grid) {
        refresh_tables();
        console.log("update table")
    }
    frappe.call({
        method:"avium_tournament.utils.get_ungenerated_scoring_templates",
        args: {
        },
        async: false,
        callback: function(r) {
            if(r.message>0){
                frappe.msgprint(__("There are some missing scoring templates, please generate them beforehand"), __("Error"))
            }
        }
    });
};

var set_assigned_judge = function(judge){
    frappe.call({
        method: 'avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.get_assigned_judging_groups',
        args:{
            judge1:judge
        },
        callback: function(r) {
            $.each(r.message, function (i) {
                console.log(r.message[i].group_code)
            });
        }
    });
};

var init_tables = function(tax_enabled){
    $('#ring_update').html("")
    //Assignment Table
    var columnDefs = [
        {
            headerName: __("Registration"),
            field: "exhibitor_name",
            cellRenderer: 'group',
            width: 180,
            pinned: 'left',
            suppressFilter: true
        },
        {
            headerName: __("Group"), field: "group_code",
            width: 45,
            pinned: 'left',
            cellRenderer: GroupCellRenderer
        },
        {headerName: __("Cage"), field: "cage_number",filter: "text", width: 50},
        {
            headerName: __("Ring A"),
            field: "ring_a",
            width: 50,
            editable: true
        },
        {
            headerName: __("Ring B"),
            field: "ring_b",
            width: 50,
            editable: true
        },
        {
            headerName: __("Ring C"),
            field: "ring_c",
            width: 50,
            editable: true
        },
        {
            headerName: __("Ring D"),
            field: "ring_d",
            width: 50,
            editable: true
        }
    ];

    if(tax_enabled){
        columnDefs.push(
            {
                headerName: __("Tax A"),
                field: "tax_a",
                width: 50,
                editable: true
            },
            {
                headerName: __("Tax B"),
                field: "tax_b",
                width: 50,
                editable: true
            },
            {
                headerName: __("Tax C"),
                field: "tax_c",
                width: 50,
                editable: true
            },
            {
                headerName: __("Tax D"),
                field: "tax_d",
                width: 50,
                editable: true
            }
        )
    }

    function GroupCellRenderer() {}

    GroupCellRenderer.prototype.init = function(params) {
        if(params.node.group){
            var tempDiv = document.createElement('div');
            this.eGui = tempDiv.firstChild;
        }
        else{
            var tempDiv = document.createElement('div');
            if (params.data.type=="I") {
                tempDiv.innerHTML = '<b style="color:blue">' + params.value + '</b>';
            } else {
                tempDiv.innerHTML = '<b style="color:green">' + params.value + '</b>';
            }
            this.eGui = tempDiv.firstChild;
        }
    };

    GroupCellRenderer.prototype.getGui = function() {
        return this.eGui;
    };

    var rowData = [];

    function getNodeChildDetails(rowItem) {
        if (rowItem.exhibitor_name) {
            return {
                group: true,
                expanded: true,
                children: rowItem.lines,
                field: 'lines',
                key: rowItem.exhibitor_name
            };
        } else {
            return null;
        }
    }

    
    function getContextMenuItems(params) {
        if(params.column.colId.startsWith("ring")){
            var cage = params.node.data;
            var selected = params.value || "";
            var node = params.node;
            var result = [
                {
                    // custom item
                    name: __('Mark as not presented') + " " + selected,
                    action: function() {
                        frappe.call({
                            method: 'avium_tournament.avium_tournament.doctype.registration.registration.update_ring',
                            args:{
                                line_name: cage.name,
                                type: cage.type,
                                attr: params.column.colId.replace("ring", "location_status"),
                                value: "Not Presented",
                                st_name: cage.st_name
                            },
                            callback: function(r) {
                                if(r.message=="ok"){
                                    node.setDataValue(params.column.colId,"NP")
                                    show_alert(__('Updated'), seconds=1)
                                }
                                else if(r.message=="not_group"){
                                    show_alert(__('This is an individual registration'))
                                }
                            }
                        })
                    },
                    cssClasses: ['redFont', 'bold']
                },
                {
                    // custom item
                    name: __('Mark as presented'),
                    action: function() {
                        frappe.call({
                            method: 'avium_tournament.avium_tournament.doctype.registration.registration.update_ring',
                            args:{
                                line_name: cage.name,
                                type: cage.type,
                                attr: params.column.colId.replace("ring", "location_status"),
                                value: "Tournament",
                                st_name: cage.st_name
                            },
                            callback: function(r) {
                                if(r.message=="ok"){
                                    node.setDataValue(params.column.colId,"")
                                    show_alert(__('Updated'), seconds=1)
                                }
                                else if(r.message=="not_group"){
                                    show_alert(__('This is an individual registration'))
                                }
                            }
                        })
                    },
                    cssClasses: ['redFont', 'bold']
                }
            ];
        }
        else{
            var result = []
        }
        return result;
    }

    
    function tabToNextCell(params) {
        var columns;
        if(tax_enabled){
            columns = ["ring_a", "ring_b", "ring_c", "ring_d", "tax_a", "tax_b", "tax_c", "tax_d"];
        }
        else{
            columns = ["ring_a", "ring_b", "ring_c", "ring_d"];
        }

        var api = frappe.pages['ring-update'].grid.api;
        var renderedRowCount = api.getModel().getRowCount();
        var current_cell = params.previousCellDef;
        var current_row = current_cell.rowIndex;
        var curr_col_index = columns.indexOf(current_cell.column.colId);

        if(curr_col_index<columns.length-1){
            var next_col=api.columnController.columnApi.getColumn(columns[curr_col_index+1]);
            var next_row=current_row;
        }
        else{
            var next_col=api.columnController.columnApi.getColumn(columns[0]);
            var next_row=current_cell.rowIndex+1;
            if(next_row<renderedRowCount-1 && api.getRowNode(current_row).group===true){
                next_row=next_row+1;
            }
        }

        if(next_row>=renderedRowCount){
            return {
                rowIndex: current_cell.rowIndex,
                column: current_cell.column
            }
        }
        else{
            var rowNode = api.getDisplayedRowAtIndex(next_row);
            var next_value = rowNode.data[next_col.colId];

            if(next_value==='-'){
                next_col=api.columnController.columnApi.getColumn(columns[0]);
                next_row=next_row+1;
                if(next_row<renderedRowCount-1 && api.getRowNode(current_row).group===true){
                    next_row=next_row+1;
                }
            }
        }

        if(next_row>=renderedRowCount){
            return {
                rowIndex: current_cell.rowIndex,
                column: current_cell.column
            }
        }

        var result = {
            rowIndex: next_row,
            column: next_col
        };

        return result;
    }


    assignmentGridOptions = {
        columnDefs: columnDefs,
        rowData: rowData,
        onGridReady: function () {
            assignmentGridOptions.api.sizeColumnsToFit();
        },
        enableColResize: true,
        suppressRowClickSelection: true,
        rowSelection: 'multiple',
        getNodeChildDetails: getNodeChildDetails,
        getContextMenuItems: getContextMenuItems,
        allowContextMenuWithControlKey: true,
        enableFilter: true,
        toolPanelSuppressSideButtons: true,
        onCellValueChanged: function(event) {
            if(event.newValue=="NP" || event.newValue==""){
                return
            }
            var reg = new RegExp('^\\d+$');
            if(!reg.test(event.newValue)){
                frappe.show_alert({message:__('Only numbers are allowed'), indicator:'red'})
                return
            }
            frappe.call({
                method: 'avium_tournament.avium_tournament.doctype.registration.registration.update_ring',
                args:{
                    line_name: event.data.name,
                    type: event.data.type,
                    attr: event.colDef.field,
                    value: event.newValue,
                    st_name: event.data.st_name
                },
                callback: function(r) {
                    if(r.message=="ok"){
                        frappe.show_alert({message:__('Group Updated'), indicator:'green', seconds:1})
                    }
                    else if(r.message=="not_group"){
                        frappe.show_alert({message:__('This is an individual registration, not saved'), indicator:'red'})
                    }
                    else if(r.message=="not_presented"){
                        frappe.show_alert({message:__('Mark this specimen as present to assign ring, not saved'), indicator:'red'});
                    }
                    else if(r.message=="not_change_judged"){
                        frappe.show_alert({message:__('Cannot set Not Presented if it\'s already judged'), indicator:'red'});
                    }
                }
            });
        },
        floatingFilter: false,
        stopEditingWhenGridLosesFocus: true,
        groupMultiAutoColumn: true,
        tabToNextCell: tabToNextCell
    };


    var eGridDiv = document.querySelector('#ring_update');
    agGrid.LicenseManager.setLicenseKey("Other_MjQ3MzQ5NTYzNDAwMA==c2eb110b024f78f48d3f4e5cb896950f");

    
    new agGrid.Grid(eGridDiv, assignmentGridOptions);

    frappe.pages['ring-update'].grid = assignmentGridOptions;
};

function onQuickFilterChanged(text) {
    frappe.pages['ring-update'].grid.api.setQuickFilter(text);
}

function judgeOnQuickFilterChanged(text){
    judgesGridOptions.api.setQuickFilter(text);
}

var refresh_tables = function(){
    frappe.call({
        method: 'avium_tournament.avium_tournament.doctype.registration.registration.get_registrations_and_rings',
        args:{
            hide_assigned:$("#hide_assigned").is(":checked"),
            registration: frappe.pages['ring-update'].registration.get_value()
        },
        callback: function(r) {
            var groups_rows = r.message;
            frappe.pages['ring-update'].grid.api.setRowData(groups_rows);
            frappe.pages['ring-update'].grid.api.sizeColumnsToFit()
        }
    });
};