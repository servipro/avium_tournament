frappe.pages['score-update'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('Score Update'),
		single_column: true
	});

	$(frappe.render_template("score_update")).appendTo(page.body);
	page.content = $(page.body).find('.filter-area');

	frappe.call({
        method:"avium_tournament.avium_tournament.doctype.tournament.tournament.get_groups",
        callback: function(r) {
			var selected_group = frappe.ui.form.make_control({
				parent: $(".selected_group"),
				df: {
					fieldtype: "Select",
					options: r.message,
					fieldname: "group",
					placeholder: __("Group"),
					label: __("Group"),
					change: function(){
						refresh_tables();
					}
				},
				render_input: true
			});
			frappe.pages['score-update'].selected_group = selected_group;
			init_tables();
			refresh_tables();
        }
	});
	
};

var set_assigned_judge = function(judge){
    frappe.call({
        method: 'avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.get_assigned_judging_groups',
        args:{
            judge1:judge
        },
        callback: function(r) {
            $.each(r.message, function (i) {
                console.log(r.message[i].group_code)
            });
        }
    });
};

var init_tables = function(){
    $('#score_update').html("")
    //Assignment Table
    var columnDefs = [
        {
			headerName: __("Group"),
			field: "group_code",
            width: 45,
			pinned: 'left',
			cellRenderer: GroupCellRenderer
        },
		{	
			headerName: __("Cage"), 
			field: "cage_number",
			filter: "text", 
			width: 50
		},
		{	
			headerName: __("Manual Scoring"), 
			field: "manual_scoring",
			filter: "text",
			editable: false,
			cellRenderer: params => {
				return `<input type='checkbox' ${params.value==1 ? 'checked' : ''} />`;
			},
			width: 50
		},
        {
            headerName: __("Score A"),
            field: "manual_score_a",
            width: 50,
            editable: true
        },
        {
            headerName: __("Score B"),
            field: "manual_score_b",
            width: 50,
            editable: true
        },
        {
            headerName: __("Score C"),
            field: "manual_score_c",
            width: 50,
            editable: true
        },
        {
            headerName: __("Score D"),
            field: "manual_score_d",
            width: 50,
            editable: true
		},
        {
            headerName: __("Harmony"),
            field: "manual_harmony",
            width: 50,
            editable: true
		}
    ];

    function GroupCellRenderer() {}

	GroupCellRenderer.prototype.init = function(params) {
			debugger;
            var tempDiv = document.createElement('div');
            if (params.data.type=="I") {
                tempDiv.innerHTML = '<b style="color:blue">' + params.value + '</b>';
            } else {
                tempDiv.innerHTML = '<b style="color:green">' + params.value + '</b>';
            }
            this.eGui = tempDiv.firstChild;
    };

	GroupCellRenderer.prototype.getGui = function() {
        return this.eGui;
	};
	
	var rowData = [];

    function tabToNextCell(params) {
        var columns = ["manual_score_a", "manual_score_b", "manual_score_c", "manual_score_d", "manual_harmony"];

        var api = frappe.pages['score-update'].grid.api;
        var renderedRowCount = api.getModel().getRowCount();
        var current_cell = params.previousCellDef;
        var current_row = current_cell.rowIndex;
        var curr_col_index = columns.indexOf(current_cell.column.colId);

        if(curr_col_index<columns.length-1){
            var next_col=api.columnController.columnApi.getColumn(columns[curr_col_index+1]);
            var next_row=current_row;
        }
        else{
            var next_col=api.columnController.columnApi.getColumn(columns[0]);
            var next_row=current_cell.rowIndex+1;
            if(next_row<renderedRowCount-1 && api.getRowNode(current_row).group===true){
                next_row=next_row+1;
            }
        }

        if(next_row>=renderedRowCount){
            return {
                rowIndex: current_cell.rowIndex,
                column: current_cell.column
            }
        }
        else{
            var rowNode = api.getDisplayedRowAtIndex(next_row);
            var next_value = rowNode.data[next_col.colId];

            if(next_value==='-'){
                next_col=api.columnController.columnApi.getColumn(columns[0]);
                next_row=next_row+1;
                if(next_row<renderedRowCount-1 && api.getRowNode(current_row).group===true){
                    next_row=next_row+1;
                }
            }
        }

        if(next_row>=renderedRowCount){
            return {
                rowIndex: current_cell.rowIndex,
                column: current_cell.column
            }
        }

        var result = {
            rowIndex: next_row,
            column: next_col
        };

        return result;
    }


    assignmentGridOptions = {
        columnDefs: columnDefs,
        rowData: rowData,
        onGridReady: function () {
            assignmentGridOptions.api.sizeColumnsToFit();
        },
        enableColResize: true,
        suppressRowClickSelection: true,
        rowSelection: 'multiple',
        allowContextMenuWithControlKey: true,
        enableFilter: true,
        toolPanelSuppressSideButtons: true,
        onCellValueChanged: function(event) {
            var reg = new RegExp('^\\d+$');
            if(!reg.test(event.newValue)){
                frappe.show_alert({message:__('Only numbers are allowed'), indicator:'red'})
                return
            }
            frappe.call({
                method: 'avium_tournament.avium_tournament.doctype.scoring_template.scoring_template.set_manual_points',
                args:{
                    name: event.data.name,
                    field: event.colDef.field,
                    value: event.newValue,
                },
                callback: function(r) {
                    if(r.message=="ok"){
                        frappe.show_alert({message:__('Scoring Template Updated'), indicator:'green', seconds:1})
                    }
                    else if(r.message=="not_group"){
                        frappe.show_alert({message:__('This is an individual registration, not saved'), indicator:'red'})
                    }
                    else if(r.message=="not_presented"){
                        frappe.show_alert({message:__('Mark this specimen as present to assign ring, not saved'), indicator:'red'});
                    }
                    else if(r.message=="not_change_judged"){
                        frappe.show_alert({message:__('Cannot set Not Presented if it\'s already judged'), indicator:'red'});
                    }
                }
            });
        },
        floatingFilter: false,
        stopEditingWhenGridLosesFocus: true,
        groupMultiAutoColumn: true,
        tabToNextCell: tabToNextCell
    };


    var eGridDiv = document.querySelector('#score_update');
    agGrid.LicenseManager.setLicenseKey("Other_MjQ3MzQ5NTYzNDAwMA==c2eb110b024f78f48d3f4e5cb896950f");

    
    new agGrid.Grid(eGridDiv, assignmentGridOptions);

    frappe.pages['score-update'].grid = assignmentGridOptions;
};

function onQuickFilterChanged(text) {
    frappe.pages['score-update'].grid.api.setQuickFilter(text);
}

function judgeOnQuickFilterChanged(text){
    judgesGridOptions.api.setQuickFilter(text);
}

var refresh_tables = function(){
	debugger;
    frappe.call({
        method: 'avium_tournament.avium_tournament.doctype.tournament.tournament.get_scoring_templates_updater',
        args:{
			selected_group: frappe.pages['score-update'].selected_group.get_value()
		},
        callback: function(r) {
			debugger;
            var scoring_templates = r.message;
            frappe.pages['score-update'].grid.api.setRowData(scoring_templates);
            frappe.pages['score-update'].grid.api.sizeColumnsToFit()
        }
    });
};