# Copyright (c) 2013, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from avium_tournament.reports.ring_inspection import RingInspectionReport

def execute(filters=None):

    filter_all = """
                AND st.classification!=0 
                AND IF(st.team_size = 1, tgroup.individual_ring_check, tgroup.team_ring_check)=0 
                AND (SELECT count(*) from `tabScoring Template` as st_all where st_all.group_code = st.group_code and status!="Judged")=0
    """
    if "show_all" in filters and filters["show_all"]==1:
        filter_all=""

    data = frappe.db.sql("""
            SELECT st.group_code, st.classification, prizedes.description_1, st.cage_number, st.ring_a, st.ring_b, st.ring_c, st.ring_d
            FROM `tabScoring Template` as st
            LEFT JOIN `tabTournament Prizes` as prize on st.prize = prize.name
            LEFT JOIN `tabTournament Prizes Description` as prizedes on prize.prize_description = prizedes.name
            LEFT JOIN `tabTournament Group` as tgroup on st.group = tgroup.name
            WHERE st.classification <= """+filters["specimens"] + " " + filter_all +"""  
            ORDER BY group_code ASC, classification ASC
    """)

    columns = [
        _("Group") + ":Link/Registration:75",
        _("Clas.") + ":Int:50",
        _("Prize") + ":Data:75",
        _("Cage") + ":Data:75",
        _("Ring A") + ":Data:120",
        _("Ring B") + ":Data:120",
        _("Ring C") + ":Data:120",
        _("Ring D") + ":Data:120"
    ]

    return columns, data

@frappe.whitelist()
def get_finished_list():
    return frappe.db.sql("""
            SELECT DISTINCT group_code from `tabScoring Template`
            ORDER BY group_code
    """)

@frappe.whitelist()
def mark_all_and_print(specimens, mark_as_checked):
    filters = {}
    filters["specimens"] = specimens
    filters["show_all"] = 0

    columns, data = execute(filters)

    groups = []

    for item in data:
        if item[0] not in groups:
            groups.append(item[0])
    
    if mark_as_checked=="1":
        for group in groups:
            mark_checked(group)
        frappe.db.commit()
    
    with RingInspectionReport(data) as report:
        donwload_name = _("Ring Inspection")
        frappe.local.response.filename = u"{name}.pdf".format(name=donwload_name.replace(" ", "-").replace("/", "-"))
        frappe.local.response.filecontent = report.get_pdf()
        frappe.local.response.type = "download"
        return

@frappe.whitelist()
def mark_checked(group):
    sts = frappe.get_all("Scoring Template", filters=[["group_code", "=", group], ["status", "!=", "Judged"]])
    if len(sts)>0:
        return "not_posible"
    ind_group = frappe.get_all("Tournament Group", filters=[["individual_code", "=", group]])
    if len(ind_group)>0:
        frappe.db.set_value("Tournament Group", ind_group[0].name, "individual_ring_check", 1)

    team_group = frappe.get_all("Tournament Group", filters=[["team_code", "=", group]])
    if len(team_group)>0:
        frappe.db.set_value("Tournament Group", team_group[0].name, "team_ring_check", 1)

    return "ok"
