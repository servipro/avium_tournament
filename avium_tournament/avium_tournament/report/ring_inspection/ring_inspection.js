// Copyright (c) 2016, SERVIPRO SL and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Ring Inspection"] = {
	"filters": [
		{
			"fieldname":"show_all",
			"label": __("Show All"),
			"fieldtype": "Check"
		},
        {
			"fieldname":"specimens",
			"label": __("Number of Specimens"),
			"fieldtype": "Select",
			"options": "1\n2\n3\n4\n5\n6\n7\n8\n9\n10",
			"default": "5"
		}
	],
	onload: function(report) {
        // dropdown for links to other financial statements
        report.page.add_inner_button(__("Mark Group as Checked"), function () {
			frappe.call({
				method:"avium_tournament.avium_tournament.report.ring_inspection.ring_inspection.get_finished_list",
				callback: function(r) {
					if (r.message) {
						group_list = " ";
						$.each(r.message, function(i,d) {
							group_list = group_list + "\n" + d[0]
						});

						frappe.prompt([
							{
							   "fieldname": "group",
							   "fieldtype": "Select",
							   "label": __("Group"),
							   "options": group_list,
							   "reqd": 1
							}],
							function(values){
								frappe.call({
                                    method: "avium_tournament.avium_tournament.report.ring_inspection.ring_inspection.mark_checked",
									args:{
                                    	"group": values.group
									},
                                    callback: function (r) {
										if(r.message=="ok"){
											frappe.msgprint(__("Updated"))
										}
										else if(r.message=="not_posible"){
											frappe.msgprint(__("Some Scoring Templates are still pending"))
										}
                                    }
                                })
							},
							__('Mark Group as Checked'),
							__('Save')
						);
					}
				},
			});
		});
		
		report.page.add_inner_button(__("Mark All as Checked and Print"), function () {
			frappe.prompt([
					{
						"fieldname":"specimens",
						"label": __("Number of Specimens"),
						"fieldtype": "Select",
						"options": "1\n2\n3\n4\n5\n6\n7\n8\n9\n10",
						"default": "5",
						"reqd": 1
					},
					{
						"fieldname": "mark_as_checked",
						"fieldtype": "Check",
						"label": __("Mark as checked")
					}
				],
				function(values){
					var w = window.open(
						frappe.urllib.get_full_url("/api/method/avium_tournament.avium_tournament.report.ring_inspection.ring_inspection.mark_all_and_print?"
							+"specimens="+encodeURIComponent(values.specimens)
							+"&mark_as_checked="+encodeURIComponent(values.mark_as_checked)
						)
					);
		
					if(!w) {
						msgprint(__("Please enable pop-ups")); return;
					}
				},
				__('Mark Group as Checked'),
				__('Save')
			);
        });
    }
};
