# Copyright (c) 2013, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	results = frappe.db.sql("""
			SELECT st.group_code, st.cage_number, reg.specimen_food
			FROM `tabScoring Template` as st
			LEFT JOIN `tabIndividual Registration` as ireg on ireg.name = st.registration
			LEFT JOIN `tabTeam Registration` as treg on treg.name = st.registration
			LEFT JOIN `tabRegistration` as reg on reg.name = IF(ireg.name IS NOT NULL, ireg.parent, treg.parent)
			LEFT JOIN `tabTournament Group` as tgroup on st.group = tgroup.name
			ORDER BY group_code ASC, cage_number ASC
	""")

	data = []
	for item in results:
		data.append((item[0],item[1],_(item[2])))

	columns = [
		_("Group") + ":Data:75",
		_("Cage") + ":Data:75",
		_("Food") + ":Data:175"
	]

	return columns, data
