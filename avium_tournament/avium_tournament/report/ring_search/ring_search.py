# Copyright (c) 2013, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	filter = ""
	# if len(filters)>0:
	# 	filter = "WHERE "
    #
	# if "exhibitor" in filters:
	# 	filter+= ""

	data = frappe.db.sql("""
			SELECT st.group_code, st.cage_number, reg.exhibitor_name, st.ring_a, st.ring_b, st.ring_c, st.ring_d, reg.name, st.name
			FROM `tabScoring Template` as st
			LEFT JOIN `tabIndividual Registration` as ireg on ireg.name = st.registration
			LEFT JOIN `tabTeam Registration` as treg on treg.name = st.registration
			LEFT JOIN `tabRegistration` as reg on reg.name = IF(ireg.name IS NOT NULL, ireg.parent, treg.parent)
			ORDER BY st.group_code ASC, st.cage_number ASC
		""" + filter)

	columns = [
		_("Group") + ":Data:75",
		_("Cage") + ":Data:75",
		_("Exhibitor") + ":Data:200",
		_("Ring A") + ":Data:100",
		_("Ring B") + ":Data:100",
		_("Ring C") + ":Data:100",
		_("Ring D") + ":Data:100",
		_("Registration") + ":Link/Registration:75",
		_("Scoring Template") + ":Link/Scoring Template:75"
	]

	return columns, data
