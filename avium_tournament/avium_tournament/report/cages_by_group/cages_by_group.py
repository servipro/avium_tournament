# Copyright (c) 2013, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):

	data = frappe.db.sql("""
            SELECT  tgro.individual_code as group_code,
                    tgro.description as group_description,
                    (
                    	SELECT MIN(cage_number)
                    	FROM `tabScoring Template`
                    	WHERE `tabScoring Template`.group_code = tgro.individual_code AND `tabScoring Template`.cage_number IS NOT NULL
                    ) as min_cage,
                    (
                    	SELECT MAX(cage_number)
                    	FROM `tabScoring Template`
                    	WHERE `tabScoring Template`.group_code = tgro.individual_code AND `tabScoring Template`.cage_number IS NOT NULL
                    ) as max_cage,
                    (
                    	SELECT COUNT(cage_number)
                    	FROM `tabScoring Template`
                    	WHERE `tabScoring Template`.group_code = tgro.individual_code AND `tabScoring Template`.cage_number IS NOT NULL
                    ) as total_cages
            FROM `tabTournament Group` as tgro
            LEFT JOIN `tabIndividual Registration` as ireg on ireg.group = tgro.name
            WHERE
                tgro.individual_code IS NOT NULL
            UNION
            SELECT  tgro.team_code as group_code,
                    tgro.description as group_description,
                    (
                    	SELECT MIN(cage_number)
                    	FROM `tabScoring Template`
                    	WHERE `tabScoring Template`.group_code = tgro.team_code AND `tabScoring Template`.cage_number IS NOT NULL
                    ) as min_cage,
                    (
                    	SELECT MAX(cage_number)
                    	FROM `tabScoring Template`
                    	WHERE `tabScoring Template`.group_code = tgro.team_code AND `tabScoring Template`.cage_number IS NOT NULL
                    ) as max_cage,
                    (
                    	SELECT COUNT(cage_number)
                    	FROM `tabScoring Template`
                    	WHERE `tabScoring Template`.group_code = tgro.team_code AND `tabScoring Template`.cage_number IS NOT NULL
                    ) as total_cages
            FROM `tabTournament Group` as tgro
            LEFT JOIN `tabTeam Registration` as treg on treg.group = tgro.name
            WHERE
                tgro.team_code IS NOT NULL
            ORDER BY
                group_code ASC
		   """)

	columns = [
		_("Group Code") + ":Data:70",
		_("Group Description") + ":Data:600",
		_("Lower Cage") + ":Data:120",
		_("Higher Cage") + ":Data:120",
		_("Total Cages") + ":Data:120"
	]

	return columns, data
