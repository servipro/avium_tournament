# Copyright (c) 2013, SERVIPRO SL and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	data = frappe.db.sql("""
			SELECT reg.name, CONCAT(reg.exhibitor_surname, ', ', reg.exhibitor_name), reg.telephone, reg.mobile_phone, reg.sing_judgement_date,
			(SELECT sum(team_size)
			   FROM `tabTeam Registration` as tr 
			   WHERE tr.parent = reg.name AND (tr.group_code LIKE 'A%' or tr.group_code LIKE 'B%' or tr.group_code LIKE  'C%' or tr.group_code LIKE  'K%'or tr.group_code LIKE  'M%')) as team_specimens,
			(SELECT GROUP_CONCAT(DISTINCT tr2.group_code)
			   FROM `tabTeam Registration` as tr2 
			   WHERE tr2.parent = reg.name AND (tr2.group_code LIKE 'A%' or tr2.group_code LIKE 'B%' or tr2.group_code LIKE  'C%' or tr2.group_code LIKE  'K%'or tr2.group_code LIKE  'M%')) as team_specimens_codes,
		    (SELECT count(*)
			   FROM `tabIndividual Registration` as tr 
			   WHERE tr.parent = reg.name AND (tr.group_code LIKE 'A%' or tr.group_code LIKE 'B%' or tr.group_code LIKE  'C%' or tr.group_code LIKE  'K%'or tr.group_code LIKE  'M%')) as ind_specimens,
			(SELECT GROUP_CONCAT(DISTINCT tr2.group_code)
			   FROM `tabIndividual Registration` as tr2 
			   WHERE tr2.parent = reg.name AND (tr2.group_code LIKE 'A%' or tr2.group_code LIKE 'B%' or tr2.group_code LIKE  'C%' or tr2.group_code LIKE  'K%'or tr2.group_code LIKE  'M%')) as ind_specimens_codes
			FROM tabRegistration as reg
			WHERE ((SELECT count(*)
		    FROM `tabIndividual Registration` as tr 
			WHERE tr.parent = reg.name AND (tr.group_code LIKE 'A%' or tr.group_code LIKE 'B%' or tr.group_code LIKE  'C%' or tr.group_code LIKE  'K%'or tr.group_code LIKE  'M%')) + (SELECT count(*)
			FROM `tabTeam Registration` as tr 
			WHERE tr.parent = reg.name AND (tr.group_code LIKE 'A%' or tr.group_code LIKE 'B%' or tr.group_code LIKE  'C%' or tr.group_code LIKE  'K%'or tr.group_code LIKE  'M%'))) > 0
		   """)

	columns = [
		_("Registration") + ":Link/Registration:120",
		_("Exhibitor") + ":Data:200",
		_("Telephone") + ":Data:100",
		_("Mobile Phone") + ":Data:120",
		_("Sing Judgement Date") + ":Data:150",
		_("Team Specimens") + ":Int:150",
		_("Team Codes") + ":Data:100",
		_("Individual Speciments") + ":Int:150",
		_("Individual Codes") + ":Data:100"
	]

	return columns, data
