import pprint
from random import randint

import frappe
from avium_tournament.utils import untie_group
import click
from frappe.commands import get_site, pass_context
from frappe.utils.password import update_password
from openpyxl import load_workbook


@click.command('import_judges')
@click.argument('path', type=str)
@pass_context
def import_judges(context, path):
    site = get_site(context)
    frappe.connect(site=site)

    file = load_workbook(filename=path, read_only=True)
    file = file.active

    no_email_number = 1

    for row in file.iter_rows():
        if row[0].value!=None and row[0].value!="":
            judge_name = row[0].value
            if not frappe.db.exists("Judge", judge_name):

                new_judge = frappe.new_doc("Judge")
                new_judge.judge_name = judge_name
                new_judge.email = "juez"+str(no_email_number)+"@avium.eu"
                new_judge.status = "Enabled"
                new_judge.password = str(randint(1000, 9999))
                new_judge.db_insert()

                user = frappe.new_doc("User")
                user.email = "juez"+str(no_email_number)+"@avium.eu"
                user.username = "juez"+str(no_email_number)
                user.first_name = new_judge.judge_name
                user.judge = new_judge.name
                user.language = "es"
                user.enabled = 1
                user.flags.no_welcome_mail = True
                user.insert()
                frappe.db.commit()

                user.add_roles("AVIUM Judge")
                frappe.db.commit()

                update_password(user.name, new_judge.password)
                frappe.db.commit()

                no_email_number+=1

    frappe.db.commit()
    frappe.destroy()

@click.command('untie_prized_templates')
@pass_context
@click.option('--commit', default=False, is_flag=True)
def untie_prized_templates(context, commit=False):
    site = get_site(context)
    frappe.connect(site=site)

    # untie_group("D-426")
    groups = frappe.db.sql("SELECT DISTINCT group_code from `tabScoring Template`", as_list=1)
    for group in groups:
        if int(group[0].split("-")[1])%2 == 0:
            untie_group(group[0], commit)


    frappe.db.commit()
    frappe.destroy()

@click.command('regenerate_concepts')
@click.argument('group', type=str)
@pass_context
def regenerate_concepts(context, group):
    site = get_site(context)
    frappe.connect(site=site)

    print("Updating group " + group)
    scoring_templates = frappe.get_list("Scoring Template", filters={"group_code": group})
    for st in scoring_templates:
        sco_template = frappe.get_doc("Scoring Template", st)
        print("Updating scoring template " + sco_template.cage_number)
        frappe.db.sql("DELETE from `tabScoring Template Concept` where parent=%s", sco_template.name)
        sco_template.concepts = []
        sco_template.gen_concepts()
        sco_template.set_parent_in_children()
        for d in sco_template.get_all_children():
            d.db_insert()

    frappe.db.commit()
    frappe.destroy()

@click.command('reload_concept_points')
@click.argument('name', type=str)
@pass_context
def reload_concept_points(context, name):
    site = get_site(context)
    frappe.connect(site=site)

    template = frappe.get_doc("Judging Template", name)

    for concept in template.concepts:
        frappe.db.sql("""
            UPDATE `tabScoring Template Concept`
            SET default_value=%s, min_value=%s, max_value=%s, negative=%s
            WHERE concept=%s
        """, [concept.default_value, concept.min_value, concept.max_value, concept.negative, concept.name])

    frappe.db.commit()
    frappe.destroy()

def update_territory_name(context):
    site = get_site(context)
    frappe.connect(site=site)

    for registration in frappe.get_all("Registration", fields=["name", "territory"]):
        if registration["territory"]:
            registration_doc = frappe.get_doc("Registration", registration["name"])
            registration_doc.territory = registration_doc.territory.title()
            registration_doc.db_update()

    frappe.db.commit()
    frappe.destroy()

commands = [import_judges, reload_concept_points, untie_prized_templates, regenerate_concepts]