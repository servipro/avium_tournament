## Avium Tournament

AVIUM Tournament App


## Upstream changes

https://github.com/frappe/frappe/compare/version-11...paurosello:avium-11
https://github.com/frappe/frappe/compare/version-13...paurosello:avium-13


## Devcontainer

#### Bench
```
docker build -t servipro/bench ./build/bench
```

#### Init Bench
```
bench init --skip-redis-config-generation --frappe-branch avium-13 --frappe-path https://github.com/paurosello/frappe.git frappe-bench
````

#### Set vars
```
bench set-mariadb-host mariadb
bench set-redis-cache-host redis-cache:6379
bench set-redis-queue-host redis-queue:6379
bench set-redis-socketio-host redis-socketio:6379
```

#### New Site
```
bench new-site avium.localhost --mariadb-root-password 123 --admin-password admin --no-mariadb-socket
```
#### Get application
```
bench get-app --branch develop avium_tournament git@gitlab.com:servipro/avium_tournament.git
bench --site avium.localhost install-app avium_tournament
```

#### Set developer mode
```
bench set-config developer_mode 1
bench clear-cache
```

### Start server
Start server: `bench start`

Start server in debug: 
```
honcho start \
    socketio \
    watch \
    schedule \
    worker_short \
    worker_long \
    worker_default
```
Then start Bench Web from VScode debugger

Alternatively you can use the VSCode launch configuration "Honcho SocketIO Watch Schedule Worker" which launches the same command as above.